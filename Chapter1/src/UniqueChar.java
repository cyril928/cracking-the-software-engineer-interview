
public class UniqueChar {
	
	public static boolean isUnique(String s) {
		if(s.length() > 256)	return false;
		
		boolean[] map = new boolean[256];
		for(int i=0;i<s.length();i++) {
			int val = s.charAt(i);
			if(map[val]) {
				return false;
			}
			else {
				map[val] = true;
			}
		}
		return true;
	}
	
	public static boolean isUnique2(String s) {
		if(s.length() > 256)	return false;
		int[] map = {0,0,0,0,0,0,0,0};
		for(int i=0;i<s.length();i++) {
			int val = s.charAt(i);
			int idx = val / 32;
			if((map[idx] & (1 << (val % 32))) > 0)
				return false;
			map[val/32] |= 1 << (val%32);
		}
		return true;
	}
	
	public static void main(String[] args) {
		System.out.println(isUnique("abdfjfdlfjajdfjdsoifji"));
		System.out.println(isUnique("qwertyuio987654asdfg"));
		System.out.println(isUnique2("abdfjfdlfjajdfjdsoifji"));
		System.out.println(isUnique2("qwertyuio987654asdfg"));
	}
}
