
public class ReplaceSpace {
	public static String replaceSpace(String s) {
		int length = s.length();
		int new_len = length;
		for(int i=0;i<length;i++) {
			if(s.charAt(i) == ' ') {
				new_len += 2;
			}
		}
		char[] str = new char[new_len];
		new_len--;
		for(int i=length-1;i>=0;i--) {
			if(s.charAt(i) == ' ') {
				str[new_len--] = '0';
				str[new_len--] = '2';
				str[new_len--] = '%';
			}
			else {
				str[new_len--] = s.charAt(i);
			}
		}
		return new String(str);
	}
	public static void main(String[] args){
		System.out.println(replaceSpace("ab cd ef"));
	}
}
