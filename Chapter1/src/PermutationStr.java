
public class PermutationStr {
	
	public static boolean isPerm(String s, String t){
		if(s == null || t == null)	return false;
		if(s.length() != t.length())	return false;
		int[] letters = new int[256];
		for(int i=0;i<s.length();i++) {
			letters[s.charAt(i)] ++; 
			letters[t.charAt(i)] --;
		}
		for(int i=0;i<256;i++){
			if(letters[i] != 0)
				return false;
		}
		return true;	
	}
	
	
	public static void main(String[] args) {
		System.out.println(isPerm("abdfjfdlfjajdfjdsoifji", "ewfwf"));
		System.out.println(isPerm("aaaabbbcc", "aaaabbc"));
		System.out.println(isPerm("aaabbbcc","ccbbaaba"));
	}
}
