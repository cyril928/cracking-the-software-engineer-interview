
public class RotateImage {
	public static void rotate(int[][] matrix) {
		for(int i=0;i<matrix.length;i++) {
			for(int j=i+1;j<matrix[0].length;j++) {
				int tmp = matrix[i][j];
				matrix[i][j] = matrix[j][i];
				matrix[j][i] = tmp;
			}
		}
		for(int i=0;i<matrix.length/2;i++) {
			for(int j=0;j<matrix[0].length;j++) {
				int tmp = matrix[i][j];
				matrix[i][j] = matrix[matrix.length-1-i][j];
				matrix[matrix.length-1-i][j] = tmp;
			}
		}
	}
	
	public static void rotate1(int[][] matrix) {
		for(int layer = 0; layer < matrix.length/2; layer++) {
			int first = layer;
			int last = matrix.length - 1 - layer;
			for(int i = first; i < last; i++) {
				int tmp;
				tmp = matrix[i][first];
				matrix[i][first] = matrix[last][i];
				matrix[last][i] = matrix[last-i][last];
				matrix[last-i][last] = matrix[first][last-i];
				matrix[first][last-i] = tmp;
			}
		}
	}
	
	
	
	public static void main(String[] args){
		int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		rotate1(matrix);
		for(int i=0;i<matrix.length;i++) {
			for(int j=0;j<matrix[0].length;j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println("");
		}
	}
}