
public class StrCompress {
	public static String compress(String s) {
		if(size(s) >= s.length()) {
			return s;
		}
		StringBuilder sb = new StringBuilder();
		char last = s.charAt(0);
		int count = 1;
		
		for(int i=1;i<s.length();i++) {
			if(s.charAt(i) != last) {
				sb.append(last);
				sb.append(count);
				count = 1;
				last = s.charAt(i);
			}
			else {
				count++;
			}
		}
		sb.append(last);
		sb.append(count);
		return sb.toString();
	}
	public static int size(String str) {
		if(str == null || str == "")	return 0;
		char c = str.charAt(0);
		int count = 1;
		int size = 1;
		for(int i=1;i<str.length();i++) {
			if(c!=str.charAt(i)) {
				c = str.charAt(i);
				count = 1;
				size += 1 + String.valueOf(count).length();
			}
			else {
				count++;
			}
		}
		size += String.valueOf(count).length();
		return size;
	}
	public static void main(String[] args){
		System.out.println(compress("aabcccccaaa"));
	}
	
}
