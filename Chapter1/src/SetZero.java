
public class SetZero {
	public static void setZero(int[][] matrix) {
		int row_len = matrix.length;
		int col_len = matrix[0].length;
		
		boolean[] row = new boolean[row_len];
		boolean[] col = new boolean[col_len];
		
		for(int i=0;i<row_len;i++) {
			for(int j=0;j<col_len;j++) {
				if(matrix[i][j] == 0) {
					row[i] = true;
					col[j] = true;
				}
			}
		}
		
		for(int i=0;i<row_len;i++) {
			if(row[i] == true) {
				for(int j=0;j<col_len;j++) {
					matrix[i][j] = 0;
				}
			}
		}
		
		for(int i=0;i<col_len;i++) {
			if(col[i] == true) {
				for(int j=0;j<row_len;j++) {
					matrix[j][i] = 0;
				}
			}
		}
		
		
	}
}
