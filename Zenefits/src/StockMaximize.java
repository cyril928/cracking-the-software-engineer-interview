import java.util.Scanner;

public class StockMaximize {
	private static Scanner sc;

	public static void main(String[] args) throws Exception {
		sc = new Scanner(System.in);
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			int sn = sc.nextInt();
			int[] stocks = new int[sn];
			for (int j = 0; j < sn; j++) {
				stocks[j] = sc.nextInt();
			}
			System.out.println(stockMaximize(stocks));
		}
	}

	public static long stockMaximize(int[] stocks) {
		int len = stocks.length;
		int max = stocks[len - 1];
		long res = 0;
		for (int i = len - 1; i >= 0; i--) {
			max = Math.max(stocks[i], max);
			res += max - stocks[i];
		}
		return res;
	}
}
