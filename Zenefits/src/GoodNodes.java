import java.util.Scanner;

public class GoodNodes {
	private static Scanner sc;

	public static int goodNodes(int[] nodes) {
		int num = 0;
		boolean[] good = new boolean[nodes.length];
		good[0] = true;
		for (int i = 0; i < nodes.length; i++) {
			if (!good[i]) {
				boolean[] visited = new boolean[nodes.length];

				int j = nodes[i];
				while (j != 1 && !visited[j - 1]) {
					good[j - 1] = true;
					visited[j - 1] = true;
					j = nodes[j - 1];
				}
				if (j != 1) {
					num++;
					nodes[j - 1] = 1;
				} else {
					good[j - 1] = true;
				}
			}
		}
		return num;
	}

	public static void main(String[] args) throws Exception {
		sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] nodes = new int[n];
		for (int i = 0; i < n; i++) {
			nodes[i] = sc.nextInt();
		}
		System.out.println(goodNodes(nodes));
	}
}
