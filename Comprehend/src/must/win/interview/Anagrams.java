package must.win.interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Anagrams {
	public static Map<String, List<String>> anagrams(String[] strArray) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		if (strArray == null || strArray.length == 0) {
			return map;
		}

		for (String str : strArray) {
			char[] charArray = str.toCharArray();
			Arrays.sort(charArray);
			String key = new String(charArray);
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<String>());
			}
			map.get(key).add(str);
		}

		return map;
	}

	public static void main(String[] args) {
		String[] strArray = { "arm", "ram", "wed", "are", "art", "tar", "dew",
				"mar" };
		Map<String, List<String>> result = anagrams(strArray);
		for (List<String> list : result.values()) {
			if (list.size() > 1) {
				for (String str : list) {
					System.out.print(str + ",");
				}
				System.out.println();
			}
		}
	}

}
