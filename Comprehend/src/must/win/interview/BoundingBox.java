package must.win.interview;

import java.util.ArrayList;
import java.util.List;

public class BoundingBox {
	public static void main(String[] args) {
		BoundingBox bb = new BoundingBox();
		List<Point> points = new ArrayList<Point>();
		List<Point> boxPoints = new ArrayList<Point>();
		points.add(new Point(0, 4));
		points.add(new Point(2, 1));
		points.add(new Point(2, 3));
		points.add(new Point(3, 5));
		points.add(new Point(4, 3));
		points.add(new Point(5, 1));

		boxPoints.add(new Point(1, 6));
		boxPoints.add(new Point(5, 2));

		for (Point p : bb.filter(points, boxPoints)) {
			System.out.println("(" + p.x + "," + p.y + ")");
		}
	}

	public List<Point> filter(List<Point> points, List<Point> boxPoints) {
		if (boxPoints.size() != 2) {
			System.out.println("invalid bounding box input!");
			return null;
		}
		int lowX = Math.min(boxPoints.get(0).x, boxPoints.get(1).x);
		int highX = boxPoints.get(0).x ^ boxPoints.get(1).x ^ lowX;

		if (lowX == highX) {
			System.out.println("invalid bounding box input!");
			return null;
		}

		int lowY = Math.min(boxPoints.get(0).y, boxPoints.get(1).y);
		int highY = boxPoints.get(0).y ^ boxPoints.get(1).y ^ lowY;

		if (lowY == highY) {
			System.out.println("invalid bounding box input!");
			return null;
		}

		List<Point> result = new ArrayList<Point>();
		for (Point p : points) {
			if (p.x <= highX && p.x >= lowX && p.y <= highY && p.y >= lowY) {
				result.add(p);
			}
		}

		return result;
	}

}

class Point {
	int x, y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
