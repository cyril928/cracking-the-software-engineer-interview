package must.win.interview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class WellBalanceString {
	public static boolean isWellBalanceString(String s) {
		if (s == null || s.length() == 0) {
			return false;
		}
		Map<Character, Character> map = new HashMap<Character, Character>();
		map.put(')', '(');
		map.put(']', '[');
		map.put('}', '{');

		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);
			if (!map.containsKey(c)) {
				stack.push(c);
			} else if (stack.isEmpty() || stack.pop() != map.get(c)) {
				return false;
			}
		}

		return stack.isEmpty();
	}

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String line = br.readLine();
			System.out.println(isWellBalanceString(line));
		} catch (IOException io) {
			io.printStackTrace();
		}
	}
}
