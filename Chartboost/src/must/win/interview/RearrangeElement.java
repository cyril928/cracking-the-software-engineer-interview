package must.win.interview;

public class RearrangeElement {
	public static void main(String[] args) {
		int[] input = new int[] { 1, 2, 0, 0, 1, 1, 2 };
		rearrangeElement(input);
		for (int val : input) {
			System.out.print(val + " ");
		}
	}

	public static void rearrangeElement(int[] input) {
		if (input == null || input.length <= 1) {
			return;
		}
		int cur = 0;
		int pl = 0;
		int pr = input.length - 1;

		// the end condition must be cur equal pr, cause when cur is 0, we have
		// to push it forward
		while (cur <= pr) {
			if (input[cur] == 0) { // current number is 0, finish left side one
									// step, move pl toward middle.
				swap(input, cur++, pl++);
			} else if (input[cur] == 2) { // current number is 2, finish right
											// side one step, move pr toward
											// middle
				swap(input, cur, pr--);
			} else {
				cur++;
			}
		}

	}

	// swap elemetn
	private static void swap(int[] A, int l, int r) {
		int temp = A[l];
		A[l] = A[r];
		A[r] = temp;
	}
}
