/*
 * Implement a function to compute square root of input number, 
 * assuming that input number will never be negative; six digits of decimals is precise enough
 * You may only use the following operations: '+', '-', '*', '/', 
 * no built-in sqrt function calls or pow function calls, 
 * minimize running time and watch out edge cases.. more i
 */
public class Sqrt {
	public static void main(String[] args) {
		Sqrt s = new Sqrt();
		System.out.println(s.sqrt(56));
		System.out.println(s.sqrt(78));
		System.out.println(s.sqrt(89521));
		System.out.println(s.sqrt(1024));
	}

	public double sqrt(int n) {
		if (n == 0) {
			return 0;
		}
		int s = 1, e = n;
		double val = 0;
		while (s <= e) {
			int mid = ((e - s) >> 1) + s;
			if (n / mid > mid) {
				s = mid + 1;
			} else if (n / mid < mid) {
				e = mid - 1;
			} else {
				val = mid;
				break;
			}
		}
		if (s > e) {
			val = e;
		}
		s = 0;
		e = 999999;
		while (s <= e) {
			int mid = ((e - s) >> 1) + s;
			double temp = val + (double) mid / 1000000;
			if (n / temp > temp) {
				s = mid + 1;
			} else if (n / temp < temp) {
				e = mid - 1;
			} else {
				return temp;
			}
		}
		return val + (double) e / 1000000;
	}
}
