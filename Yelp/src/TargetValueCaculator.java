/*
 *  Given 6 integers and 1 target value,write a function to get the target value using 
 *  6 integers with any on these operations +,*,-,/
 */
public class TargetValueCaculator {
	public static void main(String[] args) {
		TargetValueCaculator tvc = new TargetValueCaculator();
		int[] num = { 6, 3, 9, 7, 2, 5 };
		tvc.targetCaculator(num, 35);
		int[] num1 = { 7, 2, 5 };
		tvc.targetCaculator(num1, 7);
	}

	char[] operator = { '*', '/', '+', '-' };

	private int calculate(int a, int b, char oper) {
		switch (oper) {
		case '+':
			return a + b;
		case '-':
			return a - b;
		case '*':
			return a * b;
		default:
			return a / b;
		}
	}

	private void helper(int[] num, int target, int depth, boolean[] visited,
			int val, StringBuilder sb, int operIndex) {
		if (depth == num.length) {
			if (target == val) {
				System.out.println(sb.toString());
			}
		} else {
			for (int i = 0; i < num.length; i++) {
				if (!visited[i]) {
					visited[i] = true;
					int strLen = sb.length();
					if (operIndex < 2) {
						sb.append('*');
						sb.append(num[i]);
						helper(num, target, depth + 1, visited,
								calculate(val, num[i], '*'), sb, 0);
						sb.delete(strLen, sb.length());

						sb.append('/');
						sb.append(num[i]);
						helper(num, target, depth + 1, visited,
								calculate(val, num[i], '/'), sb, 0);
						sb.delete(strLen, sb.length());
					}
					for (int j = Math.max(2, operIndex); j < 4; j++) {
						sb.append(operator[j]);
						sb.append(num[i]);
						helper(num, target, depth + 1, visited,
								calculate(val, num[i], operator[j]), sb, j);
						sb.delete(strLen, sb.length());
					}
					visited[i] = false;
				}
			}
		}
	}

	public void targetCaculator(int[] num, int target) {
		int len = num.length;
		boolean[] visited = new boolean[len];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < len; i++) {
			visited[i] = true;
			sb.append(num[i]);
			helper(num, target, 1, visited, num[i], sb, 0);
			sb.delete(0, sb.length());
			visited[i] = false;
		}
	}
}
