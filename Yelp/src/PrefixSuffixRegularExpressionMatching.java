/*
 *  '*' star matches zero or more preceding characters
 *  '.' each dot matches exactly one character, any
 *  '^' prefix match, meaning that input string must start with exactly the same string 
 *  following '^', '^abc' will match 'abcdefg', but 'cdefg' doesn't
 *  $' suffix match, meaning that input string must end with exactly the same string 
 *  following '$', '$abc' will match 'cdefgabc', but 'abcdefg' doesn't
 *  without any special character: return True if pattern is in string 
 *  (contains, not exactly same), and False otherwise
 */
public class PrefixSuffixRegularExpressionMatching {

	public static void main(String[] args) {
		PrefixSuffixRegularExpressionMatching psre = new PrefixSuffixRegularExpressionMatching();
		System.out.println(psre.isMatch("ab", "ab"));
		System.out.println(psre.isMatch("ab", "a."));
		System.out.println(psre.isMatch("caej", "cad*j"));
		System.out.println(psre.isMatch("cadddjqqq", "c.d*jq*"));
		System.out.println(psre.isMatch("caddjfjeil", "^cad*j"));
		System.out.println(psre.isMatch("cddjfjeil", "^cad*j"));
		System.out.println(psre.isMatch("fjkkcaj", "$cad*j"));

		System.out.println("=========================================");

		System.out.println(psre.isMatch1("ab", "ab"));
		System.out.println(psre.isMatch1("ab", "a."));
		System.out.println(psre.isMatch1("caej", "cad*j"));
		System.out.println(psre.isMatch1("cadddjqqq", "c.d*jq*"));
		System.out.println(psre.isMatch1("caddjfjeil", "^cad*j"));
		System.out.println(psre.isMatch1("cddjfjeil", "^cad*j"));
		System.out.println(psre.isMatch1("fjkkcaj", "$cad*j"));
	}

	// recursive helper
	private boolean checkEmpty(String p, int pp) {
		if (((p.length() - pp) & 1) == 1) {
			return false;
		}
		for (int i = pp + 1; i < p.length(); i += 2) {
			if (p.charAt(i) != '*') {
				return false;
			}
		}
		return true;
	}

	// DP
	public boolean isMatch(String s, String p) {
		boolean subMatch = false;
		if (p.charAt(0) == '^') {
			p = p.substring(1);
			subMatch = true;
		} else if (p.charAt(0) == '$') {
			p = p.substring(1);
			subMatch = true;
			s = new StringBuilder(s).reverse().toString();
			char[] pcs = p.toCharArray();
			for (int i = 0; i < pcs.length; i++) {
				if (pcs[i] == '*') {
					pcs[i] = pcs[i - 1];
					pcs[i - 1] = '*';
				}
			}
			int start = 0, end = pcs.length - 1;
			while (start < end) {
				char temp = pcs[start];
				pcs[start++] = pcs[end];
				pcs[end--] = temp;
			}
			p = new String(pcs);
		}

		int sLen = s.length();
		int pLen = p.length();
		boolean[][] dp = new boolean[2][pLen + 1];
		int preRow = 0, curRow = 1;
		dp[0][0] = true;

		for (int i = 2; i <= pLen; i += 2) {
			if (p.charAt(i - 1) != '*') {
				break;
			}
			dp[0][i] = true;
		}

		for (int i = 0; i < sLen; i++) {
			for (int j = 1; j <= pLen; j++) {
				if (subMatch && dp[preRow][j]) {
					dp[curRow][j] = true;
				} else {
					dp[curRow][j] = isSameChar(p.charAt(j - 1), '*') ? (isSameChar(
							s.charAt(i), p.charAt(j - 2)) && dp[preRow][j])
							|| dp[curRow][j - 2] : isSameChar(s.charAt(i),
							p.charAt(j - 1))
							&& dp[preRow][j - 1];

				}
			}
			preRow = curRow;
			curRow ^= 1;
		}

		return dp[preRow][pLen];
	}

	// recursive
	public boolean isMatch1(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		boolean subMatch = false;
		if (p.charAt(0) == '^') {
			p = p.substring(1);
			subMatch = true;
		} else if (p.charAt(0) == '$') {
			p = p.substring(1);
			subMatch = true;
			s = new StringBuilder(s).reverse().toString();
			char[] pcs = p.toCharArray();
			for (int i = 0; i < pcs.length; i++) {
				if (pcs[i] == '*') {
					pcs[i] = pcs[i - 1];
					pcs[i - 1] = '*';
				}
			}
			int start = 0, end = pcs.length - 1;
			while (start < end) {
				char temp = pcs[start];
				pcs[start++] = pcs[end];
				pcs[end--] = temp;
			}
			p = new String(pcs);
		}

		return isMatchHelper(s, 0, p, 0, subMatch);
	}

	// recursive helper
	public boolean isMatchHelper(String s, int sp, String p, int pp,
			boolean subMatch) {
		if (sp == s.length()) {
			return checkEmpty(p, pp);
		}
		if (pp == p.length()) {
			return subMatch;
		}

		if (pp + 1 < p.length() && p.charAt(pp + 1) == '*') {
			return isMatchHelper(s, sp, p, pp + 2, subMatch)
					|| (isSameChar(s.charAt(sp), p.charAt(pp)) && isMatchHelper(
							s, sp + 1, p, pp, subMatch));
		} else {
			return isSameChar(s.charAt(sp), p.charAt(pp))
					&& isMatchHelper(s, sp + 1, p, pp + 1, subMatch);
		}

	}

	private boolean isSameChar(char a, char b) {
		return b == '.' || a == b;
	}
}
