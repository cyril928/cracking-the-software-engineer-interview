import java.util.Arrays;

/*
 * A given array represents a tree in such a way that the array
 * value gives the parent node of that particular index. The 
 * value of the root node index would always be -1. Find the 
 * height of the tree.
 * http://mp.weixin.qq.com/s?__biz=MjM5ODIzNDQ3Mw==&mid=200442080&idx=1&sn=45c23a48cab9f9f1fbfd472249e09a1d#rd
 *
 */
public class ArrayTreeHeight {
	public static void main(String[] args) {
		int[] A = { -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		ArrayTreeHeight solu = new ArrayTreeHeight();
		System.out.println(solu.getTreeHeight(A));
	}

	private int findHeight(int i, int[] A, int[] height) {
		if (height[i] != -1) {
			return height[i];
		}
		if (A[i] == -1) {
			height[i] = 0;
		} else {
			height[i] = 1 + findHeight(A[i], A, height);
		}
		return height[i];
	}

	public int getTreeHeight(int[] A) {
		int[] height = new int[A.length];
		Arrays.fill(height, -1);

		int maxHeight = -1;
		for (int i = 0; i < A.length; i++) {
			if (height[i] == -1) {
				maxHeight = Math.max(findHeight(i, A, height), maxHeight);
			}
		}

		for (int h : height) {
			System.out.print(h + " ");
		}
		return maxHeight;
	}
}
