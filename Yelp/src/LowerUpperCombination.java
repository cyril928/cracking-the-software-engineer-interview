import java.util.ArrayList;
import java.util.List;

/*
 * given a lowercase string 'ab',write a program to generate all possible 
 * lowercase and uppercase combination {'AB','Ab','aB'and 'ab'}
 */
public class LowerUpperCombination {

	public static void main(String[] args) {
		LowerUpperCombination luc = new LowerUpperCombination();
		for (String s : luc.lowerUpperCombination("zet")) {
			System.out.println(s);
		}
	}

	int diff = 'a' - 'A';

	private void helper(List<String> res, StringBuilder path, String s,
			int depth) {
		if (depth == s.length()) {
			res.add(path.toString());
			return;
		}
		for (char c = s.charAt(depth); c >= 'A'; c -= diff) {
			path.append(c);
			helper(res, path, s, depth + 1);
			path.deleteCharAt(path.length() - 1);
		}
	}

	public List<String> lowerUpperCombination(String s) {
		List<String> res = new ArrayList<String>();
		if (s == null || s.isEmpty()) {
			return res;
		}
		helper(res, new StringBuilder(), s, 0);
		return res;
	}
}
