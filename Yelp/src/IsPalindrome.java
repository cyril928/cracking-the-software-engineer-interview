import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class IsPalindrome {
	private static boolean isPalindrome(String s) {
		if (s == null) {
			return false;
		}
		if (s.length() <= 1) {
			return true;
		}

		Set<Character> set = new HashSet<Character>();
		int len = s.length();
		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);
			if (set.contains(c)) {
				set.remove(c);
			} else {
				set.add(c);
			}
		}

		return set.size() <= 1;
	}

	public static void main(String[] args) {

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));

			String input;

			while ((input = br.readLine()) != null) {
				System.out.println(isPalindrome(input));
			}

		} catch (IOException io) {
			io.printStackTrace();
		}
		/*
		 * System.out.println(isPalindrome("abc"));
		 * System.out.println(isPalindrome("abbcnnh"));
		 * System.out.println(isPalindrome("abbcnncuiopoiuppppp"));
		 * System.out.println
		 * (isPalindrome("abbc nncu&&iopoi()upp ppp**********"));
		 * System.out.println
		 * (isPalindrome("abbc &nncu&io)poi)upp ppp**********"));
		 */
	}
}
