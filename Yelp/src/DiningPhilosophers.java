import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ChopStick {
	private Lock lock;

	public ChopStick() {
		lock = new ReentrantLock();
	}

	public boolean pickUp() {
		return lock.tryLock();
	}

	public void putDown() {
		lock.unlock();
	}

}

public class DiningPhilosophers extends Thread {
	private ChopStick left, right;
	private int bites = 10;

	public DiningPhilosophers(ChopStick left, ChopStick right) {
		this.left = left;
		this.right = right;
	}

	private void chew() {

	}

	private void eat() {
		if (pickUp()) {
			chew();
			putDown();
		}
	}

	private boolean pickUp() {
		if (!left.pickUp()) {
			return false;
		}
		if (!right.pickUp()) {
			left.putDown();
			return false;
		}
		return true;
	}

	private void putDown() {
		left.putDown();
		right.putDown();
	}

	@Override
	public void run() {
		for (int i = 0; i < bites; i++) {
			eat();
		}
	}
}
