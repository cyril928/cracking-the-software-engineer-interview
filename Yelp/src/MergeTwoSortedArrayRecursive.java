public class MergeTwoSortedArrayRecursive {

	public static void main(String[] args) {
		int n = 9;
		int[] a = { 1, 3, 5, 7, 15 };
		int[] b = { 2, 4, 6, 8 };
		MergeTwoSortedArrayRecursive mt = new MergeTwoSortedArrayRecursive();
		int[] r = new int[n];
		mt.mergeTwoSortedArrayRecursive(r, 0, a, 0, b, 0);
		for (int val : r) {
			System.out.print(val + " ");
		}
	}

	private void mergeTwoSortedArrayRecursive(int[] r, int ri, int[] a, int ai,
			int[] b, int bi) {
		if (ri == r.length) {
			return;
		}
		if (ai == a.length) {
			r[ri] = b[bi];
			mergeTwoSortedArrayRecursive(r, ri + 1, a, ai, b, bi + 1);
			return;
		}
		if (bi == b.length || a[ai] <= b[bi]) {
			r[ri] = a[ai];
			mergeTwoSortedArrayRecursive(r, ri + 1, a, ai + 1, b, bi);
		} else {
			r[ri] = b[bi];
			mergeTwoSortedArrayRecursive(r, ri + 1, a, ai, b, bi + 1);
		}
	}
}
