import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReverseWords {
	public static void main(String[] args) {

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String line = br.readLine();
			if (line == null || line.length() == 0) {
				System.out.println("");
			} else {
				System.out.println(reverseWords(line));
			}

		} catch (IOException io) {
			io.printStackTrace();
		}
		/*
		 * System.out.println(isPalindrome("abc"));
		 * System.out.println(isPalindrome("abbcnnh"));
		 * System.out.println(isPalindrome("abbcnncuiopoiuppppp"));
		 * System.out.println
		 * (isPalindrome("abbc nncu&&iopoi()upp ppp**********"));
		 * System.out.println
		 * (isPalindrome("abbc &nncu&io)poi)upp ppp**********"));
		 */
	}

	private static String reverseWords(String s) {
		String[] strArray = s.split(" ");
		StringBuilder sb = new StringBuilder();

		int len = strArray.length;
		for (int i = len - 1; i >= 0; --i) {
			if (!strArray[i].equals("")) {
				sb.append(strArray[i]).append(" ");
			}
		}
		return sb.substring(0, sb.length() - 1);
	}
}
