import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class ClassRegistration {
	/*
	 * Complete the functions below.
	 */

	public static class Class {
		private int capacity, time;

		// private int avail_capacity;
		public Class(int id, int capacity, int time) {
			this.capacity = capacity;
			this.time = time;
		}

		public int getCapacity() {
			return capacity;
		}

		public int getTime() {
			return time;
		}

		public void setCapacity(int capacity) {
			this.capacity = capacity;
		}

		public void setTime(int time) {
			this.time = time;
		}
	}

	public static class Student {
		private int capacity, start, end;
		boolean[] slot;

		public Student(int id, int capacity, int start, int end) {
			this.capacity = capacity;
			this.start = start;
			this.end = end;
			slot = new boolean[end - start + 1];
		}

		public int getCapacity() {
			return capacity;
		}

		public void setCapacity(int capacity) {
			this.capacity = capacity;
		}

		public boolean takeClass(int time) {
			if (time < start || time > end || slot[time - start]) {
				return false;
			}
			slot[time - start] = true;
			return true;
		}

		public void unenrollClass(int time) {
			slot[time - start] = false;
		}

	}

	private static Map<Integer, Class> cMap = new HashMap<Integer, Class>();
	private static Map<Integer, Student> sMap = new HashMap<Integer, Student>();

	private static Map<Integer, Set<Integer>> studentListMap = new HashMap<Integer, Set<Integer>>();

	private static Map<Integer, Set<Integer>> classListMap = new HashMap<Integer, Set<Integer>>();
	private static Scanner in;

	public static String addClass(int id, int capacity, int time) {
		// If the class is added successfully,
		// return "Successfully added class ID".
		// Otherwise, return "Error adding class ID".
		if (cMap.containsKey(id)) {
			return "Error adding class " + id;
		} else {
			cMap.put(id, new Class(id, capacity, time));
			studentListMap.put(id, new TreeSet<Integer>());
			return "Successfully added class " + id;
		}
	}

	public static String addStudent(int id, int capacity, int start, int end) {
		// If the student is added successfully,
		// return "Successfully added student ID".
		// Otherwise, return "Error adding student ID".
		if (sMap.containsKey(id)) {
			return "Error adding student " + id;
		} else {
			sMap.put(id, new Student(id, capacity, start, end));
			classListMap.put(id, new TreeSet<Integer>());
			return "Successfully added student " + id;
		}
	}

	public static String enrollStudent(int studentId, int classId) {
		// If enrollment of the student in the class succeeded,
		// return "Number of free spots left in class CLASSID: FREESPOTS"
		// where FREESPOTS is the number of free spots left
		// in the class after the student enrolls.
		// Otherwise, return
		// "Enrollment of student STUDENTID in class CLASSID failed".

		if (!sMap.containsKey(studentId)) {
			return "Enrollment of student " + studentId + " in class "
					+ classId + " failed";
		}
		if (!cMap.containsKey(classId)) {
			return "Enrollment of student " + studentId + " in class "
					+ classId + " failed";
		}

		Student s = sMap.get(studentId);
		Class c = cMap.get(classId);

		if (s.getCapacity() == 0) {
			return "Enrollment of student " + studentId + " in class "
					+ classId + " failed";
		}

		if (c.getCapacity() == 0) {
			return "Enrollment of student " + studentId + " in class "
					+ classId + " failed";
		}

		if (studentListMap.get(classId).contains(studentId)) {
			return "Enrollment of student " + studentId + " in class "
					+ classId + " failed";
		}

		int time = c.getTime();
		if (!s.takeClass(time)) {
			return "Enrollment of student " + studentId + " in class "
					+ classId + " failed";
		}
		s.setCapacity(s.getCapacity() - 1);
		c.setCapacity(c.getCapacity() - 1);
		studentListMap.get(classId).add(studentId);
		classListMap.get(studentId).add(classId);
		return "Number of free spots left in class " + classId + ": "
				+ cMap.get(classId).getCapacity();
	}

	public static String infoClass(int id) {
		// If the class does not exist,
		// return "Class ID does not exist".
		// If the class is empty,
		// return "Class ID is empty".
		// Otherwise, return the string
		// "Class ID has the following students: LIST"
		// where LIST is a sorted, comma-separated list
		// of student IDs corresponding to students currently
		// in the class.
		if (!cMap.containsKey(id)) {
			return "Class " + id + " does not exist";
		}
		Set<Integer> treeSet = studentListMap.get(id);
		if (treeSet.isEmpty()) {
			return "Class " + id + " is empty";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("Class " + id + " has the following students: ");
		for (int sid : treeSet) {
			sb.append(sid).append(",");
		}
		return sb.substring(0, sb.length() - 1);
	}

	public static String infoStudent(int id) {
		// If the student does not exist,
		// return "Student ID does not exist".
		// If the student is not taking any classes,
		// return "Student ID is not taking any classes".
		// Otherwise, return the string
		// "Student ID is taking the following classes: LIST"
		// where LIST is a sorted, comma-separated list of class IDs
		// corresponding to classes that the student is
		// currently taking.
		if (!sMap.containsKey(id)) {
			return "Student " + id + " does not exist";
		}
		Set<Integer> treeSet = classListMap.get(id);
		if (treeSet.isEmpty()) {
			return "Student " + id + " is not taking any classes";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("Student " + id + " is taking the following classes: ");
		for (int cid : treeSet) {
			sb.append(cid).append(",");
		}
		return sb.substring(0, sb.length() - 1);
	}

	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		final String fileName = System.getenv("OUTPUT_PATH");
		BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
		String[] res;

		int _commands_size = Integer.parseInt(in.nextLine());
		String[] _commands = new String[_commands_size];
		String _commands_item;
		for (int _commands_i = 0; _commands_i < _commands_size; _commands_i++) {
			try {
				_commands_item = in.nextLine();
			} catch (Exception e) {
				_commands_item = null;
			}
			_commands[_commands_i] = _commands_item;
		}

		res = processCommands(_commands);
		for (int res_i = 0; res_i < res.length; res_i++) {
			bw.write(String.valueOf(res[res_i]));
			bw.newLine();
		}

		bw.close();
	}

	public static String[] processCommands(String[] commands) {
		String[] ret = new String[commands.length];
		for (int i = 0; i < commands.length; i++) {
			StringTokenizer st = new StringTokenizer(commands[i]);
			String op = st.nextToken();
			if (op.equals("ADDCLASS")) {
				int id = Integer.parseInt(st.nextToken());
				int cap = Integer.parseInt(st.nextToken());
				int time = Integer.parseInt(st.nextToken());
				ret[i] = addClass(id, cap, time);
			} else if (op.equals("REMOVECLASS")) {
				int id = Integer.parseInt(st.nextToken());
				ret[i] = removeClass(id);
			} else if (op.equals("INFOCLASS")) {
				int id = Integer.parseInt(st.nextToken());
				ret[i] = infoClass(id);
			} else if (op.equals("ADDSTUDENT")) {
				int id = Integer.parseInt(st.nextToken());
				int cap = Integer.parseInt(st.nextToken());
				int timeStart = Integer.parseInt(st.nextToken());
				int timeEnd = Integer.parseInt(st.nextToken());
				ret[i] = addStudent(id, cap, timeStart, timeEnd);
			} else if (op.equals("REMOVESTUDENT")) {
				int id = Integer.parseInt(st.nextToken());
				ret[i] = removeStudent(id);
			} else if (op.equals("INFOSTUDENT")) {
				int id = Integer.parseInt(st.nextToken());
				ret[i] = infoStudent(id);
			} else if (op.equals("ENROLLSTUDENT")) {
				int studentId = Integer.parseInt(st.nextToken());
				int classId = Integer.parseInt(st.nextToken());
				ret[i] = enrollStudent(studentId, classId);
			} else if (op.equals("UNENROLLSTUDENT")) {
				int studentId = Integer.parseInt(st.nextToken());
				int classId = Integer.parseInt(st.nextToken());
				ret[i] = unenrollStudent(studentId, classId);
			}
		}
		return ret;
	}

	public static String removeClass(int id) {
		// If the class is removed successfully,
		// return "Successfully removed class ID".
		// Otherwise, return "Error removing class ID".
		if (!cMap.containsKey(id)) {
			return "Error removing class " + id;
		}
		Set<Integer> tempSet = studentListMap.get(id);
		for (int sid : tempSet) {
			unenrollStudent(sid, id);
		}
		cMap.remove(id);
		studentListMap.remove(id);
		return "Successfully removed class " + id;
	}

	public static String removeStudent(int id) {
		// If the student is removed successfully,
		// return "Successfully removed student ID".
		// Otherwise, return "Error removing student ID".
		if (!sMap.containsKey(id)) {
			return "Error removing student " + id;
		}
		Set<Integer> tempSet = classListMap.get(id);
		for (int cid : tempSet) {
			unenrollStudent(id, cid);
		}
		sMap.remove(id);
		classListMap.remove(id);
		return "Successfully removed student " + id;
	}

	public static String unenrollStudent(int studentId, int classId) {
		// If unenrollment of the student in the class succeeded,
		// return "Number of free spots left in class CLASSID: FREESPOTS"
		// where FREESPOTS is the number of free spots left in the class
		// after the student unenrolls. Otherwise, return "Unenrollment
		// of student STUDENTID in class CLASSID failed".

		if (!sMap.containsKey(studentId)) {
			return "Unenrollment of student " + studentId + " in class "
					+ classId + " failed";
		}
		if (!cMap.containsKey(classId)) {
			return "Unenrollment of student " + studentId + " in class "
					+ classId + " failed";
		}

		Set<Integer> treeSet = classListMap.get(studentId);
		if (!treeSet.contains(classId)) {
			return "Unenrollment of student " + studentId + " in class "
					+ classId + " failed";
		}
		Student s = sMap.get(studentId);
		Class c = cMap.get(classId);
		treeSet.remove(classId);
		treeSet = studentListMap.get(classId);
		treeSet.remove(studentId);
		c.setCapacity(c.getCapacity() + 1);
		s.setCapacity(s.getCapacity() + 1);
		s.unenrollClass(c.getTime());
		return "Number of free spots left in class " + classId + ": "
				+ c.getCapacity();
	}
}
