public class CalculateRun {
	public static void main(String[] args) {
		CalculateRun cr = new CalculateRun();
		System.out.println(cr.calculateRun("a"));
		System.out.println(cr.calculateRun("aab"));
		System.out.println(cr.calculateRun("abbbbbcc"));
		System.out.println(cr.calculateRun("aabbccdd"));
	}

	public String calculateRun(String s) {
		int maxLen = 0;
		int len = 1;
		char maxChar = ' ';
		for (int i = 0; i < s.length(); i++) {
			if (i == s.length() - 1 || s.charAt(i) != s.charAt(i + 1)) {
				if (len > maxLen) {
					maxLen = len;
					maxChar = s.charAt(i);
				}
				len = 1;
			} else {
				len++;
			}
		}

		StringBuilder sb = new StringBuilder();
		while (maxLen-- > 0) {
			sb.append(maxChar);
		}
		return sb.toString();
	}
}
