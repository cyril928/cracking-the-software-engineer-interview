import java.util.Arrays;

public class DiceHistogram {
	public static void main(String[] args) {
		DiceHistogram dh = new DiceHistogram();
		dh.diceHistogram(3, 3);
	}

	public void diceHistogram(int dice, int side) {
		int[] dices = new int[dice];
		recur(dices, side, 0);
		Arrays.fill(dices, 1);

		int i = 0;
		while (true) {
			print(dices);
			dices[i]++;
			while (dices[i] > side) {
				dices[i] = 1;
				i++;
				if (i >= dice) {
					return;
				}
				dices[i]++;
			}
			i = 0;
		}

	}

	public void print(int[] dices) {
		StringBuilder sb = new StringBuilder();
		for (int dice : dices) {
			sb.append(dice).append(",");
		}
		System.out.println(sb.substring(0, sb.length() - 1));
	}

	public void recur(int[] dices, int side, int depth) {
		if (depth == dices.length) {
			// print(dices);
			return;
		}
		for (int i = 1; i <= side; i++) {
			dices[depth] = i;
			recur(dices, side, depth + 1);
		}
	}
}
