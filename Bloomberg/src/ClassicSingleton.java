public class ClassicSingleton {
	private static ClassicSingleton instance = null;

	public static ClassicSingleton getInstacne() {
		if (instance == null) {
			instance = new ClassicSingleton();
		}
		return instance;
	}

	private ClassicSingleton() {

	}
}
