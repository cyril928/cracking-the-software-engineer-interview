public class OffsetRun {
	public static void main(String[] args) {
		OffsetRun or = new OffsetRun();
		or.offsetRun("0010011");
	}

	public void offsetRun(String s) {
		int i = 0;
		int count = 1;
		for (int j = 1; j < s.length(); j++) {
			if (s.charAt(i) != s.charAt(j)) {
				if (count > 1) {
					System.out.println(i);
				}
				i = j;
				count = 1;
			} else {
				count++;
			}
		}

		if (count > 1) {
			System.out.println(i);
		}
	}
}
