package leetcode;

// Longest Palindromic Substring
/* http://n00tc0d3r.blogspot.tw/search?q=Longest+Palindromic+Substring
 * valid palindrome, we use two pointers moving towards the middle. But here, 
 * we use two pointers to towards ends.Why? If we move from ends towards middle, 
 * we need to determine the two ends. There are C(n, 2) choices and for each choice, 
 * it takes O(n) time to check whether it is a palindrome. So, the total running time is O(n^3)!
 * While, if we move from middle towards ends, we only need to iterate through the list and 
 * expand from each character. That gives us O(n^2) total running time and only O(1) space.
 * 
 * optimized solution :If the longest is already longer than the remaining part of the string, we can stop there.
 */

public class LongestPalindromicSubstring {

	private String findPalindrome(String s, int left, int right) {
		int len = s.length();
		while (left >= 0 && right < len && s.charAt(left) == s.charAt(right)) {
			left--;
			right++;
		}
		return s.substring(left + 1, right);
	}

	// O(N2)
	public String longestPalindrome(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		String result = "";
		int len = s.length();

		for (int i = 0; i < len; i++) {
			String palinStr = findPalindrome(s, i, i);
			if (palinStr.length() > result.length()) {
				result = palinStr;
			}

			palinStr = findPalindrome(s, i - 1, i);
			if (palinStr.length() > result.length()) {
				result = palinStr;
			}
		}

		return result;
	}
}
