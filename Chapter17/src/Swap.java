
public class Swap {

	public static void swap(int a, int b) {
		a = b ^ a;
		b = b ^ a;
		a = b ^ a;
		System.out.print("a = " + a + "\nb = "+ b);
	}
	
	public static void main(String[] args) {
		int a = 3;
		int b = 10;
		swap(a,b);
	}
	
}
