package must.win.leetcode;

import java.util.LinkedList;
import java.util.Queue;

// Minimum Depth of Binary Tree
/*
 * http://n00tc0d3r.blogspot.tw/2013/04/minimum-depth-of-binary-tree.html?q=Minimum+Depth+of+Binary+Tree
 * explanation, BFS with one queue, DFS prune tree
 * 
 * http://mattcb.blogspot.tw/search?q=Minimum+Depth+of+Binary+Tree
 * implementation, BFS with one/two queues.
 */
public class MinimumDepthofBinaryTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/*
	 * DFS can be improved a little bit by passing in the current min depth and
	 * then it could prune the subtress deeper than minDep.
	 */
	public int minDepth(int minDepth, int depth, TreeNode root) {
		if (root == null || depth >= minDepth) {
			return depth;
		}
		if (root.left == null && root.right == null) {
			return depth + 1;
		}
		if (root.left != null) {
			minDepth = Math.min(minDepth,
					minDepth(minDepth, depth + 1, root.left));
		}
		if (root.right != null) {
			minDepth = Math.min(minDepth,
					minDepth(minDepth, depth + 1, root.right));
		}
		return minDepth;
	}

	// BFS with one queue, best version
	public int minDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);
		int depth = 0;

		while (!queue.isEmpty()) {
			TreeNode newNode = queue.poll();
			if (newNode == null) {
				depth++;
				queue.add(null);
			} else {
				if (newNode.left == null && newNode.right == null) {
					return depth + 1;
				}
				if (newNode.left != null) {
					queue.add(newNode.left);
				}
				if (newNode.right != null) {
					queue.add(newNode.right);
				}
			}
		}
		return depth;
	}

	// BFS, with two queues.
	public int minDepth1(TreeNode root) {
		if (root == null) {
			return 0;
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		Queue<Integer> levelQueue = new LinkedList<Integer>();
		queue.add(root);
		int curLevel = 1;
		levelQueue.add(curLevel);
		while (!queue.isEmpty()) {
			TreeNode curNode = queue.poll();
			curLevel = levelQueue.poll();
			if (curNode.left == null && curNode.right == null) {
				return curLevel;
			}
			if (curNode.left != null) {
				queue.add(curNode.left);
				levelQueue.add(curLevel + 1);
			}
			if (curNode.right != null) {
				queue.add(curNode.right);
				levelQueue.add(curLevel + 1);
			}
		}
		return curLevel;
	}

	// DFS version, better
	public int minDepth2(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int leftDepth = minDepth2(root.left);
		int rightDepth = minDepth2(root.right);

		if (leftDepth == 0) {
			return rightDepth + 1;
		}
		if (rightDepth == 0) {
			return leftDepth + 1;
		}
		return Math.min(leftDepth, rightDepth) + 1;
	}

	// DFS version, by myself
	public int minDepth3(TreeNode root) {
		if (root == null) {
			return 0;
		}
		if (root.left == null && root.right == null) {
			return 1;
		}
		int leftDepth = (root.left == null) ? Integer.MAX_VALUE
				: minDepth(root.left);
		int rightDepth = (root.right == null) ? Integer.MAX_VALUE
				: minDepth(root.right);
		return Math.min(leftDepth, rightDepth) + 1;
	}

}
