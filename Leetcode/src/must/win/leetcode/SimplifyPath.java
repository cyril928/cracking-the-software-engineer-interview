package must.win.leetcode;

import java.util.Stack;

// Simplify Path
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Simplify+Path
 * With stack, not stack
 * Stack runs in time O(n) and uses O(n) space.
 * Not stack runs in time O(n) and uses O(n) space.
 * 
 * with stack, best run time performance, from Zouyou's solution
 * 
 */
public class SimplifyPath {
	// with stack helper function, best run time performance
	private void print(StringBuilder sb, Stack<String> stack) {
		if (!stack.isEmpty()) {
			String str = stack.pop();
			print(sb, stack);
			sb.append("/");
			sb.append(str);
		}
	}

	// without stack, manipulate the original split array
	public String simplifyPath(String path) {
		if (path == null || path.charAt(0) != '/') {
			return null;
		}

		String[] stubs = path.split("/+");
		int last = 0;
		for (String s : stubs) {
			if (s.equals("..") && last > 0) {
				last--;
			} else if (!s.equals("") && !s.equals(".") && !s.equals("..")) {
				stubs[last++] = s;
			}
		}

		if (last == 0) {
			return "/";
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < last; i++) {
			sb.append("/" + stubs[i]);
		}
		return sb.toString();
	}

	// with stack, best run time performance
	public String simplifyPath1(String path) {
		if (path == null || path.isEmpty()) {
			return path;
		}
		Stack<String> stack = new Stack<String>();
		String[] stubs = path.split("/");
		for (String stub : stubs) {
			if (stub.equals("..") && !stack.empty()) {
				stack.pop();
			} else if (!stub.equals(".") && !stub.equals("")
					&& !stub.equals("..")) {
				stack.push(stub);
			}
		}

		StringBuilder sb = new StringBuilder();
		if (stack.isEmpty()) {
			sb.append('/');
		} else {
			print(sb, stack);
		}
		return sb.toString();
	}

	// with stack
	public String simplifyPath2(String path) {
		if (path == null || path.charAt(0) != '/') {
			return null;
		}

		String[] stubs = path.split("/+");
		Stack<String> stack = new Stack<String>();

		for (String s : stubs) {
			if (s.equals(".")) {
				continue;
			} else if (s.equals("..") && !stack.isEmpty()) {
				stack.pop();
			} else if (!s.equals("..") && !s.equals("")) {
				stack.push(s);
			}
		}

		if (stack.isEmpty()) {
			return "/";
		}
		StringBuilder sb = new StringBuilder();
		while (!stack.isEmpty()) {
			sb.insert(0, "/" + stack.pop());
		}
		return sb.toString();
	}

	// my own solution, too much immutable string reallocation, bad!
	public String simplifyPath3(String path) {
		if (path == null || path.charAt(0) != '/') {
			return null;
		}

		String result = "/";
		String[] stubs = path.split("/+");
		for (String s : stubs) {
			if (s.equals(".")) {
				continue;
			} else if (s.equals("..")) {
				int index = result.lastIndexOf("/");
				if (index == 0) {
					index++;
				}
				result = result.substring(0, index);
			} else {
				result += (result.equals("/") ? s : "/" + s);
			}
		}

		return result;
	}
}
