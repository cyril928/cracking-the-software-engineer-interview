package must.win.leetcode;

// Jump Game II 
/*
 * http://n00tc0d3r.blogspot.com/search?q=Jump+Game
 * Time O(N) and Space O(1).
 * 
 * http://www.ninechapter.com/solutions/jump-game/
 * Time O(N2) and Space O(N). 
 */
public class JumpGameII {

	// Time O(n) and Space O(1)
	public int jump(int[] A) {
		int max = 0;
		int next = 0;
		int step = 0;
		for (int i = 0; i < A.length - 1; i++) {
			max = Math.max(i + A[i], max);
			if (i == next) {
				// unreachable
				if (max == next) {
					return -1;
				}
				next = max;
				step++;
			}
		}
		return step;
	}

	// Time O(N2) and Space O(N)
	public int jump1(int[] A) {
		int[] steps = new int[A.length];

		for (int i = 1; i < A.length; i++) {
			steps[i] = Integer.MAX_VALUE;
			for (int j = 0; j < i; j++) {
				if (steps[j] != Integer.MAX_VALUE && (A[j] + j >= i)) {
					steps[i] = steps[j] + 1;
					break;
				}
			}
		}

		return steps[A.length - 1];
	}
}
