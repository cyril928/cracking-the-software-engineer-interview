package must.win.leetcode;

// Rotate Image 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Rotate+Image
 * explanation & implementation
 */
public class RotateImage {
	public void rotate(int[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			return;
		}

		int end = matrix.length - 1;

		for (int level = 0; level < end; level++, end--) {
			for (int i = level; i < end; i++) {
				int tail = matrix.length - i - 1;
				int temp = matrix[level][i];
				matrix[level][i] = matrix[tail][level];
				matrix[tail][level] = matrix[end][tail];
				matrix[end][tail] = matrix[i][end];
				matrix[i][end] = temp;
			}
		}
	}
}
