package must.win.leetcode;

class Reader4 {
	int read4(char[] buf) {
		return 4;
	}
}

// Read N Characters Given Read4
/*
 * Clean Code Hand Book
 */
public class ReadNCharactersGivenRead4 extends Reader4 {
	public int read(char[] buf, int n) {
		char[] buffer = new char[4];
		int readBytes = 0;
		while (readBytes < n) {
			int sz = read4(buffer);
			int bytes = Math.min(n - readBytes, sz);
			System.arraycopy(buffer, 0, buf, readBytes, bytes);
			readBytes += bytes;
			if (sz < 4) {
				break;
			}
		}
		return readBytes;
	}

}
