package must.win.leetcode;

// Gas Station 
/*
 * http://blog.csdn.net/kenden23/article/details/14106137
 * explanation & implementation
 */
public class GasStation {
	public int canCompleteCircuit(int[] gas, int[] cost) {
		if (gas == null || cost == null || gas.length == 0 || cost.length == 0
				|| gas.length != cost.length) {
			return -1;
		}

		int total = 0;
		int sum = 0, startIndex = 0;
		for (int i = 0; i < cost.length; i++) {
			sum += gas[i] - cost[i];
			if (sum < 0) {
				sum = 0;
				startIndex = i + 1;
			}
			total += gas[i] - cost[i];
		}

		return total < 0 ? -1 : startIndex;
	}
}
