package must.win.leetcode;

// Reverse Bits 
/*
 * 
 */
public class ReverseBits {
	// you need treat n as an unsigned value
	public int reverseBits(int n) {
		int res = 0;
		int bits = 0;
		while (bits++ < 32) {
			res = (res << 1) | (n & 1);
			n >>= 1;
		}
		return res;
	}
}
