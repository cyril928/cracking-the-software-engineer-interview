package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Text Justification 
/* 
 * http://n00tc0d3r.blogspot.tw/search?q=Text+Justification
 * clean solution
 * 
 * my own way
 */
public class TextJustification {

	// most clean solution
	public ArrayList<String> fullJustify(String[] words, int L) {
		ArrayList<String> res = new ArrayList<String>();

		// count word length
		int len = 0, start = 0;
		for (int i = 0; i < words.length; ++i) {
			len += words[i].length();
			if ((len + i - start) > L) {
				res.add(justify(words, start, i - 1, len -= words[i].length(),
						L));
				// reset
				len = words[i].length();
				start = i;
			}
		}
		// last line
		res.add(justify(words, start, words.length - 1, len, L));
		return res;
	}

	// my own way
	public List<String> fullJustify1(String[] words, int L) {
		List<String> res = new ArrayList<String>();
		if (words == null || words.length == 0) {
			return res;
		}

		int i = 0, j = 0;
		int totalLen = 0;
		int wordCount = words.length;
		while (j < wordCount) {
			int total = words[j].length() + (j - i) + totalLen;
			if (total <= L) {
				totalLen += words[j].length();
				j++;
			} else {
				res.add(justify1(words, i, j - 1, L, totalLen));
				totalLen = 0;
				i = j;
			}
		}
		res.add(justify1(words, i, j - 1, L, totalLen));
		return res;
	}

	// most clean solution
	// generate a string of length L, containing words[start..end], inclusively
	private String justify(String[] words, int start, int end, int total, int L) {
		StringBuilder ss = new StringBuilder();
		if (end == start || end == words.length - 1) { // single word or last
														// words
			while (start < end) {
				ss.append(words[start++]);
				ss.append(stuffSpaces(1));
			}
			ss.append(words[end]);
			ss.append(stuffSpaces(L - ss.length()));
		} else {
			int spaces = (L - total) / (end - start);
			int extras = (L - total) % (end - start);
			// buld up full string
			while (start < end) {
				ss.append(words[start++]);
				ss.append(stuffSpaces(spaces));
				if (extras > 0) {
					ss.append(stuffSpaces(1));
					--extras;
				}
			}
			ss.append(words[end]);
		}
		return ss.toString();
	}

	// my own way
	private String justify1(String[] words, int start, int end, int L,
			int totalLen) {
		StringBuilder sb = new StringBuilder();
		if (start == end || end == words.length - 1) {
			for (int i = start; i < end; i++) {
				sb.append(words[i]).append(" ");
				totalLen += 1;
			}
			sb.append(words[end]);
			stuffSpace(sb, L - totalLen);
		} else {
			int spaces = (L - totalLen) / (end - start);
			int extras = (L - totalLen) % (end - start);
			for (int i = start; i < end; i++) {
				sb.append(words[i]);
				stuffSpace(sb, spaces);
				if (extras-- > 0) {
					stuffSpace(sb, 1);
				}
			}
			sb.append(words[end]);
		}
		return sb.toString();
	}

	// my own way
	private void stuffSpace(StringBuilder sb, int spaces) {
		while (spaces-- > 0) {
			sb.append(" ");
		}
	}

	// most clean solution
	// generate a string of n spaces
	private String stuffSpaces(int n) {
		if (n == 0) {
			return "";
		}
		StringBuilder ss = new StringBuilder();
		while (n > 0) {
			ss.append(' ');
			--n;
		}
		return ss.toString();
	}
}
