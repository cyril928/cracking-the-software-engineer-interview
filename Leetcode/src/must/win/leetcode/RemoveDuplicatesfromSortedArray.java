package must.win.leetcode;

// Remove Duplicates from Sorted Array 
/* 
 * http://answer.ninechapter.com/solutions/remove-duplicates-from-sorted-array/
 * O(N) use new size(index), most clean;
 * 
 * http://n00tc0d3r.blogspot.tw/2013/05/remove-element-from-arraylist.html?q=Remove+Duplicates+from+Sorted+Array
 * O(N) use duplicate count!
 * 
 */

public class RemoveDuplicatesfromSortedArray {

	// O(N) use new size(index), most clean;
	public int removeDuplicates(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int size = 0;
		int len = A.length;
		for (int i = 1; i < len; i++) {
			if (A[i] != A[size]) {
				A[++size] = A[i];
			}
		}
		return size + 1;
	}

	// O(N) use duplicate count. ex. 111222333, don't have to consider last two
	// 3, that's why this question's return value is array's new length.
	public int removeDuplicates1(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int dupCount = 0;
		int len = A.length;
		for (int i = 1; i < len; i++) {
			if (A[i] == A[i - 1]) {
				dupCount++;
			} else if (dupCount > 0) {
				A[i - dupCount] = A[i];
			}
		}
		return len - dupCount;
	}
}
