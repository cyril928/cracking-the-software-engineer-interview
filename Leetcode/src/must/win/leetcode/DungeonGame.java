package must.win.leetcode;

// Dungeon Game 
/*
 * https://oj.leetcode.com/problems/dungeon-game/solution/
 * official explanation
 * 
 * http://blog.csdn.net/likecool21/article/details/42516979
 * DP explanation & implementation
 */
public class DungeonGame {

	// 1 dimensional space
	public int calculateMinimumHP(int[][] dungeon) {
		if (dungeon == null || dungeon.length == 0) {
			return Integer.MAX_VALUE;
		}

		int row = dungeon.length;
		int col = dungeon[0].length;
		int[] dp = new int[col];

		dp[col - 1] = dungeon[row - 1][col - 1] > 0 ? 1
				: -dungeon[row - 1][col - 1] + 1;

		for (int i = col - 2; i >= 0; i--) {
			dp[i] = Math.max(dp[i + 1] - dungeon[row - 1][i], 1);
		}

		for (int i = row - 2; i >= 0; i--) {
			dp[col - 1] = Math.max(dp[col - 1] - dungeon[i][col - 1], 1);
			for (int j = col - 2; j >= 0; j--) {
				dp[j] = Math.max(1, Math.min(dp[j], dp[j + 1]) - dungeon[i][j]);
			}
		}
		return dp[0];
	}

	// 2 dimensional space
	public int calculateMinimumHP1(int[][] dungeon) {
		if (dungeon == null || dungeon.length == 0) {
			return Integer.MAX_VALUE;
		}
		int row = dungeon.length;
		int col = dungeon[0].length;
		int[][] dp = new int[row][col];

		dp[row - 1][col - 1] = dungeon[row - 1][col - 1] > 0 ? 1
				: -dungeon[row - 1][col - 1] + 1;
		for (int i = col - 2; i >= 0; i--) {
			dp[row - 1][i] = Math.max(dp[row - 1][i + 1] - dungeon[row - 1][i],
					1);
		}

		for (int i = row - 2; i >= 0; i--) {
			dp[i][col - 1] = Math.max(dp[i + 1][col - 1] - dungeon[i][col - 1],
					1);
		}

		for (int i = row - 2; i >= 0; i--) {
			for (int j = col - 2; j >= 0; j--) {
				dp[i][j] = Math.max(Math.min(dp[i + 1][j], dp[i][j + 1])
						- dungeon[i][j], 1);
			}
		}
		return dp[0][0];
	}
}
