package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Permutations
/*
 * http://www.ninechapter.com/solutions/permutations/
 * DFS, top-down result building
 * 
 * http://n00tc0d3r.blogspot.com/2013/05/permutations.html?q=Edit+Distance
 * DFS, second way, bottom-up result building
 */
public class Permutations {
	// DFS, top-down result building
	public List<List<Integer>> permute(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}

		permuteHelper(result, new ArrayList<Integer>(), num);
		return result;
	}

	// DFS, second way, bottom-up result building
	public List<List<Integer>> permute1(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}

		int len = num.length;
		if (num.length == 1) {
			List<Integer> list = new ArrayList<Integer>();
			list.add(num[0]);
			result.add(list);
		} else {
			for (int i = 0; i < len; i++) {
				// copy a new array of len-1 numbers
				int[] subset = Arrays.copyOf(num, len - 1);
				for (int j = i + 1; j < len; j++) {
					subset[j - 1] = num[j];
				}
				// append the current number to the end of permutation of n-1
				// subset
				for (List<Integer> list : permute1(subset)) {
					list.add(num[i]);
					result.add(list);
				}
			}
		}

		return result;
	}

	// DFS helper (top-down result building)
	private void permuteHelper(List<List<Integer>> result, List<Integer> list,
			int[] num) {
		if (list.size() == num.length) {
			result.add(new ArrayList<Integer>(list));
			return;
		}
		for (int i = 0; i < num.length; i++) {
			if (!list.contains(num[i])) {
				list.add(num[i]);
				permuteHelper(result, list, num);
				list.remove(list.size() - 1);
			}
		}
	}
}
