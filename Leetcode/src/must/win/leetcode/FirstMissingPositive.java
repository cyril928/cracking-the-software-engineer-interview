package must.win.leetcode;

// First Missing Positive
/* 
 * O(n) Time, Constant Space.
 * 
 * http://answer.ninechapter.com/solutions/first-missing-positive/
 * most clean solution with for and while loop combination to do number swap.
 * 
 */
public class FirstMissingPositive {

	// most clean solution with for and while loop combination to do number
	// swap.
	public int firstMissingPositive(int[] A) {
		if (A == null) {
			return 1;
		}

		int len = A.length;
		for (int i = 0; i < len; i++) {
			while (A[i] > 0 && A[i] <= len && A[i] != i + 1) {
				if (A[i] == A[A[i] - 1]) {
					break;
				}
				int temp = A[A[i] - 1];
				A[A[i] - 1] = A[i];
				A[i] = temp;
			}
		}

		for (int i = 0; i < len; i++) {
			if (A[i] != i + 1) {
				return i + 1;
			}
		}
		return A.length + 1;
	}

	// solution with only while loop to do number swap.
	public int firstMissingPositive1(int[] A) {
		if (A == null) {
			return 1;
		}

		int len = A.length;
		int i = 0;
		while (i < len) {
			if (A[i] > 0 && A[i] <= len && A[i] != i + 1 && A[i] != A[A[i] - 1]) {
				int temp = A[A[i] - 1];
				A[A[i] - 1] = A[i];
				A[i] = temp;
			} else {
				i++;
			}
		}

		i = 0;
		while (i < len && A[i] == i + 1) {
			i++;
		}
		return i + 1;
	}
}
