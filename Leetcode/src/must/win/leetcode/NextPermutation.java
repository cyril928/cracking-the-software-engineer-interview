package must.win.leetcode;

// Next Permutation
/*
 * http://n00tc0d3r.blogspot.com/2013/04/next-permutation.html?q=Next+Permutation
 * implementation & explanation
 * The algorithm below runs in time O(n) and takes O(1) space. 
 * We reverse the tail rather than sort them, since we already know they are in descending order. 
 * Some sorting algorithms may take O(nlogn) or O(n^2) time to sort a reverse ordered array 
 * rather than O(n). 
 */
public class NextPermutation {

	// my own way
	public void nextPermutation(int[] num) {
		if (num == null || num.length == 0) {
			return;
		}

		// find out the end index of ascending order tail
		int i = num.length - 1;
		while (i > 0) {
			if (num[i] > num[i - 1]) {
				break;
			}
			i--;
		}

		// reverse tail from descending order to ascending order
		reverse(num, i, num.length - 1);

		// swap the target pair
		if (i > 0) {
			int val = num[i - 1];
			for (int j = i; j < num.length; j++) {
				if (num[j] > val) {
					swap(num, i - 1, j);
					return;
				}
			}
		}
	}

	private void reverse(int[] num, int start, int end) {
		while (start < end) {
			swap(num, start++, end--);
		}
	}

	private void swap(int[] num, int i, int j) {
		int temp = num[i];
		num[i] = num[j];
		num[j] = temp;
	}
}
