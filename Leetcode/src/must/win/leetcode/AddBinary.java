package must.win.leetcode;

// Add Binary 
/* 
 * http://n00tc0d3r.blogspot.tw/2013/01/add-two-binary-numbers.html?q=Add+Binary
 * explanation & implementation, but actaully my way is better can cleaner.
 * 
 * Another thing is that (for Java) we should use StringBuilder append() method rather than String "+" operator 
 * to avoid cost. Note that what "+" operator does is:
 * create a new string;
 * make a copy of the left string;
 * append the right char/string to the end.
 * So, a series of "+" operation is O(n^2)-time, not O(n).
 * StringBuilder is smarter and you can think it as a dynamically resizing array of char. 
 * No copies for append() and it can output a String by calling toString() method.
 */
public class AddBinary {
	public String addBinary(String a, String b) {
		if (a == null || b == null) {
			return (a == null) ? b : a;
		}

		StringBuilder sb = new StringBuilder();
		int carry = 0;
		int ap = a.length() - 1, bp = b.length() - 1;
		while (ap >= 0 && bp >= 0) {
			int val = (a.charAt(ap) - '0') ^ (b.charAt(bp) - '0') ^ carry;
			sb.append(val);
			carry = (a.charAt(ap--) - '0') + (b.charAt(bp--) - '0') + carry;
			carry >>= 1;
		}

		while (ap >= 0) {
			int val = (a.charAt(ap) - '0') ^ carry;
			sb.append(val);
			carry = (a.charAt(ap--) - '0') & carry;
		}

		while (bp >= 0) {
			int val = (b.charAt(bp) - '0') ^ carry;
			sb.append(val);
			carry = (b.charAt(bp--) - '0') & carry;
		}

		if (carry == 1) {
			sb.append(1);
		}

		return sb.reverse().toString();
	}
}
