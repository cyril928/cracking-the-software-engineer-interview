package must.win.leetcode;

// Add Two Numbers 
/*
 * http://answer.ninechapter.com/solutions/add-two-numbers/
 * while(l1 != null && l2!=null) loop
 * 
 * http://n00tc0d3r.blogspot.tw/2013/01/add-two-binary-numbers.html?q=Add+Two+Numbers
 * while(l1 != null || l2!=null) loop
 * any kinds of Add Two (Binary) Numbers explanation
 * 
 */
public class AddTwoNumbers {
	// while(l1 != null && l2!=null) loop
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;
		int carry = 0;
		while (l1 != null && l2 != null) {
			int sum = l1.val + l2.val + carry;
			carry = sum / 10;
			cur.next = new ListNode(sum % 10);
			cur = cur.next;
			l1 = l1.next;
			l2 = l2.next;
		}

		while (l1 != null) {
			int sum = l1.val + carry;
			carry = sum / 10;
			cur.next = new ListNode(sum % 10);
			cur = cur.next;
			l1 = l1.next;
		}

		while (l2 != null) {
			int sum = l2.val + carry;
			carry = sum / 10;
			cur.next = new ListNode(sum % 10);
			cur = cur.next;
			l2 = l2.next;
		}

		if (carry > 0) {
			cur.next = new ListNode(carry);
		}

		return dummy.next;
	}

	// while(l1 != null || l2!=null) loop
	public ListNode addTwoNumbers1(ListNode l1, ListNode l2) {

		ListNode head = null;
		ListNode cur = null;
		int carry = 0;
		while (l1 != null || l2 != null) {
			int sum = carry;
			if (l1 != null) {
				sum += l1.val;
				l1 = l1.next;
			}
			if (l2 != null) {
				sum += l2.val;
				l2 = l2.next;
			}

			carry = sum / 10;
			if (cur == null) {
				cur = new ListNode(sum % 10);
				head = cur;
			} else {
				cur.next = new ListNode(sum % 10);
				cur = cur.next;
			}
		}

		if (carry > 0) {
			cur.next = new ListNode(carry);
		}

		return head;
	}
}
