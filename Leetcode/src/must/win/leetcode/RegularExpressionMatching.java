package must.win.leetcode;

// Regular Expression Matching 
/*
 * my way
 * DP O(N2) time, O(N) space
 * 
 * http://answer.ninechapter.com/solutions/regular-expression-matching/
 * recursion, add a dummy * as approach, clean!
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Regular+Expression+Matching
 * explanation 
 * 
 */
public class RegularExpressionMatching {
	// recursive helper
	private boolean checkEmpty(String p, int pi) {
		int len = p.length();
		if (((len - pi) & 1) == 1) {
			return false;
		}
		for (int i = pi + 1; i < len; i += 2) {
			if (p.charAt(i) != '*') {
				return false;
			}
		}
		return true;
	}

	// DP
	public boolean isMatch(String s, String p) {
		if (s == null || p == null) {
			return false;
		}

		int sLen = s.length();
		int pLen = p.length();
		boolean[][] dp = new boolean[2][pLen + 1];

		dp[0][0] = true;

		for (int i = 1; i <= pLen; i++) {
			dp[0][i] = (p.charAt(i - 1) == '*') && dp[0][i - 2];
		}

		int preRow = 0;
		int curRow = 1;
		for (int i = 1; i <= sLen; i++) {
			dp[curRow][0] = false;
			for (int j = 1; j <= pLen; j++) {
				dp[curRow][j] = (p.charAt(j - 1) == '*') ? isSameChar(
						s.charAt(i - 1), p.charAt(j - 2))
						&& dp[preRow][j] || dp[curRow][j - 2] : isSameChar(
						s.charAt(i - 1), p.charAt(j - 1))
						&& dp[preRow][j - 1];

			}
			preRow = curRow;
			curRow = curRow ^ 1;
		}
		return dp[preRow][pLen];
	}

	// recursive
	public boolean isMatch1(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		return isMatchHelper(s, 0, p, 0);
	}

	// recursive helper
	private boolean isMatchHelper(String s, int si, String p, int pi) {
		if (si == s.length()) {
			return checkEmpty(p, pi);
		}

		if (pi == p.length()) {
			return false;
		}

		if (pi + 1 < p.length() && p.charAt(pi + 1) == '*') {
			if (isSameChar(s.charAt(si), p.charAt(pi))) {
				return isMatchHelper(s, si, p, pi + 2)
						|| isMatchHelper(s, si + 1, p, pi);
			} else {
				return isMatchHelper(s, si, p, pi + 2);
			}
		} else if (isSameChar(s.charAt(si), p.charAt(pi))) {
			return isMatchHelper(s, si + 1, p, pi + 1);
		}
		return false;
	}

	// for recursive and DP
	private boolean isSameChar(char a, char b) {
		return a == b || b == '.';
	}

}
