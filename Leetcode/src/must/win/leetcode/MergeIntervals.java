package must.win.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import must.win.leetcode.InsertInterval.Interval;

// Merge Intervals
/* http://answer.ninechapter.com/solutions/merge-intervals/
 * most clean solution
 */
public class MergeIntervals {

	// most clean solution
	private class intervalComparator implements Comparator<Interval> {
		@Override
		public int compare(Interval a, Interval b) {
			return a.start - b.start;
		}
	}

	public List<Interval> merge(List<Interval> intervals) {
		if (intervals == null || intervals.size() <= 1) {
			return intervals;
		}
		Collections.sort(intervals, new intervalComparator());
		List<Interval> result = new ArrayList<Interval>();

		int size = intervals.size();
		Interval last = intervals.get(0);
		for (int i = 1; i < size; i++) {
			Interval cur = intervals.get(i);
			if (cur.start <= last.end) {
				last.end = Math.max(last.end, cur.end);
			} else {
				result.add(last);
				last = cur;
			}
		}
		result.add(last);
		return result;
	}
}
