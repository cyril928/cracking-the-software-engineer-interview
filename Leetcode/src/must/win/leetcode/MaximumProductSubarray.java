package must.win.leetcode;

// Maximum Product Subarray 
/*
 * http://blog.csdn.net/worldwindjp/article/details/39826823
 * explanation & most clean implementation O(N)
 * 
 * https://oj.leetcode.com/problems/maximum-product-subarray/solution/
 * official explanation
 * 
 */
public class MaximumProductSubarray {

	// most clean implementation O(N)
	public int maxProduct(int[] A) {
		if (A == null || A.length == 0) {
			return Integer.MIN_VALUE;
		}

		int max = 1;
		int min = 1;
		int maxProduct = Integer.MIN_VALUE;
		for (int val : A) {
			int a = val * max;
			int b = val * min;
			max = Math.max(Math.max(a, b), val);
			min = Math.min(Math.min(a, b), val);
			maxProduct = Math.max(max, maxProduct);
		}
		return maxProduct;
	}

	// my own way, not good
	public int maxProduct1(int[] A) {
		if (A == null || A.length == 0) {
			return Integer.MIN_VALUE;
		}

		// max positive product ending at the current position
		int maxEndHere = 1;
		// min negative product ending at the current position
		int minEndHere = 1;
		// Initialize overall max product
		int max = Integer.MIN_VALUE;

		int len = A.length;

		for (int i = 0; i < len; i++) {
			if (A[i] > 0) {
				maxEndHere = Math.max(A[i], maxEndHere * A[i]);
				minEndHere = Math.min(A[i], minEndHere * A[i]);
				max = Math.max(max, maxEndHere);
			} else if (A[i] < 0) {
				int temp = maxEndHere;
				maxEndHere = Math.max(A[i], minEndHere * A[i]);
				minEndHere = Math.min(A[i], temp * A[i]);
				max = Math.max(max, maxEndHere);
			} else {
				max = Math.max(max, 0);
				maxEndHere = 1;
				minEndHere = 1;
			}
		}

		return max;
	}
}
