package must.win.leetcode;

// Spiral Matrix II
/*
 * http://n00tc0d3r.blogspot.tw/search?q=spiral+matrix
 * iterative solution, explanation & implementation
 * 
 * iterative solution from ZouYou
 */
public class SpiralMatrixII {

	// iterative solution
	public int[][] generateMatrix(int n) {
		if (n < 0) {
			return null;
		}

		int val = 1;
		int[][] res = new int[n][n];
		for (int level = 0; level < n; level++, n--) {
			// top
			for (int i = level; i < n; i++) {
				res[level][i] = val++;
			}
			// right
			for (int i = level + 1; i < n; i++) {
				res[i][n - 1] = val++;
			}
			// down
			for (int i = n - 2; i >= level; i--) {
				res[n - 1][i] = val++;
			}
			// left
			for (int i = n - 2; i > level; i--) {
				res[i][level] = val++;
			}
		}

		return res;
	}

	// iterative solution from ZouYou
	public int[][] generateMatrix1(int n) {
		int[][] matrix = new int[n][n];

		int val = 1;
		int rs = 0, re = n - 1;
		while (rs < re) {
			for (int c = rs; c <= re - 1; c++) {
				matrix[rs][c] = val++;
			}
			for (int r = rs; r <= re - 1; r++) {
				matrix[r][re] = val++;
			}
			for (int c = re; c >= rs + 1; c--) {
				matrix[re][c] = val++;
			}
			for (int r = re; r >= rs + 1; r--) {
				matrix[r][rs] = val++;
			}
			rs++;
			re--;
		}

		if (rs == re) {
			matrix[rs][rs] = val;
		}

		return matrix;
	}

	// my own way
	public int[][] generateMatrix2(int n) {
		if (n < 0) {
			return null;
		}

		int val = 1;
		int[][] res = new int[n][n];
		for (int level = 0; level < (n + 1) / 2; level++) {
			int end = n - level - 1;
			// top
			for (int i = level; i <= end; i++) {
				res[level][i] = val++;
			}
			// right
			for (int i = level + 1; i <= end; i++) {
				res[i][end] = val++;
			}
			// down
			for (int i = end - 1; i >= level; i--) {
				res[end][i] = val++;
			}
			// left
			for (int i = end - 1; i > level; i--) {
				res[i][level] = val++;
			}
		}

		return res;
	}
}
