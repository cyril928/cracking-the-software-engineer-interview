package must.win.leetcode;

// String to Integer (atoi) 
/*
 * http://answer.ninechapter.com/solutions/string-to-integer-atoi/
 * has a Long capacity bug, but can pass OJ
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=ATOI
 * regular expression
 */
public class StringtoIntegeratoi {
	public int atoi(String str) {
		if (str == null) {
			return 0;
		}
		str = str.trim();
		if (str.length() == 0) {
			return 0;
		}

		int sign = 1;
		int index = 0;

		if (str.charAt(index) == '+') {
			index++;
		} else if (str.charAt(index) == '-') {
			sign = -1;
			index++;
		}
		long num = 0;
		for (; index < str.length(); index++) {
			if (str.charAt(index) < '0' || str.charAt(index) > '9') {
				break;
			}
			num = num * 10 + (str.charAt(index) - '0');
			if (sign > 0 && num > Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			} else if (sign < 0 && -num < Integer.MIN_VALUE) {
				return Integer.MIN_VALUE;
			}
		}

		return (int) num * sign;
	}
}
