package must.win.leetcode;

// Interleaving String 
/* http://n00tc0d3r.blogspot.tw/search?q=Interleaving+String
 * // DP, implementation
 * 
 * http://mattcb.blogspot.tw/2012/11/interleaving-string.html?q=Interleaving+String
 * // recursive implementation, TLE 
 */
public class InterleavingString {

	// recursive helper
	public boolean isInterleave(String s1, int s1p, String s2, int s2p,
			String s3, int s3p) {
		if (s3p == s3.length()) {
			return s1p == s1.length() && s2p == s2.length();
		}

		if (s1p < s1.length() && s1.charAt(s1p) == s3.charAt(s3p)
				&& isInterleave(s1, s1p + 1, s2, s2p, s3, s3p + 1)) {
			return true;
		}

		if (s2p < s2.length() && s2.charAt(s2p) == s3.charAt(s3p)
				&& isInterleave(s1, s1p, s2, s2p + 1, s3, s3p + 1)) {
			return true;
		}

		return false;
	}

	// DP, it takes time O(n1*n2) and space O(n2).
	public boolean isInterleave(String s1, String s2, String s3) {
		if (s1 == null || s2 == null || s3 == null) {
			return false;
		}
		int len1 = s1.length(), len2 = s2.length(), len3 = s3.length();

		if (len3 != len2 + len1) {
			return false;
		}

		// len2 + 1, plus 1 is for only s2 and s3 comparison
		boolean[] match = new boolean[len2 + 1];

		match[0] = true;
		for (int i = 0; i < len2; i++) {
			match[i + 1] = match[i] && (s2.charAt(i) == s3.charAt(i));
			if (!match[i + 1]) {
				break;
			}
		}

		for (int i = 0; i < len1; i++) {
			match[0] = match[0] && s1.charAt(i) == s3.charAt(i);
			for (int j = 1; j <= len2; j++) {
				match[j] = (match[j] && s1.charAt(i) == s3.charAt(i + j))
						|| (match[j - 1] && s2.charAt(j - 1) == s3
								.charAt(i + j));
			}
		}

		return match[len2];
	}

	// DP, it takes time O(n1*n2) and space O(n1*n2).
	public boolean isInterleave1(String s1, String s2, String s3) {
		if (s1 == null || s2 == null || s3 == null) {
			return false;
		}
		int len1 = s1.length(), len2 = s2.length(), len3 = s3.length();

		if (len3 != len2 + len1) {
			return false;
		}

		boolean[][] match = new boolean[len1 + 1][len2 + 1];

		match[0][0] = true;
		for (int i = 0; i < len1; i++) {
			if (match[i][0] && s1.charAt(i) == s3.charAt(i)) {
				match[i + 1][0] = true;
			}
		}
		for (int j = 0; j < len2; j++) {
			if (match[0][j] && s2.charAt(j) == s3.charAt(j)) {
				match[0][j + 1] = true;
			}
		}
		for (int i = 0; i < len1; i++) {
			for (int j = 0; j < len2; j++) {
				if ((match[i][j + 1] && s1.charAt(i) == s3.charAt(i + j + 1))
						|| (match[i + 1][j] && s2.charAt(j) == s3.charAt(i + j
								+ 1))) {
					match[i + 1][j + 1] = true;
				}
			}
		}

		return match[len1][len2];
	}

	// recursive, TLE
	public boolean isInterleave2(String s1, String s2, String s3) {
		return isInterleave(s1, 0, s2, 0, s3, 0);
	}
}
