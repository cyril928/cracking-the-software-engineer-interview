package must.win.leetcode;

// Binary Tree Maximum Path Sum 
/*
 * http://mattcb.blogspot.tw/search?q=Binary+Tree+Maximum+Path+Sum
 * using global variable to keep current max pathSum
 * 
 * http://n00tc0d3r.blogspot.com/2013/01/tree-path-sum.html?q=Binary+Tree+Maximum+Path+Sum
 * define new class for data delivery (single path sum & max path sum)
 */
public class BinaryTreeMaximumPathSum {
	// define new class for data delivery (single path sum & max path sum), data
	// type
	private class ResultType {
		int singlePath, maxPath;

		ResultType(int singlePath, int maxPath) {
			this.singlePath = singlePath;
			this.maxPath = maxPath;
		}
	}

	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int x) {
			this.val = x;
		}
	}

	// using global variable to keep current max pathSum, global variable
	int max;

	// using global variable to keep current max pathSum, helper function
	public int findMax(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int left = Math.max(findMax(root.left), 0);
		int right = Math.max(findMax(root.right), 0);
		max = Math.max(left + right + root.val, max);
		return root.val + Math.max(left, right);
	}

	// using global variable to keep current max pathSum
	public int maxPathSum(TreeNode root) {
		max = (root == null) ? 0 : root.val;
		findMax(root);
		return max;
	}

	// define new class for data delivery (single path sum & max path sum)
	public int maxPathSum1(TreeNode root) {
		if (root == null) {
			return 0;
		}
		ResultType res = maxSubPath(root);
		return res.maxPath;
	}

	// define new class for data delivery (single path sum & max path sum),
	// helper function
	private ResultType maxSubPath(TreeNode root) {
		if (root == null) {
			return new ResultType(0, Integer.MIN_VALUE);
		}
		ResultType leftRes = maxSubPath(root.left);
		ResultType rightRes = maxSubPath(root.right);

		int singlePath = Math.max(leftRes.singlePath, rightRes.singlePath)
				+ root.val;
		singlePath = Math.max(singlePath, 0);

		int maxPath = Math.max(leftRes.maxPath, rightRes.maxPath);
		maxPath = Math.max(maxPath, leftRes.singlePath + rightRes.singlePath
				+ root.val);
		return new ResultType(singlePath, maxPath);
	}
}
