package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Gray Code 
/*
 * http://mattcb.blogspot.tw/search?q=Gray+Code
 * tricky solution
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Gray+Code
 * good solution
 */
public class GrayCode {
	// best, tricky solution
	public List<Integer> grayCode(int n) {
		int size = 1 << n;
		List<Integer> res = new ArrayList<Integer>(size);
		for (int i = 0; i < size; i++) {
			res.add(i ^ (i >> 1));
		}
		return res;
	}

	// good solution
	public List<Integer> grayCode1(int n) {
		List<Integer> res = new ArrayList<Integer>(1 << n);
		res.add(0);
		for (int i = 0; i < n; i++) {
			int msb = 1 << i;
			int size = res.size();
			for (int j = size - 1; j >= 0; j--) {
				res.add(res.get(j) | msb);
			}
		}
		return res;
	}
}
