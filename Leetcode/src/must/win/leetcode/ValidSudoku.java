package must.win.leetcode;

import java.util.Arrays;

// Valid Sudoku 
/* http://answer.ninechapter.com/solutions/valid-sudoku/
 * This algorithm runs in time O(n^2), where n is the size of the square (n=9 in this case). 
 * The order of the running time is optimal since we have to check each spot at least once. 
 * And we use O(n) space to keep track of numbers that have seen in the current block (row/column/box).
 */
public class ValidSudoku {
	private boolean isValid(boolean[] visited, char digit) {
		if (digit == '.') {
			return true;
		}
		int num = digit - '0';
		if (num > 9 || num < 1 || visited[num - 1]) {
			return false;
		}
		visited[num - 1] = true;
		return true;
	}

	public boolean isValidSudoku(char[][] board) {
		if (board == null || board.length == 0) {
			return false;
		}

		boolean[] visited = new boolean[9];

		// validate rows
		for (int i = 0; i < 9; i++) {
			Arrays.fill(visited, false);
			for (int j = 0; j < 9; j++) {
				if (!isValid(visited, board[i][j])) {
					return false;
				}
			}
		}

		// validate cols
		for (int j = 0; j < 9; j++) {
			Arrays.fill(visited, false);
			for (int i = 0; i < 9; i++) {
				if (!isValid(visited, board[i][j])) {
					return false;
				}
			}
		}

		// validate 3*3 cells
		for (int i = 0; i < 9; i += 3) {
			for (int j = 0; j < 9; j += 3) {
				Arrays.fill(visited, false);
				for (int k = 0; k < 9; k++) {
					if (!isValid(visited, board[i + k / 3][j + k % 3])) {
						return false;
					}
				}
			}
		}

		return true;
	}
}
