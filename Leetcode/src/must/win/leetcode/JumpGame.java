package must.win.leetcode;

// Jump Game 
/*
 * http://n00tc0d3r.blogspot.com/search?q=Jump+Game
 * Time O(n) and Space O(1).
 * 
 * http://www.ninechapter.com/solutions/jump-game/
 * Time O(N2) and Space O(N).
 */
public class JumpGame {
	// Time O(n) and Space O(1)
	public boolean canJump(int[] A) {

		int i = A.length - 1;
		int j = i - 1;

		while (j >= 0) {
			if (A[j] + j >= i) {
				i = j;
			}
			j--;
		}
		return (i == 0);

		/*
		 * int next = A.length - 1; for (int i=A.length-2; i>=0; --i) { if (A[i]
		 * >= (next - i)) { next = i; } } return (next == 0);
		 */
	}

	// Time O(N2) and Space O(N).
	public boolean canJump1(int[] A) {

		boolean[] canJump = new boolean[A.length];
		canJump[0] = true;

		for (int i = 1; i < A.length; i++) {
			for (int j = 0; j < i; j++) {
				if (canJump[j] && (j + A[j] >= i)) {
					canJump[i] = true;
					break;
				}
			}
		}
		return canJump[A.length - 1];
	}
}
