package must.win.leetcode;

// Integer to Roman 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Roman+to+Integer
 * explanation & implementation
 */
public class IntegertoRoman {

	// use array only, don't have to use HashMap
	public String intToRoman(int num) {
		if (num <= 0) {
			return "";
		}

		int[] nums = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
		String[] symbols = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X",
				"IX", "V", "IV", "I" };

		StringBuilder sb = new StringBuilder();
		int i = 0;
		while (num > 0) {
			int times = num / nums[i];
			num -= times * nums[i];
			while (times > 0) {
				sb.append(symbols[i]);
				times--;
			}
			i++;
		}
		return sb.toString();
	}
}
