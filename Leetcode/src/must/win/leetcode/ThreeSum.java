package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// 3Sum 
/*
 * http://n00tc0d3r.blogspot.tw/2013/01/2sum-3sum-4sum-and-variances.html?q=3+sum
 * best implementation and explanation
 */
public class ThreeSum {
	// O(N2) time and O(1) space
	public List<List<Integer>> threeSum(int[] num) {
		if (num == null) {
			return null;
		}
		List<List<Integer>> resSet = new ArrayList<List<Integer>>();
		if (num.length < 3) {
			return resSet;
		}
		Arrays.sort(num);

		// verify (num[i] <= 0), which is a more optimized way.
		for (int i = 0; i < num.length - 2 && num[i] <= 0; i++) {
			if (i > 0 && num[i] == num[i - 1]) {
				continue;
			}
			int start = i + 1, end = num.length - 1;
			while (start < end) {
				int sum = num[i] + num[start] + num[end];
				if (sum > 0) {
					end--;
				} else if (sum < 0) {
					start++;
				} else {
					List<Integer> res = new ArrayList<Integer>();
					res.add(num[i]);
					res.add(num[start]);
					res.add(num[end]);
					resSet.add(res);
					do {
						start++;
					} while (num[start] == num[start - 1] && start < end);
					do {
						end--;
					} while (num[end] == num[end + 1] && start < end);
				}
			}
		}
		return resSet;
	}
}
