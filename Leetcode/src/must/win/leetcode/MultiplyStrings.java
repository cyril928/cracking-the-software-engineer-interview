package must.win.leetcode;

// Multiply Strings 
/* http://n00tc0d3r.blogspot.tw/search?q=Multiply+Strings
 * // O(M*N) time and O(1) space implementation inspiration
 */
public class MultiplyStrings {
	// O(M*N) time and O(1) space
	public String multiply(String num1, String num2) {
		if (num1.equals("0") || num2.equals("0")) {
			return "0";
		}
		int len1 = num1.length();
		int len2 = num2.length();
		int len3 = len1 + len2;
		int sum = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = len3 - 1; i > 0; i--) {
			for (int j = len2 - 1; j >= 0; j--) {
				if (i - j > len1) {
					break;
				}
				if (j < i) {
					sum += (num1.charAt(i - j - 1) - '0')
							* (num2.charAt(j) - '0');
				}
			}
			sb.append(sum % 10);
			sum /= 10;
		}
		if (sum > 0) {
			sb.append(sum);
		}
		return sb.reverse().toString();
	}

	// O(M*N) time and O(M+N) space
	public String multiply1(String num1, String num2) {
		if (num1.equals("0") || num2.equals("0")) {
			return "0";
		}
		int len1 = num1.length();
		int len2 = num2.length();
		int[] num3 = new int[len1 + len2];

		for (int i = len1 - 1; i >= 0; i--) {
			int carry = 0;
			for (int j = len2 - 1; j >= 0; j--) {
				int mulVal = (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
				int sum = num3[i + j + 1] + mulVal + carry;
				num3[i + j + 1] = sum % 10;
				carry = sum / 10;
			}
			num3[i] = carry;
		}

		StringBuffer sb = new StringBuffer();
		if (num3[0] != 0) {
			sb.append(num3[0]);
		}
		for (int i = 1; i < len1 + len2; i++) {
			sb.append(num3[i]);
		}
		return sb.toString();
	}
}
