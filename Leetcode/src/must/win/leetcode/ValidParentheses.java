package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

// Valid Parentheses 
/* 
 * http://n00tc0d3r.blogspot.com/search?q=Valid+Parentheses
 * most clean iterative, recursive(not understand yet)
 * 
 * http://www.ninechapter.com/solutions/valid-parentheses/
 * iterative, not clean
 * 
 */
public class ValidParentheses {

	// for iterative, not clean solution
	private boolean isValid(char c1, char c2) {
		return (c1 == '(' && c2 == ')') || (c1 == '{' && c2 == '}')
				|| (c1 == '[' && c2 == ']');
	}

	// iterative, most clean one
	public boolean isValid(String s) {
		Map<Character, Character> map = new HashMap<Character, Character>();
		map.put(')', '(');
		map.put(']', '[');
		map.put('}', '{');

		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);
			if (!map.containsKey(c)) {
				stack.push(c);
			} else if (stack.isEmpty() || stack.pop() != map.get(c)) {
				return false;
			}
		}

		return stack.isEmpty();
	}

	// iterative, not clean
	public boolean isValid1(String s) {
		if (s == null || s.length() == 0) {
			return false;
		}

		Stack<Character> stack = new Stack<Character>();
		int len = s.length();
		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);
			if ("({[".contains(String.valueOf(c))) {
				stack.push(c);
			} else {
				if (!stack.isEmpty() && isValid(stack.peek(), c)) {
					stack.pop();
				} else {
					return false;
				}
			}
		}

		return stack.isEmpty();
	}
}
