package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// Largest Number 
/*
 * http://blog.csdn.net/whuwangyi/article/details/42705317
 * explanation & implementation
 */
public class LargestNumber {

	// most clean way, like divide and conquer
	public String largestNumber(int[] num) {
		if (num == null || num.length == 0) {
			return "";
		}
		String[] strs = new String[num.length];
		for (int i = 0; i < strs.length; i++) {
			strs[i] = String.valueOf(num[i]);
		}

		Comparator<String> comparator = new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return (s2 + s1).compareTo(s1 + s2);
			}
		};

		Arrays.sort(strs, comparator);

		if (strs[0].equals("0")) {
			return "0";
		}
		StringBuilder sb = new StringBuilder();
		for (String str : strs) {
			sb.append(str);
		}
		return sb.toString();
	}

	// like divide and conquer
	public String largestNumber1(int[] num) {
		if (num == null || num.length == 0) {
			return "";
		}

		List<String> numStrList = new ArrayList<String>();
		for (int val : num) {
			numStrList.add(String.valueOf(val));
		}

		Comparator<String> comparator = new Comparator<String>() {
			@Override
			public int compare(String a, String b) {
				String s12 = a + b;
				String s21 = b + a;
				return (int) (Long.parseLong(s21) - Long.parseLong(s12));
			}
		};

		Collections.sort(numStrList, comparator);
		StringBuilder sb = new StringBuilder();
		for (String numStr : numStrList) {
			sb.append(numStr);
		}

		int i = 0;
		while (i < sb.length() && sb.charAt(i) == '0') {
			i++;
		}

		return sb.length() == i ? "0" : sb.substring(i);
	}
}
