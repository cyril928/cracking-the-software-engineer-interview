package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// Binary Tree Preorder Traversal 
/*
 * http://answer.ninechapter.com/solutions/binary-tree-preorder-traversal/
 * iterative, (add node to result when pop from stack), recursive, divide and conquer(haven't understand
 * yet?)
 */
public class BinaryTreePreorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// iterative, my own way using one stack, (add node to result when push to
	// stack)
	public List<Integer> preorderTraversal(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		Stack<TreeNode> stack = new Stack<TreeNode>();

		TreeNode cur = root;

		// go down to leftmost
		while (cur != null) {
			stack.push(cur);
			result.add(cur.val);
			cur = cur.left;
		}

		while (!stack.isEmpty()) {
			cur = stack.pop();
			// if the cur popup node has right node
			cur = cur.right;
			// go down to leftmost
			while (cur != null) {
				stack.push(cur);
				result.add(cur.val);
				cur = cur.left;
			}
		}

		return result;
	}

	// iterative, (add node to result when pop from stack)
	public List<Integer> preorderTraversal1(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		Stack<TreeNode> stack = new Stack<TreeNode>();
		if (root == null) {
			return result;
		}

		stack.push(root);

		while (!stack.isEmpty()) {
			TreeNode cur = stack.pop();
			// add node to result when pop from stack
			result.add(cur.val);
			// push right node at first
			if (cur.right != null) {
				stack.push(cur.right);
			}
			if (cur.left != null) {
				stack.push(cur.left);
			}
		}

		return result;
	}

	// recursive
	public List<Integer> preorderTraversal2(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		traversal(root, result);
		return result;
	}

	// for recursive way
	private void traversal(TreeNode root, List<Integer> result) {
		if (root == null) {
			return;
		}
		result.add(root.val);
		traversal(root.left, result);
		traversal(root.right, result);
	}
}
