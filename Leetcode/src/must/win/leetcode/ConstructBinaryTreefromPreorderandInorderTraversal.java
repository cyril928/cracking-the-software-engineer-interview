package must.win.leetcode;

import java.util.HashMap;

// Construct Binary Tree from Preorder and Inorder Traversal 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Construct+Binary+Tree+from+Inorder+and+Postorder+Traversal
 * // hashMap implementation, O(N) Time and O(N) Spaces
 * Pick up the first node in the preorder list and create a tree node with the value as root;
 * Look it up in the inorder list to determine the sizes of left and right subtrees;
 * Recursively build up the left and right subtrees.
 * 
 * http://mattcb.blogspot.tw/search?q=Construct+Binary+Tree+from+Preorder+and+Inorder+Traversal
 * // while-loop implementation, O(N2) Time and O(1) Spaces
 *  
 */
public class ConstructBinaryTreefromPreorderandInorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// while-loop version, O(N2) Time and O(1) Spaces
	public TreeNode buildTree(int[] preorder, int[] inorder) {
		int preLen = preorder.length;
		if (preLen == 0 || preLen != inorder.length) {
			return null;
		}
		return buildTreeHelper(preorder, 0, inorder, 0, preLen - 1);
	}

	// Hashmap version, O(N) Time and O(N) Spaces
	public TreeNode buildTree1(int[] preorder, int[] inorder) {
		int preLen = preorder.length;
		if (preLen == 0 || preLen != inorder.length) {
			return null;
		}
		HashMap<Integer, Integer> inorderMap = new HashMap<Integer, Integer>();
		for (int i = 0; i < preLen; i++) {
			inorderMap.put(inorder[i], i);
		}
		return buildTreeHelper1(preorder, 0, inorderMap, 0, preLen - 1);
	}

	// for while-loop version
	public TreeNode buildTreeHelper(int[] preorder, int preIndex,
			int[] inorder, int start, int end) {
		if (start > end) {
			return null;
		}
		int preVal = preorder[preIndex];
		TreeNode parent = new TreeNode(preVal);
		int index = start;
		while (index <= end && inorder[index] != preVal) {
			index++;
		}
		parent.left = buildTreeHelper(preorder, preIndex + 1, inorder, start,
				index - 1);
		parent.right = buildTreeHelper(preorder,
				preIndex + (index - start) + 1, inorder, index + 1, end);
		return parent;
	}

	// for Hashmap version
	public TreeNode buildTreeHelper1(int[] preorder, int preIndex,
			HashMap<Integer, Integer> inorderMap, int start, int end) {
		if (start > end) {
			return null;
		}
		int preVal = preorder[preIndex];
		TreeNode parent = new TreeNode(preVal);
		int index = inorderMap.get(preVal);
		parent.left = buildTreeHelper1(preorder, preIndex + 1, inorderMap,
				start, index - 1);
		parent.right = buildTreeHelper1(preorder, preIndex + (index - start)
				+ 1, inorderMap, index + 1, end);
		return parent;
	}
}
