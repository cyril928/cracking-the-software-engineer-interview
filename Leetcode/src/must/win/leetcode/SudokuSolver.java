package must.win.leetcode;

import java.util.HashSet;
import java.util.Set;

// Sudoku Solver 
/* 
 * use bit to maintain 1-9, fastest
 * 
 * http://answer.ninechapter.com/solutions/sudoku-solver/
 * most simple and clean solution, use HashSet
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Sudoku+Solver
 * maybe more efficient solution, but haven't understand yet.
 */
public class SudokuSolver {

	// use HashSet
	private boolean isValid(char[][] board, int row, int col) {
		Set<Character> container = new HashSet<Character>();
		// check row
		for (int i = 0; i < 9; i++) {
			if (board[row][i] == '.') {
				continue;
			}
			if (container.contains(board[row][i])) {
				return false;
			}
			container.add(board[row][i]);
		}

		// check col
		container = new HashSet<Character>();
		for (int i = 0; i < 9; i++) {
			if (board[i][col] == '.') {
				continue;
			}
			if (container.contains(board[i][col])) {
				return false;
			}
			container.add(board[i][col]);
		}

		// check box
		container = new HashSet<Character>();
		int x = col / 3 * 3;
		int y = row / 3 * 3;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (board[y + i][x + j] == '.') {
					continue;
				}
				if (container.contains(board[y + i][x + j])) {
					return false;
				}
				container.add(board[y + i][x + j]);
			}
		}
		return true;
	}

	// use HashSet
	private boolean solve(char[][] board) {
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board[i][j] == '.') {
					for (int k = 1; k <= 9; k++) {
						board[i][j] = (char) (k + '0');
						if (isValid(board, i, j) && solve(board)) {
							return true;
						}
					}
					board[i][j] = '.';
					return false;
				}
			}
		}
		return true;
	}

	// use bit to maintain 1-9, fastest
	public void solveSudoku(char[][] board) {
		if (board == null || board.length == 0) {
			return;
		}
		int[] rowN = new int[9];
		int[] colN = new int[9];
		int[] gridN = new int[9];

		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board[i][j] != '.') {
					int mask = 1 << (board[i][j] - '0');
					rowN[i] |= mask;
					colN[j] |= mask;
					gridN[i / 3 * 3 + j / 3] |= mask;
				}
			}
		}
		solveSudoku(board, 0, 0, rowN, colN, gridN);
	}

	// use bit to maintain 1-9, fastest
	private boolean solveSudoku(char[][] board, int r, int c, int[] rowN,
			int[] colN, int[] gridN) {
		if (r < 9) {

			while (board[r][c] != '.') {
				if (c == 8) {
					r = r + 1;
					c = 0;
				} else {
					c++;
				}
				if (r >= 9) {
					return true;
				}
			}
			int gridNIndex = r / 3 * 3 + c / 3;
			int avails = rowN[r] | colN[c] | gridN[gridNIndex];

			for (int k = 1; k <= 9; k++) {
				int mask = 1 << k;
				if ((mask & avails) == 0) {
					rowN[r] |= mask;
					colN[c] |= mask;
					gridN[gridNIndex] |= mask;
					board[r][c] = (char) (k + '0');
					if (solveSudoku(board, (c == 8) ? r + 1 : r, (c == 8) ? 0
							: c + 1, rowN, colN, gridN)) {
						return true;
					}
					rowN[r] ^= mask;
					colN[c] ^= mask;
					gridN[gridNIndex] ^= mask;
				}
			}
			board[r][c] = '.';
			return false;
		}
		return true;
	}

	// most simple and clean solution, use HashSet
	public void solveSudoku1(char[][] board) {
		solve(board);
	}

}
