package must.win.leetcode;

// Palindrome Partitioning II 
/* 
 * http://n00tc0d3r.blogspot.com/2013/05/palindrome-partitioning-ii.html?q=Palindrome
 * use DP twice:
 * One for valid palindrome substrings
 * and one for minimum number of cuts in previous substrings
 * 
 * http://yucoding.blogspot.tw/2013/08/leetcode-question-133-palindrome.html
 * iterative build palindrome table
 * f(i) = min [  f(j)+1,  j=0..i-1   and str[j:i] is palindrome
 *                   0,   if str[0,i] is palindrome ]
 */

public class PalindromePartitioningII {

	// iterative build palindrome table
	private void buildPalindromeTable(boolean[][] table, String s) {
		int len = table.length;
		for (int i = 0; i < len; i++) {
			table[i][i] = true;
		}
		for (int i = 1; i < len; i++) {
			int left = i - 1;
			int right = i + 1;
			while (right < len && left >= 0
					&& s.charAt(left) == s.charAt(right)) {
				table[left--][right++] = true;
			}
			left = i - 1;
			right = i;
			while (right < len && left >= 0
					&& s.charAt(left) == s.charAt(right)) {
				table[left--][right++] = true;
			}
		}
	}

	// use DP twice
	public int minCut(String s) {
		int len = s.length();

		// a table of palindrome substrings
		// T[i][j] == true iff s[i..j] is a palindrome
		boolean[][] palindrome = new boolean[len][len];

		// a table for minimum cuts of substrings
		// Cuts[i] = k means the minimum cuts needed for s[i..len-1] is k
		int[] Cuts = new int[len + 1];

		Cuts[len] = -1;
		for (int i = len - 1; i >= 0; --i) {
			Cuts[i] = len - i;
			for (int j = i; j < len; ++j) {
				if (s.charAt(i) == s.charAt(j)
						&& (j - i < 2 || palindrome[i + 1][j - 1])) {
					palindrome[i][j] = true;
					Cuts[i] = Math.min(Cuts[i], 1 + Cuts[j + 1]);
				}
			}
		}

		return Cuts[0];
	}

	public int minCut1(String s) {
		int len = s.length();
		boolean[][] table = new boolean[len][len];
		buildPalindromeTable(table, s);

		int[] cut = new int[len];

		for (int i = 0; i < len; i++) {
			if (table[0][i]) {
				cut[i] = 0;
				continue;
			}
			int res = Integer.MAX_VALUE;
			for (int j = 0; j < i; j++) {
				if (table[j + 1][i]) {
					res = Math.min(res, cut[j] + 1);
				}
			}
			cut[i] = res;
		}

		return cut[len - 1];
	}
}
