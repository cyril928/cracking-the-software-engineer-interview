package must.win.leetcode;

// Search in Rotated Sorted Array 
/* http://mattcb.blogspot.tw/2012/10/search-in-rotated-sorted-array.html?q=Search+in+Rotated+Sorted+Array
 * most easily understand, also 2 condition
 * 
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Search+in+Rotated+Sorted+Array
 * O(logN)
 * 3 condition (consider no rotated, rotated point on left, and rotated point on right)
 * 2 condition (rotated point not on right, rotated point on right)
 */
public class SearchinRotatedSortedArray {

	// most easily understand, also 2 condition
	public int search(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		int low = 0;
		int high = A.length - 1;

		while (low <= high) {
			int mid = (high - low) / 2 + low;
			if (target == A[mid]) {
				return mid;
			}

			// rotated point not on right side
			if (A[mid] < A[high]) {
				if (target > A[mid] && target <= A[high]) {
					low = mid + 1;
				} else {
					high = mid - 1;
				}
			}
			// rotated point on right side
			else {
				if (target < A[mid] && target >= A[low]) {
					high = mid - 1;
				} else {
					low = mid + 1;
				}
			}
		}
		return -1;
	}

	// 2 condition (rotated point not on right, rotated point on right)
	public int search1(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		int low = 0;
		int high = A.length - 1;

		while (low <= high) {
			int mid = (high - low) / 2 + low;
			if (target == A[mid]) {
				return mid;
			}
			// rotated point not on right side
			if ((A[high] > A[mid] && target > A[mid] && target <= A[high]) ||
			// rotated point on right side
					(A[high] < A[mid] && (target <= A[high] || target > A[mid]))) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		return -1;
	}

	// 3 condition (consider no rotated, rotated point on left, and rotated
	// point on right)
	public int search2(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		int low = 0;
		int high = A.length - 1;

		while (low <= high) {
			int mid = (high - low) / 2 + low;
			if (target == A[mid]) {
				return mid;
			}
			// no rotated point in array
			if ((target > A[mid] && A[high] > A[low])
					||
					// rotated point on left side
					(target > A[mid] && A[high] > A[mid] && target <= A[high])
					||
					// rotated point on right side
					(A[high] < A[mid] && (target <= A[high] || target > A[mid]))) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		return -1;
	}
}
