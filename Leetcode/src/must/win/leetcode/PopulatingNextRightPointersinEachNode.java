package must.win.leetcode;

// Populating Next Right Pointers in Each Node 
/* 
 * most clean, my own way
 * 
 * http://www.cnblogs.com/yuzhangcmu/p/4041341.html
 * full explanation & implementation
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Populating+Next+Right+Pointers+in+Each+Node 
 * optimal solution, implementation, discussion 
 * 
 * http://mattcb.blogspot.tw/2012/11/populating-next-right-pointers-in-each.html?q=Populating+Next+Right+Pointers+in+Each+Node
 * recursive solution, but not optimal
 */
public class PopulatingNextRightPointersinEachNode {
	public class TreeLinkNode {
		int val;
		TreeLinkNode left, right, next;

		TreeLinkNode(int x) {
			val = x;
		}
	}

	/*
	 * level-order traversal. If we traverse the tree level by level, simply set
	 * the next pointer to the next node in the same level and set it to null if
	 * reaching the end of a level. But that requires a queue to store the node
	 * in the current level, which could be O(n) space.
	 */
	/*
	 * The constant-extra-space also tells us no recursion since an n-level
	 * recursion usually takes at least O(n) space for stacks.
	 */

	// most clean, my own way
	public void connect(TreeLinkNode root) {
		if (root == null) {
			return;
		}

		TreeLinkNode firstNode = root;
		while (firstNode.left != null) {
			TreeLinkNode cur = firstNode;
			while (cur != null) {
				cur.left.next = cur.right;
				if (cur.next != null) {
					cur.right.next = cur.next.left;
				}
				cur = cur.next;
			}
			firstNode = firstNode.left;
		}
	}

	// one loop optimal version
	// like BFS
	public void connect1(TreeLinkNode root) {
		TreeLinkNode first = root, cur = root;
		while (cur != null) {
			if (cur.left != null && cur.right != null) {
				cur.left.next = cur.right;
			} else {
				break; // cause this is perfect binary tree, so break it for
						// performance concern
			}
			if (cur.next != null) {
				if (cur.right != null) {
					cur.right.next = cur.next.left;
				}
				cur = cur.next;
			} else {
				first = first.left;
				cur = first;
			}
		}
	}

	// recursive version, not optimal, take stack space
	// like DFS
	public void connect2(TreeLinkNode root) {
		if (root == null || root.left == null || root.right == null) {
			return;
		}
		root.left.next = root.right;
		connect2(root.left);
		if (root.next != null) {
			root.right.next = root.next.left;
		}
		connect2(root.right);
	}
}
