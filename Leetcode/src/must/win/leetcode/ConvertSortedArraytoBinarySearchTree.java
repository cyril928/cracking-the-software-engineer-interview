package must.win.leetcode;


// Convert Sorted Array to Binary Search Tree 
/*
 * http://mattcb.blogspot.tw/search?q=Convert+Sorted+Array+to+Binary+Search+Tree
 * // implementation 
 */
public class ConvertSortedArraytoBinarySearchTree {

	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	public TreeNode buildBST(int[] num, int left, int right) {
		if (left > right) {
			return null;
		}
		int mid = (right - left) / 2 + left;
		TreeNode parent = new TreeNode(num[mid]);
		parent.left = buildBST(num, left, mid - 1);
		parent.right = buildBST(num, mid + 1, right);
		return parent;
	}

	public TreeNode sortedArrayToBST(int[] num) {
		return buildBST(num, 0, num.length - 1);
	}
}
