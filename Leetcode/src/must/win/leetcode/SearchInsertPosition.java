package must.win.leetcode;

// Search Insert Position 
/* 
 * http://n00tc0d3r.blogspot.tw/2013/05/search-in-sorted-array.html?q=Search+for+a+Range
 * The only tricky part is if the target is not found, which value shall we return, low or high?
 * In binary search, if the middle is smaller than the target, we forward low to be mid+1. 
 * That tells us, when the loop terminates, either we found the target or the resulted 
 * low would be greater than the target.
 * 
 */
public class SearchInsertPosition {
	public int searchInsert(int[] A, int target) {
		if (A == null || A.length == 0) {
			return 0;
		}

		int low = 0, high = A.length - 1;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			if (A[mid] == target) {
				return mid;
			} else if (A[mid] > target) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
		}

		return low;
	}
}
