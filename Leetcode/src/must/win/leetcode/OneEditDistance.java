package must.win.leetcode;

// One Edit Distance
/*
 * Clean Code Hand Book
 * O(n) runtime, O(1) space, one pass
 */
public class OneEditDistance {
	// O(n) runtime, O(1) space, one pass
	public boolean isOneEditDistance(String s, String t) {
		if (s == null || t == null) {
			return false;
		}
		int sLen = s.length(), tLen = t.length();
		if (sLen < tLen) {
			return isOneEditDistance(t, s);
		}
		if (sLen - tLen > 1) {
			return false;
		}
		int i = 0;
		int shift = sLen - tLen;
		while (i < tLen && s.charAt(i) == t.charAt(i)) {
			i++;
		}
		if (i == tLen) {
			return shift > 0;
		}
		if (shift == 0) {
			i++;
		}
		while (i < tLen && s.charAt(i + shift) == t.charAt(i)) {
			i++;
		}
		return i == tLen;
	}
}
