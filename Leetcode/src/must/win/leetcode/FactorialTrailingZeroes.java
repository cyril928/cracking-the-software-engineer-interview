package must.win.leetcode;

// Factorial Trailing Zeroes 
/*
 * Your solution should be in logarithmic time complexity.
 * 
 * https://oj.leetcode.com/discuss/20691/my-explanation-of-the-log-n-solution
 * explanation
 */
public class FactorialTrailingZeroes {
	public int trailingZeroes(int n) {
		int res = 0;
		while (n >= 5) {
			n = n / 5;
			res += n;
		}
		return res;
	}
}
