package must.win.leetcode;

import java.util.LinkedList;
import java.util.Queue;

// Surrounded Regions 
/*
 * http://n00tc0d3r.blogspot.tw/2013/06/surrounded-regions.html (refined algorithm, don't mark four corner cells 
 * and boundary cells)
 * 
 * http://www.ninechapter.com/solutions/surrounded-regions/ other solution
 * 
 */
public class SurroundedRegions {
	int rowLen = 0;
	int colLen = 0;

	// bfs helper
	private void bfsTraverse(char[][] board, int row, int col) { // for bfs
																	// version
		if (board[row][col] != 'O') {
			return;
		}

		Queue<Integer[]> queue = new LinkedList<Integer[]>();
		queue.add(new Integer[] { row, col });
		board[row][col] = 'L';
		while (!queue.isEmpty()) {
			Integer[] coordinate = queue.poll();
			int x = coordinate[0];
			int y = coordinate[1];
			if (x - 1 > 0 && board[x - 1][y] == 'O') {
				board[x - 1][y] = 'L';
				queue.add(new Integer[] { x - 1, y });
			}
			if (x + 1 < rowLen && board[x + 1][y] == 'O') {
				board[x + 1][y] = 'L';
				queue.add(new Integer[] { x + 1, y });
			}
			if (y - 1 > 0 && board[x][y - 1] == 'O') {
				board[x][y - 1] = 'L';
				queue.add(new Integer[] { x, y - 1 });
			}
			if (y + 1 < colLen && board[x][y + 1] == 'O') {
				board[x][y + 1] = 'L';
				queue.add(new Integer[] { x, y + 1 });
			}
		}
	}

	// dfs helper
	private void dfsTraverse(char[][] board, int row, int col) { // for dfs
																	// version
		if (board[row][col] != 'O') {
			return;
		}
		board[row][col] = 'L';
		if (row - 1 > 0) {
			dfsTraverse(board, row - 1, col);
		}
		if (row + 1 < rowLen) {
			dfsTraverse(board, row + 1, col);
		}
		if (col - 1 > 0) {
			dfsTraverse(board, row, col - 1);
		}
		if (col + 1 < colLen) {
			dfsTraverse(board, row, col + 1);
		}
	}

	// bfs
	public void solve(char[][] board) { // bfs version
		if (board == null || board.length == 0 || board[0].length == 0) {
			return;
		}
		rowLen = board.length - 1;
		colLen = board[0].length - 1;

		if (colLen > 1) {
			for (int i = 1; i < rowLen; i++) {
				if (board[i][0] == 'O') {
					bfsTraverse(board, i, 1);
				}
				if (board[i][colLen] == 'O') {
					bfsTraverse(board, i, colLen - 1);
				}
			}
		}
		if (rowLen > 1) {
			for (int j = 1; j < colLen; j++) {
				if (board[0][j] == 'O') {
					bfsTraverse(board, 1, j);
				}
				if (board[rowLen][j] == 'O') {
					bfsTraverse(board, rowLen - 1, j);
				}
			}
		}

		for (int i = 1; i < rowLen; i++) {
			for (int j = 1; j < colLen; j++) {
				if (board[i][j] == 'O') {
					board[i][j] = 'X';
				} else if (board[i][j] == 'L') {
					board[i][j] = 'O';
				}
			}
		}
	}

	// dfs
	public void solve1(char[][] board) { // dfs version
		if (board == null || board.length == 0 || board[0].length == 0) {
			return;
		}
		rowLen = board.length - 1; // deduct first, cause it will reduce number
									// of calculation in further usage.
		colLen = board[0].length - 1;

		if (colLen > 1) { // only focus on board with column number > 2
			for (int i = 1; i < rowLen; i++) {
				if (board[i][0] == 'O') {
					dfsTraverse(board, i, 1);
				}
				if (board[i][colLen] == 'O') {
					dfsTraverse(board, i, colLen - 1);
				}
			}
		}

		if (rowLen > 1) { // only focus on board with row number > 2
			for (int j = 1; j < colLen; j++) {
				if (board[0][j] == 'O') {
					dfsTraverse(board, 1, j);
				}
				if (board[rowLen][j] == 'O') {
					dfsTraverse(board, rowLen - 1, j);
				}
			}
		}

		for (int i = 1; i < rowLen; i++) {
			for (int j = 1; j < colLen; j++) {
				if (board[i][j] == 'O') {
					board[i][j] = 'X';
				} else if (board[i][j] == 'L') {
					board[i][j] = 'O';
				}
			}
		}
	}
}
