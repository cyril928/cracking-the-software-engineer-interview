package must.win.leetcode;

// Maximum Subarray 
/*
 * Clean Code Hand Book
 * most clean iterative
 * 
 * http://www.geeksforgeeks.org/divide-and-conquer-maximum-sum-subarray/
 * divide and conquer implementation and explanation 
 * 
 * http://www.cppblog.com/flyinghearts/archive/2012/03/18/168269.html
 * most clean implementation, 
 * and loop MaximumSubarray problem & non-continuous MaximumSubarray problem solution
 * 
 * 
 */
public class MaximumSubarray {
	// for divide and conquer approach
	public int maxCrossSum(int[] A, int start, int mid, int end) {
		int sum = 0;
		int leftMax = Integer.MIN_VALUE;
		for (int i = mid; i >= start; i--) {
			sum += A[i];
			leftMax = Math.max(leftMax, sum);
		}

		sum = 0;
		int rightMax = Integer.MIN_VALUE;
		for (int i = mid + 1; i <= end; i++) {
			sum += A[i];
			rightMax = Math.max(rightMax, sum);
		}
		return leftMax + rightMax;
	}

	// most clean iterative
	public int maxSubArray(int[] A) {
		if (A == null || A.length == 0) {
			throw new IllegalArgumentException("xxx");
		}
		int maxEndingHere = A[0], maxSoFar = A[0];
		for (int i = 1; i < A.length; i++) {
			maxEndingHere = Math.max(maxEndingHere + A[i], A[i]);
			maxSoFar = Math.max(maxEndingHere, maxSoFar);
		}
		return maxSoFar;
	}

	// O(N) iterative implementation
	public int maxSubArray1(int[] A) {
		int max = Integer.MIN_VALUE;
		int len = A.length;
		int sum = 0;
		for (int i = 0; i < len; i++) {
			sum += A[i];
			max = Math.max(sum, max);
			if (sum < 0) {
				sum = 0;
			}
		}
		return max;
	}

	// O(nlogn) divide and conquer
	public int maxSubArray2(int[] A) {
		return maxSubArraySum(A, 0, A.length - 1);
	}

	// for divide and conquer approach
	public int maxSubArraySum(int[] A, int start, int end) {
		if (start == end) {
			return A[start];
		}
		int mid = (end - start) / 2 + start;
		return Math.max(
				Math.max(maxSubArraySum(A, start, mid),
						maxSubArraySum(A, mid + 1, end)),
				maxCrossSum(A, start, mid, end));
	}
}
