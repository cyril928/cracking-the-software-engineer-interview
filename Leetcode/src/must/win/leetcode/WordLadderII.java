package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

//Word Ladder II
/*
 * http://www.cnblogs.com/shawnhue/archive/2013/06/05/leetcode_126.html
 * explanation and implementation
 * 
 * http://yucoding.blogspot.com/2014/01/leetcode-question-word-ladder-ii.html
 * explanation
 */
//http://www.cnblogs.com/shawnhue/archive/2013/06/05/leetcode_126.html
public class WordLadderII {

	// better solution, don't store full trace path, just store word's next
	// word, and using two
	// set alternatively to do BFS
	private List<List<String>> res;
	private Map<String, HashSet<String>> trackMap;
	private HashSet<String> curLevelWord;
	private HashSet<String> nextLevelWord;
	private List<String> path;

	private void buildPath(String start, String end) {
		path.add(start);
		if (start.equals(end)) {
			List<String> newSol = new ArrayList<String>(path);
			res.add(newSol);
		} else {
			if (!trackMap.containsKey(start)) {
				return;
			}
			for (String nextWord : trackMap.get(start)) {
				buildPath(nextWord, end);
				path.remove(path.size() - 1);
			}
		}
	}

	private void findDict(String word, Set<String> dict) {
		int wordLen = word.length();
		for (int i = 0; i < wordLen; i++) {
			char[] curCharArray = word.toCharArray();
			for (char c = 'a'; c <= 'z'; c++) {
				curCharArray[i] = c;
				String curWord = new String(curCharArray);
				if (dict.contains(curWord)) {
					nextLevelWord.add(curWord);
					if (trackMap.containsKey(word)) {
						trackMap.get(word).add(curWord);
					} else {
						HashSet<String> newSet = new HashSet<String>();
						newSet.add(curWord);
						trackMap.put(word, newSet);
					}
				}
			}
		}
	}

	public List<List<String>> findLadders(String start, String end,
			Set<String> dict) {
		res = new ArrayList<List<String>>();
		if (start == null || end == null || dict == null || start.length() == 0
				|| start.length() != end.length()) {
			return res;
		}
		if (start.equals(end)) {
			List<String> sol = new ArrayList<String>();
			sol.add(start);
			sol.add(end);
			res.add(sol);
			return res;
		}

		trackMap = new HashMap<String, HashSet<String>>();
		curLevelWord = new HashSet<String>();
		nextLevelWord = new HashSet<String>();
		path = new ArrayList<String>();
		dict.add(start);
		dict.add(end);

		curLevelWord.add(start);
		while (true) {
			for (String word : curLevelWord) {
				dict.remove(word);
			}
			for (String word : curLevelWord) {
				findDict(word, dict);
			}
			if (nextLevelWord.isEmpty()) {
				break;
			}
			if (nextLevelWord.contains(end)) {
				buildPath(start, end);
				break;
			}
			curLevelWord = nextLevelWord;
			nextLevelWord = new HashSet<String>();
		}

		return res;
	}

	// TLE version solution......
	// BFS, similar to Word Ladder I
	public List<List<String>> findLadders2(String start, String end,
			Set<String> dict) {
		List<List<String>> res = new ArrayList<List<String>>();
		if (start == null || end == null || dict == null || start.length() == 0
				|| start.length() != end.length()) {
			return res;
		}
		if (start.equals(end)) {
			List<String> sol = new ArrayList<String>();
			sol.add(start);
			sol.add(end);
			res.add(sol);
			return res;
		}

		int wordLen = start.length();
		int minDistance = Integer.MAX_VALUE;

		Queue<String> wordQueue = new LinkedList<String>();
		Queue<Integer> distanceQueue = new LinkedList<Integer>();
		Queue<HashSet<String>> recordQueue = new LinkedList<HashSet<String>>();

		wordQueue.add(start);
		distanceQueue.add(1);
		HashSet<String> sol = new HashSet<String>();
		sol.add(start);
		recordQueue.add(sol);

		while (!wordQueue.isEmpty()) {
			String curWord = wordQueue.poll();
			int curDistance = distanceQueue.poll();
			HashSet<String> curSol = recordQueue.poll();

			if (curDistance == minDistance) {
				break;
			}

			for (int i = 0; i < wordLen; i++) {
				char[] curCharArray = curWord.toCharArray();
				for (char c = 'a'; c <= 'z'; c++) {
					curCharArray[i] = c;
					String newWord = new String(curCharArray);
					if (end.equals(newWord)) {
						minDistance = curDistance + 1;
						HashSet<String> newSol = new HashSet<String>(curSol);
						newSol.add(newWord);
						res.add(new ArrayList<String>(newSol));
					} else if (dict.contains(newWord)
							&& !curSol.contains(newWord)) {
						if (curDistance >= minDistance - 1) {
							continue;
						}
						wordQueue.add(newWord);
						distanceQueue.add(curDistance + 1);
						HashSet<String> newSol = new HashSet<String>(curSol);
						newSol.add(newWord);
						recordQueue.add(newSol);
					}
				}
			}
		}

		return res;
	}
}
