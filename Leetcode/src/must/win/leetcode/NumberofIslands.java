package must.win.leetcode;

import java.util.LinkedList;
import java.util.Queue;

// Number of Islands 
/*
 * 
 */
public class NumberofIslands {
	public int numIslands(char[][] grid) {
		int num = 0;
		if (grid == null || grid.length == 0) {
			return num;
		}

		int row = grid.length, col = grid[0].length;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				if (grid[i][j] == '1') {
					Queue<int[]> queue = new LinkedList<int[]>();
					queue.add(new int[] { i, j });
					grid[i][j] = '2';

					while (!queue.isEmpty()) {
						traverse(queue, grid);
					}
					num++;
				}
			}
		}
		return num;
	}

	private void traverse(Queue<int[]> queue, char[][] grid) {
		int[][] direction = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };
		int[] pos = queue.poll();
		for (int i = 0; i < 4; i++) {
			int x = pos[0] + direction[i][0];
			int y = pos[1] + direction[i][1];
			if (x >= 0 && x < grid.length && y >= 0 && y < grid[0].length
					&& grid[x][y] == '1') {
				grid[x][y] = '2';
				queue.add(new int[] { x, y });
			}
		}
	}
}
