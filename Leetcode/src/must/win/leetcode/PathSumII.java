package must.win.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

// Path Sum II 
/*
 * 1. use one current path list to store new trace.
 * 2. always use new current path list to store new trace.
 * 
 */
public class PathSumII {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	public List<List<Integer>> pathSum1(TreeNode root, int sum) {
		List<List<Integer>> result = new LinkedList<List<Integer>>();
		if (root == null) {
			return result;
		}
		LinkedList<Integer> curPath = new LinkedList<Integer>();
		pathSumHelper(root, sum, curPath, result);
		return result;
	}

	// use one current path list to store new trace.
	public void pathSumHelper(TreeNode root, int sum,
			LinkedList<Integer> curPath, List<List<Integer>> result) {
		if (root == null) {
			return;
		}
		curPath.add(root.val);
		if (root.left == null && root.right == null) {
			if (sum - root.val == 0) {
				result.add(new ArrayList<Integer>(curPath));
			}
			curPath.removeLast();
			return;
		}
		pathSumHelper(root.left, sum - root.val, curPath, result);
		pathSumHelper(root.right, sum - root.val, curPath, result);
		curPath.removeLast();
	}

	// always use new current path list to store new trace.
	public void pathSumHelper1(TreeNode root, int sum,
			LinkedList<Integer> curPath, List<List<Integer>> result) {
		if (root == null) {
			return;
		}
		curPath.add(root.val);
		if (root.left == null && root.right == null) {
			if (sum - root.val == 0) {
				result.add(curPath);
			}
			return;
		}
		pathSumHelper1(root.left, sum - root.val, new LinkedList<Integer>(
				curPath), result);
		pathSumHelper1(root.right, sum - root.val, new LinkedList<Integer>(
				curPath), result);
	}
}
