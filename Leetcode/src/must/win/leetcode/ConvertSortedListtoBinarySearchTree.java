package must.win.leetcode;

// Convert Sorted List to Binary Search Tree 
/*
 * http://www.ninechapter.com/solutions/convert-sorted-list-to-binary-search-tree/
 * more efficient way, don't have to move to middle for every segment
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Convert+Sorted+Array+to+Binary+Search+Tree
 * // implementation and explanation, move to middle solution
 */
public class ConvertSortedListtoBinarySearchTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	private ListNode cur;

	// for move to middle solution
	public TreeNode buildBST1(ListNode head, int left, int right) {
		if (left > right) {
			return null;
		}
		int mid = (right - left) / 2 + left;
		TreeNode leftNode = buildBST1(head, left, mid - 1);
		TreeNode parentNode = new TreeNode(head.val);
		parentNode.left = leftNode;
		if (head.next != null) {
			head.val = head.next.val;
			head.next = head.next.next;
		}
		parentNode.right = buildBST1(head, mid + 1, right);
		return parentNode;
	}

	// keep moving a current pointer, don't have to move to middle solution,
	// size function
	private int getLength(ListNode head) {
		int size = 0;
		while (head != null) {
			head = head.next;
			size++;
		}
		return size;
	}

	// keep moving a current pointer, don't have to move to middle solution
	public TreeNode sortedListToBST(ListNode head) {
		cur = head;
		return sortedListToBSTHelper(getLength(head));
	}

	// move to middle solution
	public TreeNode sortedListToBST1(ListNode head) {
		if (head == null) {
			return null;
		}
		ListNode cur = head;
		int len = 0;
		while (cur != null) {
			cur = cur.next;
			len++;
		}
		return buildBST1(head, 0, len - 1);
	}

	// for keep moving a current pointer, don't have to move to middle solution
	private TreeNode sortedListToBSTHelper(int size) {
		if (size == 0) {
			return null;
		}
		TreeNode leftNode = sortedListToBSTHelper(size >> 1);
		TreeNode parent = new TreeNode(cur.val);
		cur = cur.next;
		TreeNode rightNode = sortedListToBSTHelper(size - (size >> 1) - 1);

		parent.left = leftNode;
		parent.right = rightNode;

		return parent;
	}
}
