package must.win.leetcode;

import java.util.List;

// Triangle
/*
 * bottom-up -> only O(n) extra space, where n is the total number of rows in the
 * triangle.
 * 
 * http://www.ninechapter.com/solutions/triangle/
 * bottom-up -> O(N2) extra space,
 * 
 * http://mattcb.blogspot.tw/search?q=Triangle 
 * tamper the original data structure implementation
 * 
 * http://yucoding.blogspot.tw/2013/04/leetcode-question-112-triangle.html 
 * see below comment
 * 
 * More solution
 * http://www.ninechapter.com/solutions/triangle/
 * 
 */
public class Triangle {

	// only O(n) extra space, where n is the total number of rows in the
	// triangle.
	public int minimumTotal(List<List<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0) {
			return 0;
		}

		int size = triangle.size();
		int[] dp = new int[size];
		for (int i = 0; i < size; i++) {
			dp[i] = triangle.get(size - 1).get(i);
		}

		for (int i = size - 2; i >= 0; i--) {
			List<Integer> list = triangle.get(i);
			for (int j = 0; j <= i; j++) {
				dp[j] = list.get(j) + Math.min(dp[j], dp[j + 1]);
			}
		}

		return dp[0];
	}

	// bottom-up, O(N2) extra space
	public int minimumTotal1(List<List<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0) {
			return 0;
		}

		int n = triangle.size();
		int[][] sum = new int[n][n];

		for (int i = 0; i < n; i++) {
			sum[n - 1][i] = triangle.get(n - 1).get(i);
		}

		for (int i = n - 2; i >= 0; i--) {
			for (int j = 0; j <= i; j++) {
				sum[i][j] = Math.min(sum[i + 1][j], sum[i + 1][j + 1])
						+ triangle.get(i).get(j);
			}
		}

		return sum[0][0];
	}

	// tamper the original data structure, O(1) space
	public int minimumTotal2(List<List<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0) {
			return 0;
		}
		// DP dynamic programming
		int levelCount = triangle.size();
		// from bottom to top can reduce operation and simplify problem
		for (int i = levelCount - 2; i >= 0; i--) {
			List<Integer> rowList = triangle.get(i);
			List<Integer> nextRowList = triangle.get(i + 1);
			int rowSize = rowList.size();
			for (int j = 0; j < rowSize; j++) {
				rowList.set(
						j,
						rowList.get(j)
								+ Math.min(nextRowList.get(j),
										nextRowList.get(j + 1)));
			}
		}

		return triangle.get(0).get(0);
	}
}
