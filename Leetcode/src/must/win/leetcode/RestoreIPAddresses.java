package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Restore IP Addresses 
/*
 * my own way, StringBuilder, clean one
 * 
 * http://n00tc0d3r.blogspot.tw/2013/05/restore-ip-addresses.html?q=Multiply+Strings
 * StringBuidler implementation and explanation
 * 
 * 
 * http://www.ninechapter.com/solutions/restore-ip-addresses/
 * List<String> implementation
 */
public class RestoreIPAddresses {
	// StringBuidler implementation helper
	private void getAllIPs(String s, StringBuilder path, List<String> resList,
			int depth) {
		if (depth == 3) {
			if (isIPValid(s)) {
				resList.add(path.toString() + s);
			}
			return;
		}
		int sLen = s.length();
		int pLen = path.length();
		for (int i = 1; i <= 3 && i <= sLen; i++) {
			String num = s.substring(0, i);
			if (isIPValid(num)) {
				getAllIPs(s.substring(i), path.append(num).append('.'),
						resList, depth + 1);
				path.delete(pLen, pLen + i + 1);
			}
		}
	}

	// my own way, StringBuilder, clean one
	private void helper(List<String> res, StringBuilder path, String s,
			int index) {
		if (index == 4) {
			if (s.isEmpty()) {
				res.add(path.substring(0, path.length() - 1));
			}
			return;
		}
		int len = s.length();
		int pLen = path.length();
		for (int i = 1; i <= 3 && i <= len; i++) {
			if ((len - i >= 3 - index) && (len - i <= (3 - index) * 3)) {
				String str = s.substring(0, i);
				if (isValidIP(str)) {
					path.append(str);
					path.append(".");
					helper(res, path, s.substring(i), index + 1);
					path.delete(pLen, pLen + i + 1);
				}
			}
		}
	}

	// StringBuidler implementation helper
	private boolean isIPValid(String s) {
		int len = s.length();
		if (len == 1 || (!s.isEmpty() && len < 4 && !s.startsWith("0"))) {
			int num = Integer.parseInt(s);
			if (num >= 0 && num <= 255) {
				return true;
			}
		}
		return false;
	}

	// my own way, StringBuilder, clean one
	private boolean isValidIP(String s) {
		if (s.startsWith("0")) {
			return s.length() == 1;
		} else {
			int val = Integer.parseInt(s);
			return val >= 0 && val <= 255;
		}
	}

	// my own way, StringBuilder, clean one
	public List<String> restoreIpAddresses(String s) {
		List<String> res = new ArrayList<String>();
		if (s == null || s.isEmpty()) {
			return res;
		}
		helper(res, new StringBuilder(), s, 0);
		return res;
	}

	// StringBuidler implementation
	public List<String> restoreIpAddresses1(String s) {
		if (s == null) {
			return null;
		}
		List<String> resList = new ArrayList<String>();
		getAllIPs(s, new StringBuilder(), resList, 0);
		return resList;
	}

}
