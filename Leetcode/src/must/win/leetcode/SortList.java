package must.win.leetcode;

// Sort List
/* http://www.ninechapter.com/solutions/sort-list/
 * merge sort
 */
public class SortList {
	private ListNode findMiddle(ListNode head) {
		if (head == null) {
			return head;
		}
		ListNode fastNode = head, slowNode = head;
		while (fastNode.next != null && fastNode.next.next != null) {
			fastNode = fastNode.next.next;
			slowNode = slowNode.next;
		}
		return slowNode;
	}

	private ListNode merge(ListNode head1, ListNode head2) {
		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;
		while (head1 != null && head2 != null) {
			if (head1.val <= head2.val) {
				cur.next = head1;
				head1 = head1.next;
			} else {
				cur.next = head2;
				head2 = head2.next;
			}
			cur = cur.next;
		}
		if (head1 != null) {
			cur.next = head1;
		} else {
			cur.next = head2;
		}

		return dummy.next;
	}

	public ListNode sortList(ListNode head) {
		// base case
		if (head == null || head.next == null) {
			return head;
		} else { // merge sort recursive logic
			ListNode middle = findMiddle(head);
			ListNode right = sortList(middle.next);
			// need to close first linked list
			middle.next = null;
			ListNode left = sortList(head);
			return merge(left, right);
		}
	}
}
