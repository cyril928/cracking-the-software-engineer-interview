package must.win.leetcode;

// Valid Palindrome 
/* 
 * http://mattcb.blogspot.tw/search?q=Valid+Palindrome  O(n) time, O(1) space implementation
 */
public class ValidPalindrome {
	private boolean isLetter(Character c) {
		return (c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A')
				|| (c <= '9' && c >= '0');
	}

	public boolean isPalindrome(String s) {
		if (s == null) {
			return false;
		}
		if (s.length() == 0) {
			return true;
		}
		int start = 0, end = s.length() - 1;
		while (start < end) {
			if (!isLetter(s.charAt(start))) {
				start++;
				continue;
			}
			if (!isLetter(s.charAt(end))) {
				end--;
				continue;
			}
			if (Character.toLowerCase(s.charAt(start++)) != Character
					.toLowerCase(s.charAt(end--))) {
				return false;
			}
		}
		return true;
	}
}
