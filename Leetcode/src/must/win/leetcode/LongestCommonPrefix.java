package must.win.leetcode;

// Longest Common Prefix 
/*
 *  http://n00tc0d3r.blogspot.tw/search?q=Longest+Common+Prefix
 *  Method 1, start from the first char, compare it with all string, and then the second char
 *  
 *  http://answer.ninechapter.com/solutions/longest-common-prefix/
 * 	Method 2, start from the first one, compare prefix with next string, until end
 */
public class LongestCommonPrefix {
	// Method 1, start from the first char, compare it with all string, and then
	// the second char
	public String longestCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0) {
			return "";
		}

		int len = strs.length;
		for (int i = 0; i < strs[0].length(); i++) {
			for (int j = 1; j < len; j++) {
				if (i >= strs[j].length()
						|| strs[0].charAt(i) != strs[j].charAt(i)) {
					return strs[0].substring(0, i);
				}
			}
		}
		return strs[0];
	}

	// Method 2, start from the first one, compare prefix with next string,
	// until end
	public String longestCommonPrefix1(String[] strs) {
		if (strs == null || strs.length == 0) {
			return "";
		}

		int len = strs.length;
		int index = strs[0].length();
		for (int i = 1; i < len; i++) {
			int curIndex = 0;
			while (curIndex < index && curIndex < strs[i].length()
					&& strs[0].charAt(curIndex) == strs[i].charAt(curIndex)) {
				curIndex++;
			}
			index = curIndex;
		}

		return strs[0].substring(0, index);
	}
}
