package must.win.leetcode;

// Longest Substring with At Most Two Distinct Characters
/*
 * Clean Code Hand Book
 * O(N) O(N2) O(N3)
 * 
 * generalized to the case where T contains at most k distinct characters.
 * O(N)
 */
public class LongestSubstringwithAtMostTwoDistinctCharacters {
	// 0(N)
	public int lengthOfLongestSubstringTwoDistinct(String s) {
		int maxLen = 0;
		int i = 0, j = -1;
		for (int k = 1; k < s.length(); k++) {
			if (s.charAt(k) == s.charAt(k - 1)) {
				continue;
			}
			if (j >= 0 && s.charAt(j) != s.charAt(k)) {
				maxLen = Math.max(k - i, maxLen);
				i = j + 1;
			}
			j = k - 1;
		}
		return Math.max(s.length() - i, maxLen);
	}

	// generalized to the case where T contains at most k distinct characters.
	public int lengthOfLongestSubstringTwoDistinct(String s, int k) {
		int[] count = new int[256];
		int maxLen = 0;
		int numDistinct = 0;
		int i = 0;
		for (int j = 0; j < s.length(); j++) {
			if (count[s.charAt(j)] == 0) {
				numDistinct++;
			}
			count[s.charAt(j)]++;
			while (numDistinct > k) {
				count[s.charAt(i)]--;
				if (count[s.charAt(i++)] == 0) {
					numDistinct--;
				}
			}
			maxLen = Math.max(j - i + 1, maxLen);
		}
		return maxLen;
	}
}
