package must.win.leetcode;

// Read N Characters Given Read4 II - Call multiple times
/*
 * Clean Code Hand Book
 */
public class ReadNCharactersGivenRead4IICallmultipletimes extends Reader4 {

	private char[] buffer = new char[4];
	int offset = 0;

	public int read(char[] buf, int n) {
		int readBytes = 0;
		boolean eof = false;
		while (!eof && readBytes < n) {
			int sz = 4 - offset;
			if (offset == 0) {
				sz = read4(buffer);
				eof = (sz < 4);
			}
			int bytes = Math.min(n - readBytes, sz);
			System.arraycopy(buffer, offset, buf, readBytes, bytes);
			readBytes += bytes;
			offset = (offset + bytes) % 4;
		}
		return readBytes;
	}
}
