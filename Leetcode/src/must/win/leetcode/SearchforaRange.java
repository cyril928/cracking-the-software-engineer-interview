package must.win.leetcode;

// Search for a Range 
/*
 * http://answer.ninechapter.com/solutions/search-for-a-range/
 * find start and end in two loop, also O(logN)
 * 
 * http://n00tc0d3r.blogspot.tw/2013/05/search-in-sorted-array.html?q=Search+for+a+Range
 * find start and end in one loop, but too complicated, easily make mistakes.
 */
public class SearchforaRange {

	// find start and end in two loop, also O(logN)
	public int[] searchRange(int[] A, int target) {
		int[] res = { -1, -1 };
		if (A == null || A.length == 0) {
			return res;
		}

		// search for left bound
		int low = 0, high = A.length - 1;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			// find the target number
			if (A[mid] == target) {
				// find the start of target number
				if (mid == 0 || A[mid - 1] < target) {
					res[0] = mid;
					break;
				} else {
					high = mid - 1;
				}
			} else if (A[mid] > target) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
		}

		if (res[0] == -1) {
			return res;
		}

		// search for right bound
		low = res[0];
		high = A.length - 1;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			// find the target number
			if (A[mid] == target) {
				// find the end of target number
				if (mid == A.length - 1 || A[mid + 1] > target) {
					res[1] = mid;
					break;
				} else {
					low = mid + 1;
				}
			} else if (A[mid] > target) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}

		}
		return res;

	}

	// find start and end in one loop, but too complicated, easily make
	// mistakes.
	public int[] searchRange1(int[] A, int target) {
		int[] res = { -1, -1 };
		if (A == null || A.length == 0) {
			return res;
		}

		int low = 0, high = A.length - 1;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			// find the target number
			if (A[mid] == target) {
				// find the start of target number
				if (mid == 0 || A[mid - 1] < target) {
					res[0] = mid;
					// reset
					low = mid;
					high = A.length - 1;
				}
				// find the end of target number
				if (mid == A.length - 1 || A[mid + 1] > target) {
					res[1] = mid;
					// reset
					low = 0;
					high = mid;
				}

				if (res[0] > -1 && res[1] > -1) {
					return res;
				} else {
					// continue searching for beginning in (.., mid)
					if (res[0] == -1) {
						high = mid - 1;
					}
					// continue searching for end in (mid, ..)
					else {
						low = mid + 1;
					}
				}
			} else if (A[mid] > target) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
		}

		return res;

	}
}
