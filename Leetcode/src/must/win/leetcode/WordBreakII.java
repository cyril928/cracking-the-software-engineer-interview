package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Word Break II 
/*
 * http://www.binglu.me/leetcode-word-break-and-word-break-ii/
 * store next string's index, minimize memory usage
 * 
 * http://www.ninechapter.com/solutions/word-break-ii/
 * recursive store every substring
 * 
 * iterative store every substring, TLE, my own way
 * 
 */
public class WordBreakII {

	// store next string's index, minimize memory usage helper function
	private void buildSolution(List<String> res, String s,
			List<List<Integer>> record, StringBuilder sb, int cur) {
		if (cur == s.length()) {
			res.add(sb.substring(0, sb.length() - 1));
			return;
		}
		for (int index : record.get(cur)) {
			String str = s.substring(cur, index);
			sb.append(str).append(" ");
			buildSolution(res, s, record, sb, index);
			sb.delete(sb.length() - str.length() - 1, sb.length());
		}
	}

	// store next string's index, minimize memory usage
	public List<String> wordBreak(String s, Set<String> dict) {
		if (s == null || dict == null) {
			return null;
		}
		int len = s.length();
		List<List<Integer>> record = new ArrayList<List<Integer>>();
		for (int i = 0; i < len; i++) {
			record.add(new ArrayList<Integer>());
		}
		for (int i = len - 1; i >= 0; i--) {
			List<Integer> indexList = record.get(i);
			for (int j = i + 1; j <= len; j++) {
				String str = s.substring(i, j);
				if (dict.contains(str)
						&& (j == len || !record.get(j).isEmpty())) {
					indexList.add(j);
				}
			}
		}
		List<String> res = new ArrayList<String>();
		buildSolution(res, s, record, new StringBuilder(), 0);
		return res;
	}

	// recursive store very substring
	public List<String> wordBreak1(String s, Set<String> dict) {
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		return wordBreakHelper(s, dict, map);
	}

	// iterative store every substring, TLE, my own way
	public List<String> wordBreak2(String s, Set<String> dict) {
		int len = s.length();
		if (len < 1) {
			return null;
		}
		List<String> res = new ArrayList<String>();
		// Cannot create a generic array of ArrayList<String>
		List<HashSet<String>> combination = new ArrayList<HashSet<String>>();
		for (int i = 0; i < len; i++) {
			combination.add(new HashSet<String>());
		}
		boolean[] match = new boolean[len];
		Arrays.fill(match, false);

		for (int i = 0; i < len; i++) {
			if (dict.contains(s.substring(0, i + 1))) {
				combination.get(i).add(s.substring(0, i + 1));
				match[i] = true;
			}
			for (int j = 0; j < i; j++) {
				String postfixStr = s.substring(j + 1, i + 1);
				if (match[j] && dict.contains(postfixStr)) {
					for (String subres : combination.get(j)) {
						StringBuffer sb = new StringBuffer();
						sb.append(subres);
						sb.append(" ");
						sb.append(postfixStr);
						combination.get(i).add(sb.toString());
					}
					match[i] = true;
				}
			}
		}
		for (String str : combination.get(len - 1)) {
			res.add(str);
		}
		return res;
	}

	// recursive store very substring helper
	private List<String> wordBreakHelper(String s, Set<String> dict,
			Map<String, List<String>> map) {
		if (map.containsKey(s)) {
			return map.get(s);
		}
		List<String> res = new ArrayList<String>();
		if (s.isEmpty()) {
			return res;
		}
		int len = s.length();
		for (int i = 1; i <= len; i++) {
			String prefix = s.substring(0, i);
			if (dict.contains(prefix)) {
				if (i == len) {
					res.add(prefix);
				} else {
					List<String> temp = wordBreakHelper(s.substring(i), dict,
							map);
					for (String str : temp) {
						str = prefix + " " + str;
						res.add(str);
					}
				}
			}
		}
		map.put(s, res);
		return res;
	}
}
