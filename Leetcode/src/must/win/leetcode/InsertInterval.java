package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Insert Interval 
/*
 * http://answer.ninechapter.com/solutions/insert-interval/
 * most clean and intuitive solution
 * This algorithm runs in time O(n) since we touch each interval O(1) times.
 * 
 * http://n00tc0d3r.blogspot.tw/2013/03/insert-interval.html?q=Insert+Interval
 * binary search 
 * worst case running time of this algorithm could be O(n^2) if we remove intervals O(n) times. 
 * But the space is O(1) now.
 * 
 */
public class InsertInterval {
	class Interval {
		int start = 0, end = 0;

		Interval() {
			this.start = 0;
			this.end = 0;
		}

		Interval(int s, int e) {
			this.start = s;
			this.end = e;
		}
	}

	// binary search helper function
	private int findEndPos(List<Interval> src, int target) {
		int start = 0;
		int end = src.size() - 1;
		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (src.get(mid).start < target) {
				start = mid + 1;
			} else if (src.get(mid).start == target) {
				return mid + 1;
			} else {
				end = mid - 1;
			}
		}
		return start;
	}

	// binary search helper function
	private int findStartPos(List<Interval> src, int target) {
		int start = 0;
		int end = src.size() - 1;
		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (src.get(mid).end < target) {
				start = mid + 1;
			} else if (src.get(mid).end == target) {
				return mid;
			} else {
				end = mid - 1;
			}
		}
		return start;
	}

	// most clean and intuitive solution
	// This algorithm runs in time O(n) since we touch each interval O(1) times.
	// But we use extra O(n) space to save the new result array.
	public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
		if (intervals == null || newInterval == null) {
			return intervals;
		}

		List<Interval> result = new ArrayList<Interval>();
		int size = intervals.size();
		int insertPos = 0;
		for (int i = 0; i < size; i++) {
			Interval interval = intervals.get(i);
			if (newInterval.start > interval.end) {
				result.add(interval);
				insertPos++;
			} else if (newInterval.end < interval.start) {
				result.add(interval);
			} else {
				newInterval.start = Math.min(newInterval.start, interval.start);
				newInterval.end = Math.max(newInterval.end, interval.end);
			}
		}
		result.add(insertPos, newInterval);
		return result;
	}

	// binary search, worst case running time of this algorithm could be O(n^2)
	// if we remove intervals O(n) times. But the space is O(1) now.
	public List<Interval> insert1(List<Interval> intervals, Interval newInterval) {
		if (intervals == null || newInterval == null) {
			return intervals;
		}

		int prior = findStartPos(intervals, newInterval.start);
		if (prior == intervals.size()) {
			intervals.add(newInterval);
		} else if (intervals.get(prior).start > newInterval.end) {
			intervals.add(prior, newInterval);
		} else {
			int posterior = findEndPos(intervals, newInterval.end);
			newInterval.start = Math.min(newInterval.start,
					intervals.get(prior).start);
			newInterval.end = Math.max(newInterval.end,
					intervals.get(posterior - 1).end);
			intervals.subList(prior, posterior).clear();
			intervals.add(prior, newInterval);
		}
		return intervals;
	}
}
