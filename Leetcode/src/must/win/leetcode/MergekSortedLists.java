package must.win.leetcode;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

// Merge k Sorted Lists 
/* 
 * http://n00tc0d3r.blogspot.tw/2013/04/merge-k-sorted-lists.html?q=Merge+Two+Sorted+Lists
 * 1. HeapSort, Recall that in Merge Sorted Lists/Arrays I we compare the two head nodes of the 
 * two lists to find a smaller one. Now we have k lists and thus we need to compare k head nodes
 * to find the smallest one. Since we need sort numbers dynamically, HeapSort becomes a perfect fit.
 * 
 * 2. Use subroutine merge two sorted lists to solve this problem
 * 
 * 
 */
public class MergekSortedLists {

	// HeapSort
	public ListNode mergeKLists(List<ListNode> lists) {
		if (lists == null || lists.isEmpty()) {
			return null;
		}

		Comparator<ListNode> comparator = new Comparator<ListNode>() {
			@Override
			public int compare(ListNode n1, ListNode n2) {
				/*
				 * if (n1.val < n2.val) return -1; if (n1.val > n2.val) return
				 * 1; return 0;
				 */
				return n1.val - n2.val;
			}
		};

		Queue<ListNode> heap = new PriorityQueue<ListNode>(lists.size(),
				comparator);
		for (ListNode n : lists) {
			if (n != null) {
				heap.add(n);
			}
		}

		/*
		 * ListNode head = null, cur = null; while(!heap.isEmpty()){ if(head ==
		 * null) { head = heap.poll(); cur = head; } else { cur.next =
		 * heap.poll(); cur = cur.next; } if(cur.next != null) {
		 * heap.add(cur.next); } } return head;
		 */

		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;
		while (!heap.isEmpty()) {
			ListNode n = heap.poll();
			cur.next = n;
			cur = n;
			if (cur.next != null) {
				heap.add(cur.next);
			}
		}

		return dummy.next;
	}

	// Use subroutine merge two sorted lists to solve this problem
	public ListNode mergeKLists1(List<ListNode> lists) {
		if (lists == null || lists.size() == 0) {
			return null;
		}
		int last = lists.size() - 1;
		while (last > 0) {
			int cur = 0;
			while (cur < last) {
				lists.set(cur,
						mergeTwoLists(lists.get(cur++), lists.get(last--)));
			}
		}
		return lists.get(0);
	}

	// subroutine for second way.
	private ListNode mergeTwoLists(ListNode a, ListNode b) {
		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;
		while (a != null && b != null) {
			if (a.val < b.val) {
				cur.next = a;
				a = a.next;
			} else {
				cur.next = b;
				b = b.next;
			}
			cur = cur.next;
		}

		cur.next = (a != null) ? a : b;
		return dummy.next;
	}
}
