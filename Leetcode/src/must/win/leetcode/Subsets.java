package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Subsets 
/*
 * http://www.ninechapter.com/solutions/subsets/
 * DFS, top-down result building
 * 
 * http://n00tc0d3r.blogspot.com/2013/06/subsets.html?q=subset
 * DFS, bottom-up result building
 * iteration
 */

public class Subsets {
	// DFS, top-down result building
	public List<List<Integer>> subsets(int[] S) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (S == null || S.length == 0) {
			return result;
		}
		Arrays.sort(S);
		subsetsHelper(result, new ArrayList<Integer>(), S, 0);
		return result;
	}

	// DFS, bottom-up result building
	public List<List<Integer>> subsets1(int[] S) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (S == null || S.length == 0) {
			return result;
		}
		Arrays.sort(S);
		return subsetsHelper1(S, 0);
	}

	// iterative
	public List<List<Integer>> subsets2(int[] S) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (S == null || S.length == 0) {
			return result;
		}
		Arrays.sort(S);
		result.add(new ArrayList<Integer>());

		for (int i = 0; i < S.length; i++) {
			/*
			 * List<List<Integer>> newResult = new ArrayList<List<Integer>>();
			 * for(List<Integer> list : result) { List<Integer> newList = new
			 * ArrayList<Integer>(list); newList.add(S[i]);
			 * newResult.add(newList); } result.addAll(newResult);
			 */

			// use original result size
			int size = result.size();
			while (size-- > 0) {
				List<Integer> list = new ArrayList<Integer>(result.get(size));
				list.add(S[i]);
				result.add(list);
			}

		}
		return result;
	}

	// DFS, top-down result building, function helper
	private void subsetsHelper(List<List<Integer>> result, List<Integer> list,
			int[] S, int pos) {
		result.add(new ArrayList<Integer>(list));
		for (int i = pos; i < S.length; i++) {
			list.add(S[i]);
			subsetsHelper(result, list, S, i + 1);
			list.remove(list.size() - 1);
		}
	}

	// DFS, bottom-up result building, function helper
	private List<List<Integer>> subsetsHelper1(int[] S, int pos) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (pos == S.length) {
			result.add(new ArrayList<Integer>());
		} else {
			for (List<Integer> list : subsetsHelper1(S, pos + 1)) {
				// add finished list
				result.add(list);
				// add list with top-level new element
				List<Integer> newList = new ArrayList<Integer>(list);
				newList.add(0, S[pos]);
				result.add(newList);
			}
		}
		return result;
	}
}
