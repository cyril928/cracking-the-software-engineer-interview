package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Pascal's Triangle 
/* 
 * http://n00tc0d3r.blogspot.tw/2013/05/pascals-triangle.html?q=Triangle
 * implementation
 * 
 * http://yucoding.blogspot.tw/2013/04/leetcode-question-64-pascals-triangle-i.html
 * explanation
 */
public class PascalsTriangle {
	// clean implementation, my own way
	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		if (numRows < 0) {
			return res;
		}
		for (int i = 0; i < numRows; i++) {
			List<Integer> level = new ArrayList<Integer>();
			for (int j = 0; j <= i; j++) {
				if (j == 0 || j == i) {
					level.add(1);
				} else {
					level.add(res.get(i - 1).get(j) + res.get(i - 1).get(j - 1));
				}
			}
			res.add(level);
		}
		return res;
	}

	// clean implementation, too
	public List<List<Integer>> generate1(int numRows) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		if (numRows <= 0) {
			return res;
		}

		List<Integer> row = new ArrayList<Integer>();
		row.add(1);
		res.add(row);
		for (int i = 1; i < numRows; i++) {
			row = new ArrayList<Integer>();
			row.add(1);
			for (int j = 1; j < i; j++) {
				row.add(res.get(i - 1).get(j - 1) + res.get(i - 1).get(j));
			}
			row.add(1);
			res.add(row);
		}
		return res;
	}
}
