package must.win.leetcode;

//Best Time to Buy and Sell Stock III
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Best+Time+to+Buy+and+Sell+Stock  // explanation
 * http://mattcb.blogspot.tw/search?q=Best+Time+to+Buy+and+Sell+Stock // implementation
 */
public class BestTimetoBuyandSellStockIII {
	public int maxProfit(int[] prices) {
		int len = prices.length;
		if (len <= 1) {
			return 0;
		}
		int[] forwardProfit = new int[len];
		int[] backwardProfit = new int[len];
		// Actually, when we calculare profits backwards, we already have all
		// forward profits, we can calculate the best total max profit at the
		// same time and thus we don't need to store backward profit
		int min = prices[0];
		int max = prices[len - 1];
		int res = 0;

		for (int i = 1; i < len; i++) {
			min = Math.min(prices[i], min);
			forwardProfit[i] = Math.max(prices[i] - min, forwardProfit[i - 1]);
		}

		for (int i = len - 2; i >= 0; i--) {
			max = Math.max(prices[i], max);
			backwardProfit[i] = Math
					.max(max - prices[i], backwardProfit[i + 1]);
		}

		for (int i = 0; i < len; i++) {
			res = Math.max(forwardProfit[i] + backwardProfit[i], res);
		}

		return res;
	}
}
