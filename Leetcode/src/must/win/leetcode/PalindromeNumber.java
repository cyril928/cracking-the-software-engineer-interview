package must.win.leetcode;

// Palindrome Number 
/*
 * http://answer.ninechapter.com/solutions/palindrome-number/
 * reverse Integer
 * 
 * http://n00tc0d3r.blogspot.tw/2013/04/palindrome-number.html?q=Palindrome+Number
 * Alternatively, we compare digit by digit from the two ends, without saving digits into a string. ("Do not use extra space"!)
 */
public class PalindromeNumber {
	// reverse Integer
	public boolean isPalindrome(int x) {
		if (x < 0) {
			return false;
		}
		long res = 0;
		int originalVal = x;
		while (x != 0) {
			res = res * 10 + x % 10;
			x /= 10;
		}
		return ((int) res == originalVal);
	}

	// compare digit by digit from the two ends,
	public boolean isPalindrome1(int x) {
		if (x < 0) {
			return false;
		}

		int div = 1;
		while (x / div >= 10) {
			div *= 10;
		}

		while (x > 0) {
			int leftDigit = x / div;
			int rightDigit = x % 10;
			if (leftDigit != rightDigit) {
				return false;
			}
			x = (x % div) / 10;
			div /= 100;
		}

		return true;
	}
}
