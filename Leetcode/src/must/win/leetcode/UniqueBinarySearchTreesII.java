package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Unique Binary Search Trees II 
/*
 * http://www.ninechapter.com/solutions/unique-binary-search-trees-ii/
 * recursive implementation
 * 
 * http://n00tc0d3r.blogspot.tw/2013/07/unique-binary-search-trees_6.html?q=regular
 * DP
 */

public class UniqueBinarySearchTreesII {
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
			left = null;
			right = null;
		}
	}

	// recursive helper
	public List<TreeNode> generateSubTrees(int start, int end) {
		List<TreeNode> res = new ArrayList<TreeNode>();
		if (start > end) {
			res.add(null);
		} else {
			for (int i = start; i <= end; i++) {
				for (TreeNode leftChild : generateSubTrees(start, i - 1)) {
					for (TreeNode rightChild : generateSubTrees(i + 1, end)) {
						TreeNode parent = new TreeNode(i);
						parent.left = leftChild;
						parent.right = rightChild;
						res.add(parent);
					}
				}
			}
		}
		return res;
	}

	// recursive
	public List<TreeNode> generateTrees(int n) {
		if (n < 0) {
			return new ArrayList<TreeNode>();
		}
		return generateSubTrees(1, n);
	}

	// DP
	public List<TreeNode> generateTrees1(int n) {
		if (n < 0) {
			return new ArrayList<TreeNode>();
		} else if (n == 0) {
			List<TreeNode> res = new ArrayList<TreeNode>();
			res.add(null);
			return res;
		}
		// treesTable[i,l] contains a list of BSTs of [i .. i+l]
		List<List<List<TreeNode>>> treesTable = new ArrayList<List<List<TreeNode>>>();

		// construct the single node tree
		for (int i = 1; i <= n; i++) {
			List<List<TreeNode>> trees = new ArrayList<List<TreeNode>>();
			List<TreeNode> singleNodeTrees = new ArrayList<TreeNode>();
			singleNodeTrees.add(new TreeNode(i));
			trees.add(singleNodeTrees);
			treesTable.add(trees);
		}

		// build table with tree node's amount from 2 to n
		for (int len = 1; len < n; len++) {
			for (int start = 1; start <= n; start++) {
				if (start + len <= n) {
					List<TreeNode> trees = new ArrayList<TreeNode>();
					int end = start + len;
					for (int i = start; i <= end; i++) {
						if (i == end) {
							for (TreeNode leftChild : treesTable.get(start - 1)
									.get(i - 2 - start + 1)) {
								TreeNode root = new TreeNode(i);
								root.left = leftChild;
								trees.add(root);
							}
						} else if (i == start) {
							for (TreeNode rightChild : treesTable.get(i).get(
									end - 1 - i)) {
								TreeNode root = new TreeNode(i);
								root.right = rightChild;
								trees.add(root);
							}
						} else {
							for (TreeNode leftChild : treesTable.get(start - 1)
									.get(i - 2 - start + 1)) {
								for (TreeNode rightChild : treesTable.get(i)
										.get(end - 1 - i)) {
									TreeNode root = new TreeNode(i);
									root.left = leftChild;
									root.right = rightChild;
									trees.add(root);
								}
							}
						}
					}
					treesTable.get(start - 1).add(trees);
				}
			}
		}
		return treesTable.get(0).get(n - 1);
	}
}
