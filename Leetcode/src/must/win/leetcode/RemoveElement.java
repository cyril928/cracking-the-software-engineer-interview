package must.win.leetcode;

// Remove Element 
/*
 * http://n00tc0d3r.blogspot.tw/2013/05/remove-element-from-arraylist.html?q=Remove+Duplicates+from+Sorted+Array
 * O(N) use element count!
 * 
 * http://answer.ninechapter.com/solutions/remove-element/
 * O(N) change the array element order, but might run less time
 */

public class RemoveElement {

	// O(N) use new size(index), most clean
	public int removeElement(int[] A, int elem) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int len = A.length;
		int size = 0;
		for (int i = 0; i < len; i++) {
			if (A[i] != elem) {
				A[size++] = A[i];
			}
		}
		return size;
	}

	// O(N) use element count
	public int removeElement1(int[] A, int elem) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int eleCount = 0;
		int len = A.length;

		for (int i = 0; i < len; i++) {
			if (A[i] == elem) {
				eleCount++;
			} else {
				A[i - eleCount] = A[i];
			}
		}
		return len - eleCount;
	}

	// O(N) change the array element order, but might run less time
	public int removeElement2(int[] A, int elem) {
		int i = 0;
		int pointer = A.length - 1;
		while (i <= pointer) {
			if (A[i] == elem) {
				A[i] = A[pointer];
				pointer--;
			} else {
				i++;
			}
		}
		return pointer + 1;
	}
}
