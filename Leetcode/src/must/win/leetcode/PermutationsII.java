package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * http://www.ninechapter.com/solutions/permutations-ii/
 * DFS, with visited array to maintain path duplicate
 * 
 * http://n00tc0d3r.blogspot.com/2013/05/permutations.html?q=Edit+Distance
 * DFS, second way, bottom-up result building
 */

public class PermutationsII {
	// DFS, with visited array to maintain path duplicate, top-down result
	// building
	public List<List<Integer>> permuteUnique(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		Arrays.sort(num);
		permuteUniqueHelper(result, new ArrayList<Integer>(),
				new int[num.length], num);
		return result;
	}

	// DFS, top-down result building, with visited array to maintain path
	// duplicate, and hashset to
	// maintain same level duplicates
	public List<List<Integer>> permuteUnique1(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		// Arrays.sort(num);
		permuteUniqueHelper1(result, new ArrayList<Integer>(),
				new int[num.length], num);
		return result;
	}

	// DFS, second way, bottom-up result building
	public List<List<Integer>> permuteUnique2(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		int len = num.length;
		if (len == 1) {
			List<Integer> list = new ArrayList<Integer>();
			list.add(num[0]);
			result.add(list);
		} else {
			Set<Integer> set = new HashSet<Integer>();
			for (int i = 0; i < len; i++) {
				if (!set.contains(num[i])) {
					// put element into hashset to avoid same level duplicates
					set.add(num[i]);
					// copy a new array of len-1 numbers
					int[] subset = Arrays.copyOf(num, len - 1);
					// skip ith number, copy the rest number into new array
					for (int j = i + 1; j < len; j++) {
						subset[j - 1] = num[j];
					}

					// append the current number to the end of permutation of
					// n-1 subset
					for (List<Integer> list : permuteUnique2(subset)) {
						list.add(num[i]);
						result.add(list);
					}
				}
			}
		}

		return result;
	}

	// DFS, top-down result building, with visited array to maintain path
	// duplicate helper function
	private void permuteUniqueHelper(List<List<Integer>> result,
			List<Integer> list, int[] visited, int[] num) {
		if (list.size() == num.length) {
			result.add(new ArrayList<Integer>(list));
			return;
		}

		for (int i = 0; i < num.length; i++) {
			if (visited[i] == 1 || (i > 0 && num[i] == num[i - 1])
					&& visited[i - 1] == 0) {
				continue;
			}
			visited[i] = 1;
			list.add(num[i]);
			permuteUniqueHelper(result, list, visited, num);
			list.remove(list.size() - 1);
			visited[i] = 0;
		}
	}

	// DFS, top-down result building, with visited array to maintain path
	// duplicate, and hashset to
	// maintain same level duplicates, helper function
	private void permuteUniqueHelper1(List<List<Integer>> result,
			List<Integer> list, int[] visited, int[] num) {
		if (list.size() == num.length) {
			result.add(new ArrayList<Integer>(list));
			return;
		}

		Set<Integer> set = new HashSet<Integer>();

		for (int i = 0; i < num.length; i++) {
			if (visited[i] == 1 || set.contains(num[i])) {
				continue;
			}
			set.add(num[i]);
			visited[i] = 1;
			list.add(num[i]);
			permuteUniqueHelper1(result, list, visited, num);
			list.remove(list.size() - 1);
			visited[i] = 0;
		}
	}
}
