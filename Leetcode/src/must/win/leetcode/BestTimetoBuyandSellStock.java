package must.win.leetcode;

// Best Time to Buy and Sell Stock 
/*
 * http://www.ninechapter.com/solutions/best-time-to-buy-and-sell-stock/
 * cleaner implementation
 * 
 * http://mattcb.blogspot.tw/2012/11/best-time-to-buy-and-sell-stock-leetcode.html?q=Best+Time+to+Buy+and+Sell+Stock
 * Implementation
 */
public class BestTimetoBuyandSellStock {
	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}
		int minPrice = Integer.MAX_VALUE;
		int maxProfit = 0;
		for (int price : prices) {
			maxProfit = Math.max(price - minPrice, maxProfit);
			minPrice = Math.min(price, minPrice);
		}
		return maxProfit;
	}

	public int maxProfit1(int[] prices) {
		int len = prices.length;
		if (len <= 1) {
			return 0;
		}
		int maxProfit = 0;
		int minPrice = prices[0];
		for (int i = 1; i < len; i++) {
			minPrice = Math.min(prices[i], minPrice);
			maxProfit = Math.max(prices[i] - minPrice, maxProfit);
		}
		return maxProfit;
	}
}
