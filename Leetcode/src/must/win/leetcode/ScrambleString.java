package must.win.leetcode;

import java.util.HashMap;

// Scramble String
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Scramble+String
 * Recursive Backtracking implementation and explanation
 * 
 * http://www.blogjava.net/sandy/archive/2013/05/22/399605.html (better)
 * DP implementation and explanation
 * http://blog.csdn.net/fightforyourdream/article/details/17707187 (better)
 * Recursive tree pruning, DP implementation and explanation
 */
public class ScrambleString {
	// recursive with tree pruning helper function
	private boolean isScramble(String s1, int s1s, int s1e, String s2, int s2s,
			int s2e) {
		if (s1s == s1e) {
			return s1.charAt(s1s) == s2.charAt(s2s);
		}

		int[] count = new int[26];
		for (int i = s1s; i <= s1e; i++) {
			count[s1.charAt(i) - 'a']++;
		}
		for (int i = s2s; i <= s2e; i++) {
			if (count[s2.charAt(i) - 'a'] == 0) {
				return false;
			}
			count[s2.charAt(i) - 'a']--;
		}

		int len = s1e - s1s;
		for (int i = 0; i < len; i++) {
			if (isScramble(s1, s1s, s1s + i, s2, s2s, s2s + i)
					&& isScramble(s1, s1s + i + 1, s1e, s2, s2s + i + 1, s2e)) {
				return true;
			}
			if (isScramble(s1, s1s, s1s + i, s2, s2e - i, s2e)
					&& isScramble(s1, s1s + i + 1, s1e, s2, s2s, s2e - i - 1)) {
				return true;
			}
		}

		return false;
	}

	// DP, O(N^4) Time complexity, O(N^3) space complexity
	public boolean isScramble(String s1, String s2) {
		int len = s1.length();
		if (len != s2.length()) {
			return false;
		}
		if (s1.equals(s2)) {
			return true;
		}

		boolean[][][] result = new boolean[len][len][len];

		for (int i = 0; i < len; i++) {
			for (int j = 0; j < len; j++) {
				if (s1.charAt(i) == s2.charAt(j)) {
					result[i][j][0] = true;
				}
			}
		}

		for (int k = 1; k < len; k++) {
			for (int i = 0; i < len - k; i++) {
				for (int j = 0; j < len - k; j++) {
					for (int p = 1; p <= k; p++) {
						if ((result[i][j][p - 1] && result[i + p][j + p][k - p])
								|| (result[i][j + k - p + 1][p - 1] && result[i
										+ p][j][k - p])) {
							result[i][j][k] = true;
							break;
						}
					}
				}
			}
		}

		return result[0][0][len - 1];
	}

	// recursive with tree pruning
	public boolean isScramble1(String s1, String s2) {
		if (s1 == null || s2 == null) {
			return false;
		}
		if (s1.length() != s2.length()) {
			return false;
		}
		return isScramble(s1, 0, s1.length() - 1, s2, 0, s2.length() - 1);
	}

	// Recursion w/ Backtracking with tree pruning
	public boolean isScramble2(String s1, String s2) {
		if (s1.length() == 0) {
			return false;
		}
		return isScrambleHelper(s1, s2, new HashMap<String, String>());
	}

	// for Backtracking(using hashmap store partition results) method
	private boolean isScrambleHelper(String s1, String s2,
			HashMap<String, String> map) {
		int len = s1.length();
		if (len != s2.length()) {
			return false;
		}
		if (s1.equals(s2) || s1.equals(map.get(s1))) {
			return true;
		}

		// tree pruning, but cost O(nlogn) for sort
		/*
		 * char[] s1a = s1.toCharArray(); char[] s2a = s2.toCharArray();
		 * 
		 * Arrays.sort(s1a); Arrays.sort(s2a); if(!new String(s1a).equals(new
		 * String(s2a))) { return false; }
		 */

		// Tree pruning, cost O(N)
		int[] A = new int[26];
		for (int i = 0; i < len; i++) {
			A[s1.charAt(i) - 'a']++;
			A[s2.charAt(i) - 'a']--;
		}
		for (int i = 0; i < 26; i++) {
			if (A[i] != 0) {
				return false;
			}
		}

		for (int i = 1; i < len; i++) {
			String s1left = s1.substring(0, i);
			String s1right = s1.substring(i, len);
			String s2left = s2.substring(0, i);
			String s2right = s2.substring(i, len);
			if (isScrambleHelper(s1left, s2left, map)
					&& isScrambleHelper(s1right, s2right, map)) {
				map.put(s1left, s2left);
				map.put(s1right, s2right);
				return true;
			}
			s2left = s2.substring(0, len - i);
			s2right = s2.substring(len - i, len);
			if (isScrambleHelper(s1left, s2right, map)
					&& isScrambleHelper(s1right, s2left, map)) {
				map.put(s1left, s2right);
				map.put(s1right, s2left);
				return true;
			}
		}
		return false;
	}
}
