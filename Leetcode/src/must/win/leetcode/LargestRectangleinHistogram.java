package must.win.leetcode;

import java.util.Stack;

// Largest Rectangle in Histogram
/* http://n00tc0d3r.blogspot.tw/2013/03/largest-rectangle-in-histogram.html?q=Largest+Rectangle+in+Histogram
 * recursive, 2 stacks, 1 stack
 */
public class LargestRectangleinHistogram {

	// 1 stack, the height will always be increasing order in the left stack.
	// O(N) time, O(N) space
	// Best solution
	public int largestRectangleArea(int[] height) {
		if (height == null) {
			return 0;
		}
		int len = height.length;
		if (len == 0) {
			return 0;
		}
		Stack<Integer> left = new Stack<Integer>();
		int maxArea = 0;
		int cur = 0;

		while (cur < len) {
			if (left.isEmpty() || height[cur] > height[left.peek()]) {
				left.push(cur++);
			} else {
				int top = left.pop();
				int width = left.isEmpty() ? cur : (cur - left.peek() - 1);
				maxArea = Math.max(height[top] * width, maxArea);
			}
		}

		while (!left.isEmpty()) {
			int top = left.pop();
			int width = left.isEmpty() ? len : (len - left.peek() - 1);
			maxArea = Math.max(height[top] * width, maxArea);
		}
		return maxArea;
	}

	// 2 stacks(index, left), the height will always be increasing order in the
	// index stack, O(N) time, O(N) space
	public int largestRectangleArea1(int[] height) {
		if (height == null) {
			return 0;
		}
		int len = height.length;
		if (len == 0) {
			return 0;
		}
		Stack<Integer> index = new Stack<Integer>();
		Stack<Integer> left = new Stack<Integer>();
		int maxArea = 0;
		int cur = 0;
		while (cur < len) {
			if (cur == 0 || height[cur] > height[index.peek()]) {
				index.push(cur);
				left.push(cur);
			} else if (height[cur] < height[index.peek()]) {
				int lastLeft = 0;
				do {
					lastLeft = left.pop();
					maxArea = Math.max(height[index.pop()] * (cur - lastLeft),
							maxArea);
				} while (!left.isEmpty() && height[cur] < height[index.peek()]);
				index.push(cur);
				left.push(lastLeft);
			}
			cur++;
		}

		while (!index.isEmpty() && !left.isEmpty()) {
			maxArea = Math.max((len - left.pop()) * height[index.pop()],
					maxArea);
		}
		return maxArea;
	}

	// iterative TLE...., (every index can be a starting index)
	public int largestRectangleArea2(int[] height) {
		if (height == null) {
			return 0;
		}
		int len = height.length;
		int res = 0;
		for (int i = 0; i < len; i++) {
			res = Math.max(largestRectangleAreaIterative(height, i), res);
		}
		return res;
	}

	// recursive, TLE....
	public int largestRectangleArea3(int[] height) {
		return largestRectangleAreaRecursive(height, 0, height.length);
	}

	// for iterative method
	private int largestRectangleAreaIterative(int[] height, int start) {
		int len = height.length;
		int minHeight = Integer.MAX_VALUE;
		int maxArea = 0;
		for (int i = start; i < len; i++) {
			minHeight = Math.min(height[i], minHeight);
			maxArea = Math.max(minHeight * (i - start + 1), maxArea);
		}
		return maxArea;
	}

	// for recursive method
	private int largestRectangleAreaRecursive(int[] height, int left, int right) {
		if (left >= right) {
			return 0;
		}
		int minHeight = height[left];
		int minID = left;
		for (int i = left + 1; i < right; i++) {
			if (height[i] < minHeight) {
				minHeight = height[i];
				minID = i;
			}
		}
		return Math.max(minHeight * (right - left), Math.max(
				largestRectangleAreaRecursive(height, left, minID),
				largestRectangleAreaRecursive(height, minID + 1, right)));
	}
}
