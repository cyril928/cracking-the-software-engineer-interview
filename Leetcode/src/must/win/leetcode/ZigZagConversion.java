package must.win.leetcode;

// ZigZag Conversion 
/* 
 * http://n00tc0d3r.blogspot.tw/search?q=ZigZag+Conversion
 * using StringBuilder
 * http://answer.ninechapter.com/solutions/zigzag-conversion/
 * using array
 */
public class ZigZagConversion {
	// using StringBuilder
	public String convert(String s, int nRows) {
		int len = s.length();
		if (len < nRows || nRows == 1) {
			return s;
		}

		StringBuilder sb = new StringBuilder();
		int step = (nRows << 1) - 2;
		for (int i = 0; i < nRows; i++) {
			int cur = i;
			while (cur < len) {
				sb.append(s.charAt(cur));
				cur += step;
				if (i > 0 && i < nRows - 1 && (cur - (i << 1)) < len) {
					sb.append(s.charAt(cur - (i << 1)));
				}
			}
		}
		return sb.toString();
	}

	// using array
	public String convert1(String s, int nRows) {
		int len = s.length();
		if (len < nRows || nRows == 1) {
			return s;
		}

		int step = (nRows << 1) - 2;
		int count = 0;
		char[] str = new char[len];
		for (int i = 0; i < nRows; i++) {
			int interval = step - i * 2;
			for (int j = i; j < len; j += step) {
				str[count++] = s.charAt(j);
				if (i > 0 && i < nRows - 1 && (j + interval) < len) {
					str[count++] = s.charAt(j + interval);
				}
			}
		}

		return new String(str);
	}
}
