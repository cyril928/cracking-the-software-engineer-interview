package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Fraction to Recurring Decimal 
/*
 * http://blog.csdn.net/ljiabin/article/details/42025037
 * explanation & implementation
 */
public class FractiontoRecurringDecimal {
	public String fractionToDecimal(int numerator, int denominator) {
		if (denominator == 0) {
			return "";
		}
		if (numerator == 0) {
			return "0";
		}

		StringBuilder sb = new StringBuilder();
		// the result is negative
		if ((numerator < 0) ^ (denominator < 0)) {
			sb.append("-");
		}

		// to cover Integer.MIN_VALUE case
		long num = numerator, den = denominator;
		num = Math.abs(num);
		den = Math.abs(den);

		// get the integer part
		long quotient = num / den;
		sb.append(quotient);

		long remainder = (num % den) * 10;
		if (remainder == 0) {
			return sb.toString();
		}

		sb.append(".");
		// map to store remainder and start index in result string's mapping
		Map<Long, Integer> map = new HashMap<Long, Integer>();
		while (remainder != 0) {
			if (map.containsKey(remainder)) {
				int beg = map.get(remainder);
				String part1 = sb.substring(0, beg);
				String part2 = sb.substring(beg);
				return part1 + "(" + part2 + ")";
			}
			map.put(remainder, sb.length());
			quotient = remainder / den;
			sb.append(quotient);
			remainder = (remainder % den) * 10;
		}
		return sb.toString();
	}
}
