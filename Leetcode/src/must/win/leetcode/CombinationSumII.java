package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Combination Sum II 
/*
 * http://n00tc0d3r.blogspot.tw/2013/01/combination-sum.html?q=Combination+Sum
 * good remove duplicate 
 */
public class CombinationSumII {
	// top-down approach
	public List<List<Integer>> combinationSum2(int[] num, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		Arrays.sort(num);
		if (target < num[0]) {
			return result;
		}
		combinationSum2Helper(result, new ArrayList<Integer>(), num, 0, target);
		return result;
	}

	private void combinationSum2Helper(List<List<Integer>> result,
			List<Integer> path, int[] num, int index, int target) {
		if (target == 0) {
			result.add(new ArrayList<Integer>(path));
			return;
		}
		int len = num.length;
		for (int i = index; i < len; i++) {
			// to remove duplicate condition like (int[] candidates => {1,1},
			// target => 1)
			if (i > index && num[i] == num[i - 1]) {
				continue;
			}
			int newTarget = target - num[i];
			if (newTarget >= 0) {
				path.add(num[i]);
				combinationSum2Helper(result, path, num, i + 1, newTarget);
				path.remove(path.size() - 1);
			}
		}
	}

}
