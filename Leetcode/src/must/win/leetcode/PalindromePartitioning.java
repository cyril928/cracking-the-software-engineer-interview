package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

//Palindrome Partitioning 
/*
 * http://n00tc0d3r.blogspot.com/2013/05/palindrome-partitioning.html?q=Palindrome+Partitioning
 * DP + BUILD A TABLE, DP + DP(go to REFER LintCode one)
 * 
 * http://yucoding.blogspot.com/2013/08/leetcode-question-132-palindrome.html
 * recursive
 */
public class PalindromePartitioning {
	/*
	 * Solution - DP+DP
	 * 
	 * The algorithm above involves redundant calculations in the recursions.
	 * Instead of using a recursion, we can calculate the partitions backwards
	 * and store all previous results.
	 * 
	 * So, we create a table for previous results such that preRes[i] contains
	 * all of the partitions of s[i..len-1].
	 */
	private void buildPalindromeTable(boolean[][] table, String s) {
		int len = table.length;
		for (int i = 0; i < len; i++) {
			table[i][i] = true;
		}
		for (int i = 1; i < len; i++) {
			int l = i - 1;
			int r = i;
			while (l >= 0 && r < len && s.charAt(l) == s.charAt(r)) {
				table[l--][r++] = true;
			}

			l = i - 1;
			r = i + 1;
			while (l >= 0 && r < len && s.charAt(l) == s.charAt(r)) {
				table[l--][r++] = true;
			}
		}
	}

	private void findPartition(List<List<String>> res, LinkedList<String> sol,
			String s, int start) {
		int len = s.length();
		if (start >= len) {
			List<String> newSol = new LinkedList<String>(sol);
			res.add(newSol);
		} else {
			for (int i = start; i < len; i++) {
				if (isValid(start, i, s)) {
					sol.add(s.substring(start, i + 1));
					findPartition(res, sol, s, i + 1);
					sol.removeLast();
				}
			}
		}
	}

	private boolean isValid(int start, int end, String s) {
		while (start <= end) {
			if (s.charAt(start) == s.charAt(end)) {
				start++;
				end--;
			} else {
				return false;
			}
		}
		return true;
	}

	public ArrayList<ArrayList<String>> partition(String s) {
		boolean[][] table = new boolean[s.length()][s.length()];
		buildPalindromeTable(table, s);

		int len = s.length();
		HashMap<Integer, ArrayList<ArrayList<String>>> resMap = new HashMap<Integer, ArrayList<ArrayList<String>>>();

		for (int i = 0; i < len; i++) {
			ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
			for (int j = 0; j <= i; j++) {
				if (table[j][i]) {
					if (j == 0) {// only one string
						ArrayList<String> nRes = new ArrayList<String>();
						nRes.add(s.substring(0, i + 1));
						res.add(nRes);
					} else { // split to two string
						for (ArrayList<String> preRes : resMap.get(j - 1)) {
							ArrayList<String> nRes = new ArrayList<String>(
									preRes);
							nRes.add(s.substring(j, i + 1));
							res.add(nRes);
						}
					}
				}
			}
			resMap.put(i, res);
		}
		return resMap.get(len - 1);
	}

	// recursive!!!
	public List<List<String>> partitionRecursive(String s) {
		List<List<String>> res = new LinkedList<List<String>>();
		LinkedList<String> sol = new LinkedList<String>();
		if (s == null) {
			return res;
		}
		findPartition(res, sol, s, 0);
		return res;
	}
}
