package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Minimum Window Substring
/*
 * From Zouyou
 * One HashMap solution
 * 
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Minimum+Window+Substring
 * Two HashMap solution. 
 * running time is O(n), where n is the length of S. 
 * And it takes O(m) spaces for the two hash maps, where m is the length of T.
 * 
 * http://answer.ninechapter.com/solutions/minimum-window-substring/
 * clear solution.
 * 
 */
public class MinimumWindowSubstring {
	// One HashMap solution
	public String minWindow(String S, String T) {
		if (S == null || T == null) {
			throw new IllegalArgumentException("");
		}
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		for (int i = 0; i < T.length(); i++) {
			char c = T.charAt(i);
			int count = map.containsKey(c) ? map.get(c) : 0;
			map.put(c, count + 1);
		}

		int num = 0;
		int lp = 0, rp = Integer.MAX_VALUE;
		int leftBound = 0;

		for (int i = 0; i < S.length(); i++) {
			char c = S.charAt(i);
			if (!map.containsKey(c)) {
				continue;
			}
			map.put(c, map.get(c) - 1);
			if (map.get(c) >= 0) {
				num++;
			}
			while (num == T.length()) {
				if (i - leftBound + 1 < rp - lp) {
					lp = leftBound;
					rp = i + 1;
				}
				c = S.charAt(leftBound++);
				if (map.containsKey(c)) {
					map.put(c, map.get(c) + 1);
					if (map.get(c) > 0) {
						num--;
					}
				}
			}
		}

		return rp - lp > S.length() ? "" : S.substring(lp, rp);
	}

	public String minWindow1(String S, String T) {
		if (S == null || S.length() == 0) {
			return S;

		}
		if (T == null || T.length() == 0) {
			return "";
		}
		int sLen = S.length();
		int tLen = T.length();

		Map<Character, Integer> needFind = new HashMap<Character, Integer>();
		Map<Character, Integer> hasFind = new HashMap<Character, Integer>();

		for (int i = 0; i < tLen; i++) {
			char c = T.charAt(i);
			hasFind.put(c, 0);
			if (needFind.containsKey(c)) {
				needFind.put(c, needFind.get(c) + 1);
			} else {
				needFind.put(c, 1);
			}
		}

		String minWindow = null;
		int found = 0, leftBound = 0;

		for (int i = 0; i < sLen; i++) {
			char c = S.charAt(i);
			if (!needFind.containsKey(c)) {
				continue;
			}

			hasFind.put(c, hasFind.get(c) + 1);

			if (hasFind.get(c) <= needFind.get(c)) {
				found++;
			}

			if (found == tLen) {
				while (leftBound <= i) {
					char ch = S.charAt(leftBound);
					if (!needFind.containsKey(ch)) {
						leftBound++;
						continue;
					}
					if (hasFind.get(ch) > needFind.get(ch)) {
						hasFind.put(ch, hasFind.get(ch) - 1);
						leftBound++;
						continue;
					}
					break;
				}
				if (minWindow == null
						|| minWindow.length() > (i - leftBound + 1)) {
					minWindow = S.substring(leftBound, i + 1);
				}
			}
		}

		return minWindow == null ? "" : minWindow;
	}
}
