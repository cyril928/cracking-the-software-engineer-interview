package must.win.leetcode;

// Reorder List
/* 
 * http://www.programcreek.com/2013/12/in-place-reorder-a-singly-linked-list-in-java/
 * 1. Break list in the middle to two lists (use fast & slow pointers)
 * 2. Reverse the order of the second list
 * 3. Merge two list back together
 */
public class ReorderList {
	// merge two linked list
	private void mergeList(ListNode firstCur, ListNode secondCur) {
		while (firstCur != null && secondCur != null) {
			ListNode next = firstCur.next;
			firstCur.next = secondCur;
			firstCur = next;
			next = secondCur.next;
			secondCur.next = firstCur;
			secondCur = next;
		}
		// need to close first linked list
		if (firstCur != null) {
			firstCur.next = null;
		}
	}

	public void reorderList(ListNode head) {
		if (head == null) {
			return;
		}

		ListNode fastNode = head, slowNode = head;
		while (fastNode.next != null && fastNode.next.next != null) {
			fastNode = fastNode.next.next;
			slowNode = slowNode.next;
		}

		mergeList(head, reverseList(slowNode.next));
	}

	// reverse linked list
	private ListNode reverseList(ListNode head) {
		ListNode prev = null;
		ListNode cur = head;

		while (cur != null) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}
		return prev;
	}
}
