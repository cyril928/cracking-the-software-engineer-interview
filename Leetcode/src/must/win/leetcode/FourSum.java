package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// 4Sum
/*
 * http://n00tc0d3r.blogspot.tw/2013/01/2sum-3sum-4sum-and-variances.html?q=3+sum
 * best implementation and explanation
 */
public class FourSum {
	public List<List<Integer>> fourSum(int[] num, int target) {
		int len = num.length;
		List<List<Integer>> resSet = new ArrayList<List<Integer>>();
		if (num == null || len < 4) {
			return resSet;
		}

		Arrays.sort(num);
		for (int i = 0; i < len - 3; i++) {
			if (i > 0 && num[i] == num[i - 1]) {
				continue;
			}
			for (int j = i + 1; j < len - 2; j++) {
				if (j > i + 1 && num[j] == num[j - 1]) {
					continue;
				}
				int start = j + 1;
				int end = len - 1;
				while (start < end) {
					int sum = num[i] + num[j] + num[start] + num[end];
					if (sum < target) {
						do {
							start++;
						} while (start < end && num[start] == num[start - 1]);
					} else if (sum > target) {
						do {
							end--;
						} while (start < end && num[i] == num[end + 1]);
					} else {
						List<Integer> res = new ArrayList<Integer>();
						res.add(num[i]);
						res.add(num[j]);
						res.add(num[start]);
						res.add(num[end]);
						resSet.add(res);
						do {
							start++;
						} while (start < end && num[start] == num[start - 1]);
						do {
							end--;
						} while (start < end && num[i] == num[end + 1]);
					}
				}
			}
		}
		return resSet;
	}
}
