package must.win.leetcode;

// Remove Duplicates from Sorted List 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Remove+Duplicates+from+Sorted+List
 * implementation & explanation
 */
public class RemoveDuplicatesfromSortedList {

	// better style
	public ListNode deleteDuplicates(ListNode head) {
		ListNode curNode = head;

		while (curNode != null && curNode.next != null) {
			ListNode nextNode = curNode.next;
			if (curNode.val == nextNode.val) {
				curNode.next = nextNode.next;
			} else {
				curNode = nextNode;
			}
		}

		return head;
	}

	// by myself
	public ListNode deleteDuplicates1(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}

		ListNode preNode = head;
		ListNode nextNode = head.next;

		while (nextNode != null) {
			if (preNode.val == nextNode.val) {
				preNode.next = nextNode.next;
			} else {
				preNode = preNode.next;
			}
			nextNode = nextNode.next;
		}

		return head;
	}
}
