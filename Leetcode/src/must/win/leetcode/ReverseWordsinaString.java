package must.win.leetcode;

// Reverse Words in a String 
/* 
 * Clean Code Hand Book
 * best, one pass!
 * 
 * http://answer.ninechapter.com/solutions/reverse-words-in-a-string/
 * good but two pass, backward way with split function, with backward way, 
 * we can use StringBuilder append not insert
 * 
 */
public class ReverseWordsinaString {

	// best, one pass
	public String reverseWords(String s) {
		if (s == null) {
			return null;
		}

		StringBuilder sb = new StringBuilder();
		int j = s.length();
		for (int i = s.length() - 1; i >= 0; i--) {
			if (s.charAt(i) == ' ') {
				j = i;
			} else if (i == 0 || s.charAt(i - 1) == ' ') {
				if (sb.length() != 0) {
					sb.append(" ");
				}
				sb.append(s.substring(i, j));
			}
		}
		return sb.toString();
	}

	// good but two pass, backward way with better split function use, with
	// backward way,
	public String reverseWords1(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}

		s = s.trim();

		StringBuilder res = new StringBuilder();
		String[] array = s.split(" +");
		int len = array.length;

		for (int i = len - 1; i >= 0; i--) {
			res.append(array[i]).append(" ");
		}

		return res.length() == 0 ? "" : res.substring(0, res.length() - 1);
	}

	// good but two pass, backward way with split function use (can be better),
	// with backward
	// way,
	public String reverseWords2(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}

		StringBuilder res = new StringBuilder();
		String[] array = s.split(" ");
		int len = array.length;

		for (int i = len - 1; i >= 0; i--) {
			if (!array[i].equals("")) {
				res.append(array[i]).append(" ");
			}
		}

		return res.length() == 0 ? "" : res.substring(0, res.length() - 1);
	}

	// without split, backward way (bad, use StringBuilder insert)
	public String reverseWords3(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}

		s = s.trim();
		StringBuilder res = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		int len = s.length();
		boolean hasSpace = false;

		for (int i = len - 1; i >= 0; i--) {
			if (s.charAt(i) != ' ') {
				sb.insert(0, s.charAt(i));
				hasSpace = false;
			} else {
				if (!hasSpace) {
					res.append(sb + " ");
					hasSpace = true;
				}
				sb = new StringBuilder();
			}
		}
		// since the string has been trim, first char must not be space
		res.append(sb);

		return res.toString();
	}

	// without split, forward way (bad, use StringBuilder insert)
	public String reverseWords4(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}

		s = s.trim();
		StringBuilder res = new StringBuilder();
		StringBuilder sb = new StringBuilder();
		int len = s.length();
		boolean hasSpace = false;

		for (int i = 0; i < len; i++) {
			if (s.charAt(i) != ' ') {
				sb.append(s.charAt(i));
				hasSpace = false;
			} else {
				if (!hasSpace) {
					res.insert(0, " " + sb);
					hasSpace = true;
				}
				sb = new StringBuilder();
			}
		}
		// since the string has been trim, first char must not be space
		res.insert(0, sb);

		return res.toString();
	}
}
