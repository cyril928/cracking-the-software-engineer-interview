package must.win.leetcode;

// Search in Rotated Sorted Array II
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Search+in+Rotated+Sorted+Array
 * time complexity explanation, O(N)
 * 
 * http://answer.ninechapter.com/solutions/search-in-rotated-sorted-array-ii/
 * easy implementation
 */
public class SearchinRotatedSortedArrayII {
	public boolean search(int[] A, int target) {
		if (A == null) {
			return false;
		}
		int len = A.length;
		for (int i = 0; i < len; i++) {
			if (A[i] == target) {
				return true;
			}
		}
		return false;
	}
}
