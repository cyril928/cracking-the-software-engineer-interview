package must.win.leetcode;

// Container With Most Water 
/* 
 * http://n00tc0d3r.blogspot.com/2013/02/container-with-most-water.html?q=water
 * explanation & implementation
 * O(N) O(NLOGN) O(N2) solutions
 * 
 */
public class ContainerWithMostWater {
	// O(N)
	// We can apply greedy strategy as follows:
	// We always move the shorter boundary of the two. By moving the shorter
	// one, we may arrive at a higher boundary so as to get a greater volume
	// (although width decreased); it is not necessary to move the higher one
	// since no matter if the next height is greater or smaller, it won't change
	// the volume -- the shorter boundary is the limit for a container.
	public int maxArea(int[] height) {
		if (height == null || height.length < 2) {
			return 0;
		}

		int max = 0;
		int left = 0;
		int right = height.length - 1;

		while (left < right) {
			int h = Math.min(height[left], height[right]);
			max = Math.max(h * (right - left), max);
			if (height[left] < height[right]) {
				left++;
			} else {
				right--;
			}
		}
		return max;
	}

	// Naive Solution O(N2)
	// A simple way to do it is to go through the height array, calculate
	// possible volumes for the current height together with all other heights,
	// and find the maximum.
	public int maxArea1(int[] height) {
		if (height == null || height.length < 2) {
			return 0;
		}
		int maxArea = 0;
		int len = height.length;
		for (int i = 0; i < len - 1; i++) {
			for (int j = i + 1; j < len; j++) {
				int h = Math.min(height[i], height[j]);
				maxArea = Math.max(h * (j - i), maxArea);
			}
		}
		return maxArea;
	}
}
