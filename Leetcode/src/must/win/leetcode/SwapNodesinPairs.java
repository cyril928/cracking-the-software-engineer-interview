package must.win.leetcode;

// Swap Nodes in Pairs
/*
 * http://n00tc0d3r.blogspot.com/search?q=Swap+Nodes+in+Pairs
 * explanation & implementation
 */
public class SwapNodesinPairs {
	// time O(n) and it is optimal since it touch each node O(1) times and only
	// use O(1) extra space.
	public ListNode swapPairs(ListNode head) {
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode cur = dummy;

		while (cur.next != null && cur.next.next != null) {
			ListNode nextNode = cur.next.next;
			cur.next.next = nextNode.next;
			nextNode.next = cur.next;
			cur.next = nextNode;
			cur = cur.next.next;
		}

		return dummy.next;
	}
}
