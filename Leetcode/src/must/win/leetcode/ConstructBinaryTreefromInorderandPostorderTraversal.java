package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Construct Binary Tree from Inorder and Postorder Traversal 
/*
 * http://mattcb.blogspot.tw/search?q=Construct+Binary+Tree+from+Inorder+and+Postorder+Traversal
 * implementation, in findPosition() function, cause this is not BST, can't use binary search
 * while-loop version, O(N2) Time and O(1) Spaces
 * 
 * 
 * http://n00tc0d3r.blogspot.com/search?q=Construct+Binary+Tree+from+Inorder+and+Postorder+Traversal
 * HashMap implementation, O(N) Time and O(N) Spaces
 * explanation
 * 
 */
public class ConstructBinaryTreefromInorderandPostorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// while-loop version, O(N2) Time and O(1) Spaces
	public TreeNode buildTree(int[] inorder, int[] postorder) {
		if (inorder == null || postorder == null) {
			return null;
		}
		int len = inorder.length;
		if (len == 0 || len != postorder.length) {
			return null;
		}
		return buildTreeHelper(inorder, 0, len - 1, postorder, len - 1);
	}

	// Hashmap version, O(N) Time and O(N) Spaces
	public TreeNode buildTree1(int[] inorder, int[] postorder) {
		if (inorder == null || postorder == null) {
			return null;
		}
		int len = inorder.length;
		if (len == 0 || len != postorder.length) {
			return null;
		}

		// use HashMap to store inorder value and index mapping
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < len; i++) {
			map.put(inorder[i], i);
		}
		return buildTreeHelper1(map, 0, len - 1, postorder, len - 1);
	}

	// for while-loop version, O(N2) Time and O(1) Spaces
	private TreeNode buildTreeHelper(int[] inorder, int start, int end,
			int[] postorder, int postIndex) {
		if (start > end) {
			return null;
		}
		TreeNode parent = new TreeNode(postorder[postIndex]);
		int index = findPosition(inorder, start, end, postorder[postIndex]);
		parent.left = buildTreeHelper(inorder, start, index - 1, postorder,
				postIndex - 1 - (end - index));
		parent.right = buildTreeHelper(inorder, index + 1, end, postorder,
				postIndex - 1);
		return parent;
	}

	// for HashMap version
	private TreeNode buildTreeHelper1(Map<Integer, Integer> map, int start,
			int end, int[] postorder, int postIndex) {
		if (start > end) {
			return null;
		}
		TreeNode parent = new TreeNode(postorder[postIndex]);
		int index = map.get(postorder[postIndex]);
		parent.left = buildTreeHelper1(map, start, index - 1, postorder,
				postIndex - 1 - (end - index));
		parent.right = buildTreeHelper1(map, index + 1, end, postorder,
				postIndex - 1);
		return parent;
	}

	// for while-loop version, O(N2) Time and O(1) Spaces
	private int findPosition(int[] inorder, int start, int end, int key) {
		int index = start;
		while (index <= end && inorder[index] != key) {
			index++;
		}
		return index;
	}
}
