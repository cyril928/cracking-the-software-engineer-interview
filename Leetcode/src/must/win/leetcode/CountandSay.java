package must.win.leetcode;

// Count and Say 
/* 
 * my own way
 * 
 * http://n00tc0d3r.blogspot.tw/2013/02/count-and-say.html?q=Count+and+Say
 * explanation & implementation
 */
public class CountandSay {

	// my own way
	public String countAndSay(int n) {
		if (n <= 0) {
			return "";
		}
		StringBuilder res = new StringBuilder("1");
		while (--n > 0) {
			StringBuilder sb = res;
			res = new StringBuilder();
			int len = sb.length();
			int count = 1;
			for (int i = 0; i < len; i++) {
				if (i == len - 1 || sb.charAt(i) != sb.charAt(i + 1)) {
					res.append(count).append(sb.charAt(i));
					count = 1;
				} else {
					count++;
				}
			}
		}
		return res.toString();
	}

	// other way
	public String countAndSay1(int n) {
		if (n <= 0) {
			return "";
		}
		StringBuilder res = new StringBuilder("1");

		while (--n > 0) {
			StringBuilder sb = new StringBuilder();
			int len = res.length();
			int count = 1;
			for (int i = 0; i < len - 1; i++) {
				if (res.charAt(i) == res.charAt(i + 1)) {
					count++;
				} else {
					sb.append(count);
					sb.append(res.charAt(i));
					count = 1;
				}
			}
			sb.append(count);
			sb.append(res.charAt(len - 1));
			res = sb;
		}

		return res.toString();
	}
}
