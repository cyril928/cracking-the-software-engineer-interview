package must.win.leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

// Maximum Depth of Binary Tree
/*
 * http://n00tc0d3r.blogspot.com/2013/04/maximum-depth-of-binary-tree.html?q=Maximum+Depth+of+Binary+Tree
 * dfs, This algorithm touch each node once and thus runs in time O(n), where n is the total number of nodes in the tree. 
 * It requires O(h) space for recursion, where h is the height of the tree. Worst case -> unbalanced tree causes O(N) space
 * 
 * bfs
 * this algorithm runs in time O(n). Since we keep all nodes in a level in the queue, in worst case, it requires O(n) spaces. 
 * (Think about a full binary tree with n/2 nodes at bottom level.)
 */
public class MaximumDepthofBinaryTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// dfs
	public int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
	}

	// bfs
	public int maxDepth1(TreeNode root) {
		if (root == null) {
			return 0;
		}

		int depth = 0;
		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);

		while (!queue.isEmpty()) {
			depth++;
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.remove();
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
			}
		}
		return depth;
	}
}
