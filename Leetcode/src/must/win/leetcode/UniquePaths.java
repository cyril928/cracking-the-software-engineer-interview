package must.win.leetcode;

// Unique Paths
/*
 * http://n00tc0d3r.blogspot.tw/search?q=UNIQUE+PATH
 * O(min(m, n)) space DP
 * 
 * http://answer.ninechapter.com/solutions/unique-paths/
 * O(MN) space DP
 */

public class UniquePaths {

	// O(min(m, n)) space DP
	public int uniquePaths(int m, int n) {
		if (m == 0 || n == 0) {
			return 0;
		}

		int x = Math.max(m, n);
		int y = x ^ m ^ n;

		int[] result = new int[y];

		result[0] = 1;

		for (int i = 0; i < x; i++) {
			for (int j = 1; j < y; j++) {
				result[j] += result[j - 1];
			}
		}
		return result[y - 1];
	}

	// math
	public int uniquePaths1(int m, int n) {
		if (m == 0 || n == 0) {
			return 0;
		}

		int x = Math.min(m, n);
		double count = 1;
		for (int i = 0; i < x - 1; i++) {
			count *= (m + n - 2 - i);
			count /= (i + 1);
		}
		return (int) count;
	}
}
