package must.win.leetcode;

// Intersection of Two Linked Lists 
/*
 * http://www.cnblogs.com/yuzhangcmu/p/4128794.html
 * implementation & explanation
 * use lists' lengthen difference (O(n) time and O(1) space)
 * Two pointer solution (O(n) time and O(1) space)
 * 
 * https://oj.leetcode.com/problems/intersection-of-two-linked-lists/solution/
 * solution discussion
 * 
 * Notes:
 * 1. If the two linked lists have no intersection at all, return null.
 * 2. The linked lists must retain their original structure after the function returns.
 * 3. You may assume there are no cycles anywhere in the entire linked structure.
 * 4. Your code should preferably run in O(n) time and use only O(1) memory.
 */
public class IntersectionofTwoLinkedLists {
	// use lists' lengthen difference
	public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
		if (headA == null || headB == null) {
			return null;
		}
		int lenA = getLen(headA);
		int lenB = getLen(headB);

		while (lenA > lenB) {
			headA = headA.next;
			lenA--;
		}

		while (lenB > lenA) {
			headB = headB.next;
			lenB--;
		}

		while (headA != headB) {
			headA = headA.next;
			headB = headB.next;
		}

		return headA;
	}

	// Two pointer solution
	public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
		if (headA == null || headB == null) {
			return null;
		}

		ListNode curA = headA;
		ListNode curB = headB;

		while (curA != curB) {
			curA = curA.next;
			curB = curB.next;
			if (curA == null && curB == null) {
				return null;
			} else if (curA == null) {
				curA = headB;
			} else if (curB == null) {
				curB = headA;
			}
		}
		return curA;
	}

	// use lists' lengthen difference helper function
	private int getLen(ListNode head) {
		int len = 0;
		while (head != null) {
			len++;
			head = head.next;
		}
		return len;
	}
}
