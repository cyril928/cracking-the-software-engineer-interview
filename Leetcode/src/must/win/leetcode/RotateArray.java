package must.win.leetcode;

// Rotate Array
/*
 * 
 */
public class RotateArray {
	private void reverseArray(int[] A, int s, int e) {
		while (s < e) {
			int temp = A[s];
			A[s++] = A[e];
			A[e--] = temp;
		}
	}

	public void rotate(int[] nums, int k) {
		if (nums == null || nums.length == 0) {
			throw new IllegalArgumentException("");
		}
		k = k % nums.length;
		reverseArray(nums, 0, nums.length - 1);
		reverseArray(nums, 0, k - 1);
		reverseArray(nums, k, nums.length - 1);

	}
}
