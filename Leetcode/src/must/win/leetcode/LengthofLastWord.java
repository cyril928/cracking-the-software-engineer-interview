package must.win.leetcode;

// Length of Last Word
/*
 * http://n00tc0d3r.blogspot.tw/2013/03/length-of-last-word.html?q=Length+of+Last+Word
 * explanation and implementation
 */
public class LengthofLastWord {
	public int lengthOfLastWord(String s) {
		if (s == null) {
			return 0;
		}
		int ptr = s.length() - 1;
		while (ptr >= 0 && s.charAt(ptr) == ' ') {
			ptr--;
		}
		int len = 0;
		while (ptr >= 0 && s.charAt(ptr--) != ' ') {
			len++;
		}
		return len;
	}
}
