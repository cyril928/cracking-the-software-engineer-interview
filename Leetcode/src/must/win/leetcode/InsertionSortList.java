package must.win.leetcode;

// Insertion Sort List 
/*
 * http://www.ninechapter.com/solutions/insertion-sort-list/
 * with dummy node, most clean way
 */
public class InsertionSortList {

	// with dummy node, most clean way
	public ListNode insertionSortList(ListNode head) {
		ListNode dummy = new ListNode(0);
		while (head != null) {
			ListNode cur = dummy;
			while (cur.next != null && head.val > cur.next.val) {
				cur = cur.next;
			}
			ListNode next = head.next;
			head.next = cur.next;
			cur.next = head;
			head = next;
		}
		return dummy.next;
	}

	// my original way
	public ListNode insertionSortList1(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode comparePrevNode = head;
		ListNode compareCurNode = head.next;
		while (compareCurNode != null) {
			ListNode prev = null;
			ListNode cur = head;
			while (cur != compareCurNode) {
				if (compareCurNode.val < cur.val) {
					comparePrevNode.next = compareCurNode.next;
					if (prev != null) {
						prev.next = compareCurNode;
					} else {
						head = compareCurNode;
					}
					compareCurNode.next = cur;
					compareCurNode = comparePrevNode;
					break;
				}
				prev = cur;
				cur = cur.next;
			}
			comparePrevNode = compareCurNode;
			compareCurNode = compareCurNode.next;
		}
		return head;
	}
}

class ListNode {
	int val;
	ListNode next;

	ListNode(int x) {
		val = x;
		next = null;
	}
}
