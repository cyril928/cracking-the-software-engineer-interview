package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Missing Ranges
/*
 * Clean Code Hand Book
 */
public class MissingRanges {
	public List<String> findMissingRanges(int[] vals, int start, int end) {
		List<String> ranges = new ArrayList<String>();
		if (vals == null || vals.length == 0) {
			ranges.add(getRange(start, end));
			return ranges;
		}
		int prev = start - 1;
		for (int i = 0; i <= vals.length; i++) {
			int cur = (i == vals.length) ? end + 1 : vals[i];
			if (cur - prev > 1) {
				ranges.add(getRange(prev + 1, cur - 1));
			}
			prev = cur;
		}
		return ranges;
	}

	private String getRange(int from, int to) {
		return (from == to) ? String.valueOf(from) : from + "->" + to;
	}
}
