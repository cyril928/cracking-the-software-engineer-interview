package must.win.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// Binary Tree Level Order Traversal
/*
 * http://www.ninechapter.com/solutions/binary-tree-level-order-traversal/
 * using level size, one queue
 * 
 * http://n00tc0d3r.blogspot.com/2013/01/binary-tree-traversals-ii.html?q=Binary+Tree+Level+Order+Traversal
 * dummy null node, one queue
 */
public class BinaryTreeLevelOrderTraversal {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// using level size, one queue
	public List<List<Integer>> levelOrder(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}

		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		while (!queue.isEmpty()) {
			List<Integer> level = new ArrayList<Integer>();
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.remove();
				level.add(cur.val);
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
			}
			result.add(level);
		}

		return result;
	}

	// dummy null node, one queue
	public List<List<Integer>> levelOrder1(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}

		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);

		List<Integer> level = new ArrayList<Integer>();

		while (!queue.isEmpty()) {
			TreeNode curNode = queue.poll();
			if (curNode == null) {
				result.add(level);
				level = new ArrayList<Integer>();
				if (!queue.isEmpty()) {
					queue.add(null);
				}
			} else {
				level.add(curNode.val);
				if (curNode.left != null) {
					queue.add(curNode.left);
				}
				if (curNode.right != null) {
					queue.add(curNode.right);
				}
			}
		}

		return result;
	}
}
