package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

// Substring with Concatenation of All Words
/*
 * From Zouyou 
 * Use Queue to reuse the processed substring match
 * 
 * 
 * http://answer.ninechapter.com/solutions/substring-with-concatenation-of-all-words/
 * Two HashMap (to record L string frequency) solution
 * len : S length
 * n : L string number
 * m : L string length
 * Space O(n)
 * Time O((len - nm)*n)
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Substring+with+Concatenation+of+All+Words
 * Another Two HashMap implementation, the general idea is the same, but the counter logic in L string frequency is 
 * a bit different.
 * 
 * To-do (hasn't understand yet)
 * KMP string matching algorithm. 
 * Instead of restart from beginning, forward the begin pointer to next possible place.
 *  
 * 
 */
public class SubstringwithConcatenationofAllWords {
	// Use Queue to reuse the processed substring match
	public List<Integer> findSubstring(String S, String[] L) {
		List<Integer> res = new ArrayList<Integer>();
		if (S == null || L == null || L.length == 0
				|| S.length() < L[0].length() * L.length) {
			return res;
		}

		Map<String, Integer> dict = new HashMap<String, Integer>();
		for (String str : L) {
			int count = dict.containsKey(str) ? dict.get(str) : 0;
			dict.put(str, count + 1);
		}

		int wordLen = L[0].length();
		int sLen = S.length();
		int n = L.length;

		for (int i = 0; i < wordLen; i++) {
			Map<String, Integer> toFind = new HashMap<String, Integer>(dict);
			Queue<String> queue = new LinkedList<String>();
			int leftWordNum = toFind.size();
			for (int j = i; j <= sLen - wordLen; j += wordLen) {
				String subStr = S.substring(j, j + wordLen);
				queue.add(subStr);
				if (toFind.containsKey(subStr)) {
					if (toFind.get(subStr) == 1) {
						leftWordNum--;
					}
					toFind.put(subStr, toFind.get(subStr) - 1);
				}
				if (queue.size() == n) {
					if (leftWordNum == 0) {
						res.add(j - (n - 1) * wordLen);
					}
					String discardStr = queue.poll();
					if (toFind.containsKey(discardStr)) {
						if (toFind.get(discardStr) == 0) {
							leftWordNum++;
						}
						toFind.put(discardStr, toFind.get(discardStr) + 1);
					}
				}
			}
		}
		return res;
	}

	// first Two HashMap (to record L string frequency) solution
	public List<Integer> findSubstring1(String S, String[] L) {
		List<Integer> result = new ArrayList<Integer>();
		if (S == null || L.length == 0 || S.length() < L.length * L[0].length()) {
			return result;
		}
		int len = S.length();
		int n = L.length;
		int m = L[0].length();

		Map<String, Integer> toFind = new HashMap<String, Integer>();
		Map<String, Integer> found = new HashMap<String, Integer>();

		for (int i = 0; i < n; i++) {
			if (toFind.containsKey(L[i])) {
				toFind.put(L[i], toFind.get(L[i]) + 1);
			} else {
				toFind.put(L[i], 1);
			}
		}

		for (int i = 0; i <= len - n * m; i++) {
			found.clear();
			int j = 0;
			for (j = 0; j < n * m; j += m) {
				String candidateS = S.substring(i + j, i + j + m);
				if (!toFind.containsKey(candidateS)) {
					break;
				}
				if (found.containsKey(candidateS)) {
					if (found.get(candidateS) == toFind.get(candidateS)) {
						break;
					}
					found.put(candidateS, found.get(candidateS) + 1);
				} else {
					found.put(candidateS, 1);
				}
			}
			if (j == n * m) {
				result.add(i);
			}
		}
		return result;
	}

	// second Two HashMap (to record L string frequency) solution
	public List<Integer> findSubstring2(String S, String[] L) {
		List<Integer> result = new ArrayList<Integer>();
		if (S == null || L.length == 0 || S.length() < L.length * L[0].length()) {
			return result;
		}
		int len = S.length();
		int n = L.length;
		int m = L[0].length();

		Map<String, Integer> record = new HashMap<String, Integer>();

		// build L string frequency map
		for (String s : L) {
			if (record.containsKey(s)) {
				record.put(s, record.get(s) + 1);
			} else {
				record.put(s, 1);
			}
		}

		// find concatenation
		for (int i = 0; i <= len - n * m; i++) {
			Map<String, Integer> toFind = new HashMap<String, Integer>(record);
			int j = 0;
			for (j = 0; j < n; j++) {
				int k = i + j * m;
				String stub = S.substring(k, k + m);
				if (toFind.containsKey(stub)) {
					toFind.put(stub, toFind.get(stub) - 1);
					if (toFind.get(stub) == 0) {
						toFind.remove(stub);
					}
				} else {
					break;
				}
			}
			/*
			 * if(j == n) { result.add(i); }
			 */
			if (toFind.isEmpty()) {
				result.add(i);
			}
		}
		return result;
	}
}
