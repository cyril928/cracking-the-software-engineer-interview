package must.win.leetcode;

// Linked List Cycle II 
/*
 * http://www.ninechapter.com/solutions/linked-list-cycle-ii/
 * but my way is better for cleaner code
 */
public class LinkedListCycleII {

	// my way
	public ListNode detectCycle(ListNode head) {
		ListNode fastPtr = head;
		ListNode slowPtr = head;
		while (fastPtr != null && fastPtr.next != null) {
			fastPtr = fastPtr.next.next;
			slowPtr = slowPtr.next;
			if (slowPtr == fastPtr) {
				slowPtr = head;
				while (fastPtr != slowPtr) {
					fastPtr = fastPtr.next;
					slowPtr = slowPtr.next;
				}
				return fastPtr;
			}
		}
		return null;
	}
}
