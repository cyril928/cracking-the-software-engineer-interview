package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Letter Combinations of a Phone Number 
/*
 * http://www.ninechapter.com/solutions/letter-combinations-of-a-phone-number/
 * recursive
 * 
 * http://n00tc0d3r.blogspot.com/search?q=Letter+Combinations+of+a+Phone+Number
 * iterative, not understand yet
 */
public class LetterCombinationsofaPhoneNumber {
	// recursive
	public List<String> letterCombinations(String digits) {
		ArrayList<String> result = new ArrayList<String>();
		if (digits == null) {
			return result;
		}

		Map<Character, char[]> map = new HashMap<Character, char[]>();
		map.put('0', new char[] {});
		map.put('1', new char[] {});
		map.put('2', new char[] { 'a', 'b', 'c' });
		map.put('3', new char[] { 'd', 'e', 'f' });
		map.put('4', new char[] { 'g', 'h', 'i' });
		map.put('5', new char[] { 'j', 'k', 'l' });
		map.put('6', new char[] { 'm', 'n', 'o' });
		map.put('7', new char[] { 'p', 'q', 'r', 's' });
		map.put('8', new char[] { 't', 'u', 'v' });
		map.put('9', new char[] { 'w', 'x', 'y', 'z' });

		StringBuilder sb = new StringBuilder();
		letterCombinationsHelper(map, digits, sb, result);
		return result;
	}

	// iterative
	public List<String> letterCombinations1(String digits) {
		List<String> res = new ArrayList<String>();
		if (digits == null) {
			return res;
		}
		Map<Character, char[]> map = new HashMap<Character, char[]>();
		map.put('2', new char[] { 'a', 'b', 'c' });
		map.put('3', new char[] { 'd', 'e', 'f' });
		map.put('4', new char[] { 'g', 'h', 'i' });
		map.put('5', new char[] { 'j', 'k', 'l' });
		map.put('6', new char[] { 'm', 'n', 'o' });
		map.put('7', new char[] { 'p', 'q', 'r', 's' });
		map.put('8', new char[] { 't', 'u', 'v' });
		map.put('9', new char[] { 'w', 'x', 'y', 'z' });

		int len = digits.length();
		List<StringBuilder> builders = new ArrayList<StringBuilder>();
		builders.add(new StringBuilder());
		int curSize = 0;
		for (int i = 0; i < len; i++) {
			char[] chars = map.get(digits.charAt(i));
			// get the size of previous builders set
			curSize = builders.size();
			for (int j = 0; j < chars.length; j++) {
				for (int k = 0; k < curSize; k++) {
					StringBuilder sb = builders.get(k);
					// for last character's combination, just need to append to
					// previous builders set
					if (j == chars.length - 1) {
						sb.append(chars[j]);
					} else {
						StringBuilder newSb = new StringBuilder(sb.toString());
						newSb.append(chars[j]);
						builders.add(newSb);
					}
				}
			}
		}

		for (StringBuilder sb : builders) {
			res.add(sb.toString());
		}
		return res;
	}

	// recursive helper function
	private void letterCombinationsHelper(Map<Character, char[]> map,
			String digits, StringBuilder sb, ArrayList<String> result) {
		if (digits.length() == sb.length()) {
			result.add(sb.toString());
			return;
		}
		char c = digits.charAt(sb.length());
		for (char letter : map.get(c)) {
			sb.append(letter);
			letterCombinationsHelper(map, digits, sb, result);
			sb.deleteCharAt(sb.length() - 1);
		}
	}
}
