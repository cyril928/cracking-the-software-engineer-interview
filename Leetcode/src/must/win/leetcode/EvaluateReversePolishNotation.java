package must.win.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

//Evaluate Reverse Polish Notation 
/*
 * https://oj.leetcode.com/problems/evaluate-reverse-polish-notation/solution/
 * most clean way, create HashSet from Arrays.asList(); 
 */

public class EvaluateReversePolishNotation {

	// most clean way helper function
	private int eval(int x, int y, String operator) {
		switch (operator) {
		case "+":
			return x + y;
		case "-":
			return x - y;
		case "*":
			return x * y;
		default:
			return x / y;
		}
	}

	// most clean way, create HashSet from Arrays.asList();
	public int evalRPN(String[] tokens) {
		Set<String> set = new HashSet<String>(Arrays.asList("+", "-", "*", "/"));
		Stack<Integer> stack = new Stack<Integer>();

		for (String token : tokens) {
			if (set.contains(token)) {
				int y = stack.pop();
				int x = stack.pop();
				int val = eval(x, y, token);
				stack.push(val);
			} else {
				stack.push(Integer.parseInt(token));
			}
		}
		return stack.pop();
	}

	// my own way, not clean enough
	public int evalRPN1(String[] tokens) {
		if (tokens.length == 0) {
			return 0;
		}
		List<String> operators = Arrays.asList("+", "-", "*", "/");
		Stack<Integer> st = new Stack<Integer>();
		for (int i = 0; i < tokens.length; i++) {
			if (operators.contains(tokens[i])) {
				int a = st.pop();
				int b = st.pop();
				st.push(operate(b, a, tokens[i]));
			} else {
				st.push(Integer.parseInt(tokens[i]));
			}
		}
		return st.pop();
	}

	// use "str".equals(str) not "str"==str
	public int operate(int a, int b, String operator) {
		int res = 0;
		switch (operator) {
		case "+":
			res = a + b;
			break;
		case "-":
			res = a - b;
			break;
		case "*":
			res = a * b;
			break;
		case "/":
			res = a / b;
			break;
		}
		return res;
	}
}
