package must.win.leetcode;

import java.util.Arrays;

// Candy 
/*
 * http://www.ninechapter.com/solutions/candy/
 * most clean way, 1 O(N) space, and 2 pass
 * 
 * forward and backward
 */
public class Candy {
	// most clean way, 1 O(N) space, and 2 pass
	public int candy(int[] ratings) {
		if (ratings == null || ratings.length == 0) {
			return 0;
		}
		int len = ratings.length;
		int[] count = new int[len];
		Arrays.fill(count, 1);

		// forward incremental
		for (int i = 1; i < len; i++) {
			if (ratings[i] > ratings[i - 1]) {
				count[i] = count[i - 1] + 1;
			}
		}

		int sum = count[len - 1];
		// backward incremental
		for (int i = len - 2; i >= 0; i--) {
			if (ratings[i] > ratings[i + 1]) {
				if (count[i] <= count[i + 1]) {
					count[i] = count[i + 1] + 1;
				}
			}
			sum += count[i];
		}
		return sum;
	}

	// 2 O(N) space, and 3 pass
	public int candy1(int[] ratings) {
		int len = ratings.length;
		int[] lc = new int[len];
		int[] rc = new int[len];
		Arrays.fill(lc, 1);
		Arrays.fill(rc, 1);

		for (int i = 0; i < len - 1; i++) {
			if (ratings[i] < ratings[i + 1]) {
				lc[i + 1] = lc[i] + 1;
			}
		}

		for (int j = len - 1; j > 0; j--) {
			if (ratings[j - 1] > ratings[j]) {
				rc[j - 1] = rc[j] + 1;
			}
		}
		int res = 0;
		for (int k = 0; k < len; k++) {
			res += Math.max(lc[k], rc[k]);
		}
		return res;
	}
}
