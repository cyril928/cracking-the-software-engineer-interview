package must.win.leetcode;

// Compare Version Numbers 
/*
 * http://blog.csdn.net/xudli/article/details/42081113
 * implementation, most clean
 * 
 * use "." to do string split in java, be careful of that '.' is special character, so 
 * we have to use "\\." in split, just like split("\\.");
 */
public class CompareVersionNumbers {
	// my own implementation
	public int compareVersion(String version1, String version2) {
		String[] strs1 = version1.split("\\.");
		String[] strs2 = version2.split("\\.");

		int len1 = strs1.length;
		int len2 = strs2.length;
		int len = Math.min(len1, len2);
		for (int i = 0; i < len; i++) {
			int val1 = Integer.parseInt(strs1[i]);
			int val2 = Integer.parseInt(strs2[i]);
			if (val1 > val2) {
				return 1;
			} else if (val1 < val2) {
				return -1;
			}
		}

		if (len1 > len2) {
			for (int i = len2; i < len1; i++) {
				if (Integer.parseInt(strs1[i]) > 0) {
					return 1;
				}
			}
		} else if (len2 > len1) {
			for (int i = len1; i < len2; i++) {
				if (Integer.parseInt(strs2[i]) > 0) {
					return -1;
				}
			}
		}
		return 0;
	}
}
