package must.win.leetcode;

// Rotate List 
/*	
 * http://answer.ninechapter.com/solutions/rotate-list/
 * another way with dummy node
 */
public class RotateList {
	private int getLength(ListNode head) {
		int len = 0;
		while (head != null) {
			head = head.next;
			len++;
		}
		return len;
	}

	// another way with dummy node
	public ListNode rotateRight(ListNode head, int n) {
		if (head == null || n == 0) {
			return head;
		}
		int length = getLength(head);

		n = n % length;

		ListNode dummy = new ListNode(0);
		dummy.next = head;
		head = dummy;
		ListNode tail = dummy;

		for (int i = 0; i < n; i++) {
			head = head.next;
		}

		while (head.next != null) {
			head = head.next;
			tail = tail.next;
		}
		head.next = dummy.next;
		head = tail.next;
		tail.next = null;
		return head;
	}

	// by myself
	public ListNode rotateRight1(ListNode head, int n) {
		if (head == null || n == 0) {
			return head;
		}
		int length = getLength(head);

		n = n % length;
		if (n == 0) {
			return head;
		}
		n = length - n;

		ListNode cur = head;
		ListNode prevHead = null;
		while (n > 0) {
			prevHead = cur;
			cur = cur.next;
			n--;
		}

		ListNode newHead = cur;
		while (cur.next != null) {
			cur = cur.next;
		}

		cur.next = head;
		prevHead.next = null;

		return newHead;
	}
}
