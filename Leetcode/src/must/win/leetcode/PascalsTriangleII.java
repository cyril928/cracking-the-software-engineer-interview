package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Pascal's Triangle II 
/*
 * http://n00tc0d3r.blogspot.tw/2013/05/pascals-triangle.html?q=Triangle  
 * last one implementation  
 */
public class PascalsTriangleII {

	// clean implementation
	public List<Integer> getRow(int rowIndex) {
		List<Integer> res = new ArrayList<Integer>();
		if (rowIndex < 0) {
			return res;
		}

		for (int i = 0; i <= rowIndex; i++) {
			res.add(1);
			for (int j = i - 1; j > 0; j--) {
				res.set(j, res.get(j) + res.get(j - 1));
			}
		}
		return res;
	}

	// clean implementation, too
	public List<Integer> getRow1(int rowIndex) {
		if (rowIndex < 0) {
			return null;
		}
		Integer[] result = new Integer[rowIndex + 1];
		// if we do the calculation backwards for each row, we can do it in
		// place without any extra spaces.
		for (int i = 0; i <= rowIndex; i++) {
			result[0] = 1;
			for (int j = i; j > 0; j--) {
				int val = result[j - 1];
				if (j < i) {
					val += result[j];
				}
				result[j] = val;
			}
		}
		return new ArrayList<Integer>(Arrays.asList(result));
	}
}
