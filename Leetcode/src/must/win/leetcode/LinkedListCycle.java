package must.win.leetcode;

// Linked List Cycle
/*
 * http://www.ninechapter.com/solutions/linked-list-cycle/
 * but my way is better for cleaner code
 */
public class LinkedListCycle {

	// my way
	public boolean hasCycle(ListNode head) {
		ListNode fastNode = head;
		ListNode slowNode = head;

		while (fastNode != null && fastNode.next != null) {
			fastNode = fastNode.next.next;
			slowNode = slowNode.next;
			if (fastNode == slowNode) {
				return true;
			}
		}
		return false;
	}
}
