package must.win.leetcode;

// Distinct Subsequences 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Distinct+Subsequences 
 * DP implementation
 */
public class DistinctSubsequences {
	// O(N) space DP // optimal solution
	public int numDistinct(String S, String T) {
		int sLen = S.length();
		int tLen = T.length();
		if (sLen <= 0 || tLen <= 0 || sLen < tLen) {
			return 0;
		}
		int[] result = new int[tLen];
		for (int i = 0; i < sLen; i++) {
			for (int j = Math.min(tLen - 1, i); j >= 0; j--) {
				if (S.charAt(i) == T.charAt(j)) {
					result[j] += (j == 0) ? 1 : result[j - 1];
				}
			}
		}
		return result[tLen - 1];
	}

	// recursive version, will TLE
	public int numDistinct1(String S, String T) {
		if (T.isEmpty()) {
			return 1;
		}
		if (S.isEmpty()) {
			return 0;
		}
		int sum = 0;
		if (S.charAt(0) == T.charAt(0)) {
			sum = numDistinct(S.substring(1), T.substring(1));
		}
		sum += numDistinct(S.substring(1), T);
		return sum;
	}

	// O(N*M) space DP
	public int numDistinct2(String S, String T) {
		int sLen = S.length();
		int tLen = T.length();
		if (sLen <= 0 || tLen <= 0 || sLen < tLen) {
			return 0;
		}
		int[][] result = new int[tLen][sLen];
		if (S.charAt(0) == T.charAt(0)) {
			result[0][0] = 1;
		}
		for (int i = 0; i < tLen; i++) {
			for (int j = 1; j < sLen; j++) {
				result[i][j] = result[i][j - 1];
				if (S.charAt(j) == T.charAt(i)) {
					if (i == 0) {
						result[i][j] += 1;
					} else {
						result[i][j] = result[i][j - 1] + result[i - 1][j - 1];
					}
				}
			}
		}
		return result[tLen - 1][sLen - 1];
	}
}
