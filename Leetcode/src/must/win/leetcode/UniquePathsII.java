package must.win.leetcode;

// Unique Paths II 
/* http://n00tc0d3r.blogspot.tw/search?q=UNIQUE+PATH
 * O(min(m, n)) space DP, 
 * math turning point C(m+n-2, n-1) 
 * 
 * http://answer.ninechapter.com/solutions/unique-paths-ii/
 * O(MN) space DP
 */
public class UniquePathsII {
	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
		if (obstacleGrid == null || obstacleGrid.length == 0) {
			return 0;
		}
		int row = obstacleGrid.length;
		int col = obstacleGrid[0].length;

		int[] result = new int[col];

		result[0] = 1;
		for (int i = 0; i < row; i++) {
			int total = 0;
			for (int j = 0; j < col; ++j) {
				if (obstacleGrid[i][j] == 1) {
					result[j] = 0;
				} else if (j > 0) {
					result[j] += result[j - 1];
				}
				total += result[j];
			}
			// total is sum of a level
			if (total == 0) {
				return 0;
			}
		}

		return result[col - 1];
	}
}
