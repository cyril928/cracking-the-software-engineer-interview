package must.win.leetcode;

// Remove Duplicates from Sorted List II 
/*
 * http://mattcb.blogspot.tw/search?q=Remove+Duplicates+from+Sorted+List+II
 * implementation
 */
public class RemoveDuplicatesfromSortedListII {
	public ListNode deleteDuplicates(ListNode head) {
		ListNode newNode = new ListNode(0);
		ListNode prevNode = newNode;
		ListNode curNode = head;
		newNode.next = curNode;

		while (curNode != null && curNode.next != null) {
			ListNode nextNode = curNode.next;
			if (curNode.val == nextNode.val) {
				while (nextNode != null && curNode.val == nextNode.val) {
					nextNode = nextNode.next;
				}
				prevNode.next = nextNode;
			} else {
				prevNode = curNode;
			}
			curNode = nextNode;
		}
		return newNode.next;
	}
}
