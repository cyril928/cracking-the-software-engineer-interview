package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// N-Queens 
/* http://www.ninechapter.com/solutions/n-queens/
 * use ArrayList to store position information
 * 
 * http://edwardliwashu.blogspot.com/2013/01/n-queens.html?q=N-Queens
 * use Array to store position information
 */
public class NQueens {
	private String[] formatSol(int[] pos) {
		String[] res = new String[pos.length];
		for (int i = 0; i < pos.length; i++) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < pos.length; j++) {
				String val = (pos[i] == j) ? "Q" : ".";
				sb.append(val);
			}
			res[i] = sb.toString();
		}
		return res;
	}

	private boolean isValid(int[] pos, int depth, int col) {
		for (int i = 0; i < depth; i++) {
			if (pos[i] == col || (depth - i) == Math.abs(col - pos[i])) {
				return false;
			}
		}
		return true;
	}

	private void nQueens(ArrayList<String[]> res, int[] pos, int depth, int n) {
		if (depth == n) {
			String[] sol = formatSol(pos);
			res.add(sol);
		} else {
			for (int i = 0; i < n; i++) {
				if (isValid(pos, depth, i)) {
					pos[depth] = i;
					nQueens(res, pos, depth + 1, n);
				}
			}
		}
	}

	public List<String[]> solveNQueens(int n) {
		ArrayList<String[]> res = new ArrayList<String[]>();
		if (n <= 0) {
			return res;
		}
		int[] position = new int[n];
		nQueens(res, position, 0, n);
		return res;
	}
}
