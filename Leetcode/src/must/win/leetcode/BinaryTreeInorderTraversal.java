package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// Binary Tree Inorder Traversal
/*
 * http://n00tc0d3r.blogspot.com/2013/01/binary-tree-traversals-i.html
 * iterative, Since we visit each node once, this algorithm runs in time O(n) using O(h) 
 * (worst-case O(n)) spaces, where h is the depth of the tree.
 * 
 * 
 * iterative and recursive, both methods requires O(h) space, 
 * where h is the height of the tree. That said, the worst case space complexity can be O(n).
 */
public class BinaryTreeInorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// iterative, using one stack (code is cleaner)
	public List<Integer> inorderTraversal(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode cur = root;
		// find the left-most node
		while (cur != null) {
			stack.push(cur);
			cur = cur.left;
		}
		while (!stack.empty()) {
			cur = stack.pop();
			// visit
			result.add(cur.val);
			// add the right child
			cur = cur.right;
			// find the left-most node
			while (cur != null) {
				stack.push(cur);
				cur = cur.left;
			}
		} // while-stack-empty
		return result;
	}

	// iterative, my own way using one stack
	public List<Integer> inorderTraversal1(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode cur = root;
		stack.push(cur);
		while (!stack.isEmpty()) {
			// find the left-most node
			while (cur.left != null) {
				stack.push(cur.left);
				cur = cur.left;
			}

			// pop it out
			do {
				cur = stack.pop();
				result.add(cur.val);
			} while (!stack.isEmpty() && cur.right == null);

			// if current node has right node
			if (cur.right != null) {
				stack.push(cur.right);
				cur = cur.right;
			}
		}

		return result;
	}

	// recursive
	public List<Integer> inorderTraversal2(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		traversalRecursive(root, result);
		return result;
	}

	// for recursive way
	private void traversalRecursive(TreeNode root, List<Integer> result) {
		if (root == null) {
			return;
		}
		traversalRecursive(root.left, result);
		result.add(root.val);
		traversalRecursive(root.right, result);
	}
}
