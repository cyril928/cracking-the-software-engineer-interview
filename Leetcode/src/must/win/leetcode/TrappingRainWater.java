package must.win.leetcode;

import java.util.Stack;

// Trapping Rain Water 
/*
 * http://answer.ninechapter.com/solutions/trapping-rain-water/
 * iterative solution implementation, runs in time O(n) and takes O(1) space. very tricky, but clean
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Trapping+Rain+Water
 * iterative explanation, (implementation a little bit messy), 
 * stack solution, runs in time O(n) and takes O(n) space.
 * 
 * My own iteration, run in O(N) time and O(N) space
 * find the right side max height and left side max height
 */
public class TrappingRainWater {

	// iterative solution implementation, runs in time O(n) and takes O(1)
	// space. very tricky, but clean
	public int trap(int[] A) {
		if (A == null || A.length <= 2) {
			return 0;
		}

		int len = A.length;
		int sum = 0;
		int max = -1;
		int maxIndex = -1;
		int prev;

		// find the highest bar
		for (int i = 0; i < len; i++) {
			if (A[i] > max) {
				max = A[i];
				maxIndex = i;
			}
		}

		// process all bars left to the highest bar
		prev = 0;
		for (int i = 0; i < maxIndex; i++) {
			if (A[i] > prev) {
				sum += (A[i] - prev) * (maxIndex - i);
				prev = A[i];
			}
			sum -= A[i];
		}

		// process all bars right to the highest bar
		prev = 0;
		for (int i = len - 1; i > maxIndex; i--) {
			if (A[i] > prev) {
				sum += (A[i] - prev) * (i - maxIndex);
				prev = A[i];
			}
			sum -= A[i];
		}

		return sum;

	}

	// stack solution, runs in time O(n) and takes O(n) space.
	// 1. Use Stack to store the index of a bar;
	// 2. If the current one is smaller or equal then the top of the stack, push
	// it to
	// stack;
	// 3. Otherwise, pop up the top until stack is empty or top is greater than
	// the current one, add up the volume, push the current one to stack.
	public int trap1(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		Stack<Integer> stack = new Stack<Integer>();
		int sum = 0;
		for (int i = 0; i < A.length; i++) {
			while (!stack.empty() && A[i] > A[stack.peek()]) {
				int j = stack.pop();
				int hight = stack.empty() ? 0 : Math.min(A[i], A[stack.peek()])
						- A[j];
				int width = stack.empty() ? 0 : i - stack.peek() - 1;
				sum += hight * width;
			}
			stack.push(i);
		}
		return sum;
	}

	// my own iterative way, runs in time O(n) and takes O(N) space.
	public int trap2(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int[] rightMaxH = new int[A.length];
		for (int i = A.length - 1; i >= 0; i--) {
			rightMaxH[i] = (i == A.length - 1) ? A[i] : Math.max(A[i],
					rightMaxH[i + 1]);
		}

		int totalTrap = 0;
		int leftMaxH = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i] > leftMaxH) {
				leftMaxH = A[i];
			}

			int minH = Math.min(leftMaxH, rightMaxH[i]);
			totalTrap += (minH - A[i]);
		}
		return totalTrap;
	}
}
