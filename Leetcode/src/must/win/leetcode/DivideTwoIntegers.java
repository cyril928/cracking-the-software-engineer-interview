package must.win.leetcode;

// Divide Two Integers 
/*
 * http://answer.ninechapter.com/solutions/divide-two-integers/
 * O(logn) solution, implementation
 * 
 * http://n00tc0d3r.blogspot.tw/2013/02/divide-two-integers.html?q=Divide+Two+Integers
 * O(logn) solution, explanation
 */
public class DivideTwoIntegers {

	// most clena implementation
	public int divide(int dividend, int divisor) {
		boolean negative = false;
		if ((dividend > 0) ^ (divisor > 0)) {
			negative = true;
		}

		long a = Math.abs((long) dividend);
		long b = Math.abs((long) divisor);
		long ans = 0;
		while (a >= b) {
			int shift = 0;
			while ((b << shift) <= a) {
				shift++;
			}
			ans += (long) 1 << (shift - 1);
			a -= b << (shift - 1);
		}
		if (ans > Integer.MAX_VALUE && !negative) {
			return Integer.MAX_VALUE;
		}
		return negative ? -(int) ans : (int) ans;
	}
}
