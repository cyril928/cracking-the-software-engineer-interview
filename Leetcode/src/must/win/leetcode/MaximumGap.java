package must.win.leetcode;

// Maximum Gap 
/*
 * bucket sort
 * O(N) Time, O(N) Space
 * 
 * https://oj.leetcode.com/problems/maximum-gap/solution/
 * official explanation
 * 
 * http://leetcode.tgic.me/maximum-gap/index.html
 * implementation
 */
public class MaximumGap {
	class Bucket {
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;

		void add(int n) {
			max = Math.max(n, max);
			min = Math.min(n, min);
		}
	}

	public int maximumGap(int[] num) {
		if (num == null || num.length < 2) {
			return 0;
		}

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for (int val : num) {
			max = Math.max(val, max);
			min = Math.min(val, min);
		}

		int len = num.length; // also is at most bucket number
		int gap = ((max - min) / (len - 1))
				+ (((max - min) % (len - 1) > 0) ? 1 : 0);
		Bucket[] buckets = new Bucket[len];

		// add number into target bucket
		for (int val : num) {
			int index = (val - min) / gap;
			if (buckets[index] == null) {
				buckets[index] = new Bucket();
			}
			buckets[index].add(val);
		}

		// since two numbers in the same bucket can't form max gap, we only
		// consider
		// buckets[i + 1].min - buckets[i].max
		int maxGap = gap;
		int prevMax = buckets[0].max;
		for (int i = 1; i < len; i++) {
			if (buckets[i] != null) {
				maxGap = Math.max(buckets[i].min - prevMax, maxGap);
				prevMax = buckets[i].max;
			}
		}

		return maxGap;
	}
}
