package must.win.leetcode;

// Binary Tree Upside Down
/*
 * Clean Code Hand Book
 * top-down approach
 * bottom-up approach
 */
public class BinaryTreeUpsideDown {
	class TreeNode {
		int val;
		TreeNode left, right;

		TreeNode(int val) {
			this.val = val;
		}
	}

	// bottom-up approach helper
	private TreeNode dfsUpsideDown(TreeNode child, TreeNode parent) {
		if (child == null) {
			return parent;
		}
		TreeNode root = dfsUpsideDown(child.left, child);
		child.left = (parent == null) ? parent : parent.right;
		child.right = parent;
		return root;
	}

	// top-down approach (like reverse linked list)
	public TreeNode upsideDownBinaryTree(TreeNode root) {
		TreeNode parent = null, child = root, rightNode = null;
		while (child != null) {
			TreeNode leftNode = child.left;
			child.left = rightNode;
			rightNode = child.right;
			child.right = parent;
			parent = child;
			child = leftNode;
		}

		return parent;
	}

	// bottom-up approach, recursive, more clean
	public TreeNode upsideDownBinaryTree1(TreeNode root) {
		if (root == null || root.left == null) {
			return root;
		}
		TreeNode newRoot = upsideDownBinaryTree(root.left);
		root.left.left = root.right;
		root.left.right = root;
		root.left = null;
		root.right = null;
		return newRoot;
	}

	// bottom-up approach, recursive
	public TreeNode upsideDownBinaryTree2(TreeNode root) {
		return dfsUpsideDown(root, null);
	}
}
