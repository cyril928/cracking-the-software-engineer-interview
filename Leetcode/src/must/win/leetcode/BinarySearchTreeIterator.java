package must.win.leetcode;

import java.util.Stack;

// Binary Search Tree Iterator
/*
 * http://blog.csdn.net/whuwangyi/article/details/42304407
 * implementation & explanation
 */
public class BinarySearchTreeIterator {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	private Stack<TreeNode> stack;
	private TreeNode cur;

	public BinarySearchTreeIterator(TreeNode root) {
		stack = new Stack<TreeNode>();
		cur = root;
		while (cur != null) {
			stack.push(cur);
			cur = cur.left;
		}
	}

	/** @return whether we have a next smallest number */
	public boolean hasNext() {
		return !stack.empty();
	}

	/** @return the next smallest number */
	public int next() {
		TreeNode cur = stack.pop();
		int val = cur.val;
		cur = cur.right;
		while (cur != null) {
			stack.push(cur);
			cur = cur.left;
		}
		return val;
	}
}
