package must.win.leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

// Same Tree
/*
 * http://n00tc0d3r.blogspot.com/2013/05/same-tree.html?q=same+tree
 * dfs
 * bfs
 */

public class SameTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// dfs
	public boolean isSameTree(TreeNode p, TreeNode q) {
		if (p == null && q == null) {
			return true;
		}
		if (p == null || q == null) {
			return false;
		}
		return (p.val == q.val) && isSameTree(p.left, q.left)
				&& isSameTree(p.right, q.right);
	}

	// bfs
	public boolean isSameTree1(TreeNode p, TreeNode q) {
		if (p == null && q == null) {
			return true;
		}
		if (p == null || q == null) {
			return false;
		}
		Queue<TreeNode> pQueue = new ArrayDeque<TreeNode>();
		Queue<TreeNode> qQueue = new ArrayDeque<TreeNode>();

		pQueue.add(p);
		qQueue.add(q);

		while (!pQueue.isEmpty() && !qQueue.isEmpty()) {
			TreeNode curP = pQueue.remove();
			TreeNode curQ = qQueue.remove();
			if (curP.val != curQ.val) {
				return false;
			}

			// (curP.left.val == curQ.left.val) optimization, don't have to go
			// down to next level
			if (curP.left != null && curQ.left != null
					&& curP.left.val == curQ.left.val) {
				pQueue.add(curP.left);
				qQueue.add(curQ.left);
			} else if (curP.left != null || curQ.left != null) {
				return false;
			}

			// (curP.right.val == curQ.right.val) optimization, don't have to go
			// down to next level
			if (curP.right != null && curQ.right != null
					&& curP.right.val == curQ.right.val) {
				pQueue.add(curP.right);
				qQueue.add(curQ.right);
			} else if (curP.right != null || curQ.right != null) {
				return false;
			}

		}

		return pQueue.isEmpty() && qQueue.isEmpty();
	}
}
