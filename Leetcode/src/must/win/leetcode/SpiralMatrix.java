package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Spiral Matrix (this question is different from n*n spiral matrix value problem)
/* 
 * http://answer.ninechapter.com/solutions/spiral-matrix/
 * iterative solution
 * 
 * iterative solution, from ZouYou
 * 
 * http://mattcb.blogspot.tw/search?q=Spiral+Matrix
 * recursive method
 */
public class SpiralMatrix {

	// iterative solution
	public List<Integer> spiralOrder(int[][] matrix) {
		List<Integer> res = new ArrayList<Integer>();
		if (matrix == null || matrix.length == 0) {
			return res;
		}
		int m = matrix.length - 1;
		int n = matrix[0].length - 1;

		for (int i = 0; i <= m && i <= n; i++, m--, n--) {
			for (int j = i; j <= n; j++) {
				res.add(matrix[i][j]);
			}
			for (int j = i + 1; j <= m; j++) {
				res.add(matrix[j][n]);
			}
			if (i == m || i == n) {
				return res;
			}
			for (int j = n - 1; j >= i; j--) {
				res.add(matrix[m][j]);
			}
			for (int j = m - 1; j > i; j--) {
				res.add(matrix[j][i]);
			}
		}
		return res;
	}

	// iterative solution, from ZouYou
	public List<Integer> spiralOrder1(int[][] matrix) {
		List<Integer> list = new ArrayList<Integer>();
		if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
			return list;
		}
		int rs = 0, re = matrix.length - 1, cs = 0, ce = matrix[0].length - 1;

		// make sure more than two rows and two columns
		while (rs < re && cs < ce) {
			for (int c = cs; c <= ce - 1; c++) {
				list.add(matrix[rs][c]);
			}
			for (int r = rs; r <= re - 1; r++) {
				list.add(matrix[r][ce]);
			}
			for (int c = ce; c >= cs + 1; c--) {
				list.add(matrix[re][c]);
			}
			for (int r = re; r >= rs + 1; r--) {
				list.add(matrix[r][cs]);
			}
			rs++;
			cs++;
			re--;
			ce--;
		}

		// deal with the one row or one column situation
		if (rs == re) {
			for (int c = cs; c <= ce; c++) {
				list.add(matrix[rs][c]);
			}
		} else if (cs == ce) {
			for (int r = rs; r <= re; r++) {
				list.add(matrix[r][cs]);
			}
		}

		return list;
	}

	// recursive solution
	public List<Integer> spiralOrder2(int[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			return new ArrayList<Integer>();
		}
		return spiralOrderRecursive(matrix, matrix.length, matrix[0].length, 0);
	}

	// for recursive method
	private List<Integer> spiralOrderRecursive(int[][] matrix, int rowEnd,
			int colEnd, int level) {
		List<Integer> res = new ArrayList<Integer>();
		if (rowEnd <= 0 || colEnd <= 0) {
			return res;
		}
		for (int i = level; i < level + colEnd; i++) {
			res.add(matrix[level][i]);
		}
		for (int i = level + 1; i < level + rowEnd; i++) {
			res.add(matrix[i][colEnd + level - 1]);
		}
		if (rowEnd > 1) {
			for (int i = level + colEnd - 2; i >= level; i--) {
				res.add(matrix[rowEnd + level - 1][i]);
			}
		}
		if (colEnd > 1) {
			for (int i = level + rowEnd - 2; i > level; i--) {
				res.add(matrix[i][level]);
			}
		}

		res.addAll(spiralOrderRecursive(matrix, rowEnd - 2, colEnd - 2,
				level + 1));
		return res;
	}
}
