package must.win.leetcode;

/*
 * use n &= (n - 1) 
 * 
 * http://stackoverflow.com/questions/15233121/calculating-hamming-weight-in-o1
 * divide and conquer
 */
public class Numberof1Bits {
	// you need to treat n as an unsigned value
	// use n &= (n - 1)
	public int hammingWeight(int n) {
		int res = 0;
		for (; n != 0; n &= (n - 1)) {
			res++;
		}
		return res;
	}

	// divide and conquer
	public int hammingWeight1(int n) {
		int mask1 = 0x55555555;
		n = ((n >> 1) & mask1) + (n & mask1);

		int mask2 = 0x33333333;
		n = ((n >> 2) & mask2) + (n & mask2);

		int mask4 = 0x0F0F0F0F;
		n = ((n >> 4) & mask4) + (n & mask4);

		int mask8 = 0x00FF00FF;
		n = ((n >> 8) & mask8) + (n & mask8);

		int mask16 = 0x0000FFFF;
		n = ((n >> 16) & mask16) + (n & mask16);

		return n;
	}
}
