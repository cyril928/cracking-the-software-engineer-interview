package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Max Points on a Line 
/*
 * http://www.ninechapter.com/solutions/max-points-on-a-line/
 * most clean implementation
 * 
 * http://solutionleetcode.blogspot.tw/2014/05/max-points-on-line.html
 * good implementation
 * 
 * http://blog.csdn.net/doc_sgl/article/details/17103427
 * some explanation and idea to care about
 */
public class MaxPointsonaLine {
	class Point {
		int x;
		int y;

		Point() {
			x = 0;
			y = 0;
		}

		Point(int a, int b) {
			x = a;
			y = b;
		}
	}

	public int maxPoints(Point[] points) {
		if (points == null || points.length == 0) {
			return 0;
		}

		Map<Double, Integer> slopeMap = new HashMap<Double, Integer>();
		int max = 1;
		for (int i = 0; i < points.length; i++) {
			// shared point changed, map should be cleared and server the new
			// point
			slopeMap.clear();

			// maybe all points contained in the list are same points,and same
			// points' k is
			// represented by Integer.MIN_VALUE
			slopeMap.put(Double.POSITIVE_INFINITY, 1);
			int dup = 0;

			for (int j = i + 1; j < points.length; j++) {
				if (points[i].x == points[j].x && points[i].y == points[j].y) {
					dup++;
				} else {
					// if the line through two points are parallel to y
					// coordinator, then slope is
					// Integer.MAX_VALUE or Double.POSITIVE_INFINITY
					double slope = Double.POSITIVE_INFINITY;
					if (points[i].x != points[j].x) {
						/*
						 * all are ok slope = 0.0 + (double)(points[i].y -
						 * points[j].y) / (points[i].x - points[j].x); slope =
						 * 0.0 + (points[i].y - points[j].y) /
						 * (double)(points[i].x - points[j].x); slope = 0.0 +
						 * (double)(points[i].y - points[j].y) /
						 * (double)(points[i].x - points[j].x);
						 */
						/*
						 * look
						 * 0.0+(double)(points[j].y-points[i].y)/(double)(points
						 * [j].x-points[i].x) because (double)0/-1 is -0.0, so
						 * we should use 0.0+-0.0=0.0 to solve 0.0 !=-0.0
						 * problem
						 */
						slope = 0.0 + (double) (points[i].y - points[j].y)
								/ (points[i].x - points[j].x);
					}
					if (!slopeMap.containsKey(slope)) {
						slopeMap.put(slope, 2);
					} else {
						slopeMap.put(slope, slopeMap.get(slope) + 1);
					}
				}
			}
			for (int val : slopeMap.values()) {
				max = Math.max(val + dup, max);
			}

		}
		return max;
	}
}
