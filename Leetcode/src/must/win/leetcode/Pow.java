package must.win.leetcode;

// Pow(x, n) 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Pow(x,+n)
 * iterative, recursive implementation and explanation
 * O(logN) solution
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Pow(x,+n)
 * recursive
 * O(logN) solution
 * 
 */

public class Pow {
	// iterative
	public double pow(double x, int n) {
		// every number to 0 is 1
		if (n == 0) {
			return 1;
		}
		if (x == 0) {
			return (n < 0) ? Double.NaN : 0;
		}

		double res = 1;
		int num = Math.abs(n);
		while (num > 0) {
			// java operator priority * / % + - << >> >>> == != & ^ | && || = +=
			// -= *= /= %= &= ^= |= <<= >>= >>>=
			// http://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
			if ((num & 1) == 1) {
				res *= x;
			}
			x *= x;
			num >>= 1;
		}
		return (n < 0) ? 1 / res : res;
	}

	// recursive
	public double pow1(double x, int n) {
		// every number to 0 is 1
		if (n == 0) {
			return 1;
		}
		if (x == 0) {
			return (n < 0) ? Double.NaN : 0;
		}

		int num = Math.abs(n);
		double half = pow1(x, num >> 1);
		double res = 1;
		res *= half * half;
		if ((num & 1) == 1) {
			res *= x;
		}
		return (n < 0) ? 1 / res : res;
	}
}
