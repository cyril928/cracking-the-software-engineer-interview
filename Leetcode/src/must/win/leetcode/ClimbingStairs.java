package must.win.leetcode;

// Climbing Stairs 
/* DP, recursive, math
 * 
 */
public class ClimbingStairs {
	// DP, O(1) space
	public int climbStairs(int n) {
		if (n <= 0) {
			return 0;
		}
		if (n == 1 || n == 2) {
			return n;
		}
		int p1 = 1, p2 = 2;
		int res = 0;
		for (int i = 2; i < n; i++) {
			res = p1 + p2;
			p1 = p2;
			p2 = res;
		}
		return res;
	}

	// recursive
	public int climbStairs1(int n) {
		if (n <= 0) {
			return 0;
		}
		if (n == 1 || n == 2) {
			return n;
		}
		return climbStairs1(n - 1) + climbStairs1(n - 2);
	}
}
