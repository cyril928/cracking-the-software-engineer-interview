package must.win.leetcode;

// Two Sum II - Input array is sorted
/*
 * Clean Code Hand Book
 * O(n) runtime, O(1) space, Two pointers
 * O(n log n) runtime, O(1) space, Binary search
 */
public class TwoSumIIInputarrayissorted {
	// O(n) runtime, O(1) space, Two pointers
	public int[] twoSum(int[] numbers, int target) {
		if (numbers == null || numbers.length == 0) {
			throw new IllegalArgumentException("no two sum solution");
		}
		int start = 0, end = numbers.length - 1;
		while (start < end) {
			int sum = numbers[start] + numbers[end];
			if (sum < target) {
				start++;
			} else if (sum > target) {
				end--;
			} else {
				return new int[] { start + 1, end + 1 };
			}
		}
		throw new IllegalArgumentException("no two sum solution");
	}
}
