package must.win.leetcode;

import java.util.HashMap;

//LRU Cache 
/*
 *  http://n00tc0d3r.blogspot.com/search?q=lru+cache
 *  http://www.programcreek.com/2013/03/leetcode-lru-cache-java/
 *  refer to part of both implementation
 *  but best version is my LintCode version
 */
public class LRUCache {
	private class DoubleLinkedListNode {
		public int key;
		public int val;
		public DoubleLinkedListNode prev = null;
		public DoubleLinkedListNode next = null;

		public DoubleLinkedListNode(int key, int val) {
			this.key = key;
			this.val = val;
		}
	}

	private HashMap<Integer, DoubleLinkedListNode> map = new HashMap<Integer, DoubleLinkedListNode>();
	private DoubleLinkedListNode head = null;
	private DoubleLinkedListNode tail = null;
	private int capacity;

	public LRUCache(int capacity) {
		this.capacity = capacity;
	}

	public int get(int key) {
		if (map.containsKey(key)) {
			DoubleLinkedListNode node = map.get(key);
			if (node.prev != null) { // if at the head, don't have to move it
				removeNode(node);
				insertToHead(node);
			}
			return node.val;
		}
		return -1;
	}

	private void insertToHead(DoubleLinkedListNode node) {
		if (head != null) {
			head.prev = node;
		} else {
			tail = node;
		}
		node.next = head;
		node.prev = null;
		head = node;
	}

	private void removeNode(DoubleLinkedListNode node) {
		if (node.prev != null) {
			node.prev.next = node.next;
		}
		if (node.next != null) {
			node.next.prev = node.prev;
		} else {
			removeTail();
		}
	}

	private void removeTail() {
		if (tail.prev == null) { // only allow one node: capacity == 1
			head = tail = null;
		} else {
			tail = tail.prev;
			tail.next = null;
		}
	}

	public void set(int key, int value) {
		if (map.containsKey(key)) {
			DoubleLinkedListNode node = map.get(key);
			node.val = value;
			if (node.prev != null) { // if at the head, don't have to move it
				removeNode(node);
				insertToHead(node);
			}
		} else {
			if (capacity == map.size()) {
				map.remove(tail.key);
				removeTail();
			}
			DoubleLinkedListNode node = new DoubleLinkedListNode(key, value);
			insertToHead(node);
			map.put(key, node);
		}
	}
}
