package must.win.leetcode;

// Merge Two Sorted Lists 
/*
 * http://answer.ninechapter.com/solutions/merge-two-sorted-lists/
 * dummy node solution, O(N) time, O(1) space
 * 
 * http://n00tc0d3r.blogspot.tw/2013/04/merge-sorted-listsarrays-i.html?q=Merge+Two+Sorted+Lists
 * without dummy, dummy, recursive solution
 * 
 * http://n00tc0d3r.blogspot.tw/2013/04/merge-sorted-listsarrays-i.html?q=Merge+Two+Sorted+Lists
 * linkedlist info
 * 
 */
public class MergeTwoSortedLists {
	// dummy node solution, O(N) time, O(1) space
	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;

		while (l1 != null && l2 != null) {
			if (l1.val < l2.val) {
				cur.next = l1;
				l1 = l1.next;
			} else {
				cur.next = l2;
				l2 = l2.next;
			}
			cur = cur.next;
		}
		cur.next = (l1 != null) ? l1 : l2;

		return dummy.next;
	}

	// recursive
	public ListNode mergeTwoLists1(ListNode l1, ListNode l2) {
		if (l1 == null) {
			return l2;
		}
		if (l2 == null) {
			return l1;
		}

		if (l1.val < l2.val) {
			l1.next = mergeTwoLists(l1.next, l2);
			return l1;
		} else {
			l2.next = mergeTwoLists(l1, l2.next);
			return l2;
		}

	}
}
