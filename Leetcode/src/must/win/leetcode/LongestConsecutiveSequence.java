package must.win.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// Longest Consecutive Sequence
/* 
 * http://yucoding.blogspot.tw/2013/05/leetcode-question-129-longest.html    
 * Analysis: use hash map
 * 
 * http://mattcb.blogspot.tw/search?q=Longest+Consecutive+Sequence    
 * implementation, HashSet is better
 * 
 * http://www.ninechapter.com/solutions/longest-consecutive-sequence/
 * implementation, HashMap
 * 
 * Sort & search: space O(1), time O(n logn)
 * HashSet & HashMap: space O(n), time O(n)
 * 
 */
public class LongestConsecutiveSequence {

	// HashSet, after visited, remove the element
	public int longestConsecutive(int[] num) {
		if (num == null || num.length == 0) {
			return 0;
		}

		Set<Integer> set = new HashSet<Integer>();
		int max = 0;
		for (int val : num) {
			set.add(val);
		}
		for (int val : num) {
			int count = 1;
			int prev = val - 1;
			int next = val + 1;
			while (set.contains(prev)) {
				set.remove(prev--);
				count++;
			}
			while (set.contains(next)) {
				set.remove(next++);
				count++;
			}
			max = Math.max(count, max);
		}
		return max;
	}

	// visited HashMap
	public int longestConsecutive1(int[] num) {
		if (num == null || num.length == 0) {
			return 0;
		}

		Map<Integer, Boolean> visitedMap = new HashMap<Integer, Boolean>();
		for (int val : num) {
			visitedMap.put(val, false);
		}

		int maxLen = 1;
		for (int val : num) {
			// this number has been visited
			if (visitedMap.get(val)) {
				continue;
			}
			visitedMap.put(val, true);
			int curMax = 1;
			int temp = val + 1;
			while (visitedMap.containsKey(temp)) {
				visitedMap.put(temp++, true);
				curMax++;
			}

			temp = val - 1;
			while (visitedMap.containsKey(temp)) {
				visitedMap.put(temp--, true);
				curMax++;
			}
			maxLen = Math.max(curMax, maxLen);
		}
		return maxLen;
	}
}
