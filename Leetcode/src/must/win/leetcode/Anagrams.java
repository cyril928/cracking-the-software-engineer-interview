package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Anagrams 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Anagrams
 * Using hashmap,  "and" => "a1d1n1", "array" => "a2r2y1" to label string.
 * This algorithm runs in time O(n) and uses O(n) space.
 * 
 * http://answer.ninechapter.com/solutions/anagrams/
 * very trick way to generate hash, don't understand yet.
 * 
 */
public class Anagrams {
	// HashMap, "and" => "a1d1n1", "array" => "a2r2y1" to label string.
	public List<String> anagrams(String[] strs) {
		List<String> res = new ArrayList<String>();
		if (strs == null || strs.length == 0) {
			return res;
		}

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (String s : strs) {
			String key = generateKey(s);
			key = generateKey1(s);
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<String>());
			}
			map.get(key).add(s);
		}

		for (List<String> list : map.values()) {
			if (list.size() > 1) {
				res.addAll(list);
			}
		}
		return res;
	}

	private String generateKey(String s) {
		int[] count = new int[26];
		for (int i = 0; i < s.length(); i++) {
			count[s.charAt(i) - 'a']++;
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 26; i++) {
			if (count[i] > 0) {
				char c = (char) (i + 'a');
				sb.append(c).append(count[i]);
			}
		}
		return sb.toString();
	}

	private String generateKey1(String s) {
		int[] count = new int[26];
		for (int i = 0; i < s.length(); i++) {
			count[s.charAt(i) - 'a']++;
		}

		int i = 0, j = 0;
		char[] charArray = new char[s.length()];
		while (i < 26) {
			while (count[i]-- > 0) {
				charArray[j++] = (char) (i + 'a');
			}
			i++;
		}
		return String.valueOf(charArray);
	}
}
