package must.win.leetcode;

// Excel Sheet Column Title 
/*
 * http://bookshadow.com/weblog/2014/12/20/leetcode-excel-sheet-column-title/
 * most clean way, n - 1
 * 
 * http://blog.csdn.net/u012162613/article/details/42059591
 * explanation & implementation
 */
public class ExcelSheetColumnTitle {

	// most clean way, n - 1
	public String convertToTitle(int n) {
		StringBuilder sb = new StringBuilder();
		while (n > 0) {
			int r = (n - 1) % 26;
			char c = (char) ('A' + r);
			sb.append(c);
			n = (n - 1) / 26;
		}
		return sb.reverse().toString();
	}

	public String convertToTitle1(int n) {
		StringBuilder sb = new StringBuilder();
		while (n > 0) {
			int r = n % 26;
			n /= 26;
			if (r == 0) {
				sb.append('Z');
				n -= 1;
			} else {
				char c = (char) ('A' + r - 1);
				sb.append(c);
			}
		}
		return sb.reverse().toString();
	}
}
