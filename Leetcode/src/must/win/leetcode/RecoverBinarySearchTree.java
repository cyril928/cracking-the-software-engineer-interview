package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Recover Binary Search Tree
/* http://mattcb.blogspot.tw/search?q=recover
 * O(1) space, well actually O(H) space for stack, previous node as global variable
 * O(N) space
 * 
 *  
 * http://n00tc0d3r.blogspot.com/search?q=Recover+Binary+Search+Tree (very goooood!! post)
 * O(1) space, well actually O(H) space for stack, return previous node version
 * O(N) space
 * we only need to compare with previous node, we don't need to store all nodes during a traversal.
 * We only need to keep track of the previously visited node.
 * 
 * Morris Traversal(haven't understand) ? really O(n) time and O(1) space in order traversal
 * 
 * 
 * 
 */
public class RecoverBinarySearchTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// O(1) space, actually O(H) space, previous node as global variable,
	// variable set
	TreeNode prev = new TreeNode(Integer.MIN_VALUE);
	TreeNode first = null, second = null;

	// O(1) space, actually O(H) space in order traversal, previous node as
	// global variable
	public void inOrderTraversal(TreeNode node) {
		if (node == null) {
			return;
		}
		inOrderTraversal(node.left);
		if (prev.val > node.val) {
			if (first == null) {
				first = prev;
			}
			second = node;
		}
		prev = node;
		inOrderTraversal(node.right);
	}

	// O(1) space, well actually O(H) space for stack, return previous node
	// version, in order traversal
	public TreeNode inOrderTraversal1(TreeNode node, TreeNode[] candidates,
			TreeNode prev) {
		if (node == null) {
			return prev;
		}

		TreeNode last = inOrderTraversal1(node.left, candidates, prev);
		if (last != null) {
			if (last.val > node.val) {
				if (candidates[0] == null) {
					candidates[0] = last;
				}
				candidates[1] = node;
			}
		}

		return inOrderTraversal1(node.right, candidates, node);
	}

	// O(N) space, in order traversal
	public void inOrderTraversal2(TreeNode node, List<TreeNode> nodes) {
		if (node == null) {
			return;
		}
		inOrderTraversal2(node.left, nodes);
		nodes.add(node);
		inOrderTraversal2(node.right, nodes);
	}

	// O(1) space, actually O(H) space, previous node as global variable
	public void recoverTree(TreeNode root) {
		// traverse and get two elements
		inOrderTraversal(root);
		// don't get two elements into tree, return
		if (first == null || second == null) {
			return;
		}
		// swap
		int temp = first.val;
		first.val = second.val;
		second.val = temp;
	}

	// O(1) space, well actually O(H) space for stack, return previous node
	// version
	public void recoverTree1(TreeNode root) {
		TreeNode[] candidates = new TreeNode[2];
		inOrderTraversal1(root, candidates, null);
		int temp = candidates[0].val;
		candidates[0].val = candidates[1].val;
		candidates[1].val = temp;
	}

	// O(N) space
	public void recoverTree2(TreeNode root) {
		List<TreeNode> nodes = new ArrayList<TreeNode>();
		inOrderTraversal2(root, nodes);
		int i, j;
		for (i = 0; i < nodes.size() - 1
				&& nodes.get(i).val <= nodes.get(i + 1).val; i++) {
			;
		}
		for (j = nodes.size() - 1; j > 0
				&& nodes.get(j - 1).val <= nodes.get(j).val; j--) {
			;
		}
		int temp = nodes.get(i).val;
		nodes.get(i).val = nodes.get(j).val;
		nodes.get(j).val = temp;
	}
}
