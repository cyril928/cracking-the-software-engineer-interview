package must.win.leetcode;

// Find Minimum in Rotated Sorted Array II 
/*
 * http://blog.csdn.net/linhuanmars/article/details/40449299
 * inspiration and explanation
 */
public class FindMinimuminRotatedSortedArrayII {
	public int findMin(int[] num) {
		if (num == null || num.length == 0) {
			return Integer.MAX_VALUE;
		}

		int len = num.length;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < len; i++) {
			min = Math.min(num[i], min);
		}

		return min;
	}
}
