package must.win.leetcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// Binary Tree Level Order Traversal II 
/* 
 * http://www.ninechapter.com/solutions/binary-tree-level-order-traversal-ii/
 * maintain current level number, next level number, one queue
 * 
 * http://n00tc0d3r.blogspot.com/2013/01/binary-tree-traversals-ii.html?q=Binary+Tree+Level+Order+Traversal
 *    // reverse the result array
 *    Collections.reverse(resSet);
 *    This algorithm runs in time O(n).
 *    Note that if we add the value array of each level to the beginning of the result set, 
 *    it will give us an O(n^2) algorithm as each time we need to move all existing elements in the result set.
 */
public class BinaryTreeLevelOrderTraversalII {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// using level size, one queue
	public List<List<Integer>> levelOrderBottom(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}

		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);

		while (!queue.isEmpty()) {
			List<Integer> level = new ArrayList<Integer>();
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.remove();
				level.add(cur.val);
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
			}
			result.add(level);
		}

		Collections.reverse(result);
		return result;
	}

	// dummy null node, one queue
	public List<List<Integer>> levelOrderBottom1(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}

		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);

		while (!queue.isEmpty()) {
			List<Integer> level = new ArrayList<Integer>();
			TreeNode curNode = queue.poll();
			while (curNode != null) {
				level.add(curNode.val);
				if (curNode.left != null) {
					queue.add(curNode.left);
				}
				if (curNode.right != null) {
					queue.add(curNode.right);
				}
				curNode = queue.poll();
			}

			result.add(level);
			if (!queue.isEmpty()) {
				queue.add(null);
			}
		}

		Collections.reverse(result);
		return result;
	}

	// maintain current level number, next level number, one queue
	public List<List<Integer>> levelOrderBottom2(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}

		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		int curLevelNodeNum = 1;
		int nextLevelNodeNum = 0;

		while (curLevelNodeNum > 0) {
			List<Integer> level = new ArrayList<Integer>();
			while (curLevelNodeNum-- > 0) {
				TreeNode curNode = queue.remove();
				level.add(curNode.val);
				if (curNode.left != null) {
					queue.add(curNode.left);
					nextLevelNodeNum++;
				}
				if (curNode.right != null) {
					queue.add(curNode.right);
					nextLevelNodeNum++;
				}
			}

			result.add(level);
			curLevelNodeNum = nextLevelNodeNum;
			nextLevelNodeNum = 0;
		}

		Collections.reverse(result);
		return result;
	}
}
