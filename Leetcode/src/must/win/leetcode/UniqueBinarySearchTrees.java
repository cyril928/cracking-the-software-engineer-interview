package must.win.leetcode;

// Unique Binary Search Trees  
/*
 * http://www.ninechapter.com/solutions/unique-binary-search-trees/
 * DP
 * The case for 3 elements example
 * Count[3] = Count[0]*Count[2]  (1 as root)             
 * + Count[1]*Count[1]  (2 as root)
 * + Count[2]*Count[0]  (3 as root)
 * Therefore, we can get the equation:
 * Count[i] = SIGMA( Count[0...k] * [ k+1....i])     0<=k<i-1  
 * 
 * 
 * http://n00tc0d3r.blogspot.tw/2013/07/unique-binary-search-trees.html?q=regular
 * Recursive 
 * Euler's Polygon Division Problem(Catalan Number) to-do
 */
public class UniqueBinarySearchTrees {

	// DP
	public int numTrees(int n) {
		if (n < 0) {
			return 0;
		}

		int[] nums = new int[n + 1];
		nums[0] = 1;

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				nums[i] += nums[j - 1] * nums[i - j];
			}
		}
		return nums[n];
	}

	// recursive
	public int numTrees1(int n) {
		if (n <= 1) {
			return 1;
		}
		int res = 0;
		for (int i = 1; i <= n; i++) {
			res += numTrees1(i - 1) * numTrees1(n - i);
		}
		return res;
	}
}
