package must.win.leetcode;

import java.util.Stack;

// Flatten Binary Tree to Linked List 
/*
 * http://yucoding.blogspot.com/2013/01/leetcode-question-30-flatten-binary.html
 * explanation and implementation (while-loop version, better) 
 * 
 * http://www.ninechapter.com/solutions/flatten-binary-tree-to-linked-list/
 * most clean recursive
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Flatten+Binary+Tree+to+Linked+List 
 * explanation and implementation (stack, while, recursive implementation)
 * 
 * http://mattcb.blogspot.tw/search?q=Flatten+Binary+Tree+to+Linked+List
 * stack version
 * 
 */
public class FlattenBinaryTreetoLinkedList {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// most clean recursive
	private TreeNode lastNode = null;

	// Solution - Non-Recursion, No Stack, while - loop version better
	/*
	 * We can also solve the problem even without a stack: Each time when we
	 * prune a right subtree, we use while-loop to find the right-most leaf of
	 * the current left subtree, and append the subtree there.
	 */
	public void flatten(TreeNode root) {
		while (root != null) {
			if (root.left != null) {
				TreeNode preNode = root.left;
				while (preNode.right != null) {
					preNode = preNode.right;
				}
				preNode.right = root.right;
				root.right = root.left;
				root.left = null;
			}
			root = root.right;
		}
	}

	// most clean recursive
	public void flatten1(TreeNode root) {
		if (root == null) {
			return;
		}
		if (lastNode != null) {
			lastNode.left = null;
			lastNode.right = root;
		}

		lastNode = root;
		TreeNode rightNode = root.right;
		flatten1(root.left);
		flatten1(rightNode);
	}

	// recursive version
	public void flatten2(TreeNode root) {
		flattenRecursive(root);
	}

	// stack version
	public void flatten3(TreeNode root) {
		if (root == null) {
			return;
		}
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode prev = null;
		stack.push(root);
		while (!stack.isEmpty()) {
			TreeNode cur = stack.pop();
			if (cur.right != null) {
				stack.push(cur.right);
			}
			if (cur.left != null) {
				stack.push(cur.left);
			}
			cur.left = null;
			if (prev != null) {
				prev.right = cur;
			}
			prev = cur;
		}
	}

	public TreeNode flattenRecursive(TreeNode root) {
		if (root == null) {
			return null;
		}
		TreeNode rTree = root.right;
		if (root.left != null) {
			root.right = root.left;
			root.left = null;
			root = flattenRecursive(root.right);
		}
		if (rTree != null) {
			root.right = rTree;
			root = flattenRecursive(root.right);
		}
		return root;
	}
}
