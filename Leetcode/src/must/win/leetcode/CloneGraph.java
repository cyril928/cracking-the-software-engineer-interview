package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

// Clone Graph 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Clone+Graph
 * DFS(recursive) and BFS implementation 
 * In both algorithms, each vertex and each edge are visited constant times. So, both run in O(|V|+|E|) time.
 * 
 */
public class CloneGraph {

	class UndirectedGraphNode {
		int label;
		List<UndirectedGraphNode> neighbors;

		UndirectedGraphNode(int x) {
			label = x;
			neighbors = new ArrayList<UndirectedGraphNode>();
		}
	};

	// DFS(recursive) helper function
	private UndirectedGraphNode cloneDFS(UndirectedGraphNode node,
			Map<UndirectedGraphNode, UndirectedGraphNode> visited) {

		UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
		visited.put(node, newNode);
		newNode.neighbors = new ArrayList<UndirectedGraphNode>();

		for (UndirectedGraphNode neighborNode : node.neighbors) {
			if (!visited.containsKey(neighborNode)) {
				newNode.neighbors.add(cloneDFS(neighborNode, visited));
			} else {
				newNode.neighbors.add(visited.get(neighborNode));
			}
		}

		return newNode;
	}

	// DFS(recursive)
	public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
		if (node == null) {
			return null;
		}
		Map<UndirectedGraphNode, UndirectedGraphNode> visited = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();
		return cloneDFS(node, visited);
	}

	// BFS
	public UndirectedGraphNode cloneGraph1(UndirectedGraphNode node) {
		if (node == null) {
			return null;
		}

		Map<UndirectedGraphNode, UndirectedGraphNode> visited = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();
		UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
		visited.put(node, newNode);

		Queue<UndirectedGraphNode> queue = new LinkedList<UndirectedGraphNode>();
		queue.add(node);

		while (!queue.isEmpty()) {
			UndirectedGraphNode curNode = queue.poll();
			UndirectedGraphNode curNewNode = visited.get(curNode);
			for (UndirectedGraphNode neighbor : curNode.neighbors) {
				if (visited.containsKey(neighbor)) {
					curNewNode.neighbors.add(visited.get(neighbor));
				} else {
					UndirectedGraphNode neighborNewNode = new UndirectedGraphNode(
							neighbor.label);
					visited.put(neighbor, neighborNewNode);
					queue.add(neighbor);
					curNewNode.neighbors.add(neighborNewNode);
				}
			}
		}

		return newNode;
	}

}
