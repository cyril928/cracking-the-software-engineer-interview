package must.win.leetcode;

// Merge Sorted Array
/*
 * http://n00tc0d3r.blogspot.tw/2013/04/merge-sorted-listsarrays-i.html?q=Merge+Sorted+Array
 * other loop way to deal with merge sort (can be a cool alternative)
 */
public class MergeSortedArray {
	// my own way, insert into A array from the end to start. In this way, we
	// can do the in-place merge sort
	public void merge(int A[], int m, int B[], int n) {
		int cur = m + n - 1;
		--m;
		--n;
		while (m >= 0 && n >= 0) {
			if (A[m] > B[n]) {
				A[cur--] = A[m--];
			} else {
				A[cur--] = B[n--];
			}
		}
		while (n >= 0) {
			A[cur--] = B[n--];
		}
	}

	// other loop way to deal with merge sort
	public void merge1(int A[], int m, int B[], int n) {
		int tail = m + n - 1;
		--n;
		--m;
		while (n >= 0) {
			if (m < 0 || A[m] <= B[n]) {
				A[tail--] = B[n--];
			} else {
				A[tail--] = A[m--];
			}
		}
	}
}
