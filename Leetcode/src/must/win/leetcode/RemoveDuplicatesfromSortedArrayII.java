package must.win.leetcode;

// Remove Duplicates from Sorted Array II 
/*
 * http://n00tc0d3r.blogspot.tw/2013/05/remove-element-from-arraylist.html?q=Remove+Duplicates+from+Sorted+Array
 * trap explanation
 * 
 */
public class RemoveDuplicatesfromSortedArrayII {

	public static void main(String[] args) {
		RemoveDuplicatesfromSortedArrayII r = new RemoveDuplicatesfromSortedArrayII();
		int[] array;
		array = new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 3 };
		System.out.println(r.removeDuplicates1(array));
	}

	// O(N) use new size(index), most clean
	public int removeDuplicates(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int len = A.length;
		if (len <= 2) {
			return len;
		}

		int size = 2;
		for (int i = 2; i < len; i++) {
			if (A[i] != A[size - 2]) {
				A[size++] = A[i];
			}
		}
		return size;
	}

	// O(N) use duplicate count.
	public int removeDuplicates1(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int len = A.length;
		int dupCount = 0;
		// So, if you simple change the condition to if (A[i] == A[i-1] && A[i]
		// == A[i-2]),
		// you are trapped! Why?
		// In the above problem, we only need to test the one right before the
		// current one
		// which is not possible to have been overwritten. But now, we also need
		// to test
		// the one that is two nodes away which is possible to have been
		// overwritten.
		// That said, we should test the two nodes that are supposed to be ahead
		// of the current node in the new array.
		for (int i = 2; i < len; i++) {
			if (A[i] == A[i - dupCount - 2]) {
				dupCount++;
			} else if (dupCount > 0) {
				A[i - dupCount] = A[i];
			}
		}
		return len - dupCount;
	}

	// MY OWN WAY, keep the duplication count for the current number. O(N)
	public int removeDuplicates2(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int len = A.length;
		int dupCount = 0;
		int count = 0;

		for (int i = 1; i < len; i++) {
			if (A[i] == A[i - 1]) {
				count++;
				if (count >= 2) {
					dupCount++;
					continue;
				}
			} else {
				count = 0;
			}
			if (dupCount > 0) {
				A[i - dupCount] = A[i];
			}
		}
		return len - dupCount;
	}
}
