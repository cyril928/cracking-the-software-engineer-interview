package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Generate Parentheses
/*
 * http://www.ninechapter.com/solutions/generate-parentheses/
 * recursive
 */
public class GenerateParentheses {

	// recursive
	public List<String> generateParenthesis(int n) {
		List<String> result = new ArrayList<String>();
		if (n <= 0) {
			return result;
		}
		StringBuilder sb = new StringBuilder();
		genParen(result, sb, n, n);
		return result;
	}

	// recursive helper function
	private void genParen(List<String> result, StringBuilder sb, int left,
			int right) {
		if (left > right || left < 0 || right < 0) {
			return;
		}
		if (left == 0 && right == 0) {
			result.add(sb.toString());
			return;
		}
		genParen(result, sb.append('('), left - 1, right);
		sb.deleteCharAt(sb.length() - 1);
		genParen(result, sb.append(')'), left, right - 1);
		sb.deleteCharAt(sb.length() - 1);
	}
}
