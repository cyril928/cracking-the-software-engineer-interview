package must.win.leetcode;

// Populating Next Right Pointers in Each Node II
/*
 * http://mattcb.blogspot.tw/2012/11/populating-next-right-pointers-in-each_3.html?q=Populating+Next+Right+Pointers+in+Each+Node
 * recursive solution, but not optimal 
 */
public class PopulatingNextRightPointersinEachNodeII {
	public class TreeLinkNode {
		int val;
		TreeLinkNode left, right, next;

		TreeLinkNode(int x) {
			val = x;
		}
	}

	// optimal solution, not recursive,
	// like BFS
	public void connect(TreeLinkNode root) {
		TreeLinkNode first = root;
		while (first != null) {
			TreeLinkNode cur = first;
			TreeLinkNode nextFirst = null;
			while (cur != null) {
				if (cur.left != null) {
					cur.left.next = (cur.right == null) ? getNext(cur)
							: cur.right;
					if (nextFirst == null) {
						nextFirst = cur.left;
					}
				}
				if (cur.right != null) {
					cur.right.next = getNext(cur);
					if (nextFirst == null) {
						nextFirst = cur.right;
					}
				}
				cur = cur.next;
			}
			first = nextFirst;
		}
	}

	// recursive version, not optimal, take stack space
	// like DFS
	public void connect1(TreeLinkNode root) {
		if (root == null) {
			return;
		}
		if (root.left != null) {
			root.left.next = (root.right == null) ? getNext(root) : root.right;
		}
		if (root.right != null) {
			root.right.next = getNext(root);
		}

		// right first, and left! this is very tricky, be careful
		connect1(root.right);
		connect1(root.left);
	}

	private TreeLinkNode getNext(TreeLinkNode cur) {
		while (cur.next != null) {
			cur = cur.next;
			if (cur.left != null) {
				return cur.left;
			}
			if (cur.right != null) {
				return cur.right;
			}
		}
		return null;
	}
}
