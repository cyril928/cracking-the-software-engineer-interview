package must.win.leetcode;

// House Robber 
/*
 * 
 */
public class HouseRobber {
	public int rob(int[] num) {
		if (num == null || num.length == 0) {
			return 0;
		}
		int pre = 0;
		int next = 0;
		for (int i = 0; i < num.length; i++) {
			int temp = next;
			next = Math.max(num[i] + pre, next);
			pre = temp;
		}
		return next;
	}
}
