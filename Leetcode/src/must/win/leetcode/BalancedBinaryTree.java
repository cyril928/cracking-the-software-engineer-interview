package must.win.leetcode;

// Balanced Binary Tree 
/*
 * http://www.ninechapter.com/solutions/balanced-binary-tree/
 * O(N)
 * O(N2)
 */
public class BalancedBinaryTree {

	class Compare {
		int depth;
		boolean balance;

		public Compare(int depth, boolean balance) {
			this.depth = depth;
			this.balance = balance;
		}
	}

	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// my own way, O(N)
	public Compare balanceCompare(TreeNode root) {
		if (root == null) {
			return null;
		}
		Compare leftBalance = balanceCompare(root.left);
		Compare rightBalance = balanceCompare(root.right);
		if (leftBalance == null && rightBalance == null) {
			return new Compare(1, true);
		} else if (leftBalance != null && rightBalance != null) {
			if (leftBalance.balance == false || rightBalance.balance == false) {
				return new Compare(0, false);
			} else {
				int leftDepth = leftBalance.depth;
				int rightDepth = rightBalance.depth;
				if (Math.abs(leftDepth - rightDepth) > 1) {
					return new Compare(0, false);
				} else {
					return new Compare(Math.max(leftDepth, rightDepth) + 1,
							true);
				}
			}
		} else if (leftBalance != null) {
			if (leftBalance.depth > 1 || leftBalance.balance == false) {
				return new Compare(0, false);
			} else {
				return new Compare(leftBalance.depth + 1, leftBalance.balance);
			}
		} else {
			if (rightBalance.depth > 1 || rightBalance.balance == false) {
				return new Compare(0, false);
			} else {
				return new Compare(rightBalance.depth + 1, rightBalance.balance);
			}
		}
	}

	// O(N) helper function
	private int depth(TreeNode root) {
		if (root == null) {
			return 0;
		}

		int leftDepth = depth(root.left);
		int rightDepth = depth(root.right);
		if (leftDepth == -1 || rightDepth == -1
				|| Math.abs(leftDepth - rightDepth) > 1) {
			return -1;
		}
		return Math.max(leftDepth, rightDepth) + 1;
	}

	// O(N2) helper function
	private int depth1(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(depth1(root.left), depth1(root.right)) + 1;
	}

	// O(N)
	public boolean isBalanced(TreeNode root) {
		return depth(root) != -1;
	}

	// O(N2)
	public boolean isBalanced1(TreeNode root) {
		if (root == null) {
			return true;
		}
		return Math.abs(depth1(root.left) - depth1(root.right)) < 2
				&& isBalanced1(root.left) && isBalanced1(root.right);
	}

	// my own way, O(N)
	public boolean isBalanced2(TreeNode root) {
		if (root == null) {
			return true;
		}
		Compare res = balanceCompare(root);
		return res.balance;
	}
}
