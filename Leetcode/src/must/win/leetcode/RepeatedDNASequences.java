package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Repeated DNA Sequences 
/*
 * http://www.meetqun.com/thread-6010-1-1.html
 * use ASCII or 2 bit to mark A, C, G, T
 */

public class RepeatedDNASequences {
	private int charToInt(char c) {
		switch (c) {
		case 'A':
			return 0;
		case 'C':
			return 1;
		case 'G':
			return 2;
		case 'T':
			return 3;
		default:
			return -1;
		}
	}

	// use 2 bit to mark A, C, G, T
	public List<String> findRepeatedDnaSequences(String s) {
		List<String> res = new ArrayList<String>();
		if (s == null || s.length() < 10) {
			return res;
		}
		Map<Integer, Boolean> map = new HashMap<Integer, Boolean>();
		int val = 0;
		int mask = 0x3FFFF;
		for (int i = 0; i < 10; i++) {
			val = (val << 2) | charToInt(s.charAt(i));
		}
		map.put(val, false);

		for (int i = 10; i < s.length(); i++) {
			val = ((val & mask) << 2) | charToInt(s.charAt(i));
			if (!map.containsKey(val)) {
				map.put(val, false);
			} else if (!map.get(val)) {
				res.add(s.substring(i - 9, i + 1));
				map.put(val, true);
			}
		}
		return res;
	}
}
