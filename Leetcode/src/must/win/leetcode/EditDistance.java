package must.win.leetcode;

// Edit Distance
/*
 * This problem needs to consider empty String, so we need extra column & row for DP table.
 * 
 * String length M & N
 * 
 * http://n00tc0d3r.blogspot.com/2013/03/edit-distance.html?q=Edit+Distance
 * Time O(M*N) and Space O(M)
 * Time O(M*N) and Space O(min(M,N))
 * 
 * http://www.ninechapter.com/solutions/edit-distance/
 * DP, O(M*N) space
 */
public class EditDistance {

	// Time O(M*N) and Space O(min(M,N)), best one
	public int minDistance(String word1, String word2) {
		if (word1 == null || word2 == null) {
			return -1;
		}

		String s1, s2;

		if (word1.length() < word2.length()) {
			s2 = word1;
			s1 = word2;
		} else {
			s1 = word1;
			s2 = word2;
		}
		int rows = s1.length();
		int cols = s2.length();

		int[][] dist = new int[2][cols + 1];
		int prevRow = 0, curRow = 1;
		for (int i = 0; i <= cols; i++) {
			dist[prevRow][i] = i;
		}

		for (int i = 1; i <= rows; i++) {
			dist[curRow][0] = i;
			for (int j = 1; j <= cols; j++) {
				if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					dist[curRow][j] = dist[prevRow][j - 1];
				} else {
					dist[curRow][j] = 1 + Math.min(dist[prevRow][j - 1],
							Math.min(dist[curRow][j - 1], dist[prevRow][j]));
				}
			}
			prevRow ^= 1;
			curRow ^= 1;
		}
		return dist[prevRow][cols];
	}

	// Time O(M*N) and Space O(M)
	public int minDistance1(String word1, String word2) {
		if (word1 == null || word2 == null) {
			return -1;
		}

		int rows = word1.length();
		int cols = word2.length();

		int[][] dist = new int[2][cols + 1];
		int prevRow = 0, curRow = 1;
		for (int i = 0; i <= cols; i++) {
			dist[prevRow][i] = i;
		}

		for (int i = 1; i <= rows; i++) {
			dist[curRow][0] = i;
			for (int j = 1; j <= cols; j++) {
				if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
					dist[curRow][j] = dist[prevRow][j - 1];
				} else {
					dist[curRow][j] = 1 + Math.min(dist[prevRow][j - 1],
							Math.min(dist[curRow][j - 1], dist[prevRow][j]));
				}
			}
			prevRow ^= 1;
			curRow ^= 1;
		}
		return dist[prevRow][cols];
	}

	// DP, O(M*N) space
	public int minDistance2(String word1, String word2) {
		if (word1 == null || word2 == null) {
			return -1;
		}

		int rows = word1.length();
		int cols = word2.length();
		int[][] dist = new int[rows + 1][cols + 1];

		for (int i = 0; i <= cols; i++) {
			dist[0][i] = i;
		}

		for (int i = 0; i <= rows; i++) {
			dist[i][0] = i;
		}

		for (int i = 1; i <= rows; i++) {
			for (int j = 1; j <= cols; j++) {
				if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
					dist[i][j] = dist[i - 1][j - 1];
				} else {
					dist[i][j] = 1 + Math.min(dist[i - 1][j - 1],
							Math.min(dist[i - 1][j], dist[i][j - 1]));
				}
			}
		}

		return dist[rows][cols];
	}
}
