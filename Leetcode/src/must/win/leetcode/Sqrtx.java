package must.win.leetcode;

// Sqrt(x)
/*
 * http://www.ninechapter.com/solutions/sqrtx/
 * either use long or use division to avoid overflow problem
 */
public class Sqrtx {
	public int sqrt(int x) {
		if (x < 2) {
			return x;
		}
		int high = x, low = 0;
		while (low <= high) {
			int mid = low + (high - low) / 2;
			int div = x / mid;
			if (div < mid) {
				high = mid - 1;
			} else if (div > mid) {
				low = mid + 1;
			} else {
				return mid;
			}
		}
		return high;
	}
}
