package must.win.leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Word Search
/* http://yucoding.blogspot.com/2013/02/leetcode-question-124-word-search.html
 * DFS algorithm with first char verification
 * 
 * http://mattcb.blogspot.tw/2013/01/word-search.html?q=Word+Search
 * explanation of how to reduce the depth of recursive based on different the base case judge (dfs)
 * 
 * http://n00tc0d3r.blogspot.tw/2013/07/word-search.html?q=Word+Search
 * hashmap + dfs implementation & explanation
 */
public class WordSearch {

	// helper function for dfs with first char verification solution
	private boolean dfs(char[][] board, boolean[][] visited, String word,
			int i, int j, int cur) {
		if (word.charAt(cur) != board[i][j]) {
			return false;
		}
		if (cur == word.length() - 1) {
			return true;
		}

		visited[i][j] = true;

		// top, down, left, right traversal
		if ((i - 1 >= 0) && !visited[i - 1][j]
				&& dfs(board, visited, word, i - 1, j, cur + 1)) {
			return true;
		}
		if ((i + 1 < board.length) && !visited[i + 1][j]
				&& dfs(board, visited, word, i + 1, j, cur + 1)) {
			return true;
		}
		if ((j - 1 >= 0) && !visited[i][j - 1]
				&& dfs(board, visited, word, i, j - 1, cur + 1)) {
			return true;
		}
		if ((j + 1 < board[0].length) && !visited[i][j + 1]
				&& dfs(board, visited, word, i, j + 1, cur + 1)) {
			return true;
		}

		visited[i][j] = false;
		return false;
	}

	// dfs with first char verification solution
	public boolean exist(char[][] board, String word) {
		if (board == null || board.length == 0) {
			return false;
		}
		if (word.length() == 0) {
			return true;
		}

		int len = board.length;
		int width = board[0].length;
		boolean[][] visited = new boolean[len][width];

		char firstChar = word.charAt(0);
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < width; j++) {
				if (firstChar == board[i][j]) {
					if (dfs(board, visited, word, i, j, 0)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	// HashMap + dfs
	public boolean exist1(char[][] board, String word) {
		if (board == null || board.length == 0) {
			return false;
		}
		if (word.length() == 0) {
			return true;
		}

		Map<Character, List<Integer>> map = preprocess(board);
		char firstChar = word.charAt(0);
		if (!map.containsKey(firstChar)) {
			return false;
		}

		boolean[] visited = new boolean[board.length * board[0].length];
		for (int node : map.get(firstChar)) {
			if (hashdfs(map, visited, word, node, 1, board[0].length)) {
				return true;
			}
		}
		return false;
	}

	// for HashMap + dfs
	private boolean hashdfs(Map<Character, List<Integer>> map,
			boolean[] visited, String word, int preNode, int cur, int width) {
		if (cur == word.length()) {
			return true;
		}
		char targetChar = word.charAt(cur);
		if (!map.containsKey(targetChar)) {
			return false;
		}

		visited[preNode] = true;

		for (int node : map.get(targetChar)) {
			if (!visited[node] && isAdjacent(preNode, node, width)) {
				if (hashdfs(map, visited, word, node, cur + 1, width)) {
					return true;
				}
			}
		}

		visited[preNode] = false;
		return false;
	}

	// for HashMap + dfs
	private boolean isAdjacent(int n1, int n2, int width) {
		int max = Math.max(n1, n2);
		int min = max ^ n1 ^ n2;
		return (max - min == width) || ((max - min == 1) && (max % width != 0));
	}

	// for HashMap + dfs
	private Map<Character, List<Integer>> preprocess(char[][] board) {
		Map<Character, List<Integer>> map = new HashMap<Character, List<Integer>>();
		int len = board.length;
		int width = board[0].length;

		List<Integer> nodeList = null;
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < width; j++) {
				if (map.containsKey(board[i][j])) {
					nodeList = map.get(board[i][j]);
				} else {
					nodeList = new ArrayList<Integer>();
					map.put(board[i][j], nodeList);
				}
				nodeList.add(i * width + j);
			}
		}

		return map;
	}
}
