package must.win.leetcode;

// Decode Ways
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Decode+Ways
 * explanation and implementation
 */
public class DecodeWays {

	private boolean isValidCode(String s) {
		if (!s.startsWith("0")) {
			int num = Integer.parseInt(s);
			return num >= 1 && num <= 26;
		}
		return false;
	}

	// DP, O(N) time and O(1) space
	public int numDecodings(String s) {
		if (s == null) {
			return 0;
		}
		int len = s.length();

		int way1 = 0, way2 = 0, res = 0;

		// the solution order is way2, way1, res.....
		if (len > 0) {
			way2 = res = isValidCode(s.substring(0, 1)) ? 1 : 0;
		}
		if (len > 1) {
			res = isValidCode(s.substring(1, 2)) ? way2 : 0;
			res += isValidCode(s.substring(0, 2)) ? 1 : 0;
			way1 = res;
		}
		for (int i = 2; i < len; i++) {
			res = isValidCode(s.substring(i, i + 1)) ? way1 : 0;
			res += isValidCode(s.substring(i - 1, i + 1)) ? way2 : 0;
			way2 = way1;
			way1 = res;
			// the string can't be decoded anymore
			if (res == 0) {
				return 0;
			}
		}
		return res;
	}

	// DP, O(N) time and O(N) space
	public int numDecodings1(String s) {
		if (s == null) {
			return 0;
		}
		int len = s.length();
		if (len == 0) {
			return 0;
		}

		int[] res = new int[len];
		if (len > 0) {
			res[0] = isValidCode(s.substring(0, 1)) ? 1 : 0;
		}
		if (len > 1) {
			int way1 = 0, way2 = 0;
			way1 = isValidCode(s.substring(1, 2)) ? res[0] : 0;
			way2 = isValidCode(s.substring(0, 2)) ? 1 : 0;
			res[1] = way1 + way2;
		}
		for (int i = 2; i < len; i++) {
			int way1 = 0, way2 = 0;
			way1 = isValidCode(s.substring(i, i + 1)) ? res[i - 1] : 0;
			way2 = isValidCode(s.substring(i - 1, i + 1)) ? res[i - 2] : 0;
			res[i] = way1 + way2;
			// the string can't be decoded anymore
			if (res[i] == 0) {
				return 0;
			}
		}
		return res[len - 1];
	}

	// Recursive, TLE.....
	public int numDecodings2(String s) {
		if (s == null) {
			return 0;
		}

		int len = s.length();
		int res = 0;
		if (len == 0) {
			return 0;
		}
		if (len == 1) {
			return isValidCode(s) ? 1 : 0;
		}
		if (len == 2) {
			res = isValidCode(s) ? 1 : 0;
			if (isValidCode(s.substring(0, 1))) {
				res += numDecodings(s.substring(1, len));
			}
		} else {
			if (isValidCode(s.substring(0, 1))) {
				res += numDecodings(s.substring(1, len));
			}
			if (isValidCode(s.substring(0, 2))) {
				res += numDecodings(s.substring(2, len));
			}
		}
		return res;

	}

}
