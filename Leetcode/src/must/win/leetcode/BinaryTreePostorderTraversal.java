package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// Binary Tree Postorder Traversal 
/*
 * http://answer.ninechapter.com/solutions/binary-tree-postorder-traversal/
 * one stack iterative solution
 * 
 * other recursive way, think about dfs (one passes result set as parameter, one doesn't)
 * 
 * http://n00tc0d3r.blogspot.tw/2013/01/binary-tree-traversals-i.html?q=postorder+traversal
 * two stacks iterative solution
 * Postorder is to visit nodes in a left-right-root order, recursively. 
 * Recall that preorder is root-left-right. If we visit the nodes in a "mirrored preorder", 
 * that is, root-right-left, and store the values in a stack. After we finish the traversal, 
 * pop out values in the stack would give us left-right-root, 
 * which is exactly a postorder traversal!
 * 
 */
public class BinaryTreePostorderTraversal {

	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// one stack iterative solution
	public List<Integer> postorderTraversal(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		TreeNode prev = null;

		while (!stack.isEmpty()) {
			TreeNode curr = stack.peek();
			// traverse down the tree
			if (prev == null || prev.left == curr || prev.right == curr) {
				if (curr.left != null) {
					stack.push(curr.left);
				} else if (curr.right != null) {
					stack.push(curr.right);
				}
			}
			// traverse up the tree from the left
			else if (curr.left == prev) {
				if (curr.right != null) {
					stack.push(curr.right);
				}
			}
			// traverse up the tree from the right
			else {
				result.add(curr.val);
				stack.pop();
			}

			prev = curr;
		}

		return result;
	}

	// recursive way, passes result set as parameter
	public List<Integer> postorderTraversal1(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		traversal(root, result);
		return result;
	}

	// recursive way, doesn't passes result set as parameter, using addAll
	// function
	public List<Integer> postorderTraversal2(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();

		if (root == null) {
			return result;
		}

		result.addAll(postorderTraversal2(root.left));
		result.addAll(postorderTraversal2(root.right));
		result.add(root.val);
		return result;
	}

	// two stack solution, based on mirrored preorder
	public List<Integer> postorderTraversal3(TreeNode root) {
		List<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}
		Stack<TreeNode> stack = new Stack<TreeNode>();
		Stack<Integer> valStack = new Stack<Integer>();

		TreeNode cur = root;
		// find the right-most node
		while (cur != null) {
			stack.push(cur);
			valStack.push(cur.val);
			cur = cur.right;
		}

		while (!stack.empty()) {
			cur = stack.pop();
			// add the left child
			cur = cur.left;
			// find the right-most node
			while (cur != null) {
				stack.push(cur);
				valStack.push(cur.val);
				cur = cur.right;
			}
		}
		// pop values to an array
		while (!valStack.empty()) {
			result.add(valStack.pop());
		}

		return result;
	}

	// for first (passes result set as parameter) recursive way use
	private void traversal(TreeNode root, List<Integer> result) {
		if (root == null) {
			return;
		}
		traversal(root.left, result);
		traversal(root.right, result);
		result.add(root.val);
	}
}
