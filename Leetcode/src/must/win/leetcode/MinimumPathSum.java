package must.win.leetcode;

// Minimum Path Sum
/*
 * http://n00tc0d3r.blogspot.tw/2013/04/minimum-path-sum.html?q=Minimum+Path+Sum
 * DP explanation
 * 
 * my own way reduce space to O(N)
 */
public class MinimumPathSum {
	// my own way reduce space to O(N), time O(M*N)
	public int minPathSum(int[][] grid) {
		if (grid == null || grid.length == 0) {
			return Integer.MAX_VALUE;
		}
		int row = grid.length;
		int col = grid[0].length;

		int[] res = new int[col];
		res[0] = grid[0][0];
		for (int i = 1; i < col; i++) {
			res[i] = res[i - 1] + grid[0][i];
		}

		for (int i = 1; i < row; i++) {
			res[0] += grid[i][0];
			for (int j = 1; j < col; j++) {
				res[j] = grid[i][j] + Math.min(res[j], res[j - 1]);
			}
		}

		return res[col - 1];
	}
}
