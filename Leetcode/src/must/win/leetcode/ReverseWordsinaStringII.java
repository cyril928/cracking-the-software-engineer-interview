package must.win.leetcode;

// Reverse Words in a String II
/*
 * Clean Code Hand Book
 * do it in-place without allocating extra space
 */
public class ReverseWordsinaStringII {

	private void reverse(char[] s, int start, int end) {
		while (start < end) {
			char temp = s[start];
			s[start++] = s[end];
			s[end--] = temp;
		}
	}

	// do it in-place without allocating extra space
	public void reverseWords(char[] s) {
		reverse(s, 0, s.length - 1);
		for (int i = 0, j = 0; j <= s.length; j++) {
			if (j == s.length || s[j] == ' ') {
				reverse(s, i, j - 1);
				i = j + 1;
			}
		}
	}
}
