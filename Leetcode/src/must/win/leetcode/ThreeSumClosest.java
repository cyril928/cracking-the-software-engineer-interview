package must.win.leetcode;

import java.util.Arrays;

// 3Sum Closest 
/*
 * http://n00tc0d3r.blogspot.tw/2013/01/2sum-3sum-4sum-and-variances.html?q=3+sum
 * best implementation and explanation
 */
public class ThreeSumClosest {
	// Best solution
	// This solution runs in time O(n^2) and requires O(1) space.
	public int threeSumClosest(int[] num, int target) {
		int len = num.length;
		if (num == null || num.length < 3) {
			return 0;
		}
		Arrays.sort(num);
		int res = target;
		int abs = Integer.MAX_VALUE;
		for (int i = 0; i < len - 2; i++) {
			// to skip duplicate numbers;
			if (i > 0 && num[i] == num[i - 1]) {
				continue;
			}
			int start = i + 1;
			int end = len - 1;
			while (start < end) {
				int sum = num[i] + num[start] + num[end];
				int diff = sum - target;
				if (Math.abs(diff) < abs) {
					abs = Math.abs(diff);
					res = sum;
				}
				if (diff > 0) {
					do {
						end--;
					} while (start < end && num[end] == num[end + 1]);
				} else if (diff < 0) {
					do {
						start++;
					} while (start < end && num[start] == num[start - 1]);
				} else {
					return target;
				}
			}
		}
		return res;
	}

	// This solution runs in time O(n^2logn) and use O(1) space. Binary search
	// for third element.
	public int threeSumClosest1(int[] num, int target) {
		if (num == null || num.length < 3) {
			return 0;
		}
		Arrays.sort(num);
		int len = num.length;
		int abs = Integer.MAX_VALUE;
		int res = target;
		for (int i = 0; i < len - 2; i++) {
			if (i > 0 && num[i] == num[i - 1]) {
				continue;
			}
			for (int j = i + 1; j < len - 1; j++) {
				if (j > i + 1 && num[j] == num[j - 1]) {
					continue;
				}
				int start = j + 1;
				int end = len - 1;
				while (start <= end) {
					int mid = (end - start) / 2 + start;
					int sum = num[i] + num[j] + num[mid];
					int diff = sum - target;
					if (Math.abs(diff) < abs) {
						abs = Math.abs(diff);
						res = sum;
					}
					if (diff > 0) {
						end = mid - 1;
					} else if (diff < 0) {
						start = mid + 1;
					} else {
						return target;
					}
				}
			}
		}
		return res;
	}
}
