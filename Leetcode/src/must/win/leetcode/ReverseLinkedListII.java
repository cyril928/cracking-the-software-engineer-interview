package must.win.leetcode;

// Reverse Linked List II 
/*
 * my own implementation, most clean one, maintain prev and cur pointer,
 * just manipulate prev.next and cur.next pointer
 * 
 * http://n00tc0d3r.blogspot.com/2013/05/reverse-linked-list.html?q=reverse+linked+list
 * explanation & implementation with partial reverse linked list magic
 */
public class ReverseLinkedListII {

	// my own implementation, most clean one, maintain prev and cur pointer,
	// just manipulate prev.next and cur.next pointer
	public ListNode reverseBetween(ListNode head, int m, int n) {
		if (head == null) {
			return head;
		}

		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode prev = dummy;
		n = n - m;
		while (--m > 0) {
			prev = prev.next;
		}

		ListNode cur = prev.next;
		while (n-- > 0) {
			ListNode next = cur.next.next;
			cur.next.next = prev.next;
			prev.next = cur.next;
			cur.next = next;
		}
		return dummy.next;
	}

	// my own implementation
	public ListNode reverseBetween1(ListNode head, int m, int n) {
		if (head == null) {
			return null;
		}
		ListNode dummy = new ListNode(0);
		dummy.next = head;

		ListNode cur = dummy;
		ListNode prevListTail = cur;

		n = n - m;
		while (m > 0) {
			prevListTail = cur;
			cur = cur.next;
			m--;
		}

		ListNode prev = null;
		while (n >= 0) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
			n--;
		}

		prevListTail.next.next = cur;
		prevListTail.next = prev;

		return dummy.next;
	}

	// with partial reverse linked list magic
	public ListNode reverseBetween2(ListNode head, int m, int n) {
		ListNode dummy = new ListNode(0);
		dummy.next = head;

		// first if the first position, begin is the node before first.
		ListNode pre = dummy, cur = head;
		int pos = 1;

		// find the first
		while (pos < m && cur != null) {
			pre = cur;
			cur = cur.next;
			++pos;
		}

		// reverse the list
		while (pos < n && cur != null) {
			ListNode nt = cur.next.next;
			cur.next.next = pre.next;
			pre.next = cur.next;
			cur.next = nt;
			++pos;
		}

		return dummy.next;
	}
}
