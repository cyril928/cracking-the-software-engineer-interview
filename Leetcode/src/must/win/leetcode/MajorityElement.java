package must.win.leetcode;

// Majority Element 
/*
 * Moore's voting algorithm
 * Time: O(N) Space: O(1)
 * http://n00tc0d3r.blogspot.tw/search?q=Majority+Element
 * explanation and implementation
 * 
 * https://oj.leetcode.com/problems/majority-element/solution/
 * solution comparison
 */
public class MajorityElement {
	public int majorityElement(int[] num) {
		int candidate = 0;
		int count = 0;

		for (int val : num) {
			if (count == 0) {
				candidate = val;
				count = 1;
			} else if (count > 0) {
				count += (candidate == val) ? 1 : -1;
			}
		}
		return candidate;
	}
}
