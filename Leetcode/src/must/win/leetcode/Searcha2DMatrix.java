package must.win.leetcode;

// Search a 2D Matrix 
/*
 * http://n00tc0d3r.blogspot.tw/2013/05/search-2d-matrix.html?q=Search+a+2D+Matrix
 * one binary search with row, col calculation
 * two binary searches
 */
public class Searcha2DMatrix {
	// one binary search with row, col calculation
	public boolean searchMatrix(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0) {
			return false;
		}
		int rows = matrix.length;
		int cols = matrix[0].length;
		int start = 0, end = rows * cols - 1;

		while (start <= end) {
			int mid = (end - start) / 2 + start;
			int num = matrix[mid / cols][mid % cols];
			if (num < target) {
				start = mid + 1;
			} else if (num > target) {
				end = mid - 1;
			} else {
				return true;
			}
		}

		return false;
	}

	// two binary searches,
	public boolean searchMatrix1(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0) {
			return false;
		}

		int start = 0;
		int end = matrix.length - 1;

		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (matrix[mid][0] > target) {
				end = mid - 1;
			} else if (matrix[mid][0] < target) {
				start = mid + 1;
			} else {
				return true;
			}
		}

		// end might be become -1...
		if (end < 0) {
			return false;
		}

		int targetRow = end;

		// other way for first binary search
		/*
		 * while(start < end) { int mid = (end - start) / 2 + start;
		 * 
		 * if(matrix[mid][0] == target) { return true; } else if(matrix[mid][0]
		 * > target) { end = mid - 1; } else if(matrix[mid + 1][0] > target) {
		 * start = mid; break; } else { start = mid + 1; } }
		 * 
		 * 
		 * int targetRow = start;
		 */

		// second binary search for target row
		start = 0;
		end = matrix[0].length - 1;

		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (matrix[targetRow][mid] > target) {
				end = mid - 1;
			} else if (matrix[targetRow][mid] < target) {
				start = mid + 1;
			} else {
				return true;
			}
		}

		return false;
	}
}
