package must.win.leetcode;

// Find Minimum in Rotated Sorted Array
/*
 * My own way
 */
public class FindMinimuminRotatedSortedArray {
	public int findMin(int[] num) {
		if (num == null || num.length == 0) {
			return Integer.MAX_VALUE;
		}

		int low = 0, high = num.length - 1;

		while (low < high) {
			int mid = (high - low) / 2 + low;
			// mid < high, minimum number is mid number or on the left side
			if (num[mid] < num[high]) {
				high = mid;
			}
			// mid > high, minimum number is on the right side
			else if (num[mid] > num[high]) {
				low = mid + 1;
			}
		}

		return num[low];
	}
}
