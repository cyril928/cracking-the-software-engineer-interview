package must.win.leetcode;

// Plus One
/*
 * http://n00tc0d3r.blogspot.tw/2013/05/plus-one.html?q=plus+one
 * most clean one
 */
public class PlusOne {
	// most clean one
	public int[] plusOne(int[] digits) {
		if (digits == null || digits.length == 0) {
			return digits;
		}
		for (int i = digits.length - 1; i >= 0; i--) {
			if (digits[i] < 9) {
				digits[i]++;
				return digits;
			}
			digits[i] = 0;
		}

		int[] res = new int[digits.length + 1];
		res[0] = 1;
		return res;
	}

	// my own way
	public int[] plusOne1(int[] digits) {
		int carry = 1;
		for (int i = digits.length - 1; i >= 0; i--) {
			int val = digits[i] + carry;
			if (val >= 10) {
				val %= 10;
				digits[i] = val;
			} else {
				digits[i] = val;
				return digits;
			}
		}
		int[] result = new int[digits.length + 1];
		result[0] = 1;
		for (int i = 1; i < result.length; i++) {
			result[i] = digits[i - 1];
		}
		return result;
	}
}
