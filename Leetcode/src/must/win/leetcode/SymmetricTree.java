package must.win.leetcode;

import java.util.ArrayDeque;
import java.util.Queue;

// Symmetric Tree 
/*
 * http://n00tc0d3r.blogspot.com/2013/05/same-tree.html?q=same+tree
 * dfs
 * bfs
 */
public class SymmetricTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// dfs
	public boolean isSymmetric(TreeNode root) {
		return (root == null) || isSymmetric(root.left, root.right);
	}

	// dfs helper function
	private boolean isSymmetric(TreeNode p, TreeNode q) {
		if (p == null && q == null) {
			return true;
		}
		if (p == null || q == null) {
			return false;
		}

		return (p.val == q.val) && isSymmetric(p.left, q.right)
				&& isSymmetric(p.right, q.left);
	}

	// bfs
	public boolean isSymmetric1(TreeNode root) {
		return (root == null) || isSymmetric1(root.left, root.right);
	}

	// bfs helper function
	private boolean isSymmetric1(TreeNode p, TreeNode q) {
		if (p == null && q == null) {
			return true;
		}
		if (p == null || q == null) {
			return false;
		}

		Queue<TreeNode> pQue = new ArrayDeque<TreeNode>();
		Queue<TreeNode> qQue = new ArrayDeque<TreeNode>();

		pQue.add(p);
		qQue.add(q);

		while (!pQue.isEmpty() && !qQue.isEmpty()) {
			TreeNode curP = pQue.remove();
			TreeNode curQ = qQue.remove();

			if (curP.val != curQ.val) {
				return false;
			}

			if (curP.left != null && curQ.right != null
					&& curP.left.val == curQ.right.val) {
				pQue.add(curP.left);
				qQue.add(curQ.right);
			} else if (curP.left != null || curQ.right != null) {
				return false;
			}

			if (curP.right != null && curQ.left != null
					&& curP.right.val == curQ.left.val) {
				pQue.add(curP.right);
				qQue.add(curQ.left);
			} else if (curP.right != null || curQ.left != null) {
				return false;
			}
		}

		return pQue.isEmpty() && qQue.isEmpty();
	}
}
