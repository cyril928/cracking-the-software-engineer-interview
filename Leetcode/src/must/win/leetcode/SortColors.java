package must.win.leetcode;

// Sort Colors 
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Sort+Colors
 * one-pass algorithm, two-pass algorithm using counting sort.
 * 
 * generalized counting sort version
 */
public class SortColors {
	// one-pass algorithm
	public void sortColors(int[] A) {
		if (A == null || A.length <= 1) {
			return;
		}
		int cur = 0;
		int pl = 0;
		int pr = A.length - 1;

		// the end condition must be cur equal pr, cause when cur is 0, we have
		// to push it forward
		while (cur <= pr) {
			if (A[cur] == 0) {
				swap(A, cur++, pl++);
			} else if (A[cur] == 2) {
				swap(A, cur, pr--);
			} else {
				cur++;
			}
		}

	}

	// two-pass algorithm using counting sort
	public void sortColors1(int[] A) {
		if (A == null || A.length <= 1) {
			return;
		}
		int[] count = new int[3];
		for (int val : A) {
			count[val]++;
		}
		count[1] += count[0];

		int i = 0;
		while (i < count[0]) {
			A[i] = 0;
			i++;
		}
		while (i < count[1]) {
			A[i] = 1;
			i++;
		}
		while (i < A.length) {
			A[i] = 2;
			i++;
		}
	}

	// generalized counting sort version
	public void sortColors2(int[] A) {
		if (A == null || A.length == 0) {
			throw new IllegalArgumentException("");
		}
		int[] nums = new int[3];
		for (int val : A) {
			nums[val]++;
		}
		int i = 0, j = 0;
		while (i < 3) {
			while (nums[i]-- > 0) {
				A[j++] = i;
			}
			i++;
		}
	}

	// for one-pass algorithm use
	private void swap(int[] A, int l, int r) {
		int temp = A[l];
		A[l] = A[r];
		A[r] = temp;
	}
}
