package must.win.leetcode;

// N-Queens II 
/*
 * use bit-manipulation, with n <= 32 limitation
 * 
 * use one-dimensional array
 * http://yucoding.blogspot.com/2013/01/leetcode-question-60-n-queens-ii.html
 */
public class NQueensII {
	private int res;

	// use one-dimensional array
	private boolean isValid(int[] pos, int depth, int col) {
		for (int r = 0; r < depth; r++) {
			if (pos[r] == col || Math.abs(pos[r] - col) == depth - r) {
				return false;
			}
		}
		return true;
	}

	// use one-dimensional array
	private void nQueens1(int[] pos, int depth, int n) {
		if (depth == n) {
			res++;
		} else {
			for (int c = 0; c < n; c++) {
				if (isValid(pos, depth, c)) {
					pos[depth] = c;
					nQueens1(pos, depth + 1, n);
				}
			}
		}
	}

	// use bit-manipulation, with n <= 32 limitation
	public int totalNQueens(int n) {
		if (n <= 0) {
			return res;
		}
		totalNQueens(n, 0, 0, 0, 0);
		return res;
	}

	// use bit-manipulation, with n <= 32 limitation
	private void totalNQueens(int n, int row, int col, int ld, int rd) {
		if (row == n) {
			res++;
			return;
		}
		int avail = col | ld | rd;
		for (int i = 0; i < n; i++) {
			if (((avail >> i) & 1) == 0) {
				int mask = (1 << i);
				totalNQueens(n, row + 1, col | mask, (ld | mask) << 1,
						(rd | mask) >> 1);
			}
		}
	}

	// use one-dimensional array
	public int totalNQueens1(int n) {
		if (n == 0) {
			return res;
		}
		int[] pos = new int[n];
		nQueens1(pos, 0, n);
		return res;
	}
}
