package must.win.leetcode;

// Reverse Nodes in k-Group
/*
 * http://n00tc0d3r.blogspot.com/search?q=Reverse+Nodes+in+k-Group
 * explanation & implementation
 * The total running time here is still O(n) with O(1) space
 */

public class ReverseNodesinkGroup {

	// The total running time here is still O(n) with O(1) space, when go
	// through k element, reverse list once.
	public ListNode reverseKGroup(ListNode head, int k) {
		if (head == null || k <= 1) {
			return head;
		}

		ListNode dummy = new ListNode(0);
		dummy.next = head;

		ListNode prev = dummy, cur = head;

		int pos = 1;
		while (cur != null) {
			if (pos == k) {
				prev = reverseList(prev, cur);
				pos = 0;
				cur = prev.next;
			} else {
				cur = cur.next;
			}
			pos++;
		}
		return dummy.next;
	}

	// my own implementation, when go through k element, reverse list once. code
	// is not good, not intuitive
	public ListNode reverseKGroup1(ListNode head, int k) {
		if (head == null || k <= 1) {
			return head;
		}
		ListNode dummy = new ListNode(0);
		dummy.next = head;

		ListNode runNode = dummy;

		while (runNode != null) {
			// before target list node
			ListNode prevNode = runNode;
			int n = k;
			while (n-- > 0) {
				// no enough node for reversing
				runNode = runNode.next;
				if (runNode == null) {
					break;
				}
			}
			if (runNode == null) {
				break;
			}
			// after target list node
			ListNode nextNode = runNode.next;

			// update the runNode to prepare for after reversing linkedlist
			runNode = prevNode.next;

			// do reverse linked list
			ListNode prev = nextNode;
			ListNode cur = prevNode.next;

			while (cur != nextNode) {
				ListNode next = cur.next;
				cur.next = prev;
				prev = cur;
				cur = next;
			}
			prevNode.next = prev;
		}
		return dummy.next;
	}

	// reverse the linked list between pre.next and end, inclusively
	// e.g. 0 -> 1 -> 2 -> 3 -> 4 -> 5, pre = 0, end = 5
	// => 0 -> 5 -> 4 -> 3 -> 2 -> 1
	private ListNode reverseList(ListNode start, ListNode end) {
		ListNode prev = end.next;
		ListNode cur = start.next;
		ListNode endNode = end.next;

		while (cur != endNode) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}

		ListNode newPrev = start.next;
		start.next = prev;
		return newPrev;
	}
}
