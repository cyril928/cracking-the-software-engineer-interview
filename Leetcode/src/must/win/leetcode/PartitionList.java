package must.win.leetcode;

// Partition List 
/*
 * http://www.ninechapter.com/solutions/partition-list/
 * O(N) one-pass, most clean way
 */
public class PartitionList {
	// O(N) one-pass, most clean way
	public ListNode partition(ListNode head, int x) {
		if (head == null || head.next == null) {
			return head;
		}

		ListNode leftDummy = new ListNode(0);
		ListNode rightDummy = new ListNode(0);

		ListNode curLeft = leftDummy;
		ListNode curRight = rightDummy;

		while (head != null) {
			if (head.val < x) {
				curLeft.next = head;
				curLeft = curLeft.next;
			} else {
				curRight.next = head;
				curRight = curRight.next;
			}
			head = head.next;
		}

		curLeft.next = rightDummy.next;
		curRight.next = null;

		return leftDummy.next;
	}
}
