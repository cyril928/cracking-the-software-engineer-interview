package must.win.leetcode;

// Median of Two Sorted Arrays 
/*
 * http://www.ninechapter.com/solutions/median-of-two-sorted-arrays/
 * reduce to find kth number of two sorted array, clean and good
 * http://www.acmerblog.com/leetcode-solution-median-of-two-sorted-arrays-6270.html
 * explanation
 * 
 * http://n00tc0d3r.blogspot.com/search?q=Median+of+Two+Sorted+Arrays
 * not understand yet
 * 
 */
public class MedianofTwoSortedArrays {
	// find kth number of two sorted array
	private int findKth(int[] A, int A_start, int[] B, int B_start, int k) {
		if (A_start >= A.length) {
			return B[B_start + k - 1];
		}
		if (B_start >= B.length) {
			return A[A_start + k - 1];
		}

		if (k == 1) {
			return Math.min(A[A_start], B[B_start]);
		}

		int A_index = A_start + (k >> 1) - 1;
		int A_key = A_index < A.length ? A[A_index] : Integer.MAX_VALUE;

		int B_index = B_start + (k >> 1) - 1;
		int B_key = B_index < B.length ? B[B_index] : Integer.MAX_VALUE;

		if (A_key < B_key) {
			return findKth(A, A_start + (k >> 1), B, B_start, k - (k >> 1));
		} else {
			return findKth(A, A_start, B, B_start + (k >> 1), k - (k >> 1));
		}

	}

	// reduce to find kth number of two sorted array problem
	public double findMedianSortedArrays(int A[], int B[]) {
		int len = A.length + B.length;
		if ((len & 1) == 0) {
			return (findKth(A, 0, B, 0, len >> 1) + findKth(A, 0, B, 0,
					(len >> 1) + 1)) / 2.0;
		} else {
			return findKth(A, 0, B, 0, (len >> 1) + 1);
		}
	}
}
