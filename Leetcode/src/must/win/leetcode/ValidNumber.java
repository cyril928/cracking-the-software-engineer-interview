package must.win.leetcode;

// Valid Number 
/*
 * 2. -> valid
 * .2 -> valid
 * e2 -> not 
 * 2e -> not
 * 2.3e2 -> valid
 * 2e2.3 -> not valid
 * 2e+2 -> 200 valid
 * 2e-2 -> 0.02 valid
 * http://answer.ninechapter.com/solutions/valid-number/
 * iterative
 * 
 * http://n00tc0d3r.blogspot.tw/2013/06/valid-number.html?q=valid+number
 * finite state machine, not understand yet.
 */
public class ValidNumber {
	// iterative
	public boolean isNumber(String s) {
		if (s == null) {
			return false;
		}
		s = s.trim();
		if (s.isEmpty()) {
			return false;
		}
		int i = 0, e = s.length() - 1;
		if (s.charAt(i) == '+' || s.charAt(i) == '-') {
			i++;
		}
		boolean num = false;
		boolean firstDot = false;
		boolean firstE = false;

		while (i <= e) {
			char c = s.charAt(i);
			if (Character.isDigit(c)) {
				num = true;
			} else if (c == '.') {
				if (firstDot || firstE) {
					return false;
				}
				firstDot = true;
			} else if (c == 'e') {
				if (!num || firstE) {
					return false;
				}
				firstE = true;
				num = false;
			} else if (c == '+' || c == '-') {
				if (s.charAt(i - 1) != 'e') {
					return false;
				}
			} else {
				return false;
			}
			i++;
		}
		return num;
	}
}
