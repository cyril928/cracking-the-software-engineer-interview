package must.win.leetcode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

// Word Ladder 
/*
 * http://www.programcreek.com/2012/12/leetcode-word-ladder/
 * implementation
 * 
 * http://n00tc0d3r.blogspot.com/2013/07/word-ladder.html?q=word+ladder
 * explanation, DFS, BFS, Dijkstra's algorithm preprocessing, Floyd's algorithm preprocessing
 * 
 */
public class WordLadder {
	// BFS
	public int ladderLength(String start, String end, Set<String> dict) {
		if (start == null | end == null || dict == null || start.length() == 0
				|| start.length() != end.length() || start.equals(end)
				|| dict.size() == 0) {
			return 0;
		}

		int wordLen = start.length();

		Queue<String> wordQueue = new LinkedList<String>();
		Queue<Integer> distanceQueue = new LinkedList<Integer>();

		wordQueue.add(start);
		distanceQueue.add(1);

		while (!wordQueue.isEmpty()) {
			String curWord = wordQueue.poll();
			int curDistance = distanceQueue.poll();

			for (int i = 0; i < wordLen; i++) {
				char[] curCharArray = curWord.toCharArray();
				for (char c = 'a'; c <= 'z'; c++) {
					curCharArray[i] = c;
					String newWord = new String(curCharArray);
					if (newWord.equals(end)) {
						return curDistance + 1;
					} else if (dict.contains(newWord)) {
						dict.remove(newWord);
						wordQueue.add(newWord);
						distanceQueue.add(curDistance + 1);
					}
				}
			}
		}
		return 0;
	}
}
