package must.win.leetcode;

//Best Time to Buy and Sell Stock II
/*
 * http://www.ninechapter.com/solutions/best-time-to-buy-and-sell-stock-ii/  
 * implementation
 */
public class BestTimetoBuyandSellStockII {
	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}
		int maxProfit = 0;
		for (int i = 1; i < prices.length; i++) {
			if (prices[i] > prices[i - 1]) {
				maxProfit += prices[i] - prices[i - 1];
			}
		}
		return maxProfit;
	}
}
