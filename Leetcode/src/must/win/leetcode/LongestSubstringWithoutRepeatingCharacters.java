package must.win.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

// Longest Substring Without Repeating Characters
/*
 * http://answer.ninechapter.com/solutions/longest-substring-without-repeating-characters/
 * HashSet with leftBound approach
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Longest+Substring+Without+Repeating+Characters
 * HashMap method
 * 
 */
public class LongestSubstringWithoutRepeatingCharacters {

	// HashSet with leftBound approach
	public int lengthOfLongestSubstring(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		Set<Character> set = new HashSet<Character>();

		int max = 0, leftBound = 0;
		int len = s.length();

		for (int i = 0; i < len; i++) {
			if (set.contains(s.charAt(i))) {
				while (s.charAt(leftBound) != s.charAt(i)) {
					set.remove(s.charAt(leftBound));
					leftBound++;
				}
				leftBound++;
			} else {
				set.add(s.charAt(i));
				max = Math.max(i - leftBound + 1, max);
			}
		}
		return max;
	}

	// HashMap method
	public int lengthOfLongestSubstring1(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		Map<Character, Integer> map = new HashMap<Character, Integer>();
		int start = 0, max = 0;
		int len = s.length();
		int i = 0;
		for (i = 0; i < len; i++) {
			if (map.containsKey(s.charAt(i)) && map.get(s.charAt(i)) >= start) {
				max = Math.max(i - start, max);
				start = map.get(s.charAt(i)) + 1;
			}
			map.put(s.charAt(i), i);
		}
		max = Math.max(i - start, max);

		return max;
	}
}
