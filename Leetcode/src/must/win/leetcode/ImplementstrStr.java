package must.win.leetcode;

// Implement strStr() 
/*
 * https://www.youtube.com/watch?v=0eaFFO1QlPA
 * KMP
 * 
 * http://answer.ninechapter.com/solutions/implement-strstr/
 * naive way O(mn) time
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Implement+strStr()
 * To-do job -> KMP algorithm 
 * This pre-process function runs in time O(m) and the algorithm runs in time O(n).
 *
 * There are several well-known algorithms for string matching, such as Rabin-Karp algorithm, KMP algorithm, 
 * Boyer-Moore algorithm, etc., and of course, brute force string matching. Most of these algorithm have been 
 * taught in advanced algorithm class. Here is a very nice tutorial from TopCoder about Rabin-Karp and KMP.
 */

public class ImplementstrStr {

	// KMP O(m+N)
	private void buildPrefixTable(String needle, int[] table) {
		int i = -1;
		table[0] = i;
		for (int j = 1; j < table.length; j++) {
			while (i > -1 && needle.charAt(i + 1) != needle.charAt(j)) {
				i = table[i];
			}
			if (needle.charAt(i + 1) == needle.charAt(j)) {
				i++;
			}
			table[j] = i;
		}
	}

	// KMP O(m+N)
	private int match(String haystack, String needle, int[] table) {
		int i = -1;
		for (int j = 0; j < haystack.length(); j++) {
			while (i > -1 && haystack.charAt(j) != needle.charAt(i + 1)) {
				i = table[i];
			}
			if (haystack.charAt(j) == needle.charAt(i + 1)) {
				i++;
			}
			if (i == needle.length() - 1) {
				return j - needle.length() + 1;
			}
		}
		return -1;
	}

	// KMP O(m+N)
	public int strStr(String haystack, String needle) {
		if (haystack == null || needle == null) {
			return -1;
		}
		if (haystack.length() < needle.length()) {
			return -1;
		}

		if (needle.isEmpty()) {
			return 0;
		}
		int[] table = new int[needle.length()];
		buildPrefixTable(needle, table);
		return match(haystack, needle, table);
	}

	// naive way O(mn) time
	public int strStr1(String haystack, String needle) {
		if (haystack == null || needle == null) {
			return -1;
		}
		int hLen = haystack.length();
		int nLen = needle.length();

		for (int i = 0; i <= hLen - nLen; i++) {
			int j = 0;
			for (j = 0; j < nLen; j++) {
				if (haystack.charAt(i + j) != needle.charAt(j)) {
					break;
				}
			}
			if (j == nLen) {
				return i;
			}
		}
		return -1;
	}
}
