package must.win.leetcode;

import java.util.Arrays;
import java.util.Stack;

// Maximal Rectangle 
/*
 * https://oj.leetcode.com/discuss/20240/share-my-dp-solution
 * clean DP solution
 * 
 * http://www.cnblogs.com/lichen782/p/leetcode_maximal_rectangle.html
 * iterative implementation and explanation
 * 
 * http://n00tc0d3r.blogspot.tw/2013/04/maximum-rectangle.html?q=Largest+Rectangle+in+Histogram
 * stack solution(best) implementation and explanation
 */
public class MaximalRectangle {

	// for stack method
	private int helper(int[] height) {
		Stack<Integer> stack = new Stack<Integer>();
		int max = 0;
		int len = height.length;
		for (int i = 0; i <= len; i++) {
			int h = (i == len) ? -1 : height[i];
			while (!stack.isEmpty() && h < height[stack.peek()]) {
				int j = stack.pop();
				int w = stack.isEmpty() ? i : i - stack.peek() - 1;
				max = Math.max(w * height[j], max);
			}
			stack.push(i);
		}
		return max;
	}

	// clean DP solution
	public int maximalRectangle(char[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			return 0;
		}
		int maxArea = 0;

		int m = matrix.length;
		int n = matrix[0].length;
		int[] left = new int[n];
		int[] right = new int[n];
		int[] height = new int[n];
		Arrays.fill(right, n);
		for (int i = 0; i < m; i++) {
			int curLeft = 0, curRight = n;
			// compute height (can do this from either side)
			for (int j = 0; j < n; j++) {
				if (matrix[i][j] == '1') {
					height[j] += 1;
				} else {
					height[j] = 0;
				}
			}
			// compute left (from left to right)
			for (int j = 0; j < n; j++) {
				if (matrix[i][j] == '1') {
					left[j] = Math.max(left[j], curLeft);
				} else {
					left[j] = 0;
					curLeft = j + 1;
				}
			}
			// compute right (from right to left)
			for (int j = n - 1; j >= 0; j--) {
				if (matrix[i][j] == '1') {
					right[j] = Math.min(right[j], curRight);
				} else {
					right[j] = n;
					curRight = j;
				}
			}

			for (int j = 0; j < n; j++) {
				maxArea = Math.max((right[j] - left[j]) * height[j], maxArea);
			}
		}
		return maxArea;
	}

	// stack method, reduce to (Largest Rectangle in Histogram) problem.
	// This algorithm runs in time O(n*m), where n and m are the number of rows
	// and columns in the matrix, respectively. It takes O(n*m) space for the
	// table and O(m) space for stack.
	public int maximalRectangle1(char[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			return 0;
		}
		int maxArea = 0, row = matrix.length, col = matrix[0].length;
		int[] height = new int[col];

		maxArea = Math.max(helper(height), maxArea);
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				if (matrix[i][j] == '0') {
					height[j] = 0;
				} else {
					height[j] += 1;
				}
			}
			maxArea = Math.max(helper(height), maxArea);
		}
		return maxArea;
	}

	// O(N^4) time complexity, iterative
	public int maximalRectangle2(char[][] matrix) {
		if (matrix == null) {
			return 0;
		}
		int len = matrix.length;
		if (len == 0) {
			return 0;
		}
		int width = matrix[0].length;
		int res = 0;
		for (int i = 0; i < len; i++) {
			for (int j = 0; j < width; j++) {
				res = Math.max(maxRectangle(matrix, i, j), res);
			}
		}
		return res;
	}

	// for O(N^4) time complexity method, iterative
	private int maxRectangle(char[][] matrix, int row, int col) {
		int maxArea = 0;
		int arrayWidth = matrix[0].length;
		int arrayLen = matrix.length;

		int minWidth = Integer.MAX_VALUE;
		for (int i = row; i < arrayLen && matrix[i][col] == '1'; i++) {
			int width = 0;
			while ((col + width < arrayWidth)
					&& (matrix[i][col + width] == '1')) {
				width++;
			}
			minWidth = Math.min(width, minWidth);
			maxArea = Math.max((i - row + 1) * minWidth, maxArea);
		}

		return maxArea;
	}
}
