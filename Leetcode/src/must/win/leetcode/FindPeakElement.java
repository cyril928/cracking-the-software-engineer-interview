package must.win.leetcode;

// Find Peak Element 
/*
 * http://blog.csdn.net/u010367506/article/details/41943309
 * most clean binary search
 */
public class FindPeakElement {

	// most clean binary search
	public int findPeakElement(int[] num) {
		if (num == null || num.length == 0) {
			return -1;
		}
		int low = 0, high = num.length - 1;
		/*
		 * if(low == high) return low or high, because we choose every half
		 * after ensuring that number is greater than one of its neighbor
		 */
		while (low < high) {
			int mid = (high - low) / 2 + low;
			if (num[mid] < num[mid + 1]) {
				low = mid + 1;
			} else {
				high = mid;
			}
		}
		return low;
	}

	// my own way, not clean
	public int findPeakElement1(int[] num) {
		if (num == null || num.length == 0) {
			return -1;
		}
		if (num.length == 1) {
			return 0;
		}
		int low = 0, high = num.length - 1;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			if (mid == 0) {
				if (num[mid] > num[mid + 1]) {
					return mid;
				} else {
					low = 1;
				}
			} else if (mid == num.length - 1) {
				if (num[mid] > num[mid - 1]) {
					return mid;
				} else {
					high = num.length - 2;
				}
			} else {
				if (num[mid] < num[mid - 1]) {
					high = mid - 1;
				} else if (num[mid] < num[mid + 1]) {
					low = mid + 1;
				} else {
					return mid;
				}
			}
		}
		return -1;
	}
}
