package must.win.leetcode;

import java.util.TreeSet;

// Wildcard Matching 
/* http://answer.ninechapter.com/solutions/wildcard-matching/
 * DP, Time: O(|s||p|), Space: O(|s|), Time can also optimize to O(|s||p|)
 * 
 * http://n00tc0d3r.blogspot.tw/2013/05/wildcard-matching.html?q=Wildcard+Matching
 * DP (O(MN) Time and O(N) space) explanation, and 
 * recursive (The worst case running time for this algorithm is exponential. 
 * Consider a case where isMatch("aaa...aaa", "*a*a*a...a*a*a*"). TLE)
 * 
 * 
 * my more efficient recursive code
 * 
 */
public class WildcardMatching {

	// other recursive way helper
	private boolean checkEmpty(String p, int pi) {
		for (int i = pi; i < p.length(); i++) {
			if (p.charAt(i) != '*') {
				return false;
			}
		}
		return true;
	}

	// other recursive way helper
	private boolean isMatch(String s, int si, String p, int pi) {
		if (si == s.length()) {
			return checkEmpty(p, pi);
		}

		if (pi == p.length()) {
			return false;
		}

		if (p.charAt(pi) == '*') {
			for (int i = si; i < s.length(); i++) {
				if (isMatch(s, i, p, pi + 1)) {
					return true;
				}
			}
			return false;
		} else if (s.charAt(si) == p.charAt(pi) || p.charAt(pi) == '?') {
			return isMatch(s, si + 1, p, pi + 1);
		} else {
			return false;
		}
	}

	// DP, Time: O(|s||p|), Space: O(|s|), Time can also optimize to O(|s||p|),
	// with two variable to store previous row's first true index.
	public boolean isMatch(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		// without this optimization, it will fail for large data set, eliminate
		// the case like s:qwer p:qwer***s
		int plenNoStar = 0;
		for (char c : p.toCharArray()) {
			if (c != '*') {
				plenNoStar++;
			}
		}
		if (plenNoStar > s.length()) {
			return false;
		}

		s = " " + s;
		p = " " + p;

		int sLen = s.length();
		int pLen = p.length();
		boolean allStar = true;
		boolean[] match = new boolean[sLen];
		match[0] = true;

		int preFirstTruePos = 0;

		for (int i = 1; i < pLen; i++) {
			if (p.charAt(i) != '*') {
				allStar = false;
			}
			int newFirstTruePos = Integer.MAX_VALUE;
			for (int j = sLen - 1; j >= 0; j--) {
				if (j == 0) {
					match[j] = allStar;
				} else if (p.charAt(i) != '*') {
					match[j] = (match[j - 1] && (p.charAt(i) == s.charAt(j) || p
							.charAt(i) == '?'));
				} else {
					match[j] = (j >= preFirstTruePos);
				}
				if (match[j]) {
					newFirstTruePos = Math.min(j, newFirstTruePos);
				}
			}
			preFirstTruePos = newFirstTruePos;
		}
		return match[sLen - 1];
	}

	// DP, Time: O(|s||p|), Space: O(|s|), Time can also optimize to O(|s||p|),
	// with TreeSet to store previous row's first true index.
	public boolean isMatch1(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		// without this optimization, it will fail for large data set, eliminate
		// the case like s:qwer p:qwer***s
		int plenNoStar = 0;
		for (char c : p.toCharArray()) {
			if (c != '*') {
				plenNoStar++;
			}
		}
		if (plenNoStar > s.length()) {
			return false;
		}

		// add dummy space, for easily handle starting index.
		s = " " + s;
		p = " " + p;

		int sLen = s.length();
		int pLen = p.length();
		boolean allStar = true;
		boolean[] match = new boolean[sLen];
		match[0] = true;
		// use TreeSet to store previous row's true index.
		TreeSet<Integer> firstTrueSet = new TreeSet<Integer>();
		firstTrueSet.add(0);

		for (int i = 1; i < pLen; i++) {
			if (p.charAt(i) != '*') {
				allStar = false;
			}
			for (int j = sLen - 1; j >= 0; j--) {
				if (j == 0) {
					match[j] = allStar;
				} else if (p.charAt(i) != '*') {
					match[j] = (match[j - 1] && (p.charAt(i) == s.charAt(j) || p
							.charAt(i) == '?'));
				} else {
					int firstTruePos = firstTrueSet.isEmpty() ? Integer.MAX_VALUE
							: firstTrueSet.first();
					match[j] = (j >= firstTruePos);
				}
				if (match[j]) {
					firstTrueSet.add(j);
				} else {
					firstTrueSet.remove(j);
				}
			}
		}
		return match[sLen - 1];
	}

	// recursive (The worst case running time for this algorithm is exponential.
	// TLE
	public boolean isMatch2(String s, String p) {
		if (s == null || p == null) {
			return false;
		}

		int sLen = s.length();
		int pLen = p.length();
		if (sLen == 0 && pLen == 0) {
			return true;
		}
		// one len is 0, the other one is not
		if (sLen == 0 || pLen == 0) {
			return false;
		}

		// eliminate the case like s:qwer p:qwer***s
		int pNoStarLen = 0;
		int i = 0;
		while (i < pLen) {
			if (p.charAt(i) != '*') {
				pNoStarLen++;
			}
			i++;
		}
		if (pNoStarLen > sLen) {
			return false;
		}

		// single char match
		if (p.charAt(0) != '*') {
			if (s.charAt(0) == p.charAt(0) || p.charAt(0) == '?') {
				return isMatch(s.substring(1), p.substring(1));
			}
		}
		// face * in p
		else {
			// skip duplicate '*'
			int noStarIndex = 0;
			while (noStarIndex < pLen && p.charAt(noStarIndex) == '*') {
				noStarIndex++;
			}
			for (int j = 0; j <= sLen; j++) {
				if (isMatch(s.substring(j), p.substring(noStarIndex))) {
					return true;
				}
			}
		}
		return false;
	}

	// other recursvie way
	public boolean isMatch3(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		return isMatch(s, 0, p, 0);
	}
}
