package must.win.leetcode;

// Single Number II
/*
 * http://www.acmerblog.com/leetcode-single-number-ii-5394.html
 * 1 pass, most efficient way
 * 
 * http://www.ninechapter.com/solutions/single-number-ii/
 * 32 pass, easier to understand
 */
public class SingleNumberII {
	// one pass
	public int singleNumber(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}
		int ones = 0, twos = 0, threes = 0;
		int len = A.length;
		for (int i = 0; i < len; i++) {
			// twos should be accumulative
			twos |= ones & A[i];
			// ones should be accumulative
			ones ^= A[i];
			// threes is like a temp variable, it's not accumulative
			threes = twos & ones;
			twos &= ~threes;
			ones &= ~threes;
		}
		return ones;
	}

	// 32 pass
	public int singleNumber1(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}
		int result = 0;
		int[] bits = new int[32];
		for (int i = 0; i < 32; i++) {
			for (int val : A) {
				bits[i] += ((val >> i) & 1);
			}
			bits[i] %= 3;
			result |= (bits[i] << i);
		}
		return result;
	}

	// 32p pass refined version
	public int singleNumber2(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}

		int res = 0;
		for (int i = 0; i < 32; i++) {
			int accu = 0;
			int bit = (1 << i);
			for (int val : A) {
				accu += ((bit & val) != 0) ? 1 : 0;
			}
			res |= ((accu % 3) << i);
		}

		return res;
	}

}
