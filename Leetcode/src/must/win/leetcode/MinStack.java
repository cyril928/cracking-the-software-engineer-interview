package must.win.leetcode;

import java.util.Stack;

// MinStack
/*
 * https://oj.leetcode.com/problems/min-stack/solution/
 * O(n) space with Minor space optimization
 * push, pop and min operation all in O(1) cost
 * 
 */
public class MinStack {

	// O(n) runtime, O(n) space with Minor space optimization:
	private Stack<Integer> stack = new Stack<Integer>();
	private Stack<Integer> minStack = new Stack<Integer>();

	public int getMin() {
		return minStack.peek();
	}

	public void pop() {
		if (stack.pop().equals(minStack.peek())) {
			minStack.pop();
		}
	}

	public void push(int x) {
		stack.push(x);
		if (minStack.isEmpty() || x <= minStack.peek()) {
			minStack.push(x);
		}
	}

	public int top() {
		return stack.peek();
	}
}
