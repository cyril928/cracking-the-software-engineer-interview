package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Two Sum

/*
 * http://n00tc0d3r.blogspot.tw/2013/01/2sum-3sum-4sum-and-variances.html?q=3+sum
 * best implementation and explanation
 */
public class TwoSum {

	// O(n) Time also require O(n) space.
	public int[] twoSum(int[] numbers, int target) {
		int[] res = new int[2];
		if (numbers == null || numbers.length < 2) {
			return res;
		}
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		int len = numbers.length;
		for (int i = 0; i < len; i++) {
			if (map.containsKey(target - numbers[i])) {
				res[0] = map.get(target - numbers[i]);
				res[1] = i + 1;
			} else {
				map.put(numbers[i], i + 1);
			}
		}
		return res;
	}
}
