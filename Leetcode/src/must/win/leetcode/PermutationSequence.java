package must.win.leetcode;

// Permutation Sequence
/*
 * http://www.ninechapter.com/solutions/permutation-sequence/
 * Math, Backtrace
 */
public class PermutationSequence {
	public String getPermutation(int n, int k) {
		boolean[] used = new boolean[n];
		k = k - 1;
		int factor = 1;
		for (int i = 1; i < n; i++) {
			factor *= i;
		}
		StringBuilder sb = new StringBuilder();
		while (n-- > 0) {
			int index = k / factor;
			k = k % factor;
			for (int i = 0; i < used.length; i++) {
				if (!used[i]) {
					if (index == 0) {
						used[i] = true;
						char c = (char) (i + '1');
						sb.append(c);
						break;
					} else {
						index--;
					}
				}
			}
			factor /= (n > 1 ? n : 1);
		}
		return sb.toString();
	}
}
