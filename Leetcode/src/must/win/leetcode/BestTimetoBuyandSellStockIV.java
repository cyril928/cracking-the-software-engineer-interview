package must.win.leetcode;

// Best Time to Buy and Sell Stock IV 
/*
 * https://leetcode.com/discuss/25603/a-concise-dp-solution-in-java
 * O(K*N) DP
 * Because buy and sell prices may not be the same, when k>len/2, that means we 
 * can do as many transactions as we want. So, in case k>len/2, this problem is 
 * same to Best Time to Buy and Sell Stock III.
 * t[i][j] = Math.max(t[i][j - 1], prices[j] + tmpMax) gives us the maximum price 
 * when we sell at this price; tmpMax = Math.max(tmpMax, t[i - 1][j] - prices[j]) 
 * gives us the value when we buy at this price and leave this value for prices[j+1].
 * 
 * 
 * http://www.dddxhh.me/post/2015-03-03#toc_5
 * O(K*N) DP, O(K*N2) DP
 * 
 */
public class BestTimetoBuyandSellStockIV {
	private int helper(int[] prices) {
		int profit = 0;
		for (int i = 1; i < prices.length; i++) {
			if (prices[i] > prices[i - 1]) {
				profit += (prices[i] - prices[i - 1]);
			}
		}
		return profit;
	}

	// Time: O(K*N) Space: O(K*N) DP
	public int maxProfit(int k, int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}
		int len = prices.length;
		if (k >= len) {
			return helper(prices);
		}

		int[][] dp = new int[k + 1][len];

		for (int i = 1; i <= k; i++) {
			int tmpMax = -prices[0];
			for (int j = 1; j < len; j++) {
				dp[i][j] = Math.max(dp[i][j - 1], prices[j] + tmpMax);
				tmpMax = Math.max(-prices[j] + dp[i - 1][j - 1], tmpMax);
			}
		}
		return dp[k][len - 1];
	}
}
