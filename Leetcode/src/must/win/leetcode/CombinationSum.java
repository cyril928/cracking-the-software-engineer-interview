package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Combination Sum 
/* 
 * http://answer.ninechapter.com/solutions/combination-sum/
 * top-down approach with remove duplication scheme
 */

public class CombinationSum {

	// top-down recursive approach
	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (candidates == null || candidates.length == 0) {
			return result;
		}

		Arrays.sort(candidates);
		if (target < candidates[0]) {
			return result;
		}
		combinationSumHelper(result, new ArrayList<Integer>(), candidates, 0,
				target);
		return result;
	}

	// bottom-up recursive approach
	public List<List<Integer>> combinationSum1(int[] candidates, int target) {
		if (candidates == null || candidates.length == 0) {
			return new ArrayList<List<Integer>>();
		}

		Arrays.sort(candidates);
		if (target < candidates[0]) {
			return new ArrayList<List<Integer>>();
		}

		return combinationSumHelper1(candidates, 0, target);
	}

	// top-down recursive approach helper function
	private void combinationSumHelper(List<List<Integer>> result,
			List<Integer> path, int[] candidates, int index, int target) {
		if (target == 0) {
			result.add(new ArrayList<Integer>(path));
			return;
		}
		int len = candidates.length;
		for (int i = index; i < len; i++) {
			int newTarget = target - candidates[i];
			if (newTarget >= 0) {
				path.add(candidates[i]);
				combinationSumHelper(result, path, candidates, i, newTarget);
				path.remove(path.size() - 1);
			}
		}
	}

	// bottom-up recursive approach helper function
	List<List<Integer>> combinationSumHelper1(int[] candidates, int index,
			int target) {
		List<List<Integer>> results = new ArrayList<List<Integer>>();
		if (target == 0) {
			List<Integer> result = new ArrayList<Integer>();
			results.add(result);
			return results;
		}
		int len = candidates.length;
		for (int i = index; i < len; i++) {
			int newTarget = target - candidates[i];
			if (newTarget >= 0) {
				for (List<Integer> result : combinationSumHelper1(candidates,
						i, newTarget)) {
					result.add(0, candidates[i]);
					results.add(result);
				}
			}
		}
		return results;
	}
}
