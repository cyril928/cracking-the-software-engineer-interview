package must.win.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Subsets II 
/*
 * http://www.ninechapter.com/solutions/subsets-ii/
 * DFS, top-down result building
 * 
 * http://n00tc0d3r.blogspot.com/2013/06/subsets.html?q=subset
 * iterative
 * 
 * http://n00tc0d3r.blogspot.com/2013/06/subsets.html?q=subset
 * DFS, bottom-up result building
 */
public class SubsetsII {
	// DFS, top-down result building
	public List<List<Integer>> subsetsWithDup(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		Arrays.sort(num);
		subsetsWithDupHelper(result, new ArrayList<Integer>(), num, 0);
		return result;
	}

	// DFS, bottom-up result building, function helper
	public List<List<Integer>> subsetsWithDup1(int[] num) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		Arrays.sort(num);
		return subsetsWithDupHelper1(num, num.length - 1);
	}

	// iterative
	public List<List<Integer>> subsetsWithDup2(int[] num) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return res;
		}
		Arrays.sort(num);
		res.add(new ArrayList<Integer>());

		int i = 0;
		while (i < num.length) {
			int j = i;
			while (j < num.length && num[j] == num[i]) {
				j++;
			}
			int size = res.size();
			while (size-- > 0) {
				int n = j - i;
				while (n > 0) {
					List<Integer> newList = new ArrayList<Integer>(
							res.get(size));
					for (int k = 0; k < n; k++) {
						newList.add(num[i]);
					}
					res.add(newList);
					n--;
				}
			}
			i = j;
		}
		return res;
	}

	// DFS, top-down result building, function helper
	private void subsetsWithDupHelper(List<List<Integer>> result,
			List<Integer> list, int[] num, int pos) {
		result.add(new ArrayList<Integer>(list));
		for (int i = pos; i < num.length; i++) {
			if (i != pos && num[i] == num[i - 1]) {
				continue;
			}
			list.add(num[i]);
			subsetsWithDupHelper(result, list, num, i + 1);
			list.remove(list.size() - 1);
		}
	}

	// DFS, bottom-up result building, function helper
	private List<List<Integer>> subsetsWithDupHelper1(int[] num, int pos) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (pos < 0) {
			result.add(new ArrayList<Integer>());
		} else {
			int sameNum = 1; // number of continuous same number in num[]
			while (pos >= 0) {
				if (pos > 0 && num[pos] == num[pos - 1]) {
					sameNum++;
					pos--;
				} else {
					break;
				}
			}
			result = subsetsWithDupHelper1(num, pos - 1);
			int size = result.size();
			while (size-- > 0) {
				List<Integer> list = new ArrayList<Integer>(result.get(size));
				for (int i = 1; i <= sameNum; i++) {
					list.add(num[pos]);
					result.add(list);
					list = new ArrayList<Integer>(list);
				}
			}
		}
		return result;
	}

}
