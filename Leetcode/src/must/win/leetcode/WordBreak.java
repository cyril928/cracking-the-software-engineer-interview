package must.win.leetcode;

import java.util.Set;

// Word Break 
/*
 * http://www.ninechapter.com/solutions/word-break/
 * dp with optimization
 * 
 * http://n00tc0d3r.blogspot.com/search?q=word+segment
 * dp & recursive
 * 
 */
public class WordBreak {
	// dp with optimization helper function
	private int getMaxLength(Set<String> dict) {
		int maxLength = 0;
		for (String word : dict) {
			maxLength = Math.max(maxLength, word.length());
		}
		return maxLength;
	}

	// dp with optimization
	public boolean wordBreak(String s, Set<String> dict) {
		if (s == null || s.length() == 0) {
			return false;
		}

		int maxLength = getMaxLength(dict);
		boolean[] canSegment = new boolean[s.length() + 1];

		canSegment[0] = true;
		for (int i = 1; i <= s.length(); i++) {
			canSegment[i] = false;
			for (int j = 1; j <= maxLength && j <= i; j++) {
				if (!canSegment[i - j]) {
					continue;
				}
				String word = s.substring(i - j, i);
				if (dict.contains(word)) {
					canSegment[i] = true;
					break;
				}
			}
		}

		return canSegment[s.length()];
	}

	// dp
	public boolean wordBreak1(String s, Set<String> dict) {
		int len = s.length();
		if (len < 1) {
			return false;
		}
		boolean[] match = new boolean[len];
		for (int i = 0; i < len; i++) {
			if (dict.contains(s.substring(0, i + 1))) {
				match[i] = true;
				continue;
			}
			for (int j = 0; j < i; j++) {
				if (match[j] && dict.contains(s.substring(j + 1, i + 1))) {
					match[i] = true;
					break;
				}
			}
		}
		return match[len - 1];
	}

	// recursive way
	public boolean wordBreak2(String s, Set<String> dict) {
		if (s.length() == 0 || s == null || dict == null) {
			return false;
		}
		if (dict.contains(s)) {
			return true;
		}

		for (int i = 0; i < s.length(); i++) {
			String prefix = s.substring(0, i + 1);
			if (dict.contains(prefix)) {
				if (wordBreak1(s.substring(i + 1), dict)) {
					return true;
				}
			}
		}
		return false;
	}
}
