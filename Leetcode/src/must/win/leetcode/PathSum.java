package must.win.leetcode;

// Path Sum
/*
 * http://www.ninechapter.com/solutions/path-sum/
 * implementation
 * be careful that the question is asked about root-leaf!!! path 
 */
public class PathSum {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	public boolean hasPathSum(TreeNode root, int sum) {
		if (root == null) {
			return false;
		}
		if (root.left == null && root.right == null) {
			return sum == root.val;
		}
		return hasPathSum(root.left, sum - root.val)
				|| hasPathSum(root.right, sum - root.val);
	}
}
