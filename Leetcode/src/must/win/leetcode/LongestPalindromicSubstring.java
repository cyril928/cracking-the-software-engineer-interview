package must.win.leetcode;

// Longest Palindromic Substring
/* 
 * 
 * middle out way with minimum time and space so far
 * 
 * 
 * http://n00tc0d3r.blogspot.tw/search?q=Longest+Palindromic+Substring
 * middle out way  O(N2) time O(1) space
 * valid palindrome, we use two pointers moving towards the middle. But here, 
 * we use two pointers to towards ends.Why? If we move from ends towards middle, 
 * we need to determine the two ends. There are C(n, 2) choices and for each choice, 
 * it takes O(n) time to check whether it is a palindrome. So, the total running time is O(n^3)!
 * While, if we move from middle towards ends, we only need to iterate through the list and 
 * expand from each character. That gives us O(n^2) total running time and only O(1) space.
 * 
 * optimized solution :If the longest is already longer than the remaining part of the string, we can stop there.
 * 
 * 
 * http://www.cnblogs.com/yuzhangcmu/p/4189068.html
 * DP  O(N2) time O(N2) space
 * 
 * http://blog.csdn.net/hopeztm/article/details/7932245
 * Manacher's Algorithm (to-do) O(N)
 */

public class LongestPalindromicSubstring {

	// middle out with little optimization
	private String findPalindrome(String s, int left, int right) {
		int len = s.length();
		while (left >= 0 && right < len && s.charAt(left) == s.charAt(right)) {
			left--;
			right++;
		}
		return s.substring(left + 1, right);
	}

	// middle out way with minimum time and space so far
	public String longestPalindrome(String s) {
		int lsp = 0, rsp = 0;
		int len = s.length();
		for (int i = 0; i < len - (rsp - lsp) / 2; i++) {
			int lp = i - 1, rp = i + 1;
			while (lp >= 0 && rp < len && s.charAt(lp) == s.charAt(rp)) {
				lp--;
				rp++;
			}
			if (rp - lp - 1 > rsp - lsp) {
				lsp = lp + 1;
				rsp = rp;
			}
			lp = i;
			rp = i + 1;
			while (lp >= 0 && rp < len && s.charAt(lp) == s.charAt(rp)) {
				lp--;
				rp++;
			}
			if (rp - lp - 1 > rsp - lsp) {
				lsp = lp + 1;
				rsp = rp;
			}
		}

		return s.substring(lsp, rsp);
	}

	// middle out with little optimization
	public String longestPalindrome1(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		String result = "";
		int len = s.length();

		for (int i = 0; i < len - result.length() / 2; i++) {
			String palinStr = findPalindrome(s, i, i);
			if (palinStr.length() > result.length()) {
				result = palinStr;
			}

			palinStr = findPalindrome(s, i - 1, i);
			if (palinStr.length() > result.length()) {
				result = palinStr;
			}
		}

		return result;
	}

	// DP (N2) time O(N2) space
	public String longestPalindrome2(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		int len = s.length();
		boolean[][] dp = new boolean[len][len];
		String res = "";
		int maxLen = 0;
		for (int i = len - 1; i >= 0; i--) {
			for (int j = i; j < len; j++) {
				if (s.charAt(i) == s.charAt(j)
						&& (j - i <= 2 || dp[i + 1][j - 1])) {
					dp[i][j] = true;
					if (maxLen < j - i + 1) {
						maxLen = j - i + 1;
						res = s.substring(i, j + 1);
					}
				}
			}
		}
		return res;
	}
}
