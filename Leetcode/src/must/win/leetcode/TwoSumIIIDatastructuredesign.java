package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Two Sum III - Data structure design
/*
 * Clean Code Hand Book
 * add - O(n) runtime, find - O(1) runtime, O(n2) space - Store pair sums in hash table
 * add - O(log n) runtime, find - O(n) runtime, O(n) space - Binary search + Two pointers:
 * add - O(1) runtime, find - O(n) runtime, O(n) space - Store input in hash table
 */
public class TwoSumIIIDatastructuredesign {
	Map<Integer, Integer> map = new HashMap<Integer, Integer>();

	public void add(int input) {
		int count = map.containsKey(input) ? map.get(input) : 0;
		map.put(input, count + 1);
	}

	public boolean find(int val) {
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			int key = entry.getKey();
			if (map.containsKey(val - key)) {
				if (val - key != key || map.get(val - key) >= 2) {
					return true;
				}
			}
		}
		return false;
	}
}
