package must.win.leetcode;

// Remove Nth Node From End of List 
/*
 * http://www.ninechapter.com/solutions/remove-nth-node-from-end-of-list/
 * implementation
 */
public class RemoveNthNodeFromEndofList {
	public ListNode removeNthFromEnd(ListNode head, int n) {
		if (n <= 0) {
			return null;
		}

		ListNode dummy = new ListNode(0);
		dummy.next = head;

		while (n > 0) {
			if (head == null) {
				return null;
			}
			head = head.next;
			n--;
		}

		ListNode preDeleteNode = dummy;
		while (head != null) {
			preDeleteNode = preDeleteNode.next;
			head = head.next;
		}
		preDeleteNode.next = preDeleteNode.next.next;
		return dummy.next;
	}
}
