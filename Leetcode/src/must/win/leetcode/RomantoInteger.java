package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Roman to Integer 
/* int:  1000,  900, 500, 400,  100,  90,  50,   40,   10,   9,   5,    4,   1
 * roman: "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"
 *
 * http://answer.ninechapter.com/solutions/roman-to-integer/
 * backward solution
 * http://n00tc0d3r.blogspot.tw/search?q=Roman+to+Integer
 * forward solution
 * 
 */
public class RomantoInteger {

	// backward solution
	public int romanToInt(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		Map<Character, Integer> map = new HashMap<Character, Integer>();
		map.put('M', 1000);
		map.put('D', 500);
		map.put('C', 100);
		map.put('L', 50);
		map.put('X', 10);
		map.put('V', 5);
		map.put('I', 1);

		int len = s.length();
		int res = map.get(s.charAt(len - 1));
		for (int i = len - 2; i >= 0; i--) {
			if (map.get(s.charAt(i + 1)) <= map.get(s.charAt(i))) {
				res += map.get(s.charAt(i));
			} else {
				res -= map.get(s.charAt(i));
			}
		}

		return res;
	}

	// forward solution
	public int romanToInt1(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}

		Map<Character, Integer> map = new HashMap<Character, Integer>();
		map.put('M', 1000);
		map.put('D', 500);
		map.put('C', 100);
		map.put('L', 50);
		map.put('X', 10);
		map.put('V', 5);
		map.put('I', 1);

		int res = 0;
		int len = s.length();
		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);
			if (i + 1 < len && map.get(s.charAt(i + 1)) > map.get(c)) {
				res += map.get(s.charAt(i + 1)) - map.get(c);
				i++;
			} else {
				res += map.get(c);
			}
		}

		return res;
	}

}
