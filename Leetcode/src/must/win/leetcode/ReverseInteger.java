package must.win.leetcode;

// Reverse Integer
/*
 * Clean Code Hand Book
 * most clean iterative
 * 
 * http://n00tc0d3r.blogspot.tw/2013/05/reverse-integer-binary-bits.html?q=reverse+integer
 * explanation, also give another problem -> reverse binary
 */
public class ReverseInteger {

	// most clean iterative
	public int reverse(int x) {
		int res = 0;
		while (x != 0) {
			if (Math.abs(res) > 214748364) {
				return 0;
			}
			res = res * 10 + x % 10;
			x /= 10;
		}
		return res;
	}

	// iterative
	public int reverse1(int x) {
		long res = 0;
		while (x != 0) {
			res = res * 10 + x % 10;
			x /= 10;
		}
		if (res > Integer.MAX_VALUE || res < Integer.MIN_VALUE) {
			return 0;
		}
		return (int) res;
	}

	// recursive
	public int reverse2(int x) {
		return reverseRecursive(x, 0);
	}

	// for recursive helper funciton
	private int reverseRecursive(int x, long res) {
		if (x == 0) {
			if (res > Integer.MAX_VALUE || res < Integer.MIN_VALUE) {
				return 0;
			}
			return (int) res;
		}
		return reverseRecursive(x / 10, res * 10 + x % 10);
	}
}
