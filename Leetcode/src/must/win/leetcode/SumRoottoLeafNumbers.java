package must.win.leetcode;

// Sum Root to Leaf Numbers
/*
 * http://mattcb.blogspot.tw/search?q=Sum+Root+to+Leaf+Numbers*
 * version1
 * http://yucoding.blogspot.tw/2013/05/leetcode-question-130-sum-root-to-leaf.html*
 * version2
 * 
 * Once we see this kind of problem, no matter what sum is required to output, 
 * "all root-to-leaf" phrase reminds us the classic Tree Traversal or Depth-First-Search algorithm.
 *  Then according to the specific problem, compute and store the values we need. 
 *  Here in this problem, while searching deeper, add the values up (times 10 + current value), 
 *  and add the sum to final result if meet the leaf node (left and right child are both NULL).
 * 
 */

/* can't be like C passing int pointer of sum value and accumulate all of path sum*/
public class SumRoottoLeafNumbers {
	public class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode(int x) {
			val = x;
		}
	}

	private int dfs(TreeNode root, int sum) {
		if (root == null) {
			return 0;
		}
		int res = 10 * sum + root.val;
		if (root.left == null && root.right == null) {
			return res;
		}
		return dfs(root.left, res) + dfs(root.right, res);
	}

	public int sumNumbers(TreeNode root) {
		return dfs(root, 0);
	}
}
