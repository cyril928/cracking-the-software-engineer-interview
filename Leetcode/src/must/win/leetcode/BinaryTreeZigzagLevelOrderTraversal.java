package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// Binary Tree Zigzag Level Order Traversal 
/*
 * http://n00tc0d3r.blogspot.com/2013/01/binary-tree-traversals-ii.html?q=Binary+Tree+Level+Order+Traversal
 * stack solution
 */
public class BinaryTreeZigzagLevelOrderTraversal {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		boolean normalOrder = true;

		stack.push(root);
		while (!stack.isEmpty()) {
			Stack<TreeNode> nextLevelStack = new Stack<TreeNode>();
			List<Integer> level = new ArrayList<Integer>();

			while (!stack.isEmpty()) {
				TreeNode curNode = stack.pop();
				level.add(curNode.val);
				if (normalOrder) {
					if (curNode.left != null) {
						nextLevelStack.push(curNode.left);
					}
					if (curNode.right != null) {
						nextLevelStack.push(curNode.right);
					}
				} else {
					if (curNode.right != null) {
						nextLevelStack.push(curNode.right);
					}
					if (curNode.left != null) {
						nextLevelStack.push(curNode.left);
					}
				}
			}

			result.add(level);
			stack = nextLevelStack;
			normalOrder = !normalOrder;
		}

		return result;
	}
}
