package must.win.leetcode;

import java.util.ArrayList;
import java.util.List;

// Combinations
/*
 * http://n00tc0d3r.blogspot.tw/2013/01/combinations.html?q=Combinations
 * top-down recording recursive (with tree pruning), bottom-up recording recursive (with tree pruning), iterative
 * 
 */
public class Combinations {

	// top-down recording recursive (with tree pruning)
	public List<List<Integer>> combine(int n, int k) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (k < 1 || n < k) {
			return result;
		}
		combineHelper(result, new ArrayList<Integer>(), 1, n, k);
		return result;
	}

	// bottom-up recording recursive (with tree pruning)
	public List<List<Integer>> combine1(int n, int k) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (k < 1 || n < k) {
			return result;
		}
		if (k == 1) {
			for (int i = n; i >= 1; i--) {
				List<Integer> path = new ArrayList<Integer>();
				path.add(i);
				result.add(path);
			}
		} else {
			for (int i = n; i >= k; i--) {
				List<List<Integer>> subRes = combine(i - 1, k - 1);
				for (List<Integer> path : subRes) {
					path.add(i);
				}
				result.addAll(subRes);
			}
		}
		return result;
	}

	// iterative
	public List<List<Integer>> combine2(int n, int k) {
		List<List<Integer>> results = new ArrayList<List<Integer>>();
		if (k < 1 || n < k) {
			return results;
		}

		for (; k > 0; k--) {
			int num = results.size();
			if (num == 0) {
				for (int i = 1; i <= n - k + 1; i++) {
					List<Integer> res = new ArrayList<Integer>();
					res.add(i);
					results.add(res);
				}
				continue;
			}

			for (int j = 0; j < num; j++) {
				List<Integer> cur = results.get(j);
				int lastVal = cur.get(cur.size() - 1);
				for (int i = lastVal + 1; i < n - k + 1; i++) {
					List<Integer> res = new ArrayList<Integer>(cur);
					res.add(i);
					results.add(res);
				}
				cur.add(n - k + 1);

			}
		}
		return results;
	}

	// top-down recording recursive (with tree pruning) helper function
	private void combineHelper(List<List<Integer>> result, List<Integer> path,
			int s, int n, int k) {
		if (k == 0) {
			result.add(new ArrayList<Integer>(path));
		} else {
			for (int i = s; i <= n - k + 1; i++) {
				path.add(i);
				combineHelper(result, path, i + 1, n, k - 1);
				path.remove(path.size() - 1);
			}
		}
	}

}
