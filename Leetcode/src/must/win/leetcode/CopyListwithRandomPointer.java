package must.win.leetcode;

import java.util.HashMap;
import java.util.Map;

// Copy List with Random Pointer 
/*
 * http://www.programcreek.com/2012/12/leetcode-copy-list-with-random-pointer/
 * O(N) time and O(1) space solution, will break original list structure.
 * O(N) time and O(N) space solution, HashMap solution.
 *	
 * http://blog.csdn.net/fightforyourdream/article/details/16879561
 * picture
 * 
 */
public class CopyListwithRandomPointer {
	class RandomListNode {
		int label;
		RandomListNode next, random;

		RandomListNode(int x) {
			this.label = x;
		}
	}

	// O(N) time and O(1) space solution
	public RandomListNode copyRandomList(RandomListNode head) {
		if (head == null) {
			return null;
		}

		// combine two list
		RandomListNode cur = head;
		while (cur != null) {
			RandomListNode nextNode = cur.next;
			RandomListNode newNode = new RandomListNode(cur.label);
			newNode.next = nextNode;
			cur.next = newNode;
			cur = nextNode;
		}

		// assign the random pointer of new node
		cur = head;
		while (cur != null) {
			if (cur.random != null) {
				cur.next.random = cur.random.next;
			}
			cur = cur.next.next;
		}

		// break two list
		cur = head;
		RandomListNode newHead = head.next;
		while (cur != null) {
			RandomListNode temp = cur.next;
			cur.next = temp.next;
			if (temp.next != null) {
				temp.next = temp.next.next;
			}
			cur = cur.next;
		}
		return newHead;
	}

	// O(N) time and O(N) space solution
	public RandomListNode copyRandomList1(RandomListNode head) {
		if (head == null) {
			return null;
		}

		Map<RandomListNode, RandomListNode> map = new HashMap<RandomListNode, RandomListNode>();

		RandomListNode newHead = new RandomListNode(head.label);
		RandomListNode p = head;
		RandomListNode q = newHead;
		map.put(head, newHead);

		p = p.next;
		while (p != null) {
			RandomListNode newNode = new RandomListNode(p.label);
			map.put(p, newNode);
			q.next = newNode;
			q = newNode;
			p = p.next;
		}

		p = head;
		q = newHead;
		while (p != null) {
			if (p.random != null) {
				q.random = map.get(p.random);
			}
			p = p.next;
			q = q.next;
		}

		return newHead;
	}
}
