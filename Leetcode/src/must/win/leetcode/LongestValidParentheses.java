package must.win.leetcode;

import java.util.Stack;

// Longest Valid Parentheses
/*
 * http://n00tc0d3r.blogspot.tw/2013/04/longest-valid-parentheses.html?q=Longest+Valid+Parentheses
 * stack solution, O(n) time and O(n) space for the stack.
 * in place solution, O(n) time and O(1) space.
 * 
 * 
 * http://answer.ninechapter.com/solutions/longest-valid-parentheses/
 * similar to above stack solution, but using accumulated length rather than tracking the previous location.
 * 
 */
public class LongestValidParentheses {

	// for in-place solution
	private int findValidParentheses(String s, int start, int end, int step,
			char c) {
		int maxLen = 0;
		int len = 0;
		int lefts = 0;

		for (int i = start; i != end; i += step) {
			if (s.charAt(i) == c) {
				lefts++;
			} else {
				if (lefts > 0) {
					lefts--;
					len++;
					if (lefts == 0) {
						maxLen = Math.max(len * 2, maxLen);
					}
				} else if (lefts == 0) {
					len = 0;
				}
			}
		}
		return maxLen;
	}

	// stack solution, O(n) time and O(n) space for the stack.
	public int longestValidParentheses(String s) {
		if (s == null || s.length() < 2) {
			return 0;
		}
		int sLen = s.length();
		int maxLen = 0;
		int last = -1;

		Stack<Integer> stack = new Stack<Integer>();
		for (int i = 0; i < sLen; i++) {
			if (s.charAt(i) == '(') {
				stack.push(i);
			} else {
				if (!stack.isEmpty()) {
					stack.pop();
					if (stack.isEmpty()) {
						maxLen = Math.max(i - last, maxLen);
					} else {
						maxLen = Math.max(i - stack.peek(), maxLen);
					}
				} else { // no match left
					last = i;
				}
			}
		}
		return maxLen;
	}

	// in-place solution
	public int longestValidParentheses1(String s) {
		if (s == null || s.length() < 2) {
			return 0;
		}
		return Math.max(findValidParentheses(s, 0, s.length(), 1, '('),
				findValidParentheses(s, s.length() - 1, -1, -1, ')'));
	}
}
