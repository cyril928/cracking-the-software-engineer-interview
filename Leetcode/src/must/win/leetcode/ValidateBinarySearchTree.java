package must.win.leetcode;

import java.util.Stack;

// Validate Binary Search Tree
/*	http://www.ninechapter.com/solutions/validate-binary-search-tree/
 *  dfs with previous node value and current node value checking (but has bug of Integer.MIN_VALUE)
 * 
 *  http://n00tc0d3r.blogspot.com/2013/04/validate-binary-search-tree.html?q=validate
 *  dfs with user-level stack and previous node value and current node value checking
 */
public class ValidateBinarySearchTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// dfs with previous node value and current node value checking
	private TreeNode last = null;

	public boolean isValidBST(TreeNode root) {
		if (root == null) {
			return true;
		}
		if (!isValidBST(root.left)) {
			return false;
		}
		if (last != null) {
			if (last.val >= root.val) {
				return false;
			}
		}
		last = root;
		if (!isValidBST(root.right)) {
			return false;
		}
		return true;
	}

	// dfs with user-level stack and previous node value and
	// current node value checking
	public boolean isValidBST1(TreeNode root) {
		if (root == null) {
			return true;
		}

		TreeNode prev = null;
		Stack<TreeNode> stack = new Stack<TreeNode>();

		TreeNode cur = root;

		while (cur != null) {
			stack.push(cur);
			cur = cur.left;
		}

		while (!stack.isEmpty()) {
			cur = stack.pop();
			if (prev != null && prev.val >= cur.val) {
				return false;
			}
			prev = cur;
			cur = prev.right; // cur = cur.right; cause Eclipse always show
								// warning for this..
			while (cur != null) {
				stack.push(cur);
				cur = cur.left;
			}
		}

		return true;
	}

	// dfs with left & right range checking (but has bug of Integer.MIN_VALUE
	// Integer.MAX_VALUE)
	public boolean isValidBST2(TreeNode root) {
		return isValidRange(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}

	// dfs with left & right range checking (but has bug of Integer.MIN_VALUE
	// Integer.MAX_VALUE), helper
	// function
	private boolean isValidRange(TreeNode root, int min, int max) {
		if (root == null) {
			return true;
		}
		if (root.val <= min || root.val >= max) {
			return false;
		}
		return isValidRange(root.left, min, root.val)
				&& isValidRange(root.right, root.val, max);
	}
}
