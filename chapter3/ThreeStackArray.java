public class ThreeStackArray {

   int stackSize = 300;
   int[] buffer = new int[stackSize * 3];
   int[] stackPointer = {0,0,0}; 

   void push(int stackNum, int value) { 
       stackPointer[stackNum]++;
       buffer[stackNum * stackSize + stackPointer[stackNum]] = value;
   }
   int pop(int stackNum) {
       int index = stackNum * stackSize + stackPointer[stackNum];
       int value = buffer[index];
       stackPointer[stackNum]--;
       buffer[index] = 0;
       return value;
   }
   int peek(int stackNum) {
       int index = stackNum * stackSize + stackPointer[stackNum];
       return buffer[index]; 
   }
   boolean isEmpty(int stackNum){
       if(stackPointer[stackNum] == 0)
           return true;
       else
           return false;   
   }
}
