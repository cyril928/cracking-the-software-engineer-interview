class Stack {
   Node top;
   public void push(int item) {
       Node n = new Node(item);
       n.next = top;
       top = n;
   }
   public Object pop() {
       if(top == null)
           return null;
       Object item = top.data;
       top = top.next;
       return item;
   }
}

class Node {
    Node next;
    int data;
    public Node(int value) {
        this.data = value;
        this.next = null;
    }
}
