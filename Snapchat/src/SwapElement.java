import java.util.HashMap;
import java.util.Map;


public class SwapElement {
	public void swapElement(int[] A, int[] B) {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for(int i = 0; i < A.length; i++) {
			map.put(A[i], i);
		}
		
		for(int i = 0; i < A.length - 1; i++) {
			int b = B[i];
			int j = map.get(b);
			swap(A, i, j);
			map.put(A[j], j);
		}
	}
	
	private void swap(int[] A, int a, int b) {
		int temp = A[a];
		A[a] = A[b];
		A[b] = temp;
	}
	public static void main(String[] args) {
		
		int[] A = {1, 2, 3};
		int[] B = {1, 2, 3};
		SwapElement se = new SwapElement();
		se.swapElement(A, B);
		for(int val : A) {
			System.out.print(val + " ");
		}
		System.out.println();
		for(int val : B) {
			System.out.print(val + " ");
		}
		
	}
}
