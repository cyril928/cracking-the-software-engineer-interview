
public class GameOfLive {
	public class Cell {
		boolean alive;
		int neighbors;
		public Cell(boolean alive) {
			this.alive = alive;
		}
	}
	
	Cell[][] grid;
	int row, col;
	public GameOfLive(boolean[][] grid) {
		row = grid.length;
		col = grid[0].length;
		this.grid = new Cell[row][col];
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < col; j++) {
				this.grid[i][j] = (grid[i][j]) ? new Cell(true) : new Cell(false);
			}
		}
	}
	
	public void run() {
		calculateNeighbor();
		updateStatus();
		printBoard();
	}
	
	private void calculateNeighbor() {
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < col; j++) {
				int neighbor = 0;
				for(int m = -1; m <= 1; m++) {
					for(int n = -1; n <= 1; n++) {
						if(validIndex(i, j, m, n) && grid[i + m][j + n].alive) {
							neighbor++;
						}
					}
				}
				grid[i][j].neighbors = neighbor;
			}
		}
	}
	
	private boolean validIndex(int i, int j, int m, int n) {
		if((i+m) >= row || (i+m) < 0) {
			return false;
		}
		if((j+n) >= col || (j + n) < 0) {
			return false;
		}
		if(m == 0 && n == 0) {
			return false;
		}
		return true;
	}
	
	private void updateStatus() {
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < col; j++) {
				if(grid[i][j].neighbors < 2 || grid[i][j].neighbors > 3) {
					grid[i][j].alive = false;
				}
				else if(grid[i][j].neighbors == 3) {
					grid[i][j].alive = true;
				}
			}
		}
	}
	
	private void printBoard() {
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < col; j++) {
				System.out.print((grid[i][j].alive ? 1 : 0) + " ");
			}
			System.out.println();
		}
		System.out.println("===============");
	}
	
	public static void main(String[] args) {
		boolean[][] grid = {{false, false, false}, 
							{true, true, true}, 
							{false, false, false}};
		GameOfLive g = new GameOfLive(grid);
		g.run();
		g.run();
		g.run();
	}
	
}
