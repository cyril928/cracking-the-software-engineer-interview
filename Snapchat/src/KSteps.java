
public class KSteps {
	public int kStepsPath(int row, int col, int k) {
		int[][][] steps = new int[row][col][k + 1];
		steps[0][0][0] = 1;
		for(int kk = 1; kk <= k; kk++) {
			for(int i = 0; i < row; i++) {
				for(int j = 0; j < col; j++) {
					int path = 0;
					for(int m = -1; m <= 1; m++) {
						for(int n = -1; n <= 1; n++) {
							if(validIndex(i, j, m, n, row, col)) {
								path += steps[i + m][j + n][kk - 1];
							}
						}
					}
					steps[i][j][kk] = path;
				}
			}
		}
		return steps[row - 1][col - 1][k];
	}
	
	private boolean validIndex(int i, int j, int m, int n, int row, int col) {
		if(m == 0 && n == 0) {
			return false;
		}
		if((i + m) < 0 || (i + m) >= row) {
			return false;
		}
		if((j + n) < 0 || (j + n) >= col) {
			return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		KSteps k = new KSteps();
		System.out.println(k.kStepsPath(3, 3, 2));
		System.out.println(k.kStepsPath(3, 3, 3));
		
	}
	
}
