
public class BigInteger {
	
	public String add(String A, String B) {
		int aCur = A.length() - 1;
		int bCur = B.length() - 1;
		int carry = 0;
		StringBuilder sb = new StringBuilder();
		while(aCur >= 0 && bCur >= 0) {
			int aVal = A.charAt(aCur--) - '0';
			int bVal = B.charAt(bCur--) - '0';
			int sum = aVal + bVal + carry;
			sb.append(sum % 10);
			carry = sum / 10;
		}
		
		if(A.length() != B.length()) {
			int cur = (A.length() > B.length()) ? aCur : bCur;
			String Str = (A.length() > B.length()) ? A : B;
			while(cur >= 0) {
				int val = Str.charAt(cur--) - '0';
				int sum = val + carry;
				sb.append(sum % 10);
				carry = sum / 10;
			}
		}
		
		if(carry > 0) {
			sb.append(1);
		}
		return sb.reverse().toString();
	}
	public static void main(String[] args) {
		BigInteger bi = new BigInteger();
		System.out.println(bi.add("1234", "5678"));
		System.out.println(bi.add("1234", "0"));
		System.out.println(bi.add("9234", "5678"));
	}
}
