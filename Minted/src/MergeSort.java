public class MergeSort {
	public static void main(String[] args) {
		MergeSort ms = new MergeSort();
		int[] num = { 4, 78, 5, 90, 3, 2, 56, 100, 42 };
		ms.mergeSort(num);
		for (int val : num) {
			System.out.print(val + " ");
		}
	}

	private void merge(int[] num, int[] temp, int start, int mid, int end) {
		int cur = start;
		int lp = start, rp = mid + 1;
		while (lp <= mid && rp <= end) {
			if (temp[lp] <= temp[rp]) {
				num[cur++] = temp[lp++];
			} else {
				num[cur++] = temp[rp++];
			}
		}
		if (lp <= mid) {
			System.arraycopy(temp, lp, num, cur, mid - lp + 1);
		}
	}

	public void mergeSort(int[] num) {
		int[] temp = new int[num.length];
		mergeSort(num, temp, 0, num.length - 1);
	}

	private void mergeSort(int[] num, int[] temp, int start, int end) {
		if (start >= end) {
			return;
		}
		int mid = ((end - start) >> 1) + start;
		mergeSort(num, temp, start, mid);
		mergeSort(num, temp, mid + 1, end);
		System.arraycopy(num, start, temp, start, end - start + 1);
		merge(num, temp, start, mid, end);
	}

}
