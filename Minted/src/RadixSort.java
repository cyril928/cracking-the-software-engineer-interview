import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RadixSort {

	public static void main(String[] args) {
		int[] num = { 2394, 67, 34, 941, 841, 1920, 8, 1, 4, 10, 1, 30, 156 };
		RadixSort rs = new RadixSort();
		rs.radixSort(num, 4);
		for (int val : num) {
			System.out.print(val + " ");
		}
	}

	public void radixSort(int[] num, int digit) {
		List<Queue<Integer>> bucket = new ArrayList<Queue<Integer>>();
		for (int i = 0; i < 10; i++) {
			bucket.add(new LinkedList<Integer>());
		}
		int div = 1, mod = 10;
		for (int i = 0; i < digit; i++, div *= 10, mod *= 10) {
			for (int val : num) {
				bucket.get(val % mod / div).add(val);
			}
			int pos = 0;
			for (Queue<Integer> q : bucket) {
				while (!q.isEmpty()) {
					num[pos++] = q.poll();
				}
			}
		}
	}
}
