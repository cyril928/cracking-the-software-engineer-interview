public class BubbleSort {
	public static void main(String[] args) {
		BubbleSort bs = new BubbleSort();
		int[] num = { 4, 78, 5, 90, 3, 2, 56, 100, 42 };
		bs.bubbleSort(num);
		for (int val : num) {
			System.out.print(val + " ");
		}
	}

	public void bubbleSort(int[] num) {

		for (int i = 0; i < num.length; i++) {
			boolean pass = true;
			for (int j = 1; j < num.length - i; j++) {
				if (num[j - 1] > num[j]) {
					int temp = num[j - 1];
					num[j - 1] = num[j];
					num[j] = temp;
					pass = false;
				}
			}
			if (pass) {
				break;
			}
		}
	}
}
