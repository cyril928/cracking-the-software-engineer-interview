import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

// Bounded Blocking Queue with queue and two lock implementation
/*
 * We use two Reentrant Locks to replace the use of synchronized methods. 
 * With separate locks for put and take, a consumer and a producer can access the queue 
 * at the same time (if it is neither empty nor full). 
 * 
 * A reentrant lock provides the same basic behaviors as a Lock does by using synchronized 
 * methods and statements.
 * Beyond that, it is owned by the thread last successfully locking and thus when the same 
 * thread invokes lock() again, it will return immediately without lock it again.
 * 
 * Together with lock, we use Condition to replace the object monitor (wait and notifyAll). 
 * A Condition instance is intrinsically bound to a lock. Thus, we can use it to signal threads 
 * that are waiting for the associated lock. Even better, multiple condition instances can 
 * be associated with one single lock and each instance will have its own wait-thread-set, 
 * which means instead of waking up all threads waiting for a lock, we can wake up a predefined 
 * subset of such threads. Similar to wait(), Condition.await() can atomically release the 
 * associated lock and suspend the current thread.
 * 
 * We use Atomic Integer for the count of elements in the queue to ensure that the count will be updated atomically.
 * 
 * http://n00tc0d3r.blogspot.tw/2013/08/implement-bounded-blocking-queue.html?q=plus+one
 * 
 */
public class BoundedBlockingQueueIII<E> {
	private final Queue<E> queue = new LinkedList<E>();
	private final int capacity;
	private final AtomicInteger count = new AtomicInteger(0);
	private final ReentrantLock putLock = new ReentrantLock();
	private final ReentrantLock takeLock = new ReentrantLock();

	private final Condition notFull = putLock.newCondition();
	private final Condition notEmpty = takeLock.newCondition();

	public BoundedBlockingQueueIII(int capacity) {
		this.capacity = capacity;
	}

	public void add(E e) throws InterruptedException {

		int oldCount = -1;
		putLock.lock();
		try {
			// we use count as a wait condition although count isn't protected
			// by a lock
			// since at this point all other put threads are blocked, count can
			// only
			// decrease (via some take thread).
			while (count.get() == capacity) {
				notFull.await();
			}

			queue.add(e);
			oldCount = count.getAndIncrement();
			if (oldCount + 1 < capacity) {
				notFull.signal(); // notify other producers for count change
			}
		} finally {
			putLock.unlock();
		}

		// notify other waiting consumers
		if (oldCount == 0) {
			takeLock.lock();
			try {
				notEmpty.signal();
			} finally {
				takeLock.unlock();
			}
		}
	}

	/*
	 * Retrieves, but does not remove, the head of this queue, or returns null
	 * if this queue is empty.
	 */
	public E peek() {
		if (count.get() == 0) {
			return null;
		}

		takeLock.lock();
		try {
			return queue.peek();
		} finally {
			takeLock.unlock();
		}
	}

	public E remove() throws InterruptedException {
		E e;

		int oldCount = -1;
		takeLock.lock();
		try {
			while (count.get() == 0) {
				notEmpty.await();
			}

			e = queue.remove();
			oldCount = count.getAndDecrement();
			if (oldCount > 1) {
				notEmpty.signal(); // notify other consumers for count change
			}
		} finally {
			takeLock.unlock();
		}

		// notify other waiting producers
		if (oldCount == capacity) {
			putLock.lock();
			try {
				notFull.signal();
			} finally {
				putLock.unlock();
			}
		}

		return e;
	}

	public int size() {
		return count.get();
	}
}
