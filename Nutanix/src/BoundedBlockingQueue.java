// array implementation
public class BoundedBlockingQueue {
	public static void main(String[] args) throws InterruptedException {
		BoundedBlockingQueue bbq = new BoundedBlockingQueue(10);
		bbq.enqueue(0);
		bbq.dequeue();
	}

	private int size;
	private int[] buffer;
	private int putIn = 0, takeOut = 0;
	private int count = 0;

	public BoundedBlockingQueue(int size) {
		this.size = size;
		buffer = new int[this.size];
	}

	public synchronized int dequeue() throws InterruptedException {
		while (count == 0) {
			wait();
		}
		int e = buffer[takeOut];
		takeOut = (takeOut + 1) % size;
		count--;
		if (count == size - 1) {
			notifyAll();
		}
		return e;
	}

	private synchronized void enqueue(int e) throws InterruptedException {
		while (count == size) {
			wait();
		}
		buffer[putIn] = e;
		putIn = (putIn + 1) % size;
		count++;
		if (count == 1) {
			notifyAll();
		}
	}
}
