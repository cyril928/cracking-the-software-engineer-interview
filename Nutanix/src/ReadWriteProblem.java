public class ReadWriteProblem {
	private int numReaders = 0;
	private int numWriters = 0;
	private int numWaitingReaders = 0;
	private int numWaitingWriters = 0;
	private boolean okToWrite = true;
	private long startWaitingReadersTime = 0;

	private long age() {
		return 1;
	}

	public synchronized void endRead(int i) {
		numReaders--;
		okToWrite = (numReaders == 0);
		if (okToWrite) {
			notifyAll();
		}
	}

	public synchronized void endWrite(int i) {
		numWriters--;
		okToWrite = (numWaitingReaders == 0);
		startWaitingReadersTime = age();
		notifyAll();
	}

	public synchronized void startRead(int i) throws InterruptedException {
		long readerArrivalTime = 0;
		if (numWaitingWriters > 0 || numWriters > 0) {
			numWaitingReaders++;
			readerArrivalTime = age();
			while (readerArrivalTime >= startWaitingReadersTime) {
				wait();
			}
			numWaitingReaders--;
		}
		numReaders++;

	}

	public synchronized void startWrite(int i) throws InterruptedException {
		if (numReaders > 0 || numWriters > 0) {
			numWaitingWriters++;
			okToWrite = false;
			while (!okToWrite) {
				wait();
			}
			numWaitingWriters--;
		}
		okToWrite = false;
		numWriters++;
	}
}
