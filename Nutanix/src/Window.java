import java.util.AbstractMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Set;

public class Window {
	class Message {
		long key;
		long value;

		public Message(long k, long v) {
			key = k;
			value = v;
		}
	}

	// just simply return something for this function, cause this question
	// assume we have already have this function, just for passing compiler
	static long get_time() {
		return 1;
	}

	long duration;
	Hashtable<Long, AbstractMap.SimpleEntry<Long, Long>> window;

	// Window duration is in milliseconds.
	public Window(long window_duration) {
		this.duration = window_duration;
		this.window = new Hashtable<Long, AbstractMap.SimpleEntry<Long, Long>>();
	}

	// Adds the given message within the Window class.
	void AddMessage(Message message) {
		long cur_time = get_time();
		AbstractMap.SimpleEntry<Long, Long> timeValEntry = new AbstractMap.SimpleEntry<Long, Long>(
				cur_time, message.value);
		window.put(message.key, timeValEntry);
		this.purgeMessage();
	}

	// Returns the average of the value of all the Messages within the Window.
	double GetAverage() {
		long res = 0;
		int count = 0;
		long cur_time = get_time();
		Set<Long> keys = window.keySet();
		for (long key : keys) {
			AbstractMap.SimpleEntry<Long, Long> timeValEntry = window.get(key);
			if (timeValEntry.getKey() + this.duration > cur_time) {
				res += timeValEntry.getValue();
				count++;
			}
		}
		if (count == 0) {
			return 0;
		}
		return res / count;
	}

	// Returns either the Message if the key exists in the Window or NULL
	// otherwise. Note that if a Message with the given key was already added
	// into Window but the time has since elapsed, NULL must be returned as the
	// Message is no longer valid.
	Message GetMessage(long key) {
		if (!window.containsKey(key)) {
			return null;
		} else {
			AbstractMap.SimpleEntry<Long, Long> timeValEntry = window.get(key);
			if (timeValEntry.getKey() + this.duration < get_time()) {
				window.remove(key);
				return null;
			}
			return new Message(key, timeValEntry.getValue());
		}
	}

	void purgeMessage() {
		long cur_time = get_time();
		Set<Long> keys = window.keySet();
		LinkedList<Long> removeKeys = new LinkedList<Long>();
		for (long key : keys) {
			AbstractMap.SimpleEntry<Long, Long> timeValEntry = window.get(key);
			if (timeValEntry.getKey() + this.duration < cur_time) {
				removeKeys.add(key);
			}
		}
		for (long key : removeKeys) {
			window.remove(key);
		}
	}
}
