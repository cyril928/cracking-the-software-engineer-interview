import java.io.IOException;
import java.util.*;

public class PossibleFactor {
	public static void possibleFactor(int n) {
		if(n == 0) return;
		ArrayList<Integer> res = new ArrayList<Integer>();
		genMulStr(res, n);
	}
	
	public static void genMulStr(ArrayList<Integer> res, int n) {
		if(n == 1) {
			if(res.size() == 1) {
				System.out.println(res.get(0)+"*1");
			}
			else {
				for(int i = 0; i < res.size() - 1; i++) {
					System.out.print(res.get(i) + "*");
				}
				System.out.println(res.get(res.size() - 1));
			}
		}
		for(int i = 2; i <= n; i++) {
			if(n % i == 0) {
				if(res.size() > 0 && i < res.get(res.size()-1)) continue;
				ArrayList<Integer> nres = new ArrayList<Integer>(res);
				nres.add(i);
				genMulStr(nres, n/i);
			}
		}
	}
	public static void main(String[] args) throws IOException {
		possibleFactor(40);
	}
}
