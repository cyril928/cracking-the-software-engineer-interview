import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

// Bounded Blocking Queue with queue implementation
/* 
 * Write a multithreaded bounded Blocking Queue where the capacity of the queue is limited. 
 * Implement size, add, remove, and peek methods.
 * 
 * http://n00tc0d3r.blogspot.tw/2013/08/implement-bounded-blocking-queue.html?q=plus+one
 * 
 */
public class BoundedBlockingQueueII<E> {
	private final Queue<E> queue = new LinkedList<E>();
	private final int capacity;
	private final AtomicInteger count = new AtomicInteger(0);

	public BoundedBlockingQueueII(int capacity) {
		this.capacity = capacity;
	}

	public synchronized void add(E e) throws RuntimeException,
			InterruptedException {
		if (e == null) {
			throw new NullPointerException("Null element is not allowed.");
		}

		while (count.get() == capacity) {
			wait();
		}

		queue.add(e);
		int oldCount = count.getAndIncrement();
		if (oldCount == 0) {
			notifyAll(); // notify other waiting threads (could be producers or
							// consumers)
		}
	}

	/*
	 * Retrieves, but does not remove, the head of this queue, or returns null
	 * if this queue is empty.
	 */
	public E peek() {
		if (count.get() == 0) {
			return null;
		}
		synchronized (this) {
			return queue.peek();
		}
	}

	public synchronized E remove() throws InterruptedException {

		while (count.get() == 0) {
			wait();
		}

		E e = queue.remove();
		int oldCount = count.getAndDecrement();
		if (oldCount == this.capacity) {
			notifyAll(); // notify other waiting threads (could be producers or
							// consumers)
		}
		return e;
	}

	public int size() {
		return count.get();
	}
}
