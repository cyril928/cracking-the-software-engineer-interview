package edu.ini.hacks;
/**
 * 
 * @author CyrilLee
 *
 */
public class Minion {
	private int eyes;
	private String color;
	private boolean hasHair;
	
	/**
	 * 
	 * @param eyes
	 * @param color
	 * @param hasHair
	 */
	public Minion(int eyes, String color, boolean hasHair) {
		super();
		this.eyes = eyes;
		this.color = color;
		this.hasHair = hasHair;
	}
	
}
