package edu.ini.hacks;

public class Sample {
	private double grades;
	private String name;
	private int number;

	public double getGrades() {
		return grades;
	}

	public String getName() {
		return name;
	}

	public int getNumber() {
		return number;
	}

	public void setGrades(double grades) {
		this.grades = grades;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumber(int number) {
		this.number = number;
	}
}
