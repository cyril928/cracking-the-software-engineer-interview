class Queue {

   LinkedListNode first, last;
   public Object dequeue() {
       if(first == null) 
           return null;
       else {
           Object item = first.data;
           first = first.next; 
           return item;
       }
   }
   
   public void enqueue(Object item) {
       if(first == null) {
           first = new LinkedListNode(item);
           last = first;
       }
       else {
           last.next = new LinkedListNode(item);
           last = last.next;
       }
   }
}
