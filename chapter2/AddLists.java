public class AddLists {
    public LinkedListNode addLists(LinkedListNode l1, LinkedListNode l2, int carry) {
        if(l1 == null && l2 == null) return null;
        int value = carry;
        
        if(l1 != null)
            value += l1.data;
        if(l2 != null)
            value += l2.data;
        
              
        LinkedListNode n = new LinkedListNode(value % 10);
        LinkedListNode more = addLists(l1 == null ? null : l1.next, 
                                       l2 == null ? null : l2.next,
                                       value / 10);
        n.setNext(more);
        return n;    
    }
}
