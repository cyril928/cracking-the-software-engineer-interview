public class LinkedListNode<T> {
    T data;
    LinkedListNode next = null;
    public LinkedListNode(T d) {
        this.data = d;
    }

    void setNext(LinkedListNode next) {
        this.next = next;
    }
}
