class IsPalindrom {
    
    public boolean isPalindrom(LinkedListNode head) {
        LinkedListNode fast;
        LinkedListNode slow;
        
        Stack<Integer> stack = new Stack<Integer>();
        
        while(fast != null && fast.next != null) {
            stack.push(slow.data);
            slow = slow.next;
            fast = fast.next.next;
        }
        
        if(fast != null)
            slow = slow.next;
        
        while(slow != null) {
            int top = stack.pop().intValue();
            if(slow.data != top)
                return false;
            slow = slow.next;
        }
        
        return true;
    }     

}
