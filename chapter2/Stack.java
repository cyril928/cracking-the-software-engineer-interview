class Stack<T> {
   
    LinkedListNode top; 
    
    public void push(T item) {
        LinkedListNode node = new LinkedListNode(item);
        node.next = top;
        top = node;      
    }
    
    public Object pop() {
        if(top != null) {
            Object item = top.data;
            top = top.next;
            return item;
        }
        return null;
    }
    
    public Object peek() {
        return top.data;
    }    

}
