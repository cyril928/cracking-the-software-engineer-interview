import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/*
 * We annotate the top of our JavaScript files with directives like "require('otherfile.js'); 
 * require('otherfile2.js');" to indicate dependencies between JS files. 
 * Write code that print out script file name in the order that if A require on B,
 * print A before B.
 * Please focus on finding the proper order, not on parsing the require statements or 
 * loading files. Assume you are given input: dependencies: a map from file name to an 
 * array of the names of files  by that file
 * 
 *  A
 * / \
 * B <- C
 * Map : A -> B,C
 *           C->B
 * You should print in the order A, C, B
 */
public class JSDependencyManager {
	public static void main(String[] args) {

		JSDependencyManager jdm = new JSDependencyManager();
		Map<String, List<String>> map = new HashMap<String, List<String>>();
		map.put("A", Arrays.asList("C", "B"));
		map.put("C", Arrays.asList("B"));
		jdm.findDependencyOrder(map);

		map.clear();

		map.put("B", Arrays.asList("A"));
		map.put("C", Arrays.asList("A"));
		map.put("D", Arrays.asList("B", "E", "A"));
		map.put("E", Arrays.asList("C"));
		map.put("F", Arrays.asList("C"));
		jdm.findDependencyOrder(map);
	}

	public void findDependencyOrder(Map<String, List<String>> map) {
		// use set to keep track which files have been visited
		Set<String> visited = new HashSet<String>();
		List<String> res = new ArrayList<String>();
		for (String file : map.keySet()) {
			if (!visited.contains(file)) {
				helper(file, res, visited, map);
			}
		}
		StringBuilder sb = new StringBuilder();
		for (int i = res.size() - 1; i >= 0; i--) {
			if (sb.length() != 0) {
				sb.append(", ");
			}
			sb.append(res.get(i));
		}
		System.out.println(sb.toString());
	}

	// use dfs to build topological order
	private void helper(String file, List<String> res, Set<String> visited,
			Map<String, List<String>> map) {
		if (visited.contains(file)) {
			return;
		}
		visited.add(file);
		if (map.containsKey(file)) {
			for (String dfile : map.get(file)) {
				helper(dfile, res, visited, map);
			}
		}
		res.add(file);
	}
}
