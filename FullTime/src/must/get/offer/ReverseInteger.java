package must.get.offer;
public class ReverseInteger {

	public static void main(String args[]) {
		System.out.println(reverseIterative(123456700));
		System.out.println(reverse(456789000));
	}

	private static int reverse(int num) {
		return reverseRecursive(num, 0);
	}

	private static int reverseIterative(int num) {
		int q;
		int res = 0;
		do {
			q = num / 10;
			res = res * 10 + num % 10;
			num = q;
		} while (q != 0);
		return res;
	}

	private static int reverseRecursive(int num, int res) {
		if (num == 0) {
			return res;
		}
		return reverseRecursive(num / 10, res * 10 + num % 10);
	}
}
