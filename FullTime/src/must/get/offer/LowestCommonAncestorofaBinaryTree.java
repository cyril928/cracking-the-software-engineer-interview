package must.get.offer;

/*
 * Given a binary tree, find the lowest common ancestor of two given nodes in the tree.
 * Cracking the coding interview 4.7
 */
public class LowestCommonAncestorofaBinaryTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		TreeNode(int val) {
			this.val = val;
		}
	}

	public TreeNode commonAncestor(TreeNode root, TreeNode p, TreeNode q) {
		if (!coverNode(root, p) || !coverNode(root, q) || p == null
				|| q == null) {
			return null;
		}
		return commonAncestorHelper(root, p, q);
	}

	private TreeNode commonAncestorHelper(TreeNode root, TreeNode p, TreeNode q) {
		if (root == null) {
			return null;
		}
		if (root == p || root == q) {
			return root;
		}
		boolean is_p_in_left = coverNode(root.left, p);
		boolean is_q_in_left = coverNode(root.left, q);
		if (is_p_in_left != is_q_in_left) {
			return root;
		}

		return is_p_in_left ? commonAncestorHelper(root.left, p, q)
				: commonAncestorHelper(root.right, p, q);
	}

	private boolean coverNode(TreeNode root, TreeNode p) {
		if (root == null) {
			return false;
		}
		if (root == p) {
			return true;
		}
		return coverNode(root.left, p) || coverNode(root.right, p);
	}
}
