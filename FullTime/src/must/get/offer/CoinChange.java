package must.get.offer;

// Coin Change
/*
 * http://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/
 * recursive, DP
 * 
 * 
 */
public class CoinChange {

	// DP, O(N) space, for O(NM) space, go to GeeksforGeeks and refer it
	public static int coinChange(int[] coin, int index, int val) {
		int[] res = new int[val + 1];
		res[0] = 1;

		for (int i = 0; i < index; i++) {
			for (int j = coin[i]; j < val + 1; j++) {
				res[j] = res[j] + res[i - coin[j]];
			}
		}
		return res[val];
	}

	// recursive
	public static int coinChange1(int[] coin, int index, int val) {

		// If n is 0 then there is 1 solution (do not include any coin)
		if (val == 0) {
			return 1;
		}
		// If n is less than 0 then no solution exists
		// If there are no coins and n is greater than 0, then no solution exist
		if (val < 0 || index < 0) {
			return 0;
		}

		return coinChange1(coin, index - 1, val)
				+ coinChange1(coin, index, val - coin[index]);

	}

	public static void main(String[] args) {
		int[] coin = new int[] { 2, 1, 3 };
		int value = 5;
		int num = coinChange1(coin, coin.length - 1, value);
		System.out.println(num);
		num = coinChange1(coin, coin.length - 1, value);
		System.out.println(num);
	}
}
