package must.get.offer;
public class IsPalindrome {
	public static boolean isPalindrome(String s) {
		int len = s.length();
		int start = 0, end = len - 1;
		while (start < end) {
			if (s.charAt(start++) != s.charAt(end--)) {
				return false;
			}
		}
		return true;
	}

	public static boolean isPalindromeRecursive(String s, int start, int end) {
		if (start >= end) {
			return true;
		}
		return s.charAt(start) == s.charAt(end)
				&& isPalindromeRecursive(s, start + 1, end - 1);
	}

	public static void main(String[] args) {
		System.out.println(isPalindrome("abcddcba"));
		System.out.println(isPalindrome("abcdecba"));
		System.out.println(isPalindrome("cdgdc"));
		System.out.println(isPalindrome("cdgec"));
		System.out.println(isPalindromeRecursive("abcddcba", 0,
				"abcddcba".length() - 1));
		System.out.println(isPalindromeRecursive("abcdecba", 0,
				"abcdecba".length() - 1));
		System.out.println(isPalindromeRecursive("cdgdc", 0,
				"cdgdc".length() - 1));
		System.out.println(isPalindromeRecursive("cdgec", 0,
				"cdgec".length() - 1));
	}
}
