package must.get.offer;

import java.util.ArrayList;
import java.util.List;

public class LowestCommonAncestorWithParentPointer {
	class TreeNode {
		TreeNode parent;
		char val;
		List<TreeNode> childNodes;

		public TreeNode(char val) {
			this.val = val;
			this.childNodes = new ArrayList<TreeNode>();
		}
	}

	public static void main(String[] arg) {
	}

}
