package must.get.offer;
public class ReverseString {
	public static void main(String args[]) {
		System.out.println(reverseIterative("cyril lee"));
		System.out.println(reverse("cyril lee"));
	}

	private static String reverse(String s) {
		if (s == null) {
			return null;
		}
		return reverseRecursive(s, 0, s.length() - 1);
	}

	private static String reverseIterative(String s) {
		if (s == null) {
			return null;
		}
		char[] charStr = s.toCharArray();
		int start = 0;
		int end = s.length() - 1;
		while (start < end) {
			char c = charStr[start];
			charStr[start++] = charStr[end];
			charStr[end--] = c;
		}

		return new String(charStr);
	}

	private static String reverseRecursive(String s, int start, int end) {
		if (start >= end) {
			return s;
		}
		char[] charStr = s.toCharArray();
		char c = charStr[start];
		charStr[start] = charStr[end];
		charStr[end] = c;
		return reverseRecursive(new String(charStr), start + 1, end - 1);
	}

}
