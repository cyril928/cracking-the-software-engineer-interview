package must.get.offer;

public class Queue<E> {
	class Node {
		E val;
		Node next;

		public Node(E val) {
			this.val = val;
		}
	}

	Node first, last;

	E dequeue() {
		if (first != null) {
			E val = first.val;
			first = first.next;
			if (first == null) {
				last = null;
			}
			return val;
		}
		return null;
	}

	void enqueue(E e) {
		Node newNode = new Node(e);
		if (last != null) {
			last.next = newNode;
			last = newNode;
		} else {
			first = last = newNode;
		}
	}
}
