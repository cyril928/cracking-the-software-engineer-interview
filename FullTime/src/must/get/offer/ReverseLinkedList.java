package must.get.offer;

class ListNode {
	int val;
	ListNode next;

	public ListNode(int val) {
		this.val = val;
		next = null;
	}

	@Override
	public String toString() {
		ListNode node = this;
		StringBuilder sb = new StringBuilder("");
		while (node != null) {
			sb.append(node.val + "->");
			node = node.next;
		}
		sb.append("TAIL");
		return sb.toString();
	}
}

public class ReverseLinkedList {
	public static void main(String[] args) {
		ListNode test = new ListNode(1);
		test.next = new ListNode(2);
		test.next.next = new ListNode(3);
		test.next.next.next = new ListNode(4);
		System.out.println(test.toString());
		// System.out.println(reverse(test).toString());
		System.out.println(reverseRecursive(test).toString());
	}

	public static ListNode reverse(ListNode head) {

		ListNode prev = null;
		ListNode cur = head;
		ListNode next = null;
		while (cur != null) {
			next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}
		return prev;
	}

	public static ListNode reverseRecursive(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode remainHead = reverseRecursive(head.next);
		// ListNode cur = remainHead;
		// while(cur.next != null) {
		// cur = cur.next;
		// }
		head.next.next = head;
		head.next = null;
		return remainHead;
	}

}