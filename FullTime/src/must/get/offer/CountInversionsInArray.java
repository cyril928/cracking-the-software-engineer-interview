package must.get.offer;

// 
/*
 * http://www.geeksforgeeks.org/counting-inversions/
 * O(N2) naive way
 * O(nlogn) merge sort
 */

public class CountInversionsInArray {

	public static void main(String[] args) {
		int[] num = new int[] { 1, 20, 6, 4, 5 };
		int res = numInversion(num);
		System.out.println(res);

		res = numInversion1(num);
		for (int i = 0; i < num.length; i++) {
			System.out.print(num[i] + " ");
		}
		System.out.println();
		System.out.println(res);
	}

	// O(nlogn) merge sort helper function
	private static int merge(int[] num, int[] helper, int start, int mid,
			int end) {
		int res = 0;
		for (int i = start; i <= end; i++) {
			helper[i] = num[i];
		}

		int helperLeft = start;
		int helperRight = mid + 1;
		int current = start;

		while (helperLeft <= mid && helperRight <= end) {
			if (helper[helperLeft] <= helper[helperRight]) {
				num[current++] = helper[helperLeft++];
			} else if (helper[helperLeft] > helper[helperRight]) {
				// key algorithm
				res += (mid - helperLeft + 1);
				num[current++] = helper[helperRight++];
			}
		}
		int remaining = mid - helperLeft;
		{
			for (int i = 0; i <= remaining; i++) {
				num[current++] = helper[helperLeft++];
			}
		}
		return res;
	}

	// O(nlogn) merge sort helper function
	private static int mergeSort(int[] num, int[] helper, int start, int end) {
		if (start < end) {
			int res = 0;
			int mid = (end - start) / 2 + start;
			res = mergeSort(num, helper, start, mid)
					+ mergeSort(num, helper, mid + 1, end)
					+ merge(num, helper, start, mid, end);
			return res;
		} else {
			return 0;
		}
	}

	// O(N2) naive way
	private static int numInversion(int[] num) {
		int len = num.length;
		int res = 0;
		for (int i = 0; i < len - 1; i++) {
			for (int j = i + 1; j < len; j++) {
				res += (num[i] > num[j]) ? 1 : 0;
			}
		}
		return res;
	}

	// O(nlogn) merge sort
	private static int numInversion1(int[] num) {
		int[] helper = new int[num.length];
		return mergeSort(num, helper, 0, num.length - 1);
	}

}
