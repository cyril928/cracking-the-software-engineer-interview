package must.get.offer;

public class ConvertIntToString {
	public static String convertIntToString(int n) {
		if (n == 0) {
			return "0";
		}

		StringBuilder sb = new StringBuilder();
		long num = n;
		num = Math.abs(num);

		while (num > 0) {
			int digit = (int) (num % 10);
			// char c = (char) (digit + '0');
			sb.append(digit);
			num /= 10;
		}

		if (n < 0) {
			sb.append('-');
		}
		return sb.reverse().toString();
	}

	public static void main(String[] args) {
		System.out.println(convertIntToString(Integer.MAX_VALUE));
		System.out.println(convertIntToString(Integer.MIN_VALUE));
		System.out.println(convertIntToString(-376));
		System.out.println(convertIntToString(3459));
	}
}
