package must.get.offer;

public class CompressStringUsingLetterCount {
	public static String compressStringUsingLetterCount(String s) {
		if (s == null || s.length() < 3) {
			return s;
		}

		StringBuilder res = new StringBuilder();

		s = s + " ";
		int len = s.length();
		int count = 1;
		int i = 0, j = 1;
		while (j < len) {
			if (s.charAt(i) == s.charAt(j)) {
				count++;
			} else {
				if (count > 2) {
					res.append(count);
				}
				res.append(s.charAt(i));
				if (count == 2) {
					res.append(s.charAt(i));
				}
				i = j;
				count = 1;
			}
			j++;
		}

		return res.toString();
	}

	public static void main(String[] args) {
		System.out.println(compressStringUsingLetterCount("abcdddde"));
		System.out.println(compressStringUsingLetterCount("adbddeef"));
		System.out.println(compressStringUsingLetterCount("abbbcccd"));
	}
}
