package must.get.offer;

public class Stack<E> {
	class Node {
		Node next;
		E val;

		public Node(E val) {
			this.val = val;
		}
	}

	Node top = null;

	E peek() {
		return top.val;
	}

	E pop() {
		if (top != null) {
			E res = top.val;
			top = top.next;
			return res;
		}
		return null;
	}

	void push(E val) {
		Node newNode = new Node(val);
		newNode.next = top;
		top = newNode;
	}

}
