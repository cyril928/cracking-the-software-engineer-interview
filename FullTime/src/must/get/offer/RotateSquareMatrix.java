package must.get.offer;
/*
 * http://n00tc0d3r.blogspot.tw/search?q=Spiral+Matrix
 * all kinds of matrix rotation
 */
public class RotateSquareMatrix {

	public static void main(String[] args) {
		int[][] a = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 },
				{ 13, 14, 15, 16 } };
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}

		rotate(a);
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void rotate(int[][] matrix) {
		for (int level = 0, len = matrix.length; level < len; level++, len--) {
			int end = len - 1;
			for (int pos = level; pos < end; pos++) {
				int tail = matrix.length - pos - 1;
				int temp = matrix[level][pos];
				matrix[level][pos] = matrix[tail][level];
				matrix[tail][level] = matrix[end][tail];
				matrix[end][tail] = matrix[pos][end];
				matrix[pos][end] = temp;
			}
		}
	}
}
