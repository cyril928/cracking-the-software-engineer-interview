import java.util.Collections;
import java.util.TreeSet;

public class Elevator {

	private enum Status {
		UP, DOWN, STOP
	}

	// elevator status
	private Status status;

	// non-duplicated queue for serving going up in this round.
	private TreeSet<Integer> servingUpFloorsSet = new TreeSet<Integer>();

	// TreeSet can maintain increasing or decreasing order for integer element

	// non-duplicated queue for serving going up in next round (after going
	// down)
	private TreeSet<Integer> unservedUpFloorsSet = new TreeSet<Integer>();

	// non-duplicated queue for serving going down in this round.
	private TreeSet<Integer> servingDownFloorsSet = new TreeSet<Integer>(
			Collections.reverseOrder());

	// non-duplicated queue for serving going down in next round (after going
	// up)
	private TreeSet<Integer> unservedDownFloorsSet = new TreeSet<Integer>(
			Collections.reverseOrder());

	// elevator current floor
	private int cur_floor;

	// initialize elevator state
	public Elevator() {
		this.status = Status.STOP;
	}

	private void arrivalMsg(int floor) {
		this.cur_floor = floor;
		System.out.println("elevator arrives floor " + floor);
	}

	public void dispatch() {
		if (status == Status.STOP) {
			if (!servingUpFloorsSet.isEmpty()) { // going to serve up floors
													// request.
				status = Status.UP;
				dispatch(); // start serving.
			} else if (!servingDownFloorsSet.isEmpty()) { // going to serve down
															// floors request.
				status = Status.DOWN;
				dispatch(); // start serving.
			} else {
				System.out.println("no more job can do!!!");
			}

		} else if (status == Status.UP) {
			int next_floor = servingUpFloorsSet.first();
			arrivalMsg(next_floor);
			servingUpFloorsSet.remove(next_floor);
			if (servingUpFloorsSet.isEmpty()) {
				servingUpFloorsSet.addAll(unservedUpFloorsSet);
				unservedUpFloorsSet.clear();
				if (!servingDownFloorsSet.isEmpty()) { // change to serve for
														// going down
					status = Status.DOWN;
				} else if (servingUpFloorsSet.isEmpty()) { // no more request,
															// stop elevator
					status = Status.STOP;
				}
				// else keep serve for going up.
			}
		} else if (status == Status.DOWN) {
			int next_floor = servingDownFloorsSet.first();
			arrivalMsg(next_floor);
			servingDownFloorsSet.remove(next_floor);
			if (servingDownFloorsSet.isEmpty()) {
				servingDownFloorsSet.addAll(unservedDownFloorsSet);
				unservedDownFloorsSet.clear();
				if (!servingUpFloorsSet.isEmpty()) { // change to serve for
														// going up
					status = Status.UP;
				} else if (servingDownFloorsSet.isEmpty()) { // no more request,
																// stop elevator
					status = Status.STOP;
				}
				// else keep serve for going down.
			}
		}

	}

	public void summon(int from_floor, int to_floor) {
		if (from_floor < to_floor) { // user want to up
			if (status == Status.UP) { // elevator is going up or stop
				if (from_floor >= cur_floor) { // user request haven't pass
												// current floor
					servingUpFloorsSet.add(from_floor);
					servingUpFloorsSet.add(to_floor);
				} else { // user request pass current floor, reserve for next
							// round (after going down)
					unservedUpFloorsSet.add(from_floor);
					unservedUpFloorsSet.add(to_floor);
				}
			} else { // elevator is going down, but user request to go up, add
						// to queue for later processing.
				servingUpFloorsSet.add(from_floor);
				servingUpFloorsSet.add(to_floor);
			}
		} else { // user want to down
			if (status == Status.DOWN) { // elevator is going down or stop
				if (from_floor <= cur_floor) { // user request haven't pass
												// current floor
					servingDownFloorsSet.add(from_floor);
					servingDownFloorsSet.add(to_floor);
				} else { // user request pass current floor, reserve for next
							// round (after going up)
					unservedDownFloorsSet.add(from_floor);
					unservedDownFloorsSet.add(to_floor);
				}
			} else { // elevator is going up, but user request to go down, add
						// to queue for later processing.
				servingDownFloorsSet.add(from_floor);
				servingDownFloorsSet.add(to_floor);
			}
		}
	}

}
