import java.util.Arrays;

public class Solution {
	public static void main(String[] args) {
		int[] A = { -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		Solution solu = new Solution();
		System.out.println(solu.getTreeHeight(A));
	}

	public int findHeight(int[] A, int[] height, int index) {
		// the height has been calculated, return it immediately
		if (height[index] != -1) {
			return height[index];
		}
		// else, calculate the height and then return it
		if (A[index] == -1) {
			height[index] = 0;
		} else {
			height[index] = findHeight(A, height, A[index]) + 1;
		}
		return height[index];
	}

	/**
	 * The basic idea is to shift the array right by n
	 */
	public int getTreeHeight(int[] A) {
		int[] height = new int[A.length];
		int maxHeight = -1;

		Arrays.fill(height, -1);

		for (int i = 0; i < A.length; i++) {
			maxHeight = Math.max(maxHeight, findHeight(A, height, i));
		}

		return maxHeight;
	}
}
