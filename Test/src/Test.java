import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Stack;
import java.util.TreeSet;

public class Test {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	private static void absTest() {
		System.out.println(Math.abs(0));
		System.out.println(Math.abs(-3));
		System.out.println(Math.abs(Integer.MAX_VALUE));
		System.out.println(Math.abs(Integer.MIN_VALUE));
	}

	private static void charArrayInitTest() {
		// char array initialize test
		char[] chararray = new char[] { 'a', 'b', 'c' };
		for (char chr : chararray) {
			System.out.println(chr);
		}
		char[] chararray1 = { 'd', 'e', 'f' };
		for (char chr : chararray1) {
			System.out.println(chr);
		}
		Character[] chararray2 = new Character[] { 'g', 'h', 'i' };
		for (char chr : chararray2) {
			System.out.println(chr);
		}
		Character[] chararray3 = { 'j', 'k', 'l' };
		for (char chr : chararray3) {
			System.out.println(chr);
		}
		// char array initialize test (in map)
		Map<Integer, char[]> map = new HashMap<Integer, char[]>();
		map.put(1, new char[] { 'a', 'b', 'c' });
		map.put(2, new char[] { 'a', 'b', 'c' });
	}

	private static void charArrayInitTest1() {
		char[] chars = new char[10];
		for (char c : chars) {
			System.out.print(c);
		}
		System.out.println();
	}

	private static void charPlusIntTest() {
		int i = 0;
		System.out.println(i + 'a');
	}

	// char array initialize test
	public static void main(String[] args) {
		List<HashSet<String>> res = new LinkedList<HashSet<String>>();
		Test test = new Test();
		test.buildPath(res);
		for (HashSet<String> set : res) {
			for (String s : set) {
				System.out.println(s);
			}
		}

		// System.out.println(Integer.parseInt("000"));
		String s = "a";
		// System.out.println(s.substring(1));
		double a = 0.0;
		double b = -0.0;
		HashSet<Double> set = new HashSet<Double>();
		set.add(a);
		if (a == b) {
			System.out.println("We are equal");
		}
		if (!set.contains(b)) {
			System.out.println("We are different");
		}
		System.out.println(a / -1);
		System.out.println(b / -1);
		System.out.println(b / 1);
		int c = 0;
		int d = -0;
		System.out.println(c / -1);
		System.out.println(d / -1);
		System.out.println(d / 1);

		s = "/home/cyril///foo/job";
		String[] split = s.split("/+");
		for (String str : split) {
			System.out.println(str);
		}
		System.out.println("first str is " + split[0]);

		try {
			int aa = 10 / 1;
			System.out.println(aa);
		} catch (Exception e) {
			System.out.println("catch statement");
		} finally {
			System.out.println("finally statement");
		}

		// test division
		System.out.println(-7 / -3);
		System.out.println(-7 % -3);
		System.out.println(7 / -3);
		System.out.println(7 % -3);
		System.out.println(-7 / 3);
		System.out.println(-7 % 3);

		// test substring method
		System.out.println("JIE".substring(2, 2));

		// test java return statement scope
		testJavaReturnScope();

		// test shift, AND, + , operator priority
		int size = 2;
		size = (size & 1) + (size >> 1);
		System.out.println(size);

		System.out.println("".trim());

		int ii = 0;
		for (; ii < 3; ii++) {
			;
		}
		System.out.println("ii = " + ii);

		System.out.println(Math.pow(0, 0));
		System.out.println(-369 % 10);

		long num = Integer.MAX_VALUE + 1000;
		System.out.println(Integer.MAX_VALUE);
		System.out.println(num);
		num = (long) Integer.MAX_VALUE + (long) 1000;
		System.out.println(num);
		System.out.println((int) num);

		TreeSet<Integer> treeSet = new TreeSet<Integer>(
				Collections.reverseOrder());
		treeSet.add(5);
		treeSet.add(1);
		treeSet.add(8);
		treeSet.add(7);
		treeSet.add(4);
		treeSet.add(6);
		treeSet.add(6);
		System.out.println(treeSet.first());
		System.out.println(treeSet.last());
		for (Integer i : treeSet) {
			System.out.print(i + " ");
		}
		System.out.println();

		String str = "qwetyrete";
		int[] charNum = new int[26];
		for (int i = 0; i < str.length(); i++) {
			int index = str.charAt(i) - 'a';
			charNum[index]++;
			charNum[str.charAt(i) - 'a']++;
		}

		splitTest();

		String ss = "sdf";
		int inti = 39;
		System.out.println(ss + inti + 'a');

		int test_val = 3;
		while (test_val-- > 0) {
			;
		}
		System.out.println("first while loop approach val is: " + test_val);
		test_val = 3;
		while (test_val > 0) {
			test_val--;
		}
		System.out.println("second while loop approach val is: " + test_val);

		ArrayList<Integer> alist = new ArrayList<Integer>();
		alist.add(1);
		alist.add(2);
		alist.add(3);
		alist.add(4);

		ArrayList<Integer> blist = new ArrayList<Integer>(alist);
		blist.add(0, 9);
		System.out.println("alist includes : ");
		for (int val : alist) {
			System.out.println(val);
		}
		System.out.println("blist includes : ");
		for (int val : blist) {
			System.out.println(val);
		}

		int[] testarray = new int[3];

		System.out.println("testarray length is " + testarray.length);

		System.out.println(444);

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(null);
		TreeNode node = stack.pop();
		if (node == null) {
			System.out.println("pass stack null test");
		}

		int n = Integer.parseInt("-2147483648");
		System.out.println(n);

		int n1 = Integer.parseInt("0999");
		System.out.println(n1);

		// test whether can assign int to long
		int intval = 3;
		long longval = intval;
		System.out.println(longval);

		// random % test
		Random rr = new Random();
		int val = rr.nextInt();
		int modval = val % 1;
		System.out.println("random val : " + val);
		System.out.println("random mod : " + modval);

		System.out.println(Integer.MAX_VALUE - Integer.MIN_VALUE);
		System.out.println(Integer.MAX_VALUE + Integer.MIN_VALUE);

		// char array initialize test
		charArrayInitTest();

		// abs test
		absTest();

		// shift test
		shiftTest();

		// char plus int test
		charPlusIntTest();

		// try finally test
		tryFinallyTest();

		// test int converted to long
		testInttoLong(-1);
		testInttoLong(-3);
		testInttoLong(-5);

		int allOnes = ~0;
		int i = (1 << 31) - 1;
		System.out.println(allOnes << 31);
		System.out.println(allOnes << 32);
		System.out.println(i);

		// char array initialization test
		charArrayInitTest1();
	}

	private static void shiftTest() {
		System.out.println(-3 >> 1 >> 1 >> 1);
		System.out.println(-1 >> 1);
		System.out.println(1 >> 1);
	}

	private static void splitTest() {
		String[] strArray = "   this is a    test  ".split(" +");

		int iii = 0;
		for (String s1 : strArray) {
			System.out.println(iii + " " + s1);
			iii++;
		}

		String[] strArray1 = " this is a  test  ".split(" ");
		iii = 0;
		for (String s1 : strArray1) {
			System.out.println(iii + " " + s1);
			iii++;
		}

		String[] strArray2 = " test".split(" ");
		iii = 0;
		for (String s1 : strArray2) {
			System.out.println(iii + " " + s1);
			iii++;
		}

		String[] strArray3 = "".split(" +");
		iii = 0;
		for (String s1 : strArray3) {
			System.out.println(iii + " " + s1);
			iii++;
		}
	}

	private static void testInttoLong(int n) {
		System.out.println((long) n);
	}

	private static int testJavaReturnScope() {
		int a = 7;
		int b = 2;
		if (b > 0) {
			return a;
		} else {
			return a + 1;
		}

	}

	private static int tryFinallyTest() {
		try {
			return 3;
		} finally {
			System.out.println("pass try finally scope test");
		}
	}

	private void buildPath(List<HashSet<String>> res) {
		HashSet<String> set = new HashSet<String>();
		set.add("sdfff");
		res.add(set);
	}
}
