import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ChemFormula {
	public static void main(String[] args) {
		ChemFormula cf = new ChemFormula();
		cf.chemFormula("NaCl");
		System.out.println();
		cf.chemFormula("Na(Cl2(O3H)2)5");
		System.out.println();
		cf.chemFormula("Na7((Cl2(O3H)2(CO2)4)5)6");
		System.out.println();
		System.out.println();
	}

	public void chemFormula(String s) {
		int i = 0, j = 0;
		int len = s.length();
		Stack<Map<String, Integer>> stack = new Stack<Map<String, Integer>>();

		while (i < len) {
			char c = s.charAt(i);
			if (Character.isLetter(c)) {
				j++;
				while (j < len && Character.isLowerCase(s.charAt(j))) {
					j++;
				}
				String elem = s.substring(i, j);
				i = j;
				while (j < len && Character.isDigit(s.charAt(j))) {
					j++;
				}
				int val = (j == i) ? 1 : Integer.parseInt(s.substring(i, j));

				if (!stack.isEmpty()) {
					Map<String, Integer> map = stack.peek();
					int count = map.containsKey(elem) ? map.get(elem) : 0;
					map.put(elem, val + count);
				} else {
					Map<String, Integer> map = new HashMap<String, Integer>();
					map.put(elem, val);
					stack.push(map);
				}
			} else if (c == '(') {
				stack.push(new HashMap<String, Integer>());
				j++;

			} else if (c == ')') {
				j++;
				while (j < len && Character.isDigit(s.charAt(j))) {
					j++;
				}
				int val = 1;
				Map<String, Integer> map = stack.pop();
				if (j != i) {
					val = Integer.parseInt(s.substring(i + 1, j));
					for (Map.Entry<String, Integer> entry : map.entrySet()) {
						map.put(entry.getKey(), entry.getValue() * val);
					}
				}
				if (!stack.isEmpty()) {
					Map<String, Integer> preMap = stack.peek();
					for (Map.Entry<String, Integer> entry : map.entrySet()) {
						int count = preMap.containsKey(entry.getKey()) ? preMap
								.get(entry.getKey()) : 0;
						preMap.put(entry.getKey(), count + entry.getValue());
					}
				}
			}
			i = j;

		}
		for (Map.Entry<String, Integer> entry : stack.pop().entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}
}
