import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

public class CompanyChart {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(
				"src/org_chart.in"));
		BufferedWriter writer = new BufferedWriter(new FileWriter(
				"src/org_chart.out"));

		int numCases = Integer.parseInt(reader.readLine());

		CompanyChart cc = new CompanyChart();

		for (int caseNum = 1; caseNum <= numCases; caseNum++) {
			cc.buildMap(reader.readLine());
			writer.write("Case #" + caseNum + "\n");
			cc.outputChart(writer);
		}

		writer.close();
		reader.close();
	}

	Map<String, String[]> infoMap;
	Map<String, TreeSet<String>> hierarchMap;

	String ceoName;

	public void buildMap(String line) {
		infoMap = new HashMap<String, String[]>();
		hierarchMap = new HashMap<String, TreeSet<String>>();
		String[] personInfo = line.split("--");
		for (String pi : personInfo) {
			String[] cols = pi.split(",");
			infoMap.put(cols[0], new String[] { cols[2], cols[3] });
			if (cols[1].equals("NULL")) {
				ceoName = cols[0];
				continue;
			}
			if (!hierarchMap.containsKey(cols[1])) {
				hierarchMap.put(cols[1], new TreeSet<String>());
			}
			hierarchMap.get(cols[1]).add(cols[0]);
		}

	}

	public void outputChart(BufferedWriter writer) throws IOException {
		outputChartHelper(writer, ceoName, 0);
	}

	private void outputChartHelper(BufferedWriter writer, String name, int depth)
			throws IOException {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			sb.append('-');
		}
		sb.append(name);
		sb.append(" (");
		sb.append(infoMap.get(name)[0]);
		sb.append(") ");
		sb.append(infoMap.get(name)[1]);
		sb.append("\n");
		writer.write(sb.toString());
		if (hierarchMap.containsKey(name)) {
			for (String sub : hierarchMap.get(name)) {
				outputChartHelper(writer, sub, depth + 1);
			}
		}
	}
}
