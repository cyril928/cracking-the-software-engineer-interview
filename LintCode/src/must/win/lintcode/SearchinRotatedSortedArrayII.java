package must.win.lintcode;

// Search in Rotated Sorted Array II (LeetCode)
/*
 * 
 */
public class SearchinRotatedSortedArrayII {
	/**
	 * param A : an integer ratated sorted array and duplicates are allowed
	 * param target : an integer to be search return : a boolean
	 */
	public boolean search(int[] A, int target) {
		if (A == null) {
			return false;
		}
		for (int val : A) {
			if (val == target) {
				return true;
			}
		}
		return false;
	}
}
