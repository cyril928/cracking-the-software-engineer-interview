package must.win.lintcode;

import java.util.HashMap;
import java.util.Map;

// Construct Binary Tree from Preorder and Inorder Traversal (LeetCode)
/*
 * 
 */
public class ConstructBinaryTreefromPreorderandInorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param preorder
	 *            : A list of integers that preorder traversal of a tree
	 * @param inorder
	 *            : A list of integers that inorder traversal of a tree
	 * @return : Root of a tree
	 */
	public TreeNode buildTree(int[] preorder, int[] inorder) {
		if (preorder == null || inorder == null) {
			return null;
		}
		int len = preorder.length;
		if (len == 0 || len != inorder.length) {
			return null;
		}

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < len; i++) {
			map.put(inorder[i], i);
		}

		return buildTreeHelper(map, 0, len - 1, preorder, 0);
	}

	private TreeNode buildTreeHelper(Map<Integer, Integer> map, int start,
			int end, int[] preorder, int preIndex) {
		if (start > end) {
			return null;
		}
		int val = preorder[preIndex];
		TreeNode parent = new TreeNode(val);
		int index = map.get(val);

		parent.left = buildTreeHelper(map, start, index - 1, preorder,
				preIndex + 1);
		parent.right = buildTreeHelper(map, index + 1, end, preorder, preIndex
				+ 1 + (index - start));

		return parent;
	}
}
