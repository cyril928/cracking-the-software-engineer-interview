package must.win.lintcode;

import java.util.ArrayList;

// Insert Interval (LeetCode)
/*
 * 
 */
public class InsertInterval {
	public class Interval {
		int start, end;

		public Interval(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}

	/**
	 * Insert newInterval into intervals.
	 * 
	 * @param intervals
	 *            : Sorted interval list.
	 * @param newInterval
	 *            : A new interval.
	 * @return: A new sorted interval list.
	 */
	public ArrayList<Interval> insert(ArrayList<Interval> intervals,
			Interval newInterval) {
		ArrayList<Interval> result = new ArrayList<Interval>();

		int insertPos = 0;
		for (Interval interval : intervals) {
			if (interval.end < newInterval.start) {
				result.add(interval);
				insertPos++;
			} else if (interval.start > newInterval.end) {
				result.add(interval);
			} else {
				newInterval.start = Math.min(newInterval.start, interval.start);
				newInterval.end = Math.max(newInterval.end, interval.end);
			}
		}
		result.add(insertPos, newInterval);

		return result;
	}
}
