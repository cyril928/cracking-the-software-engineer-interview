package must.win.lintcode;

// Minimum Path Sum (LeetCode)
/*
 * 
 */
public class MinimumPathSum {
	/**
	 * @param grid
	 *            : a list of lists of integers.
	 * @return: An integer, minimizes the sum of all numbers along its path
	 */
	public int minPathSum(int[][] grid) {
		if (grid == null || grid.length == 0) {
			return Integer.MAX_VALUE;
		}
		int row = grid.length;
		int col = grid[0].length;

		int[] res = new int[col];

		res[0] = grid[0][0];
		for (int i = 1; i < col; i++) {
			res[i] = grid[0][i] + res[i - 1];
		}
		for (int i = 1; i < row; i++) {
			res[0] += grid[i][0];
			for (int j = 1; j < col; j++) {
				res[j] = grid[i][j] + Math.min(res[j], res[j - 1]);
			}
		}
		return res[col - 1];
	}
}
