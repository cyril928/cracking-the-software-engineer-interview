package must.win.lintcode;

// A + B Problem
/*
 * 
 */
public class APlusBProblem {

	// most clean way
	public int aplusb(int a, int b) {
		while (b != 0) {
			int carry = a & b;
			a = a ^ b;
			b = carry << 1;
		}
		return a;
	}

	// my own way
	public int aplusb1(int a, int b) {
		int carry = a & b;
		int sum = a ^ b;

		while (carry != 0) {
			int temp = (carry << 1);
			carry = sum & temp;
			sum ^= temp;
		}

		return sum;
	}
}
