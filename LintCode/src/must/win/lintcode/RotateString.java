package must.win.lintcode;

// Rotate String
/*
 * 
 */
public class RotateString {
	private void reverse(char[] A, int s, int e) {
		while (s < e) {
			char temp = A[s];
			A[s++] = A[e];
			A[e--] = temp;
		}
	}

	/*
	 * param A: A string param offset: Rotate string with offset. return:
	 * Rotated string.
	 */
	public char[] rotateString(char[] A, int offset) {
		if (A == null || A.length == 0) {
			return A;
		}
		offset %= A.length;
		reverse(A, 0, A.length - 1);
		reverse(A, 0, offset - 1);
		reverse(A, offset, A.length - 1);
		return A;
	}
}
