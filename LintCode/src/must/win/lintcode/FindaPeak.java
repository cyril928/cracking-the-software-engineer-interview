package must.win.lintcode;

// Find a Peak
/*
 * Time complexity O(logN)
 */
public class FindaPeak {
	/**
	 * @param A
	 *            : An integers array.
	 * @return: return any of peek positions.
	 */
	public int findPeak(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}

		int start = 0;
		int end = A.length - 1;
		int mid = -1;

		while (start <= end) {
			mid = (end - start) / 2 + start;
			// ascending in right and left
			if (mid == 0 || mid == A.length - 1) {
				break;
			}
			if (A[mid - 1] > A[mid] && A[mid + 1] > A[mid]) {
				// use the property that the numbers in adjacent positions are
				// different
				if (A[end] - A[mid] < end - mid) {
					start = mid + 1;
				} else {
					end = mid - 1;
				}
			}
			// ascending in right
			else if (A[mid + 1] > A[mid]) {
				start = mid + 1;
			}
			// ascending in left
			else if (A[mid - 1] > A[mid]) {
				end = mid - 1;
			} else {
				return mid;
			}
		}
		return -1;
	}
}
