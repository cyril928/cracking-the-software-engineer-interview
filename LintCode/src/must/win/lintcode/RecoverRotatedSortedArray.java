package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// Recover Rotated Sorted Array
/*
 * http://www.ninechapter.com/solutions/recover-rotated-sorted-array/
 * In-place, O(1) extra space and O(n) time.
 */
public class RecoverRotatedSortedArray {
	/**
	 * @param nums
	 *            : The rotated sorted array
	 * @return: The recovered sorted array
	 */
	public void recoverRotatedSortedArray(ArrayList<Integer> nums) {
		for (int i = 0; i < nums.size() - 1; i++) {
			if (nums.get(i) > nums.get(i + 1)) {
				reverse(nums, 0, i);
				reverse(nums, i + 1, nums.size() - 1);
				reverse(nums, 0, nums.size() - 1);
				return;
			}
		}
	}

	private void reverse(List<Integer> nums, int start, int end) {
		while (start < end) {
			int temp = nums.get(start);
			nums.set(start++, nums.get(end));
			nums.set(end--, temp);
		}
	}
}
