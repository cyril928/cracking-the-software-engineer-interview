package must.win.lintcode;

// Remove Duplicates from Sorted Array (LeetCode)
/*
 * 
 */
public class RemoveDuplicatesfromSortedArray {
	/**
	 * @param A
	 *            : a list of integers
	 * @return : return an integer
	 */
	public int removeDuplicates(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}

		int i = 1;
		for (int cur = 1; cur < nums.length; cur++) {
			if (nums[cur] != nums[cur - 1]) {
				nums[i++] = nums[cur];
			}
		}

		return i;
	}
}
