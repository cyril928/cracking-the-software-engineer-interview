package must.win.lintcode;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

// Merge k Sorted Lists (LeetCode)
/*
 * 
 */
public class MergekSortedLists {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	private class ListNodeComparator implements Comparator<ListNode> {
		@Override
		public int compare(ListNode n1, ListNode n2) {
			return n1.val - n2.val;
		}
	}

	/**
	 * @param lists
	 *            : a list of ListNode
	 * @return: The head of one sorted list.
	 */
	public ListNode mergeKLists(List<ListNode> lists) {
		if (lists == null || lists.size() == 0) {
			return null;
		}

		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;

		Queue<ListNode> heap = new PriorityQueue<ListNode>(lists.size(),
				new ListNodeComparator());

		for (ListNode node : lists) {
			if (node != null) {
				heap.add(node);
			}
		}

		while (!heap.isEmpty()) {
			cur.next = heap.poll();
			cur = cur.next;
			if (cur.next != null) {
				heap.add(cur.next);
			}
		}

		return dummy.next;
	}
}
