package must.win.lintcode;

// Search a 2D Matrix II
/*
 * O(m+n) time and O(1) extra space
 */
public class Searcha2DMatrixII {
	/**
	 * @param matrix
	 *            : A list of lists of integers
	 * @param: A number you want to search in the matrix
	 * @return: An integer indicate the occurrence of target in the given matrix
	 */
	public int searchMatrix(int[][] matrix, int target) {
		if (matrix == null || matrix.length == 0) {
			return 0;
		}
		int total = 0;
		int j = matrix[0].length - 1;
		for (int[] row : matrix) {
			while (j >= 0 && row[j] >= target) {
				if (row[j] == target) {
					total++;
				}
				j--;
			}
		}
		return total;
	}
}
