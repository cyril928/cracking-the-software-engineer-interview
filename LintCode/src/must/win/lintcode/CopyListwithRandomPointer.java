package must.win.lintcode;

// Copy List with Random Pointer (LeetCode)
/*
 * 
 */
public class CopyListwithRandomPointer {
	class RandomListNode {
		int label;
		RandomListNode next, random;

		RandomListNode(int x) {
			this.label = x;
		}
	}

	/**
	 * @param head
	 *            : The head of linked list with a random pointer.
	 * @return: A new head of a deep copy of the list.
	 */
	public RandomListNode copyRandomList(RandomListNode head) {
		if (head == null) {
			return null;
		}

		// combine two linkedlist
		RandomListNode curNode = head;
		while (curNode != null) {
			RandomListNode newNode = new RandomListNode(curNode.label);
			newNode.next = curNode.next;
			curNode.next = newNode;
			curNode = newNode.next;
		}

		// assign random node to new linkedlist
		curNode = head;
		while (curNode != null) {
			if (curNode.random != null) {
				curNode.next.random = curNode.random.next;
			}
			curNode = curNode.next.next;
		}

		// split as two linkedlist
		RandomListNode newHead = head.next;
		curNode = head;
		while (curNode != null) {
			RandomListNode nextNode = curNode.next.next;
			if (nextNode != null) {
				curNode.next.next = nextNode.next;
			}
			curNode.next = nextNode;
			curNode = nextNode;
		}

		return newHead;
	}
}
