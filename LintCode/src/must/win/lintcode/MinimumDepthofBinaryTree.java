package must.win.lintcode;

import java.util.ArrayDeque;
import java.util.Queue;

// Minimum Depth of Binary Tree (LeetCode)
/*
 * BFS
 * DFS with Tree Prune 
 */
public class MinimumDepthofBinaryTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// DFS with Tree Prune helper function
	private int helper(TreeNode root, int minDepth, int depth) {
		if (depth >= minDepth || root == null) {
			return depth;
		}

		if (root.left == null && root.right == null) {
			return depth + 1;
		}

		if (root.left != null) {
			minDepth = Math.min(helper(root.left, minDepth, depth + 1),
					minDepth);
		}

		if (root.right != null) {
			minDepth = Math.min(helper(root.right, minDepth, depth + 1),
					minDepth);
		}

		return minDepth;
	}

	// BFS
	public int minDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}

		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);
		int depth = 0;

		while (!queue.isEmpty()) {
			int size = queue.size();
			depth++;
			while (size-- > 0) {
				TreeNode curNode = queue.remove();
				if (curNode.left == null && curNode.right == null) {
					return depth;
				}
				if (curNode.left != null) {
					queue.add(curNode.left);
				}
				if (curNode.right != null) {
					queue.add(curNode.right);
				}
			}
		}
		return depth;
	}

	// DFS with Tree Prune
	public int minDepth1(TreeNode root) {
		return helper(root, Integer.MAX_VALUE, 0);
	}
}
