package must.win.lintcode;

import java.util.ArrayList;

// N-Queens (LeetCode)
/*
 * Can you do it without recursion?
 */

// Can you do it without recursion?
public class NQueens {
	private ArrayList<String> formSol(int[] position) {
		ArrayList<String> result = new ArrayList<String>();
		for (int index : position) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < position.length; j++) {
				String val = (j == index) ? "Q" : ".";
				sb.append(val);
			}
			result.add(sb.toString());
		}
		return result;
	}

	private boolean isValid(int[] position, int depth, int col) {
		for (int i = 0; i < depth; i++) {
			if (position[i] == col
					|| Math.abs(col - position[i]) == (depth - i)) {
				return false;
			}
		}
		return true;
	}

	private void nQueensDfs(ArrayList<ArrayList<String>> result,
			int[] position, int depth, int n) {

		if (depth == n) {
			ArrayList<String> sol = formSol(position);
			result.add(sol);
			return;
		}
		for (int i = 0; i < n; i++) {
			if (isValid(position, depth, i)) {
				position[depth] = i;
				nQueensDfs(result, position, depth + 1, n);
			}
		}
	}

	/**
	 * Get all distinct N-Queen solutions
	 * 
	 * @param n
	 *            : The number of queens
	 * @return: All distinct solutions For example, A string '...Q' shows a
	 *          queen on forth position
	 */
	ArrayList<ArrayList<String>> solveNQueens(int n) {
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		if (n <= 0) {
			return result;
		}
		int[] position = new int[n];
		nQueensDfs(result, position, 0, n);
		return result;
	}
}
