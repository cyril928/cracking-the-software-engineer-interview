package must.win.lintcode;


// Linked List Cycle (LeetCode)
/*
 * 
 */
public class LinkedListCycle {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param head
	 *            : The first node of linked list.
	 * @return: True if it has a cycle, or false
	 */
	public boolean hasCycle(ListNode head) {
		ListNode fastNode = head, slowNode = head;
		while (fastNode != null && fastNode.next != null) {
			fastNode = fastNode.next.next;
			slowNode = slowNode.next;
			if (fastNode == slowNode) {
				return true;
			}
		}
		return false;
	}
}
