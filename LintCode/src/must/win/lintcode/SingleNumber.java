package must.win.lintcode;

// Single Number (LeetCode)
public class SingleNumber {
	/**
	 * @param A
	 *            : an integer array return : a integer
	 */
	public int singleNumber(int[] A) {
		if (A == null) {
			return 0;
		}
		int res = 0;
		for (int a : A) {
			res ^= a;
		}
		return res;
	}
}
