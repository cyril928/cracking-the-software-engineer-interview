package must.win.lintcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

// Majority Number III
/*
 * http://www.meetqun.com/thread-3177-1-1.html
 * explanation & implementation
 * 
 * the basic idea is to reduce the k different numbers at the same time, 
 * which will not affect the "majority" feature.
 */
public class MajorityNumberIII {
	/**
	 * @param nums
	 *            : A list of integers
	 * @param k
	 *            : As described
	 * @return: The majority number
	 */
	public int majorityNumber(ArrayList<Integer> nums, int k) {
		// map<number, counter>
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int num : nums) {
			if (map.containsKey(num)) {
				map.put(num, map.get(num) + 1);
			} else {
				// already got k different elements, decrease each element's
				// count and remove entries with 0
				if (map.size() == k) {
					Iterator<Integer> iter = map.keySet().iterator();
					while (iter.hasNext()) {
						int key = iter.next();
						if (map.get(key) == 1) {
							/*
							 * note : directly use map.remove(key) in iterator
							 * scope, will cause
							 * java.util.ConcurrentModificationException
							 */
							iter.remove();
						} else {
							map.put(key, map.get(key) - 1);
						}
					}
				} else {
					map.put(num, 1);
				}
			}
		}

		int candidate = 0;
		int maxCount = 0;

		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
			if (entry.getValue() > maxCount) {
				maxCount = entry.getValue();
				candidate = entry.getKey();
			}
		}

		return candidate;
	}
}
