package must.win.lintcode;

// Longest Common Prefix (LeetCode)
/*
 * 
 */
public class LongestCommonPrefix {
	public String longestCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0) {
			return "";
		}
		int lastIndex = 0;
		while (lastIndex < strs[0].length()) {
			char c = strs[0].charAt(lastIndex);
			for (int i = 1; i < strs.length; i++) {
				if (lastIndex >= strs[i].length()
						|| strs[i].charAt(lastIndex) != c) {
					return strs[0].substring(0, lastIndex);
				}
			}
			lastIndex++;
		}
		return strs[0];
	}
}
