package must.win.lintcode;

// Reverse Words in a String (LeetCode)

public class ReverseWordsinaString {
	public String reverseWords(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}

		s = s.trim();
		String[] arrays = s.split(" +");

		StringBuilder sb = new StringBuilder();
		for (int i = arrays.length - 1; i >= 0; i--) {
			sb.append(arrays[i]).append(" ");
		}

		return sb.length() == 0 ? "" : sb.substring(0, sb.length() - 1);
	}
}
