package must.win.lintcode;

import java.util.ArrayList;
import java.util.Collections;

// Unique Subsets (LeetCode)
/*
 * 
 */
public class UniqueSubsets {
	/**
	 * @param S
	 *            : A set of numbers.
	 * @return: A list of lists. All valid subsets.
	 */
	public ArrayList<ArrayList<Integer>> subsetsWithDup(ArrayList<Integer> S) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (S == null || S.size() == 0) {
			return result;
		}
		Collections.sort(S);
		subsetsWithDupHelper(result, new ArrayList<Integer>(), S, 0);
		return result;
	}

	private void subsetsWithDupHelper(ArrayList<ArrayList<Integer>> result,
			ArrayList<Integer> path, ArrayList<Integer> S, int pos) {

		result.add(new ArrayList<Integer>(path));
		int size = S.size();
		for (int i = pos; i < size; i++) {
			if (i != pos && S.get(i) == S.get(i - 1)) {
				continue;
			}
			path.add(S.get(i));
			subsetsWithDupHelper(result, path, S, i + 1);
			path.remove(path.size() - 1);
		}
	}
}
