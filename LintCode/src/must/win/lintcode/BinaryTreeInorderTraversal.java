package must.win.lintcode;

import java.util.ArrayList;
import java.util.Stack;

// Binary Tree Inorder Traversal (LeetCode)
/*
 * 
 */
public class BinaryTreeInorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: Inorder in ArrayList which contains node values.
	 */
	public ArrayList<Integer> inorderTraversal(TreeNode root) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode cur = root;
		stack.add(cur);
		while (!stack.isEmpty()) {
			while (cur.left != null) {
				stack.push(cur.left);
				cur = cur.left;
			}

			do {
				cur = stack.pop();
				result.add(cur.val);
			} while (!stack.isEmpty() && cur.right == null);

			if (cur.right != null) {
				cur = cur.right;
				stack.push(cur);
			}
		}

		return result;
	}
}
