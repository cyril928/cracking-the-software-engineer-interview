package must.win.lintcode;

import java.util.ArrayList;
import java.util.Stack;

// Binary Tree Postorder Traversal (LeetCode)
public class BinaryTreePostorderTraversal {
	class TreeNode {
		TreeNode right, left;
		int val;

		TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: Postorder in ArrayList which contains node values.
	 */
	public ArrayList<Integer> postorderTraversal(TreeNode root) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		TreeNode prev = null;

		while (!stack.empty()) {
			TreeNode cur = stack.peek();
			if (prev == null || prev.left == cur || prev.right == cur) {
				if (cur.left != null) {
					stack.push(cur.left);
				} else if (cur.right != null) {
					stack.push(cur.right);
				}
			} else if (prev == cur.left) {
				if (cur.right != null) {
					stack.push(cur.right);
				}
			} else {
				result.add(cur.val);
				stack.pop();
			}
			prev = cur;
		}

		return result;
	}
}
