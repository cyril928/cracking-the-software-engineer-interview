package must.win.lintcode;

// Update Bits
/*
 * Cracking the Coding Interview
 */
public class UpdateBits {
	/**
	 * @param n
	 *            , m: Two integer
	 * @param i
	 *            , j: Two bit positions return: An integer
	 */
	// java allOnes << 32 => -1 not 0
	public int updateBits(int n, int m, int i, int j) {
		int left = (j == 31) ? 0 : ~0 << (j + 1);
		int right = (1 << i) - 1;
		n = n & (left | right);
		m <<= i;
		return n | m;
	}
}
