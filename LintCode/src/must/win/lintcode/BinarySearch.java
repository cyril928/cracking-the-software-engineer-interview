package must.win.lintcode;

// Binary Search
public class BinarySearch {
	/**
	 * @param nums
	 *            : The integer array.
	 * @param target
	 *            : Target to find.
	 * @return: The first position of target. Position starts from 0.
	 */
	public int binarySearch(int[] nums, int target) {
		if (nums == null || nums.length == 0) {
			return -1;
		}

		int low = 0, high = nums.length - 1;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			// focus on the value smaller than target
			if (nums[mid] >= target) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
		}
		// check whether target exist or not
		return (nums[low] == target) ? low : -1;
	}
}
