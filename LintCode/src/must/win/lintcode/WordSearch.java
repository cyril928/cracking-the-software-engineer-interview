package must.win.lintcode;

// Word Search (LeetCode)
/*
 * 
 */
public class WordSearch {
	/**
	 * @param board
	 *            : A list of lists of character
	 * @param word
	 *            : A string
	 * @return: A boolean
	 */
	public boolean exist(char[][] board, String word) {
		if (board == null || board.length == 0 || word == null) {
			return false;
		}
		if (word.isEmpty()) {
			return true;
		}

		int rows = board.length;
		int cols = board[0].length;

		boolean[][] visited = new boolean[rows][cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				if (searchDFS(board, word, 0, visited, i, j)) {
					return true;
				}

			}
		}
		return false;
	}

	private boolean searchDFS(char[][] board, String word, int index,
			boolean[][] visited, int i, int j) {

		if (word.charAt(index) != board[i][j]) {
			return false;
		} else if (index == word.length() - 1) {
			return true;
		}
		visited[i][j] = true;

		if (i > 0 && !visited[i - 1][j]
				&& searchDFS(board, word, index + 1, visited, i - 1, j)) {
			return true;
		}
		if (i < board.length - 1 && !visited[i + 1][j]
				&& searchDFS(board, word, index + 1, visited, i + 1, j)) {
			return true;
		}
		if (j > 0 && !visited[i][j - 1]
				&& searchDFS(board, word, index + 1, visited, i, j - 1)) {
			return true;
		}
		if (j < board[0].length - 1 && !visited[i][j + 1]
				&& searchDFS(board, word, index + 1, visited, i, j + 1)) {
			return true;
		}
		visited[i][j] = false;
		return false;
	}
}
