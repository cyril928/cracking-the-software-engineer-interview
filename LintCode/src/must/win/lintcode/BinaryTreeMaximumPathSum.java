package must.win.lintcode;

// Binary Tree Maximum Path Sum (LeetCode)
/*
 * using global variable to keep current max pathSum
 */
public class BinaryTreeMaximumPathSum {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: An integer.
	 */

	// using global variable to keep current max pathSum
	private int maxPath;

	private int findMax(TreeNode root) {
		if (root == null) {
			return 0;
		}
		int leftSum = Math.max(findMax(root.left), 0);
		int rightSum = Math.max(findMax(root.right), 0);
		maxPath = Math.max(leftSum + rightSum + root.val, maxPath);
		return Math.max(leftSum, rightSum) + root.val;
	}

	public int maxPathSum(TreeNode root) {
		maxPath = (root == null) ? 0 : root.val;
		findMax(root);
		return maxPath;
	}
}
