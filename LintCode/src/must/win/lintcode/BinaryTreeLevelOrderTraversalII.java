package must.win.lintcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

// Binary Tree Level Order Traversal II (LeetCode)
/*
 * 
 */
public class BinaryTreeLevelOrderTraversalII {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: buttom-up level order a list of lists of integer
	 */
	public ArrayList<ArrayList<Integer>> levelOrderButtom(TreeNode root) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (root == null) {
			return result;
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);

		while (!queue.isEmpty()) {
			ArrayList<Integer> level = new ArrayList<Integer>();
			TreeNode cur = queue.poll();
			do {
				level.add(cur.val);
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
				cur = queue.poll();
			} while (cur != null);

			result.add(level);

			if (!queue.isEmpty()) {
				queue.add(null);
			}
		}

		Collections.reverse(result);
		return result;
	}
}
