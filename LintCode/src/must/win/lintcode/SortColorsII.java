package must.win.lintcode;

// Sort Colors II
/*
 * without using extra memory (run SortColors I algorithm several times)
 * 
 * A rather straight forward solution is a two-pass algorithm using counting sort. 
 * That will cost O(k) extra memory.
 * Can you do it without using extra memory?
 */
public class SortColorsII {
	/**
	 * @param colors
	 *            : A list of integer
	 * @param k
	 *            : An integer
	 * @return: nothing
	 */
	public void sortColors2(int[] colors, int k) {
		if (colors == null || colors.length == 0 || k < 2) {
			return;
		}

		int lp = 0, rp = colors.length - 1, cur = 0;
		for (int leftColor = 1, rightColor = k; leftColor < rightColor; leftColor++, rightColor--) {
			cur = lp;
			while (cur <= rp) {
				if (colors[cur] == leftColor) {
					swap(colors, cur++, lp++);
				} else if (colors[cur] == rightColor) {
					swap(colors, cur, rp--);
				} else {
					cur++;
				}
			}
		}
	}

	private void swap(int colors[], int a, int b) {
		int temp = colors[a];
		colors[a] = colors[b];
		colors[b] = temp;
	}
}
