package must.win.lintcode;

// Distinct Subsequences (LeetCode)
/*
 * O(N) space
 */
public class DistinctSubsequences {
	/**
	 * @param S
	 *            , T: Two string.
	 * @return: Count the number of distinct subsequences
	 */
	public int numDistinct(String S, String T) {
		if (S == null || T == null || S.isEmpty() || T.isEmpty()) {
			return 0;
		}
		int sLen = S.length();
		int tLen = T.length();

		if (sLen < tLen) {
			return 0;
		}

		int[] count = new int[tLen];
		for (int i = 0; i < sLen; i++) {
			for (int j = Math.min(i, tLen - 1); j >= 0; j--) {
				if (S.charAt(i) == T.charAt(j)) {
					count[j] += (j == 0) ? 1 : count[j - 1];
				}
			}
		}

		return count[tLen - 1];
	}
}
