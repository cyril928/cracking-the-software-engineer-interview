package must.win.lintcode;

// Kth Prime Number
/*
 * O(n log n) or O(n) time
 * https://github.com/jnozsc/lintcode/blob/master/Kth_Prime_Number.cpp
 * implementation
 */
public class KthPrimeNumber {
	/**
	 * @param k
	 *            : The number k.
	 * @return: The kth prime number as description.
	 */
	public long kthPrimeNumber(int k) {
		long[] dp = new long[k + 1];
		dp[0] = 1;
		int i3 = 0, i5 = 0, i7 = 0;

		for (int i = 1; i <= k; i++) {
			long next = Math.min(Math.min(dp[i3] * 3, dp[i5] * 5), dp[i7] * 7);
			dp[i] = next;
			if (next == dp[i3] * 3) {
				i3++;
			}
			if (next == dp[i5] * 5) {
				i5++;
			}
			if (next == dp[i7] * 7) {
				i7++;
			}
		}
		return dp[k];
	}
}
