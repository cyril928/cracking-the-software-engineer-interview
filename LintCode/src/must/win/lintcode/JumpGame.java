package must.win.lintcode;

// Jump Game (LeetCode)
/*
 * 
 */
public class JumpGame {
	/**
	 * @param A
	 *            : A list of integers
	 * @return: The boolean answer
	 */
	public boolean canJump(int[] A) {
		if (A == null || A.length == 0) {
			return false;
		}

		int end = A.length - 1;
		for (int i = end - 1; i >= 0; i--) {
			if (A[i] >= (end - i)) {
				end = i;
			}
		}

		return (end == 0);

	}
}
