package must.win.lintcode;

// k Sum
/*
 * http://www.cnblogs.com/lishiblog/p/4182145.html
 * explanation & implementation
 * 3 dimensional DP
 * 
 * 
 * http://codeanytime.blogspot.tw/2014/12/lintcode-k-sum.html
 * Recursive, 2, 3 dimensional DP
 */
public class KSum {
	// 2 dimensional space
	public int kSum(int A[], int k, int target) {
		if (A == null || A.length < k) {
			return 0;
		}
		int len = A.length;
		int[][] dp = new int[k + 1][target + 1];

		for (int i = 1; i <= len; i++) {
			for (int j = Math.min(i, k); j >= 1; j--) {
				if (j == 1) {
					if (A[i - 1] <= target) {
						dp[1][A[i - 1]] = 1;
					}
				} else {
					for (int val = target; val > A[i - 1]; val--) {
						dp[j][val] += dp[j - 1][val - A[i - 1]];
					}
				}
			}
		}

		return dp[k][target];
	}

	// 3 dimensional space
	public int kSum1(int A[], int k, int target) {
		if (A == null || A.length < k) {
			return 0;
		}
		int len = A.length;
		int[][][] dp = new int[k + 1][len + 1][target + 1];

		for (int i = 0; i < len; i++) {
			if (A[i] <= target) {
				for (int j = i + 1; j <= len; j++) {
					dp[1][j][A[i]] = 1;
				}
			}
		}

		for (int i = 2; i <= k; i++) {
			for (int j = i; j <= len; j++) {
				for (int val = 1; val <= target; val++) {
					dp[i][j][val] += dp[i][j - 1][val];
					if (val - A[j - 1] > 0) {
						dp[i][j][val] += dp[i - 1][j - 1][val - A[j - 1]];
					}
				}
			}
		}

		return dp[k][len][target];
	}
}
