package must.win.lintcode;


// Convert Sorted List to Binary Search Tree (LeetCode)
/*
 * 
 */
public class ConvertSortedListtoBinarySearchTree {

	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param head
	 *            : The first node of linked list.
	 * @return: a tree node
	 */
	private ListNode cur;

	private int getLength(ListNode head) {
		int size = 0;
		while (head != null) {
			head = head.next;
			size++;
		}
		return size;
	}

	public TreeNode sortedListToBST(ListNode head) {
		cur = head;
		return sortedListToBSTHelper(getLength(head));
	}

	private TreeNode sortedListToBSTHelper(int size) {
		if (size == 0) {
			return null;
		}
		TreeNode leftNode = sortedListToBSTHelper(size >> 1);
		TreeNode parent = new TreeNode(cur.val);
		cur = cur.next;
		TreeNode rightNode = sortedListToBSTHelper(size - (size >> 1) - 1);

		parent.left = leftNode;
		parent.right = rightNode;

		return parent;
	}
}
