package must.win.lintcode;

import java.util.HashSet;
import java.util.Set;

// Longest Consecutive Sequence (LeetCode)
/*
 * O(n) Time Complexity
 */
public class LongestConsecutiveSequence {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return an integer
	 */
	public int longestConsecutive(int[] num) {
		if (num == null || num.length == 0) {
			return 0;
		}

		Set<Integer> set = new HashSet<Integer>();
		for (int val : num) {
			set.add(val);
		}

		int maxLen = 1;
		for (int val : num) {
			if (set.contains(val)) {
				int curMax = 1;
				int prev = val - 1;
				int next = val + 1;
				while (set.contains(next)) {
					curMax++;
					set.remove(next++);
				}

				while (set.contains(prev)) {
					curMax++;
					set.remove(prev--);
				}
				set.remove(val);
				maxLen = Math.max(curMax, maxLen);
			}
		}
		return maxLen;
	}
}
