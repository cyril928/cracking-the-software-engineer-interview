package must.win.lintcode;

// strStr (LeetCode)
/*
 * O(N) to-do.... 
 */
public class StrStr {
	/**
	 * Returns a index to the first occurrence of target in source, or -1 if
	 * target is not part of source.
	 * 
	 * @param source
	 *            string to be scanned.
	 * @param target
	 *            string containing the sequence of characters to match.
	 */

	// O(MN), need to go for O(N) solution
	public int strStr(String source, String target) {
		if (source == null || target == null) {
			return -1;
		}

		int sLen = source.length();
		int tLen = target.length();

		for (int i = 0; i <= sLen - tLen; i++) {
			int j = 0;
			while (j < tLen) {
				if (source.charAt(i + j) != target.charAt(j)) {
					break;
				}
				j++;
			}
			if (j == tLen) {
				return i;
			}
		}

		return -1;
	}
}
