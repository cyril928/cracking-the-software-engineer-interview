package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;

// 4 Sum (LeetCode)
public class FourSum {
	/**
	 * @param numbers
	 *            : Give an array numbers of n integer
	 * @param target
	 *            : you need to find four elements that's sum of target
	 * @return : Find all unique quadruplets in the array which gives the sum of
	 *         zero.
	 */

	// This solution runs in time O(n^3) and requires O(1) space.
	public ArrayList<ArrayList<Integer>> fourSum(int[] numbers, int target) {
		ArrayList<ArrayList<Integer>> resultSet = new ArrayList<ArrayList<Integer>>();
		if (numbers == null || numbers.length < 4) {
			return resultSet;
		}

		Arrays.sort(numbers);
		int len = numbers.length;
		for (int i = 0; i < len - 3; i++) {
			// to skip duplicate numbers;
			if (i != 0 && numbers[i] == numbers[i - 1]) {
				continue;
			}
			for (int j = i + 1; j < len - 2; j++) {
				// to skip duplicate numbers;
				if (j != i + 1 && numbers[j] == numbers[j - 1]) {
					continue;
				}

				int start = j + 1, end = len - 1;
				while (start < end) {
					int sum = numbers[i] + numbers[j] + numbers[start]
							+ numbers[end];
					if (sum > target) {
						// to skip duplicate numbers, improve efficiency
						do {
							end--;
						} while (start < end
								&& numbers[end] == numbers[end + 1]);
					} else if (sum < target) {
						// to skip duplicate numbers, improve efficiency
						do {
							start++;
						} while (start < end
								&& numbers[start] == numbers[start - 1]);
					} else {
						ArrayList<Integer> result = new ArrayList<Integer>();
						result.add(numbers[i]);
						result.add(numbers[j]);
						result.add(numbers[start]);
						result.add(numbers[end]);
						resultSet.add(result);
						// to skip duplicate numbers, improve efficiency
						do {
							end--;
						} while (start < end
								&& numbers[end] == numbers[end + 1]);
						// to skip duplicate numbers, improve efficiency
						do {
							start++;
						} while (start < end
								&& numbers[start] == numbers[start - 1]);
					}
				}
			}
		}
		return resultSet;
	}
}
