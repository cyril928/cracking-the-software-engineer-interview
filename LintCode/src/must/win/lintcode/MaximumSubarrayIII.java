package must.win.lintcode;

import java.util.ArrayList;

// Maximum Subarray III
/*
 * This is similar to The Painter's Partition Problem. DP formula is  
 * M[n, k] = Max{ M[j, k-1] + S(j) } , j=k-1..n
 * We divide the n values into two parts first. The first half is 0 to j-1 and 
 * the 2nd from j to n-1. The 1st half should be sub-divided into k-1 parts and
 * the 2nd a single part. The value of 2nd half, S(j), is actually the max sum
 * from j to n-1.
 * 
 * http://www.cnblogs.com/lishiblog/p/4183917.html
 * implementation
 */
public class MaximumSubarrayIII {
	/**
	 * @param nums
	 *            : A list of integers
	 * @param k
	 *            : An integer denote to find k non-overlapping subarrays
	 * @return: An integer denote the sum of max k non-overlapping subarrays
	 */
	public int maxSubArray(ArrayList<Integer> nums, int k) {
		if (nums == null || nums.size() < k) {
			return 0;
		}
		int len = nums.size();
		int[] dp = new int[len + 1];

		for (int i = 1; i <= k; i++) {
			for (int j = len; j >= i; j--) {
				int sum = 0;
				dp[j] = Integer.MIN_VALUE;
				int maxSum = Integer.MIN_VALUE;
				for (int p = j - 1; p >= i - 1; p--) {
					sum += nums.get(p);
					maxSum = Math.max(sum, maxSum);
					dp[j] = Math.max(dp[p] + maxSum, dp[j]);
					if (sum < 0) {
						sum = 0;
					}
				}
			}
		}

		return dp[len];
	}
}
