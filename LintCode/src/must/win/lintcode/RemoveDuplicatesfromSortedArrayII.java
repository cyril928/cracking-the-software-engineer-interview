package must.win.lintcode;

// Remove Duplicates from Sorted Array II (LeetCode)
/*
 * 
 */
public class RemoveDuplicatesfromSortedArrayII {
	// use current index
	/**
	 * @param A
	 *            : a array of integers
	 * @return : return an integer
	 */
	public int removeDuplicates(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}

		int size = 2;
		for (int i = 2; i < nums.length; i++) {
			if (nums[i] != nums[size - 2]) {
				nums[size++] = nums[i];
			}
		}
		return size;
	}

	/**
	 * @param A
	 *            : a array of integers
	 * @return : return an integer
	 */

	// use duplication count
	public int removeDuplicates1(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}

		int dupCount = 0;

		for (int i = 2; i < nums.length; i++) {
			if (nums[i] == nums[i - 2 - dupCount]) {
				dupCount++;
			} else if (dupCount > 0) {
				nums[i - dupCount] = nums[i];
			}
		}

		return nums.length - dupCount;
	}
}
