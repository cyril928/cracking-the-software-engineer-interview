package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;

// Search for a Range (LeetCode)
/*
 * Time O(log n)
 */
public class SearchforaRange {
	/**
	 * @param A
	 *            : an integer sorted array
	 * @param target
	 *            : an integer to be inserted return : a list of length 2,
	 *            [index1, index2]
	 */
	public ArrayList<Integer> searchRange(ArrayList<Integer> A, int target) {
		ArrayList<Integer> result = new ArrayList<Integer>(
				Arrays.asList(-1, -1));
		if (A == null || A.size() == 0) {
			return result;
		}

		int start = 0, end = A.size() - 1;
		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (A.get(mid) >= target) {
				end = mid - 1;
			} else {
				start = mid + 1;
			}
		}

		// can't find target value, be careful that start maybe over
		// A.size() - 1
		if (start >= A.size() || A.get(start) != target) {
			return result;
		}
		result.set(0, start);

		start = 0;
		end = A.size() - 1;
		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (A.get(mid) <= target) {
				start = mid + 1;
			} else {
				end = mid - 1;
			}
		}
		result.set(1, end);

		return result;
	}
}
