package must.win.lintcode;

import java.util.ArrayList;

// Search a 2D Matrix (LeetCode)
/*
 * O(log(n) + log(m)) time
 */
public class Searcha2DMatrix {
	/**
	 * @param matrix
	 *            , a list of lists of integers
	 * @param target
	 *            , an integer
	 * @return a boolean, indicate whether matrix contains target
	 */
	// O(log(n) + log(m)) time
	public boolean searchMatrix(ArrayList<ArrayList<Integer>> matrix, int target) {
		if (matrix == null || matrix.size() == 0 || matrix.get(0).size() == 0) {
			return false;
		}

		int m = matrix.size();
		int n = matrix.get(0).size();
		int start = 0;
		int end = m * n - 1;

		while (start <= end) {
			int mid = (end - start) / 2 + start;
			int val = matrix.get(mid / n).get(mid % n);
			if (val > target) {
				end = mid - 1;
			} else if (val < target) {
				start = mid + 1;
			} else {
				return true;
			}
		}
		return false;
	}
}
