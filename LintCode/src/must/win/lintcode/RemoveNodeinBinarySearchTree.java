package must.win.lintcode;

// Remove Node in Binary Search Tree
/*
 * recursive
 * iterative, cleaner, with dummy node
 * iterative, but not clean
 */
public class RemoveNodeinBinarySearchTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// recursive
	private TreeNode findSuccessor(TreeNode root) {
		while (root.right != null) {
			root = root.right;
		}
		return root;
	}

	// iterative, cleaner, with dummy node
	private TreeNode findSuccessor1(TreeNode parent, TreeNode child) {
		TreeNode root = parent;
		while (child.right != null) {
			parent = child;
			child = child.right;
		}
		if (parent != root) {
			parent.right = child.left;
			child.left = root.left;
		}
		return child;
	}

	// iterative, but not clean
	// method to find successor, next highest-value after delNode
	private TreeNode findSuccessor2(TreeNode delNode) {
		TreeNode successorParent = delNode;
		TreeNode successor = delNode;
		TreeNode cur = delNode.right;

		while (cur != null) {
			successorParent = successor;
			successor = cur;
			cur = cur.left;
		}

		// the successor is not the right child of delNode
		if (successor != delNode.right) {
			successorParent.left = successor.right;
			successor.right = delNode.right;
		}

		return successor;
	}

	// recursive
	public TreeNode removeNode(TreeNode root, int value) {
		if (root == null) {
			return null;
		}
		if (root.val > value) {
			root.left = removeNode(root.left, value);
		} else if (root.val < value) {
			root.right = removeNode(root.right, value);
		} else {
			if (root.left == null || root.right == null) {
				root = (root.left == null) ? root.right : root.left;
			} else {
				TreeNode successor = findSuccessor(root.left);
				successor.left = removeNode(root.left, successor.val);
				successor.right = root.right;
				return successor;
			}
		}
		return root;
	}

	// iterative, cleaner, with dummy node
	public TreeNode removeNode1(TreeNode root, int value) {
		boolean isLeftChild = false;
		TreeNode dummy = new TreeNode(0);
		dummy.right = root;
		TreeNode parent = dummy;
		TreeNode cur = root;
		while (cur != null) {
			if (cur.val == value) {
				break;
			}
			parent = cur;
			if (cur.val > value) {
				cur = cur.left;
				isLeftChild = true;
			} else if (cur.val < value) {
				cur = cur.right;
				isLeftChild = false;
			}
		}

		if (cur == null) {
			return root;
		}

		if (cur.left == null || cur.right == null) {
			TreeNode child = (cur.left == null) ? cur.right : cur.left;
			if (isLeftChild) {
				parent.left = child;
			} else {
				parent.right = child;
			}
		} else {
			TreeNode successor = findSuccessor1(cur, cur.left);
			if (isLeftChild) {
				parent.left = successor;
			} else {
				parent.right = successor;
			}
			successor.right = cur.right;
		}
		return dummy.right;
	}

	// iterative, but not clean
	public TreeNode removeNode2(TreeNode root, int value) {
		if (root == null) {
			return root;
		}

		TreeNode parent = null;
		boolean isLeftChild = false;

		TreeNode cur = root;
		while (cur != null) {
			if (value == cur.val) {
				break;
			}
			parent = cur;
			if (value < cur.val) {
				isLeftChild = true;
				cur = cur.left;
			} else if (value > cur.val) {
				isLeftChild = false;
				cur = cur.right;
			}
		}

		if (cur == null) {
			return root;
		}

		// no child
		if (cur.left == null && cur.right == null) {
			if (parent == null) {
				root = null;
			} else if (isLeftChild) {
				parent.left = null;
			} else {
				parent.right = null;
			}
		} else if (cur.left == null) { // no left child
			if (parent == null) {
				root = cur.right;
			} else if (isLeftChild) {
				parent.left = cur.right;
			} else {
				parent.right = cur.right;
			}
		} else if (cur.right == null) { // no right child
			if (parent == null) {
				root = cur.left;
			} else if (isLeftChild) {
				parent.left = cur.left;
			} else {
				parent.right = cur.left;
			}
		} else { // has two children
			TreeNode successor = findSuccessor2(cur);
			if (parent == null) {
				root = successor;
			} else if (isLeftChild) {
				parent.left = successor;
			} else {
				parent.right = successor;
			}
			successor.left = cur.left;
		}

		return root;
	}
}
