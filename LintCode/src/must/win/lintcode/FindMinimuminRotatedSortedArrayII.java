package must.win.lintcode;

// Find Minimum in Rotated Sorted Array II (LeetCode)
/*
 * 
 */
public class FindMinimuminRotatedSortedArrayII {
	/**
	 * @param num
	 *            : a rotated sorted array
	 * @return: the minimum number in the array
	 */
	public int findMin(int[] num) {
		if (num == null || num.length == 0) {
			return Integer.MAX_VALUE;
		}

		int low = 0, high = num.length - 1;
		while (low < high) {
			int mid = (high - low) / 2 + low;
			if (num[mid] < num[high]) {
				high = mid;
			} else if (num[mid] > num[high]) {
				low = mid + 1;
			} else {
				high--;
			}
		}

		return num[low];
	}
}
