package must.win.lintcode;

// Trailing Zeros
/*
 * 
 */
public class TrailingZeros {
	/*
	 * param n: As desciption return: An integer, denote the number of trailing
	 * zeros in n!
	 */
	public long trailingZeros(long n) {
		if (n <= 0) {
			return 0;
		}
		long res = 0;
		long d = 5;
		while (d <= n) {
			res += (n / d);
			d *= 5;
		}
		return res;
	}
}
