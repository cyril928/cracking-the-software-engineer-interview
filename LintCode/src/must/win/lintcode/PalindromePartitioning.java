package must.win.lintcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Palindrome Partitioning (LeetCode)
/*
 * DP(build solution list) + DP(build palindrome table)
 */
public class PalindromePartitioning {
	/**
	 * @param s
	 *            : A string
	 * @return: A list of lists of string
	 */
	public List<List<String>> partition(String s) {
		if (s == null || s.length() == 0) {
			return new ArrayList<List<String>>();
		}

		// this map is used to store previous results,
		// preResMap.get(i) is all partitions of s[0, i]
		Map<Integer, List<List<String>>> preResMap = new HashMap<Integer, List<List<String>>>();

		int len = s.length();

		// build up a table for palindrome substrings
		boolean[][] palindromeTable = new boolean[len][len];

		// cleaner coding for below j inner loop
		List<List<String>> dummy = new ArrayList<List<String>>();
		dummy.add(new ArrayList<String>());
		preResMap.put(-1, dummy);

		for (int i = 0; i < len; i++) {
			List<List<String>> partitions = new ArrayList<List<String>>();
			for (int j = 0; j <= i; j++) {
				if (s.charAt(j) == s.charAt(i)
						&& (i - j < 2 || palindromeTable[j + 1][i - 1])) {
					palindromeTable[j][i] = true;
					for (List<String> pp : preResMap.get(j - 1)) {
						List<String> np = new ArrayList<String>();
						np.addAll(pp);
						np.add(s.substring(j, i + 1));
						partitions.add(np);
					}
				}
			}
			preResMap.put(i, partitions);
		}

		return preResMap.get(len - 1);
	}
}
