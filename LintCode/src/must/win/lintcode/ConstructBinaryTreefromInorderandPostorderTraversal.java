package must.win.lintcode;


// Construct Binary Tree from Inorder and Postorder Traversal (LeetCode)
/*
 * 
 */
public class ConstructBinaryTreefromInorderandPostorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param inorder
	 *            : A list of integers that inorder traversal of a tree
	 * @param postorder
	 *            : A list of integers that postorder traversal of a tree
	 * @return : Root of a tree
	 */
	public TreeNode buildTree(int[] inorder, int[] postorder) {
		if (inorder == null || postorder == null) {
			return null;
		}
		int len = inorder.length;
		if (len == 0 || len != postorder.length) {
			return null;
		}
		return buildTreeHelper(inorder, 0, len - 1, postorder, len - 1);
	}

	private TreeNode buildTreeHelper(int[] inorder, int start, int end,
			int[] postorder, int postIndex) {
		if (start > end) {
			return null;
		}
		TreeNode parent = new TreeNode(postorder[postIndex]);
		int index = findPosition(inorder, start, end, postorder[postIndex]);
		parent.left = buildTreeHelper(inorder, start, index - 1, postorder,
				postIndex - 1 - (end - index));
		parent.right = buildTreeHelper(inorder, index + 1, end, postorder,
				postIndex - 1);
		return parent;
	}

	private int findPosition(int[] inorder, int start, int end, int key) {
		int index = start;
		while (index <= end && inorder[index] != key) {
			index++;
		}
		return index;
	}
}
