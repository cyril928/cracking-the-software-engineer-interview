package must.win.lintcode;

import java.util.ArrayList;

// Backpack
/*
 * http://en.wikipedia.org/wiki/Knapsack_problem
 * 0/1 knapsack, unbounded knapsack
 */
public class Backpack {
	/**
	 * @param m
	 *            : An integer m denotes the size of a backpack
	 * @param A
	 *            : Given n items with size A[i]
	 */
	public int backPack(int m, ArrayList<Integer> A) {
		if (m == 0 || A == null || A.size() == 0) {
			return 0;
		}
		int[] maxSize = new int[m + 1];

		// for only consider one item. (first deal with boundary case)
		for (int i = 0; i < m; i++) {
			if (A.get(0) <= i) {
				maxSize[i] = A.get(0);
			}
		}

		// for consider multiple item
		for (int i = 1; i < A.size(); i++) {
			int itemSize = A.get(i);
			for (int j = m; j >= 1; j--) {
				if (itemSize > j) {
					maxSize[j] = maxSize[j];
				} else {
					maxSize[j] = Math.max(maxSize[j - itemSize] + itemSize,
							maxSize[j]);
				}
			}
		}

		return maxSize[m];
	}
}
