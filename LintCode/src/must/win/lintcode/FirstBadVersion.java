package must.win.lintcode;

// First Bad Version
/*
 * Do not call isBadVersion exceed O(logN) times.
 */
public class FirstBadVersion {

	/**
	 * @param n
	 *            : An integers.
	 * @return: An integer which is the first bad version.
	 */
	public int findFirstBadVersion(int n) {
		int start = 1, end = n;

		while (start <= end) {
			int midVersion = (end - start) / 2 + start;
			if (VersionControl.isBadVersion(midVersion)) {
				if (midVersion == 1
						|| !VersionControl.isBadVersion(midVersion - 1)) {
					return midVersion;
				} else {
					end = midVersion - 1;
				}
			} else {
				if (midVersion == n) {
					return -1;
				}
				if (VersionControl.isBadVersion(midVersion + 1)) {
					return midVersion + 1;
				} else {
					start = midVersion + 1;
				}
			}
		}

		return -1;
	}
}

// for passing compile
class VersionControl {
	static boolean isBadVersion(int k) {
		return true;
	}
}
