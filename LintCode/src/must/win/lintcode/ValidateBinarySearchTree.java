package must.win.lintcode;

import java.util.Stack;

// Validate Binary Search Tree (LeetCode)
/*
 * recursive inorder traversal
 * iterative (user-level stack) inorder traversal
 */
public class ValidateBinarySearchTree {

	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: True if the binary tree is BST, or false
	 */
	// recursive inorder traversal, variale
	TreeNode prev = null;

	// recursive inorder traversal
	public boolean isValidBST(TreeNode root) {
		if (root == null) {
			return true;
		}
		if (!isValidBST(root.left)) {
			return false;
		}
		if (prev != null) {
			if (prev.val >= root.val) {
				return false;
			}
		}
		prev = root;
		if (!isValidBST(root.right)) {
			return false;
		}
		return true;
	}

	// iterative (user-level stack) inorder traversal
	public boolean isValidBST1(TreeNode root) {
		if (root == null) {
			return true;
		}

		TreeNode prev = null;
		Stack<TreeNode> stack = new Stack<TreeNode>();

		TreeNode cur = root;
		while (cur != null) {
			stack.push(cur);
			cur = cur.left;
		}

		while (!stack.isEmpty()) {
			cur = stack.pop();
			if (prev != null) {
				if (prev.val >= cur.val) {
					return false;
				}
			}
			prev = cur;
			cur = prev.right;
			while (cur != null) {
				stack.push(cur);
				cur = cur.left;
			}
		}

		return true;
	}
}
