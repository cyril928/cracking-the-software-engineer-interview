package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// Combinations (LeetCode)
/*
 * 
 */
public class Combinations {
	/**
	 * @param n
	 *            : Given the range of numbers
	 * @param k
	 *            : Given the numbers of combinations
	 * @return: All the combinations of k numbers out of 1..n
	 */

	// top-down recording recursive
	public List<List<Integer>> combine(int n, int k) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (n <= 0 || k <= 0 || k > n) {
			return result;
		}
		combineHelper(result, 1, n, k, new ArrayList<Integer>());
		return result;
	}

	// iterative(bottom-up)
	/**
	 * @param n
	 *            : Given the range of numbers
	 * @param k
	 *            : Given the numbers of combinations
	 * @return: All the combinations of k numbers out of 1..n
	 */
	public List<List<Integer>> combine1(int n, int k) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (n <= 0 || k <= 0 || k > n) {
			return result;
		}

		result.add(new ArrayList<Integer>());
		while (k > 0) {
			List<List<Integer>> newRes = new ArrayList<List<Integer>>();
			for (List<Integer> path : result) {
				int lastValue = 0;
				if (!path.isEmpty()) {
					lastValue = path.get(path.size() - 1);
				}
				for (int i = lastValue + 1; i <= n - k + 1; i++) {
					List<Integer> newPath = new ArrayList<Integer>(path);
					newPath.add(i);
					newRes.add(newPath);
				}
			}
			result = newRes;
			k--;
		}
		return result;
	}

	// top-down recording recursive helper
	private void combineHelper(List<List<Integer>> result, int start, int n,
			int k, List<Integer> path) {
		if (k == 0) {
			result.add(new ArrayList<Integer>(path));
		} else {
			for (int i = start; i <= n - k + 1; i++) {
				path.add(i);
				combineHelper(result, i + 1, n, k - 1, path);
				path.remove(path.size() - 1);
			}
		}
	}
}
