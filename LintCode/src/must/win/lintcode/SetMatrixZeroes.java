package must.win.lintcode;

// Set Matrix Zeroes (LeetCode)
/*
 * Do it in place.
 */
public class SetMatrixZeroes {
	/**
	 * @param matrix
	 *            : A list of lists of integers
	 * @return: Void
	 */
	public void setZeroes(int[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			return;
		}

		int row = matrix.length;
		int col = matrix[0].length;

		// record the first row and col's zero condition
		boolean isZeroRow = false;
		boolean isZeroCol = false;
		for (int i = 0; i < col; i++) {
			if (matrix[0][i] == 0) {
				isZeroRow = true;
				break;
			}
		}
		for (int i = 0; i < row; i++) {
			if (matrix[i][0] == 0) {
				isZeroCol = true;
				break;
			}
		}

		// mark the zero row or col in the first row and first col
		for (int i = 1; i < row; i++) {
			for (int j = 1; j < col; j++) {
				if (matrix[i][j] == 0) {
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				}
			}
		}

		// set the target row and col as 0
		for (int i = 1; i < row; i++) {
			for (int j = 1; j < col; j++) {
				if (matrix[i][0] == 0 || matrix[0][j] == 0) {
					matrix[i][j] = 0;
				}
			}
		}

		if (isZeroRow) {
			for (int i = 0; i < col; i++) {
				matrix[0][i] = 0;
			}
		}

		if (isZeroCol) {
			for (int i = 0; i < row; i++) {
				matrix[i][0] = 0;
			}
		}
	}
}
