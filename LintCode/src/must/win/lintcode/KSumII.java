package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// k Sum II
/*
 * http://www.meetqun.com/thread-3550-1-1.html
 * implementation, DFS
 */
public class KSumII {
	/**
	 * @param A
	 *            : an integer array.
	 * @param k
	 *            : a positive integer (k <= length(A))
	 * @param target
	 *            : a integer
	 * @return a list of lists of integer
	 */
	public ArrayList<ArrayList<Integer>> kSumII(int A[], int k, int target) {
		if (A == null || A.length < k) {
			return null;
		}

		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		kSumIIHelper(res, new ArrayList<Integer>(), k, target, 0, A);
		return res;
	}

	private void kSumIIHelper(List<ArrayList<Integer>> res, List<Integer> path,
			int k, int target, int pos, int A[]) {
		if (target == 0 && k == 0) {
			res.add(new ArrayList<Integer>(path));
		} else if (target == 0 || k == 0) {
			return;
		} else {
			int len = A.length;
			for (int i = pos; i < len; i++) {
				path.add(A[i]);
				kSumIIHelper(res, path, k - 1, target - A[i], i + 1, A);
				path.remove(path.size() - 1);
			}
		}
	}
}
