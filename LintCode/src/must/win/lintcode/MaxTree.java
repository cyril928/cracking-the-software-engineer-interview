package must.win.lintcode;

import java.util.Stack;

// Max Tree
/*
 * O(n) time complexity
 * 
 * http://www.cnblogs.com/lishiblog/p/4187936.html
 * implementation & explanation
 * iterative 
 * recursive (to-do)
 */
public class MaxTree {
	class TreeNode {
		int val;
		TreeNode left, right;

		TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param A
	 *            : Given an integer array with no duplicates.
	 * @return: The root of max tree.
	 */
	public TreeNode maxTree(int[] A) {
		if (A == null || A.length == 0) {
			return null;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(new TreeNode(A[0]));

		for (int i = 1; i < A.length; i++) {
			if (A[i] < stack.peek().val) {
				TreeNode node = new TreeNode(A[i]);
				stack.push(node);
			} else {
				TreeNode child = stack.pop();
				while (!stack.empty() && stack.peek().val < A[i]) {
					TreeNode parent = stack.pop();
					parent.right = child;
					child = parent;
				}
				TreeNode node = new TreeNode(A[i]);
				node.left = child;
				stack.push(node);
			}
		}

		TreeNode root = stack.pop();
		while (!stack.isEmpty()) {
			stack.peek().right = root;
			root = stack.pop();
		}
		return root;
	}
}
