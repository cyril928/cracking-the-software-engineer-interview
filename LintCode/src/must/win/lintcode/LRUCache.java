package must.win.lintcode;

import java.util.HashMap;
import java.util.Map;

// LRU Cache (LeetCode)
/*
 * 
 */
public class LRUCache {
	class DoubleListNode {
		public int key, val;
		public DoubleListNode prev, next;

		DoubleListNode(int key, int val) {
			this.key = key;
			this.val = val;
		}
	}

	private int capacity;
	private Map<Integer, DoubleListNode> map;

	private DoubleListNode head, tail;

	// @param capacity, an integer
	public LRUCache(int capacity) {
		this.capacity = capacity;
		map = new HashMap<Integer, DoubleListNode>();
		head = tail = null;
	}

	private void addToHead(DoubleListNode node) {
		node.next = head;
		if (head != null) { // not empty list
			head.prev = node;
		} else {
			tail = node;
		}
		head = node;
	}

	// @return an integer
	public int get(int key) {
		if (map.containsKey(key)) {
			DoubleListNode node = map.get(key);
			moveToHead(node);
			return node.val;
		} else {
			return -1;
		}
	}

	private void moveToHead(DoubleListNode node) {
		if (node != head) {
			if (node == tail) { // node is last node
				tail = node.prev;
			} else {
				node.next.prev = node.prev;
			}
			node.prev.next = node.next;
			addToHead(node);
		}
	}

	private void removeFromTail() {
		if (head == tail) {
			head = tail = null;
		} else {
			tail = tail.prev;
			tail.next = null;
		}
	}

	// @param key, an integer
	// @param value, an integer
	// @return nothing
	public void set(int key, int value) {
		if (map.containsKey(key)) {
			DoubleListNode node = map.get(key);
			moveToHead(node);
			node.val = value;
		} else {
			if (map.size() >= this.capacity) {
				map.remove(tail.key);
				removeFromTail();
			}
			DoubleListNode newNode = new DoubleListNode(key, value);
			addToHead(newNode);
			map.put(key, newNode);
		}
	}
}
