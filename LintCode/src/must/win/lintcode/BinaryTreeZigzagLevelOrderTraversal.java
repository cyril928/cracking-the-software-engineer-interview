package must.win.lintcode;

import java.util.ArrayList;
import java.util.Stack;

// Binary Tree Zigzag Level Order Traversal (LeetCode)
/*
 * 
 */
public class BinaryTreeZigzagLevelOrderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: A list of lists of integer include the zigzag level order
	 *          traversal of its nodes' values
	 */
	public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		boolean normalOrder = true;
		while (!stack.isEmpty()) {
			Stack<TreeNode> nextLevelStack = new Stack<TreeNode>();
			ArrayList<Integer> level = new ArrayList<Integer>();
			while (!stack.isEmpty()) {
				TreeNode cur = stack.pop();
				level.add(cur.val);
				if (normalOrder) {
					if (cur.left != null) {
						nextLevelStack.push(cur.left);
					}
					if (cur.right != null) {
						nextLevelStack.push(cur.right);
					}
				} else {
					if (cur.right != null) {
						nextLevelStack.push(cur.right);
					}
					if (cur.left != null) {
						nextLevelStack.push(cur.left);
					}
				}
			}
			result.add(level);
			stack = nextLevelStack;
			normalOrder = !normalOrder;
		}

		return result;
	}
}
