package must.win.lintcode;


// Sort List (LeetCode)
/*
 * 
 */
public class SortList {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	private ListNode findMiddle(ListNode head) {
		ListNode fastNode = head;
		ListNode slowNode = head;
		ListNode tailNode = head;
		while (fastNode != null && fastNode.next != null) {
			fastNode = fastNode.next.next;
			tailNode = slowNode;
			slowNode = slowNode.next;
		}
		tailNode.next = null;
		return slowNode;
	}

	private ListNode merge(ListNode left, ListNode right) {
		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;
		while (left != null && right != null) {
			if (left.val <= right.val) {
				cur.next = left;
				left = left.next;
			} else {
				cur.next = right;
				right = right.next;
			}
			cur = cur.next;
		}
		if (left != null) {
			cur.next = left;
		} else {
			cur.next = right;
		}
		return dummy.next;
	}

	/**
	 * @param head
	 *            : The head of linked list.
	 * @return: You should return the head of the sorted linked list, using
	 *          constant space complexity.
	 */
	public ListNode sortList(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode middle = findMiddle(head);
		return merge(sortList(head), sortList(middle));
	}
}
