package must.win.lintcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Anagrams (LeetCode)
/*
 * 
 */
public class Anagrams {
	/**
	 * @param strs
	 *            : A list of strings
	 * @return: A list of strings
	 */
	public List<String> anagrams(String[] strs) {
		List<String> res = new ArrayList<String>();
		if (strs == null || strs.length == 0) {
			return res;
		}

		Map<String, List<String>> map = new HashMap<String, List<String>>();
		for (String str : strs) {
			String key = generateKey(str);
			if (!map.containsKey(key)) {
				map.put(key, new ArrayList<String>());
			}
			map.get(key).add(str);
		}

		for (List<String> list : map.values()) {
			if (list.size() > 1) {
				res.addAll(list);
			}
		}

		return res;
	}

	private String generateKey(String s) {
		int[] nums = new int[26];
		for (int i = 0; i < s.length(); i++) {
			nums[s.charAt(i) - 'a']++;
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 26; i++) {
			if (nums[i] != 0) {
				char c = (char) (i + 'a');
				sb.append(c).append(nums[i]);
			}
		}
		return sb.toString();
	}
}
