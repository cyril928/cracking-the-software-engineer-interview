package must.win.lintcode;

// Remove Duplicates from Sorted List II (LeetCode)
/*
 * 
 */
public class RemoveDuplicatesfromSortedListII {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param ListNode
	 *            head is the head of the linked list
	 * @return: ListNode head of the linked list
	 */
	public ListNode deleteDuplicates(ListNode head) {
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode prev = dummy;
		ListNode cur = head;

		while (cur != null && cur.next != null) {
			if (cur.val == cur.next.val) {
				while (cur.next != null && cur.val == cur.next.val) {
					cur = cur.next;
				}
				prev.next = cur.next;
			} else {
				prev = cur;
			}
			cur = cur.next;
		}
		return dummy.next;
	}
}
