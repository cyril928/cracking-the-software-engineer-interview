package must.win.lintcode;

// Median
/*
 * http://www.cc.gatech.edu/~mihail/medianCMU.pdf
 * O(n) average time, O(N2) worst case time
 * Quick Select
 * 
 * http://codeanytime.blogspot.tw/2014/12/median.html
 * implementation
 */
public class Median {
	private int findKthNumber(int[] nums, int start, int end, int k) {
		int pivot = nums[start];
		int i = start + 1;
		int j = end;
		while (i <= j) {
			if (nums[i] < pivot) {
				i++;
			} else {
				swap(nums, i, j);
				j--;
			}
		}
		swap(nums, j, start);
		if (k == j + 1) {
			return pivot;
		} else if (k > j + 1) {
			return findKthNumber(nums, j + 1, end, k);
		} else { // k < i + 1
			return findKthNumber(nums, start, j - 1, k);
		}
	}

	/**
	 * @param nums
	 *            : A list of integers.
	 * @return: An integer denotes the middle number of the array.
	 */
	public int median(int[] nums) {
		int k = (nums.length >> 1) + (nums.length & 1);
		return findKthNumber(nums, 0, nums.length - 1, k);
	}

	private void swap(int[] A, int i, int j) {
		int temp = A[i];
		A[i] = A[j];
		A[j] = temp;
	}
}
