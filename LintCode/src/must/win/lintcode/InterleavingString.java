package must.win.lintcode;

// Interleaving String (LeetCode)
/*
 * DP, it takes time O(n1*n2) and space O(n2).
 */
public class InterleavingString {
	/**
	 * Determine whether s3 is formed by interleaving of s1 and s2.
	 * 
	 * @param s1
	 *            , s2, s3: As description.
	 * @return: true or false.
	 */
	// DP, it takes time O(n1*n2) and space O(n2).
	public boolean isInterleave(String s1, String s2, String s3) {
		if (s1 == null || s2 == null || s3 == null) {
			return false;
		}
		if (s1.length() + s2.length() != s3.length()) {
			return false;
		}

		int len1 = s1.length();
		int len2 = s2.length();
		boolean[] match = new boolean[len2 + 1];

		match[0] = true;

		for (int i = 0; i < len2; i++) {
			match[i + 1] = match[i] && (s2.charAt(i) == s3.charAt(i));
			if (!match[i + 1]) {
				break;
			}
		}

		for (int i = 0; i < len1; i++) {
			match[0] = match[0] && s1.charAt(i) == s3.charAt(i);
			for (int j = 0; j < len2; j++) {
				match[j + 1] = (match[j] && s2.charAt(j) == s3
						.charAt(i + j + 1))
						|| (match[j + 1] && s1.charAt(i) == s3
								.charAt(i + j + 1));
			}
		}

		return match[len2];
	}
}
