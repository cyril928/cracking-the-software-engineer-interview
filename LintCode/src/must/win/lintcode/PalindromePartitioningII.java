package must.win.lintcode;

// Palindrome Partitioning II (LeetCode)
/*
 * 
 */
public class PalindromePartitioningII {
	private boolean[][] buildPalindromeTable(String s) {
		int len = s.length();
		boolean[][] palindromeTable = new boolean[len][len];

		for (int i = 0; i < len; i++) {
			palindromeTable[i][i] = true;
		}

		for (int i = 0; i < len - 1; i++) {
			int m = i;
			int n = i + 1;
			while (m >= 0 && n < len && s.charAt(m) == s.charAt(n)) {
				palindromeTable[m--][n++] = true;
			}
			m = i;
			n = i + 2;
			while (m >= 0 && n < len && s.charAt(m) == s.charAt(n)) {
				palindromeTable[m--][n++] = true;
			}
		}

		return palindromeTable;
	}

	/**
	 * @param s
	 *            a string
	 * @return an integer
	 */
	public int minCut(String s) {
		if (s == null || s.isEmpty()) {
			return 0;
		}

		boolean[][] palindromeTable = buildPalindromeTable(s);

		int len = s.length();
		int[] minCut = new int[len];

		for (int i = 1; i < len; i++) {
			if (palindromeTable[0][i]) {
				minCut[i] = 0;
				continue;
			}
			minCut[i] = Integer.MAX_VALUE;
			for (int j = 0; j < i; j++) {
				if (palindromeTable[j + 1][i]) {
					minCut[i] = Math.min(minCut[j], minCut[i]);
				}
			}
			minCut[i] += 1;
		}

		return minCut[len - 1];
	}
}
