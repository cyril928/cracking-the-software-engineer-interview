package must.win.lintcode;

// Rotate List (LeetCode)
/*
 * 
 */
public class RotateList {
	class ListNode {
		int val;
		ListNode next;

		ListNode(int val) {
			this.val = val;
		}
	}

	private int getListSize(ListNode head) {
		int len = 0;
		while (head != null) {
			len++;
			head = head.next;
		}
		return len;
	}

	/**
	 * @param head
	 *            : the List
	 * @param k
	 *            : rotate to the right k places
	 * @return: the list after rotation
	 */
	public ListNode rotateRight(ListNode head, int k) {
		if (head == null || head.next == null || k == 0) {
			return head;
		}
		int size = getListSize(head);
		k = k % size;
		int n = size - k - 1;

		ListNode tail = head;
		while (tail.next != null) {
			tail = tail.next;
		}

		tail.next = head;

		ListNode cur = head;
		while (n-- > 0) {
			cur = cur.next;
		}
		head = cur.next;
		cur.next = null;
		return head;
	}
}
