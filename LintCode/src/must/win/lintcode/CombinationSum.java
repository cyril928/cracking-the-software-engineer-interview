package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Combination Sum (LeetCode)
/*
 * 
 */
public class CombinationSum {
	/**
	 * @param candidates
	 *            : A list of integers
	 * @param target
	 *            :An integer
	 * @return: A list of lists of integers
	 */
	// top-down recording recursive
	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (candidates == null || candidates.length == 0) {
			return result;
		}

		Arrays.sort(candidates);
		if (target < candidates[0]) {
			return result;
		}
		helper(result, candidates, 0, target, new ArrayList<Integer>());
		return result;
	}

	public void helper(List<List<Integer>> result, int[] candidates, int index,
			int target, List<Integer> path) {

		if (target == 0) {
			result.add(new ArrayList<Integer>(path));
		} else {
			if (target < candidates[index]) {
				return;
			}
			int len = candidates.length;
			for (int i = index; i < len; i++) {
				path.add(candidates[i]);
				helper(result, candidates, i, target - candidates[i], path);
				path.remove(path.size() - 1);
			}
		}
	}
}
