package must.win.lintcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Word Ladder II (LeetCode)
/*
 * 
 */
public class WordLadderII {
	// dfs (reverse tracking)
	private void buildPaths(List<List<String>> res, List<String> path,
			String start, String cur, Map<String, Set<String>> backTraceMap) {
		path.add(cur);
		if (cur.equals(start)) {
			List<String> sol = new ArrayList<String>(path);
			Collections.reverse(sol);
			res.add(sol);
			return;
		}
		for (String preWord : backTraceMap.get(cur)) {
			buildPaths(res, path, start, preWord, backTraceMap);
			path.remove(path.size() - 1);
		}
	}

	/**
	 * @param start
	 *            , a string
	 * @param end
	 *            , a string
	 * @param dict
	 *            , a set of string
	 * @return a list of lists of string
	 */
	public List<List<String>> findLadders(String start, String end,
			Set<String> dict) {
		List<List<String>> res = new ArrayList<List<String>>();

		dict.add(end);
		Map<String, Set<String>> backTraceMap = new HashMap<String, Set<String>>();
		Set<String> curLevelWords = new HashSet<String>();
		Set<String> nextLevelWords = new HashSet<String>();
		curLevelWords.add(start);

		while (!curLevelWords.isEmpty()) {
			/*
			 * to reomve all the nodes from dict in current level, because these
			 * nodes can't show in the shortest path's following levels
			 */
			for (String word : curLevelWords) {
				dict.remove(word);
			}
			findWord(curLevelWords, nextLevelWords, backTraceMap, dict);
			if (nextLevelWords.contains(end)) {
				buildPaths(res, new ArrayList<String>(), start, end,
						backTraceMap);
				return res;
			}
			curLevelWords = nextLevelWords;
			nextLevelWords = new HashSet<String>();
		}
		return res;
	}

	// bfs, build reverse tracking map
	private void findWord(Set<String> curLevelWords,
			Set<String> nextLevelWords, Map<String, Set<String>> backTraceMap,
			Set<String> dict) {
		for (String word : curLevelWords) {
			for (int i = 0; i < word.length(); i++) {
				char[] newCharArray = word.toCharArray();
				for (char c = 'a'; c <= 'z'; c++) {
					newCharArray[i] = c;
					String newWord = new String(newCharArray);
					if (dict.contains(newWord)) {
						nextLevelWords.add(newWord);
						if (!backTraceMap.containsKey(newWord)) {
							backTraceMap.put(newWord, new HashSet<String>());
						}
						backTraceMap.get(newWord).add(word);
					}
				}
			}
		}
	}
}
