package must.win.lintcode;

// Merge Sorted Array II (LeetCode)
/*
 * 
 */
public class MergeSortedArrayII {
	/**
	 * @param A
	 *            : sorted integer array A which has m elements, but size of A
	 *            is m+n
	 * @param B
	 *            : sorted integer array B which has n elements
	 * @return: void
	 */
	public void mergeSortedArray(int[] A, int m, int[] B, int n) {
		int cur = m + n - 1;
		m--;
		n--;
		while (m >= 0 && n >= 0) {
			if (A[m] <= B[n]) {
				A[cur--] = B[n--];
			} else {
				A[cur--] = A[m--];
			}
		}
		while (n >= 0) {
			A[cur--] = B[n--];
		}
	}
}
