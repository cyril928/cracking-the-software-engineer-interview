package must.win.lintcode;

// Regular Expression Matching (LeetCode)
/*
 * DP? (to-do)
 */
public class RegularExpressionMatching {
	private boolean checkEmpty(String p) {
		if ((p.length() & 1) == 1) {
			return false;
		}
		for (int i = 1; i < p.length(); i += 2) {
			if (p.charAt(i) != '*') {
				return false;
			}
		}
		return true;
	}

	private boolean compare(char a, char b) {
		return a == b || b == '.';
	}

	public boolean isMatch(String s, String p) {
		if (s == null || p == null) {
			return false;
		}

		if (s.isEmpty()) {
			return checkEmpty(p);
		}

		if (p.isEmpty()) {
			return false;
		}

		char sc1 = s.charAt(0);
		char pc1 = p.charAt(0), pc2 = '0';

		if (p.length() > 1) {
			pc2 = p.charAt(1);
		}

		if (pc2 == '*') {
			if (compare(sc1, pc1)) {
				return isMatch(s, p.substring(2)) || isMatch(s.substring(1), p);
			} else {
				return isMatch(s, p.substring(2));
			}
		} else {
			return compare(sc1, pc1) && isMatch(s.substring(1), p.substring(1));
		}
	}
}
