package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Combination Sum II (LeetCode)
/*
 * 
 */
public class CombinationSumII {
	/**
	 * @param num
	 *            : Given the candidate numbers
	 * @param target
	 *            : Given the target number
	 * @return: All the combinations that sum to target
	 */
	// top-down recording recursive
	public List<List<Integer>> combinationSum2(int[] num, int target) {
		List<List<Integer>> result = new ArrayList<List<Integer>>();
		if (num == null || num.length == 0) {
			return result;
		}
		Arrays.sort(num);
		helper(result, num, 0, target, new ArrayList<Integer>());
		return result;
	}

	public void helper(List<List<Integer>> result, int[] num, int index,
			int target, List<Integer> path) {
		if (target == 0) {
			result.add(new ArrayList<Integer>(path));
		} else {
			int len = num.length;
			for (int i = index; i < len; i++) {
				if (i > index && num[i] == num[i - 1]) {
					continue;
				}
				if (target < num[i]) {
					return;
				}
				path.add(num[i]);
				helper(result, num, i + 1, target - num[i], path);
				path.remove(path.size() - 1);
			}
		}
	}
}
