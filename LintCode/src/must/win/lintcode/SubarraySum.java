package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// Subarray Sum
/*
 * http://stackoverflow.com/questions/5534063/zero-sum-subarray
 * explanation
 * O(N) time complexity
 * 
 * sort by accumulative sum and sum's index
 * O(NlogN) from Zouyou
 */
public class SubarraySum {
	// self-defined class for sort by accumulative sum and sum's index
	class Element implements Comparable<Element> {
		private int sum, index;

		public Element(int sum, int index) {
			this.sum = sum;
			this.index = index;
		}

		@Override
		public int compareTo(Element e) {
			int diff = this.sum - e.sum;
			if (diff != 0) {
				return diff;
			}
			diff = this.index - e.index;
			return diff;
		}
	}

	/**
	 * @param nums
	 *            : A list of integers
	 * @return: A list of integers includes the index of the first number and
	 *          the index of the last number
	 */
	public ArrayList<Integer> subarraySum(int[] nums) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (nums == null || nums.length == 0) {
			return result;
		}
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		// to handle the case that subarray starts from the start of array
		map.put(0, -1);
		int sum = 0;
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
			if (map.containsKey(sum)) {
				result.add(map.get(sum) + 1);
				result.add(i);
				return result;
			} else {
				map.put(sum, i);
			}
		}
		return result;
	}

	// sort by accumulative sum and sum's index
	public ArrayList<Integer> subarraySum1(int[] nums) {

		ArrayList<Integer> res = new ArrayList<Integer>();
		if (nums == null || nums.length == 0) {
			return res;
		}

		Element[] es = new Element[nums.length + 1];
		int sum = 0;
		es[0] = new Element(0, -1);
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
			es[i + 1] = new Element(sum, i);
		}

		Arrays.sort(es);
		for (int i = 1; i <= nums.length; i++) {
			if (es[i].sum == es[i - 1].sum) {
				res.add(es[i - 1].index + 1);
				res.add(es[i].index);
				return res;
			}
		}
		return res;
	}
}
