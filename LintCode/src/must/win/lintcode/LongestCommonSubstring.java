package must.win.lintcode;

// Longest Common Substring
/*
 * http://www.geeksforgeeks.org/longest-common-substring/
 * Time Complexity: O(m*n)
 * Auxiliary Space: O(m*n)
 */
public class LongestCommonSubstring {
	/**
	 * @param A
	 *            , B: Two string.
	 * @return: the length of the longest common substring.
	 */

	// Auxiliary Space: O(MIN(m, n))
	public int longestCommonSubstring(String A, String B) {
		if (A == null || B == null || A.isEmpty() || B.isEmpty()) {
			return 0;
		}

		String sMax, sMin;
		if (A.length() <= B.length()) {
			sMax = B;
			sMin = A;
		} else {
			sMin = B;
			sMax = A;
		}

		int maxLen = sMax.length();
		int minLen = sMin.length();
		int[] dp = new int[minLen + 1];
		int result = 0;

		for (int i = 0; i < maxLen; i++) {
			for (int j = minLen - 1; j >= 0; j--) {
				if (sMax.charAt(i) == sMin.charAt(j)) {
					dp[j + 1] = dp[j] + 1;
					result = Math.max(dp[j + 1], result);
				} else {
					dp[j + 1] = 0;
				}
			}
		}

		return result;
	}
}
