package must.win.lintcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

// Unique Permutations (LeetCode)
/*
 * 
 */
public class UniquePermutations {
	public ArrayList<ArrayList<Integer>> permuteUnique(ArrayList<Integer> nums) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (nums == null || nums.size() == 0) {
			return result;
		}
		permuteUniqueHelper(result, new ArrayList<Integer>(), nums,
				new boolean[nums.size()]);
		return result;
	}

	private void permuteUniqueHelper(ArrayList<ArrayList<Integer>> result,
			ArrayList<Integer> path, ArrayList<Integer> nums, boolean[] visited) {
		if (path.size() == nums.size()) {
			result.add(new ArrayList<Integer>(path));
			return;
		}
		int len = nums.size();
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < len; i++) {
			if (visited[i] || set.contains(nums.get(i))) {
				continue;
			}
			visited[i] = true;
			set.add(nums.get(i));
			path.add(nums.get(i));
			permuteUniqueHelper(result, path, nums, visited);
			path.remove(path.size() - 1);
			visited[i] = false;

		}
	}
}
