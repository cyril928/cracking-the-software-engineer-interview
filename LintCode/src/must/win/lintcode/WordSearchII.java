package must.win.lintcode;

import java.util.ArrayList;

// Word Search II
/*
 * http://blog.csdn.net/brandohero/article/details/41858463#t5
 * common dfs implementation can pass all cases, because ArrayList<String> words is not big.
 * 
 * 
 * http://www.sanfoundry.com/java-program-ternary-search-tree/
 * java Ternary search tree implementation
 * 
 * http://igoro.com/archive/efficient-auto-complete-with-a-ternary-search-tree/
 * Efficient auto-complete with a ternary search tree
 * info about Trie and Ternary search tree
 * 
 * Trie Memory Limit Exceeded
 * Ternary search tree Memory Limit Exceeded
 * but Ternary search tree is litter better than Trie for space utilization
 * 
 */
public class WordSearchII {

	class TrieNode {
		public final int ALPHABET_SIZE = 26;
		public TrieNode[] children = new TrieNode[ALPHABET_SIZE];
		/*
		 * public boolean endString = false; TrieNode(boolean mEndString) {
		 * endString = mEndString; }
		 */
	}

	class TSTNode {
		private char val;
		public TSTNode left, center, right;

		/*
		 * public boolean endString = false;
		 */
		TSTNode(char val) {
			this.val = val;
		}

	}

	// for common dfs implementation
	private int[][] dr = { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };

	// Ternary search tree implementation
	public TSTNode insertString1(TSTNode node, char[][] board, int i, int j,
			boolean[][] visited) {

		if (node == null) {
			node = new TSTNode(board[i][j]);
		}

		if (board[i][j] < node.val) {
			node.left = insertString1(node.left, board, i, j, visited);
		} else if (board[i][j] > node.val) {
			node.right = insertString1(node.right, board, i, j, visited);
		} else {
			visited[i][j] = true;

			if (i > 0 && !visited[i - 1][j]) {
				node.center = insertString1(node.center, board, i - 1, j,
						visited);
			}
			if (i < board.length - 1 && !visited[i + 1][j]) {
				node.center = insertString1(node.center, board, i + 1, j,
						visited);
			}
			if (j > 0 && !visited[i][j - 1]) {
				node.center = insertString1(node.center, board, i, j - 1,
						visited);
			}
			if (j < board[0].length - 1 && !visited[i][j + 1]) {
				node.center = insertString1(node.center, board, i, j + 1,
						visited);
			}

			visited[i][j] = false;
		}
		return node;
	}

	// Trie Implementation
	public void insertString2(TrieNode node, char[][] board, int i, int j,
			boolean[][] visited) {

		char c = board[i][j];
		if (node.children[c - 'a'] == null) {
			node.children[c - 'a'] = new TrieNode();
		}

		visited[i][j] = true;
		if (i > 0 && !visited[i - 1][j]) {
			insertString2(node.children[c - 'a'], board, i - 1, j, visited);
		}
		if (i < board.length - 1 && !visited[i + 1][j]) {
			insertString2(node.children[c - 'a'], board, i + 1, j, visited);
		}
		if (j > 0 && !visited[i][j - 1]) {
			insertString2(node.children[c - 'a'], board, i, j - 1, visited);
		}
		if (j < board[0].length - 1 && !visited[i][j + 1]) {
			insertString2(node.children[c - 'a'], board, i, j + 1, visited);
		}
		visited[i][j] = false;
	}

	// Ternary search tree implementation
	public boolean lookUp1(TSTNode node, String word) {
		int pos = 0;
		while (node != null) {
			char c = word.charAt(pos);
			if (c < node.val) {
				node = node.left;
			} else if (c > node.val) {
				node = node.right;
			} else {
				if (++pos == word.length()) {
					return true;
				}
				node = node.center;
			}
		}
		return false;
	}

	// Trie Implementation
	public boolean lookUp2(TrieNode node, String word, int pos) {
		if (node == null) {
			return pos == word.length();
		}
		if (pos == word.length()) {
			return true;
		}
		char c = word.charAt(pos);
		if (node.children[c - 'a'] != null) {
			return lookUp2(node.children[c - 'a'], word, pos + 1);
		}
		return false;
	}

	// for common dfs implementation
	public boolean searchDFS(char[][] board, int i, int j, String word,
			int index) {
		if (word.charAt(index) != board[i][j]) {
			return false;
		}
		if (index == word.length() - 1) {
			return true;
		}

		char temp = board[i][j];
		board[i][j] = '#';

		for (int k = 0; k < 4; k++) {
			int x = i + dr[k][0], y = j + dr[k][1];
			if (x >= 0 && x < board.length && y >= 0 && y < board[0].length) {
				if (board[x][y] != '#'
						&& searchDFS(board, x, y, word, index + 1)) {
					board[i][j] = temp;
					return true;
				}
			}
		}
		board[i][j] = temp;
		return false;
	}

	// common dfs implementation
	public ArrayList<String> wordSearchII(char[][] board,
			ArrayList<String> words) {
		ArrayList<String> result = new ArrayList<String>();
		if (board == null || board.length == 0 || words == null
				|| words.isEmpty()) {
			return result;
		}

		int row = board.length;
		int col = board[0].length;

		for (String word : words) {
			for (int i = 0; i < row; i++) {
				int j = 0;
				for (; j < col; j++) {
					if (searchDFS(board, i, j, word, 0)) {
						result.add(word);
						break;
					}
				}
				if (j < col) {
					break;
				}
			}
		}

		return result;
	}

	// Ternary search tree implementation
	public ArrayList<String> wordSearchII1(char[][] board,
			ArrayList<String> words) {
		ArrayList<String> result = new ArrayList<String>();
		if (board == null || board.length == 0 || words == null
				|| words.isEmpty()) {
			return result;
		}

		int row = board.length;
		int col = board[0].length;
		boolean[][] visited = new boolean[row][col];

		// use char board to build Ternary search tree, like preprocessing
		TSTNode root = null;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				root = insertString1(root, board, i, j, visited);
			}
		}

		// look up whether the word in Ternary search tree or not
		for (String word : words) {
			if (lookUp1(root, word)) {
				result.add(word);
			}
		}
		return result;
	}

	// Trie Implementation
	public ArrayList<String> wordSearchII2(char[][] board,
			ArrayList<String> words) {
		ArrayList<String> result = new ArrayList<String>();
		if (board == null || board.length == 0 || words == null
				|| words.isEmpty()) {
			return result;
		}

		int row = board.length;
		int col = board[0].length;
		boolean[][] visited = new boolean[row][col];

		// use char board to build Trie, like preprocessing
		TrieNode root = new TrieNode();
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				insertString2(root, board, i, j, visited);
			}
		}

		// look up whether the word in Trie or not
		for (String word : words) {
			if (lookUp2(root, word, 0)) {
				result.add(word);
			}
		}
		return result;
	}
}
