package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeMap;

// Subarray Sum Closest
/*
 * O(NlogN) time complexity
 * Self-defined comparable class implementation
 * 
 * TreeMap implementation
 * 
 * http://www.quora.com/How-do-you-find-the-sum-of-the-subvector-with-the-sum-closest-to-a-given-real-number
 * just need closet sum solution, don't need index (use TreeSet)
 */
public class SubarraySumClosest {
	class Element implements Comparable<Element> {
		private int sum, index;

		public Element(int sum, int index) {
			this.sum = sum;
			this.index = index;
		}

		@Override
		public int compareTo(Element e) {
			int diff = this.sum - e.sum;
			if (diff != 0) {
				return diff;
			}
			diff = this.index - e.index;
			return diff;
		}
	}

	/**
	 * @param nums
	 *            : A list of integers
	 * @return: A list of integers includes the index of the first number and
	 *          the index of the last number
	 */
	// Self-defined comparable class implementation
	public ArrayList<Integer> subarraySumClosest(int[] nums) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (nums == null || nums.length == 0) {
			return res;
		}

		Element[] es = new Element[nums.length + 1];
		int minIndex = 0;
		int sum = 0;
		es[0] = new Element(sum, -1);
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
			es[i + 1] = new Element(sum, i);
		}

		Arrays.sort(es);
		for (int i = 0; i < nums.length; i++) {
			if (es[i + 1].sum - es[i].sum < es[minIndex + 1].sum
					- es[minIndex].sum) {
				minIndex = i;
			}
		}

		if (es[minIndex].index > es[minIndex + 1].index) {
			res.add(es[minIndex + 1].index + 1);
			res.add(es[minIndex].index);
		} else {
			res.add(es[minIndex].index + 1);
			res.add(es[minIndex + 1].index);
		}
		return res;
	}

	// TreeMap implementation
	public ArrayList<Integer> subarraySumClosest1(int[] nums) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (nums == null || nums.length == 0) {
			return result;
		}
		result.add(0, 0);
		result.add(1, 0);
		int minDiff = Integer.MAX_VALUE;
		TreeMap<Integer, Integer> map = new TreeMap<Integer, Integer>();
		// to handle the case that subarray starts from the start of array
		map.put(0, -1);

		int sum = 0;
		for (int i = 0; i < nums.length; i++) {
			sum += nums[i];
			if (map.containsKey(sum)) {
				result.set(0, map.get(sum) + 1);
				result.set(1, i);
				return result;
			} else {
				if (sum > map.firstKey()) {
					if (sum - map.floorKey(sum) < minDiff) {
						minDiff = sum - map.floorKey(sum);
						result.set(0, map.get(map.floorKey(sum)) + 1);
						result.set(1, i);
					}
				}
				if (sum < map.lastKey()) {
					if (map.ceilingKey(sum) - sum < minDiff) {
						minDiff = map.ceilingKey(sum) - sum;
						result.set(0, map.get(map.ceilingKey(sum)) + 1);
						result.set(1, i);
					}
				}
				map.put(sum, i);
			}
		}
		return result;
	}
}
