package must.win.lintcode;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

// Maximum Depth of Binary Tree (LeetCode)
/*
 * dfs
 * bfs
 */
public class MaximumDepthofBinaryTree {

	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: An integer.
	 */

	// dfs
	public int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
	}

	// bfs, size version
	public int maxDepth1(TreeNode root) {
		if (root == null) {
			return 0;
		}

		int res = 0;
		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);

		while (!queue.isEmpty()) {
			res++;
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.remove();
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
			}
		}
		return res;
	}

	// bfs, dummy version
	public int maxDepth2(TreeNode root) {
		if (root == null) {
			return 0;
		}

		int res = 0;
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);

		while (!queue.isEmpty()) {
			TreeNode cur = queue.poll();
			while (cur != null) {
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
				cur = queue.poll();
			}
			res++;
			if (!queue.isEmpty()) {
				queue.add(null);
			}
		}

		return res;
	}
}
