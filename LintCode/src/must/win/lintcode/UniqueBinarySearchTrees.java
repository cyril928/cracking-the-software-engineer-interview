package must.win.lintcode;

// Unique Binary Search Trees (LeetCode)
/*
 * 
 */
public class UniqueBinarySearchTrees {
	/**
	 * @paramn n: An integer
	 * @return: An integer
	 */
	public int numTrees(int n) {
		if (n < 0) {
			return 0;
		}

		int[] nums = new int[n + 1];
		nums[0] = 1;

		for (int i = 1; i <= n; i++) {
			int num = 0;
			for (int j = 1; j <= i; j++) {
				num += nums[j - 1] * nums[i - j];
			}
			nums[i] = num;
		}
		return nums[n];
	}
}
