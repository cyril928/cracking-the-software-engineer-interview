package must.win.lintcode;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

// Serialization and Deserialization Of Binary Tree
/*
 * http://fisherlei.blogspot.com/2013/03/interview-serialize-and-de-serialize.html
 * Preorder recursive
 * BFS
 * 
 * http://leetcode.com/2010/09/saving-binary-search-tree-to-file.html
 * why choose preorder instead of postorder & inorder
 * 
 */
public class SerializationandDeserializationOfBinaryTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// global variable for preorder recursive
	private int index = 0;

	/**
	 * This method will be invoked second, the argument data is what exactly you
	 * serialized at method "serialize", that means the data is not given by
	 * system, it's given by your own serialize method. So the format of data is
	 * designed by yourself, and deserialize it here as you serialize it in
	 * "serialize" method.
	 */
	// preorder iterative
	public TreeNode deserialize(String data) {
		String[] dataArray = data.split(",");
		Stack<TreeNode> stack = new Stack<TreeNode>();
		if (dataArray[0].equals("#")) {
			return null;
		}

		TreeNode root = new TreeNode(Integer.parseInt(dataArray[0]));
		TreeNode cur = root;
		stack.push(cur);
		int i = 1;
		while (i < dataArray.length && !dataArray[i].equals("#")) {
			cur.left = new TreeNode(Integer.parseInt(dataArray[i]));
			cur = cur.left;
			stack.push(cur);
			i++;
		}

		while (!stack.empty()) {
			i++;
			cur = stack.pop();
			if (i < dataArray.length && !dataArray[i].equals("#")) {
				cur.right = new TreeNode(Integer.parseInt(dataArray[i]));
				cur = cur.right;
				stack.push(cur);
				i++;
				while (i < dataArray.length && !dataArray[i].equals("#")) {
					cur.left = new TreeNode(Integer.parseInt(dataArray[i]));
					cur = cur.left;
					stack.push(cur);
					i++;
				}
			}
		}

		return root;
	}

	// preorder recursive
	public TreeNode deserialize1(String data) {
		index = 0;
		String[] dataArray = data.split(",");
		return deserializeHelper(dataArray);
	}

	// bfs
	public TreeNode deserialize2(String data) {
		if (data == null || data.isEmpty() || data.equals("#")) {
			return null;
		}

		String[] datas = data.split(",");
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		TreeNode root = new TreeNode(Integer.parseInt(datas[0]));
		queue.add(root);

		int i = 1;
		while (!queue.isEmpty()) {
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.poll();
				if (!datas[i].equals("#")) {
					cur.left = new TreeNode(Integer.parseInt(datas[i]));
					queue.add(cur.left);
				}
				i++;
				if (!datas[i].equals("#")) {
					cur.right = new TreeNode(Integer.parseInt(datas[i]));
					queue.add(cur.right);
				}
				i++;
			}
		}
		return root;
	}

	// preorder recursive deserialize helper
	public TreeNode deserializeHelper(String[] dataArray) {
		if (dataArray[index].equals("#")) {
			return null;
		}
		TreeNode parent = new TreeNode(Integer.parseInt(dataArray[index]));
		index++;
		parent.left = deserializeHelper(dataArray);
		index++;
		parent.right = deserializeHelper(dataArray);
		return parent;
	}

	/**
	 * This method will be invoked first, you should design your own algorithm
	 * to serialize a binary tree which denote by a root node to a string which
	 * can be easily deserialized by your own "deserialize" method later.
	 */
	// preorder iterative
	public String serialize(TreeNode root) {
		StringBuilder sb = new StringBuilder();
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		while (!stack.empty()) {
			TreeNode node = stack.pop();
			if (node != null) {
				stack.push(node.right);
				stack.push(node.left);
				sb.append(node.val + ",");
			} else {
				sb.append("#,");
			}
		}

		return sb.substring(0, sb.length() - 1);
	}

	// preorder recursive
	public String serialize1(TreeNode root) {
		StringBuilder sb = new StringBuilder();
		serializeHelper(root, sb);
		return sb.substring(0, sb.length() - 1);
	}

	// bfs
	public String serialize2(TreeNode root) {
		if (root == null) {
			return "#";
		}
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		StringBuilder sb = new StringBuilder();
		queue.add(root);
		sb.append(root.val).append(",");

		while (!queue.isEmpty()) {
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.poll();
				if (cur.left != null) {
					sb.append(cur.left.val).append(",");
					queue.add(cur.left);
				} else {
					sb.append("#,");
				}
				if (cur.right != null) {
					sb.append(cur.right.val).append(",");
					queue.add(cur.right);
				} else {
					sb.append("#,");
				}
			}
		}
		return sb.substring(0, sb.length() - 1);
	}

	// preorder recursive serialize helper
	public void serializeHelper(TreeNode root, StringBuilder sb) {
		if (root == null) {
			sb.append("#,");
			return;
		}
		sb.append(root.val).append(",");
		serializeHelper(root.left, sb);
		serializeHelper(root.right, sb);
	}
}
