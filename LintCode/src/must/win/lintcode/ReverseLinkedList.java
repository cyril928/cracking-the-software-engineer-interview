package must.win.lintcode;

// Reverse Linked List
public class ReverseLinkedList {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param head
	 *            : The head of linked list.
	 * @return: The new head of reversed linked list.
	 */
	// iterative
	public ListNode reverse(ListNode head) {
		ListNode prev = null;
		ListNode cur = head;
		while (cur != null) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}

		return prev;
	}

	// recursive
	public ListNode reverse1(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode newHead = reverse1(head.next);
		head.next.next = head;
		head.next = null;
		return newHead;
	}
}
