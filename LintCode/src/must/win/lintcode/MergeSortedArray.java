package must.win.lintcode;

import java.util.ArrayList;

// Merge Sorted Array
/*
 * How can you optimize your algorithm if one array is very large and the other is very small?
 */
public class MergeSortedArray {
	/**
	 * @param A
	 *            and B: sorted integer array A and B.
	 * @return: A new sorted integer array
	 */
	public ArrayList<Integer> mergeSortedArray(ArrayList<Integer> A,
			ArrayList<Integer> B) {

		ArrayList<Integer> result = new ArrayList<Integer>();
		int i = 0, j = 0;
		int aLen = A.size();
		int bLen = B.size();
		while (i < aLen && j < bLen) {
			if (A.get(i) <= B.get(j)) {
				result.add(A.get(i++));
			} else {
				result.add(B.get(j++));
			}
		}
		while (i < aLen) {
			result.add(A.get(i++));
		}
		while (j < bLen) {
			result.add(B.get(j++));
		}

		return result;
	}
}
