package must.win.lintcode;

// Backpack II
/*
 * 
 */
public class BackpackII {
	/**
	 * @param m
	 *            : An integer m denotes the size of a backpack
	 * @param A
	 *            & V: Given n items with size A[i] and value V[i]
	 * @return: The maximum value
	 */
	public int backPackII(int m, int[] A, int V[]) {

		/*
		 * V(m, n) = Max(V(m, n - 1), V(m - A[n], n - 1) + V[n]) if m >= A[n]
		 * V(m, n) = V(m, n - 1) if m < A[n]
		 */

		if (m < 0 || A == null || V == null || A.length != V.length) {
			return Integer.MIN_VALUE;
		}

		int itemNum = A.length;
		int[] value = new int[m + 1];

		for (int i = 1; i <= m; i++) {
			if (i >= A[0]) {
				value[i] = V[0];
			}
		}

		for (int i = 1; i < itemNum; i++) {
			for (int j = m; j > 0; j--) {
				if (j >= A[i]) {
					value[j] = Math.max(value[j], value[j - A[i]] + V[i]);
				}
			}
		}
		return value[m];
	}
}
