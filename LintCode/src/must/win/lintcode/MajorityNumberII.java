package must.win.lintcode;

import java.util.ArrayList;

// Majority Number II
/*
 * http://www.cnblogs.com/yuzhangcmu/p/4175779.html
 * explanation & implementation
 */
public class MajorityNumberII {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return: The majority number that occurs more than 1/3
	 */
	public int majorityNumber(ArrayList<Integer> nums) {
		int n1 = 0, n2 = 0;
		int cnt1 = 0, cnt2 = 0;

		for (int num : nums) {
			if (cnt1 == 0) {
				n1 = num;
				cnt1 = 1;
			} else if (cnt1 != 0 && n1 == num) {
				cnt1++;
			} else if (cnt2 == 0) {
				n2 = num;
				cnt2 = 1;
			} else if (cnt2 != 0 && n2 == num) {
				cnt2++;
			} else {
				cnt1--;
				cnt2--;
			}
		}

		cnt1 = 0;
		cnt2 = 0;
		for (int num : nums) {
			if (num == n1) {
				cnt1++;
			} else if (num == n2) {
				cnt2++;
			}
		}

		return (cnt1 > cnt2) ? n1 : n2;
	}
}
