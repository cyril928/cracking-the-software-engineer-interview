package must.win.lintcode;

import java.util.ArrayList;

// Next Permuation (LeetCode)
/*
 * 
 */
public class NextPermuation {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return: A list of integers that's next permuation
	 */
	public ArrayList<Integer> nextPermuation(ArrayList<Integer> nums) {
		if (nums == null || nums.size() == 0) {
			return nums;
		}

		// go to the end of ascending order from the end
		int i = nums.size() - 1;
		while (i > 0 && nums.get(i) <= nums.get(i - 1)) {
			i--;
		}

		// reverse the tail part
		reverse(nums, i, nums.size() - 1);

		if (i > 0) {
			int changeTargetVal = nums.get(i - 1);
			for (int j = i; j < nums.size(); j++) {
				if (nums.get(j) > changeTargetVal) {
					swap(nums, i - 1, j);
					break;
				}
			}
		}

		return nums;
	}

	private void reverse(ArrayList<Integer> nums, int start, int end) {
		while (start < end) {
			swap(nums, start++, end--);
		}
	}

	private void swap(ArrayList<Integer> nums, int a, int b) {
		int temp = nums.get(a);
		nums.set(a, nums.get(b));
		nums.set(b, temp);
	}
}
