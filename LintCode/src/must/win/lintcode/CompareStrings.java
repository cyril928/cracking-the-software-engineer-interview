package must.win.lintcode;

// Compare Strings
/*
 * 
 */
public class CompareStrings {
	/**
	 * @param A
	 *            : A string includes Upper Case letters
	 * @param B
	 *            : A string includes Upper Case letter
	 * @return : if string A contains all of the characters in B return true
	 *         else return false
	 */
	public boolean compareStrings(String A, String B) {
		if (A == null || B == null) {
			return false;
		}
		if (A.length() < B.length()) {
			return false;
		}

		int[] count = new int[26];

		int aLen = A.length();
		for (int i = 0; i < aLen; i++) {
			count[A.charAt(i) - 'A']++;
		}

		int bLen = B.length();
		for (int i = 0; i < bLen; i++) {
			if (count[B.charAt(i) - 'A'] == 0) {
				return false;
			}
			count[B.charAt(i) - 'A']--;
		}

		return true;
	}
}
