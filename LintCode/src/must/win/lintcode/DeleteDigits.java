package must.win.lintcode;

// Delete Digits
/*
 * My own way
 * Greedy O(N)
 */
public class DeleteDigits {
	/**
	 * @param A
	 *            : A positive integer which has N digits, A is a string.
	 * @param k
	 *            : Remove k digits.
	 * @return: A string
	 */

	// My own way, Greedy O(N)
	public String deleteDigits(String A, int k) {
		if (A.length() <= k) {
			return "";
		}

		int len = A.length();

		int min = Integer.MAX_VALUE;
		int minIndex = 0;

		StringBuilder sb = new StringBuilder();
		boolean startAppend = false;

		int n = len - k;
		while (n-- > 0) {
			min = Integer.MAX_VALUE;
			for (int i = minIndex; i <= k; i++) {
				if ((A.charAt(i) - '0') < min) {
					min = A.charAt(i) - '0';
					minIndex = i;
				}
			}
			minIndex++;
			k++;
			if (min != 0) {
				startAppend = true;
			}
			if (startAppend) {
				sb.append(min);
			}
		}

		return sb.toString();
	}
}
