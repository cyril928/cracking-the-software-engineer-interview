package must.win.lintcode;

// Convert String to Integer
/*
 * 
 */
public class ConvertStringtoInteger {
	public int atoi(String str) {
		long num = 0;
		boolean negative = false;

		str = str.trim();
		if (str.isEmpty()) {
			return 0;
		}
		if (str.charAt(0) == '-') {
			negative = true;
			str = str.substring(1);
		} else if (str.charAt(0) == '+') {
			str = str.substring(1);
		}
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) != '.' && !Character.isDigit(str.charAt(i))) {
				break;
			}
			if (str.charAt(i) == '.') {
				break;
			}
			num = num * 10 + str.charAt(i) - '0';
			if (!negative && num >= Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			} else if (negative && -num <= Integer.MIN_VALUE) {
				return Integer.MIN_VALUE;
			}
		}
		if (negative) {
			return -(int) num;
		} else {
			return (int) num;
		}
	}
}
