package must.win.lintcode;

import java.util.ArrayList;

// Permutations (LeetCode)
public class Permutations {
	/**
	 * @param nums
	 *            : A list of integers.
	 * @return: A list of permutations.
	 */
	public ArrayList<ArrayList<Integer>> permute(ArrayList<Integer> nums) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (nums == null || nums.size() == 0) {
			return result;
		}
		permuteHelper(result, new ArrayList<Integer>(), nums);
		return result;
	}

	private void permuteHelper(ArrayList<ArrayList<Integer>> result,
			ArrayList<Integer> path, ArrayList<Integer> nums) {
		if (path.size() == nums.size()) {
			result.add(new ArrayList<Integer>(path));
			return;
		}
		for (int num : nums) {
			if (!path.contains(num)) {
				path.add(num);
				permuteHelper(result, path, nums);
				path.remove(path.size() - 1);
			}
		}
	}
}
