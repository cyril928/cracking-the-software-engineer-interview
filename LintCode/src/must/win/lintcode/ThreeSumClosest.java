package must.win.lintcode;

import java.util.Arrays;

// 3 Sum Closest (LeetCode)
/*
 * 
 */
public class ThreeSumClosest {
	// This solution runs in time O(n^2) and requires O(1) space.
	/**
	 * @param numbers
	 *            : Give an array numbers of n integer
	 * @param target
	 *            : An integer
	 * @return : return the sum of the three integers, the sum closest target.
	 */
	public int threeSumClosest(int[] numbers, int target) {
		if (numbers == null || numbers.length < 3) {
			return 0;
		}

		Arrays.sort(numbers);
		int diff = Integer.MAX_VALUE;
		int result = 0;

		int len = numbers.length;
		for (int i = 0; i < len - 2; i++) {
			// to skip duplicate numbers;
			if (i > 0 && numbers[i] == numbers[i - 1]) {
				continue;
			}

			int start = i + 1, end = len - 1;
			while (start < end) {
				int sum = numbers[i] + numbers[start] + numbers[end];
				if (sum == target) {
					return target;
				} else {
					if (Math.abs(sum - target) < diff) {
						diff = Math.abs(sum - target);
						result = sum;
					}
					if (sum > target) {
						do {
							end--;
						} while (start < end
								&& numbers[end] == numbers[end + 1]);
					} else {
						do {
							start++;
						} while (start < end
								&& numbers[start] == numbers[start - 1]);
					}
				}
			}
		}

		return result;
	}
}
