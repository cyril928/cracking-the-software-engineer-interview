package must.win.lintcode;


// Linked List Cycle II (LeetCode)
/*
 * 
 */
public class LinkedListCycleII {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param head
	 *            : The first node of linked list.
	 * @return: The node where the cycle begins. if there is no cycle, return
	 *          null
	 */
	public ListNode detectCycle(ListNode head) {
		ListNode fastNode = head, slowNode = head;
		while (fastNode != null && fastNode.next != null) {
			fastNode = fastNode.next.next;
			slowNode = slowNode.next;
			if (fastNode == slowNode) {
				slowNode = head;
				while (fastNode != slowNode) {
					fastNode = fastNode.next;
					slowNode = slowNode.next;
				}
				return fastNode;
			}
		}
		return null;
	}
}
