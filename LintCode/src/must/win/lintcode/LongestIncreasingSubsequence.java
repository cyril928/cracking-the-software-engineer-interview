package must.win.lintcode;

// Longest Increasing Subsequence
/*
 * http://codeanytime.blogspot.com/2015/01/longest-increasing-subsequence.html
 * Time complexity O(NlogN)
 * 
 * http://okckd.github.io/blog/2014/06/24/Longest-Increasing-Subsequence/
 * Time complexity O(N^2) 
 */
public class LongestIncreasingSubsequence {
	// Time complexity O(NlogN) binary search
	private int binarySearchInsert(int[] A, int lastIndex, int target) {
		int start = 0, end = lastIndex;
		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (A[mid] > target) {
				end = mid - 1;
			} else {
				start = mid + 1;
			}
		}
		A[start] = target;
		return start;
	}

	/**
	 * @param nums
	 *            : The integer array
	 * @return: The length of LIS (longest increasing subsequence)
	 */

	// Time complexity O(NlogN)
	public int longestIncreasingSubsequence(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}

		int len = nums.length;
		int maxLen = 0;
		int[] A = new int[len];
		A[0] = nums[0];

		for (int i = 1; i < len; i++) {
			maxLen = Math.max(binarySearchInsert(A, maxLen, nums[i]), maxLen);
		}
		return maxLen + 1;
	}

	// Time complexity O(N^2)
	public int longestIncreasingSubsequence1(int[] nums) {
		if (nums == null || nums.length == 0) {
			return 0;
		}

		int len = nums.length;
		int[] dp = new int[len];
		for (int i = 0; i < len; i++) {
			int maxLen = 0;
			for (int j = 0; j < i; j++) {
				if (nums[j] <= nums[i]) {
					maxLen = Math.max(dp[j], maxLen);
				}
			}
			dp[i] = maxLen + 1;
		}

		int res = 1;
		for (int val : dp) {
			res = Math.max(val, res);
		}

		return res;
	}
}
