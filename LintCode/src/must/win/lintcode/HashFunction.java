package must.win.lintcode;

// Hash Function
/*
 * 
 */
public class HashFunction {
	/**
	 * @param key
	 *            : A String you should hash
	 * @param HASH_SIZE
	 *            : An integer
	 * @return an integer
	 */
	public int hashCode(char[] key, int HASH_SIZE) {
		long sum = 0;
		int index = key.length - 1;
		for (char c : key) {
			// do mode frequently to avoid overflow
			sum = (sum + c * pow(index, HASH_SIZE)) % HASH_SIZE;
			index--;
		}

		return (int) (sum % HASH_SIZE);
	}

	// pow(33, n), because Math.pow() return double value which can't hold
	// big integer
	private long pow(int n, int HASH_SIZE) {
		long power = 1;
		long r = 33 % HASH_SIZE;
		while (n != 0) {
			if ((n & 1) == 1) {
				// do mode frequently to avoid overflow
				power = (power * r) % HASH_SIZE;
			}
			n >>= 1;
			// do mode frequently to avoid overflow
			r = (r * r) % HASH_SIZE;
		}

		return power % HASH_SIZE;
	}
}
