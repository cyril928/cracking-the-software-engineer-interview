package must.win.lintcode;

// Fast Power
/*
 * O(logN)
 */
public class FastPower {
	/*
	 * @param a, b, n: 32bit integers
	 * 
	 * @return: An integer
	 */
	public int fastPower(int a, int b, int n) {
		long power = 1;
		long r = a % b;
		while (n > 0) {
			if ((n & 1) == 1) {
				power = (power * r) % b;
			}
			n >>= 1;
			r = (r * r) % b;
		}
		return (int) (power % b);
	}
}
