package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// Single Number III
/*
 * http://okckd.github.io/blog/2014/06/28/Single-Number-III/
 * O(n) time, O(1) extra space.
 * 
 * The main idea of the solution is how to remove repeated elements. It's definitely using XOR.
 * We need to try solve this problem like we did in 'Single Number I' but how? 
 * We can divide the array into 2 part, each part containing 1 non-repeating number.
 * 
 * Split by one of bit of all elements's XOR
 */
public class SingleNumberIII {
	/**
	 * @param A
	 *            : An integer array
	 * @return : Two integers
	 */
	public List<Integer> singleNumberIII(int[] A) {
		List<Integer> result = new ArrayList<Integer>();
		if (A == null || A.length == 0) {
			return result;
		}

		int xor = 0;
		for (int val : A) {
			xor ^= val;
		}

		// Get the rightmost set bit in xor
		int splitNum = xor & (~(xor - 1));

		int x = 0, y = 0;
		for (int val : A) {
			// don't use > 0, because for signed number
			if ((val & splitNum) != 0) {
				x ^= val;
			} else {
				y ^= val;
			}
		}

		result.add(x);
		result.add(y);
		return result;
	}
}
