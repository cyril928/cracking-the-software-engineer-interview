package must.win.lintcode;

// N-Queens II (LeetCode)
/*
 * 
 */
public class NQueensII {
	/**
	 * Calculate the total number of distinct N-Queen solutions.
	 * 
	 * @param n
	 *            : The number of queens.
	 * @return: The total number of distinct solutions.
	 */
	private int num = 0;

	private boolean isValid(int[] usedCols, int depth, int val) {
		for (int i = 0; i < depth; i++) {
			if (val == usedCols[i] || Math.abs(val - usedCols[i]) == depth - i) {
				return false;
			}
		}
		return true;
	}

	private void nQueensDFS(int[] usedCols, int depth) {
		int n = usedCols.length;
		if (depth == n) {
			num++;
			return;
		}
		for (int i = 0; i < n; i++) {
			if (isValid(usedCols, depth, i)) {
				usedCols[depth] = i;
				nQueensDFS(usedCols, depth + 1);
			}
		}
	}

	public int totalNQueens(int n) {
		if (n <= 0) {
			return 0;
		}
		nQueensDFS(new int[n], 0);
		return num;
	}
}
