package must.win.lintcode;

import java.util.ArrayList;

// Search Insert Position (LeetCode)
/*
 * 
 */
public class SearchInsertPosition {
	/**
	 * param A : an integer sorted array param target : an integer to be
	 * inserted return : an integer
	 */
	public int searchInsert(ArrayList<Integer> A, int target) {
		int start = 0;
		int end = A.size() - 1;
		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (A.get(mid) > target) {
				end = mid - 1;
			} else if (A.get(mid) < target) {
				start = mid + 1;
			} else {
				return mid;
			}
		}
		return start;
	}
}
