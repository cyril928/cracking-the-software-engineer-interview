package must.win.lintcode;

import java.util.Arrays;
import java.util.Comparator;

// Largest Number (LeetCode)
/*
 * 
 */
public class LargestNumber {
	/**
	 * @param num
	 *            : A list of non negative integers
	 * @return: A string
	 */
	public String largestNumber(int[] num) {
		if (num == null || num.length == 0) {
			return "";
		}
		String[] strs = new String[num.length];
		for (int i = 0; i < strs.length; i++) {
			strs[i] = String.valueOf(num[i]);
		}

		Comparator<String> comparator = new Comparator<String>() {
			@Override
			public int compare(String s1, String s2) {
				return (s2 + s1).compareTo(s1 + s2);
			}
		};

		Arrays.sort(strs, comparator);

		if (strs[0].equals("0")) {
			return "0";
		}
		StringBuilder sb = new StringBuilder();
		for (String str : strs) {
			sb.append(str);
		}
		return sb.toString();
	}
}
