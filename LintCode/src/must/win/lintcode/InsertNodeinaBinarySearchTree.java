package must.win.lintcode;

// Insert Node in a Binary Search Tree
/*
 * iterative, Do it without recursion
 * recursive
 */
public class InsertNodeinaBinarySearchTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of the binary search tree.
	 * @param node
	 *            : insert this node into the binary search tree
	 * @return: The root of the new binary search tree.
	 */
	// iterative, Do it without recursion
	public TreeNode insertNode(TreeNode root, TreeNode node) {

		if (root == null) {
			return node;
		}

		TreeNode cur = root;
		while (true) {
			if (node.val <= cur.val) {
				if (cur.left == null) {
					cur.left = node;
					break;
				}
				cur = cur.left;
			} else {
				if (cur.right == null) {
					cur.right = node;
					break;
				}
				cur = cur.right;
			}
		}
		return root;
	}

	// recursive
	public TreeNode insertNode1(TreeNode root, TreeNode node) {
		if (root == null) {
			return node;
		}

		if (root.val >= node.val) {
			root.left = insertNode(root.left, node);
		} else {
			root.right = insertNode(root.right, node);
		}
		return root;
	}
}
