package must.win.lintcode;

// Sort Colors (LeetCode)
/*
 * one-pass algorithm using only constant space
 */
public class SortColors {

	// 1 pass, two pointers
	/**
	 * @param nums
	 *            : A list of integer which is 0, 1 or 2
	 * @return: nothing
	 */
	public void sortColors(int[] nums) {
		if (nums == null || nums.length == 0) {
			return;
		}

		int lp = 0, rp = nums.length - 1, cur = 0;
		while (cur <= rp) {
			if (nums[cur] == 0) {
				swap(nums, lp++, cur++);
			} else if (nums[cur] == 2) {
				swap(nums, rp--, cur);
			} else {
				cur++;
			}
		}
	}

	// 2 pass, counting sort
	public void sortColors1(int[] nums) {
		if (nums == null || nums.length == 0) {
			return;
		}

		int[] count = new int[3];
		for (int val : nums) {
			count[val]++;
		}

		int index = 0;
		for (int i = 0; i < count.length; i++) {
			while (count[i]-- > 0) {
				nums[index++] = i;
			}
		}
	}

	private void swap(int[] nums, int a, int b) {
		int temp = nums[a];
		nums[a] = nums[b];
		nums[b] = temp;
	}
}
