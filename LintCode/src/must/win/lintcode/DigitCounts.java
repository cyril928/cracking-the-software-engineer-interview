package must.win.lintcode;

// Digit Counts
/*
 * better solution -> to do
 */
public class DigitCounts {
	/*
	 * param k : As description. param n : As description. return: An integer
	 * denote the count of digit k in 1..n
	 */
	public int digitCounts(int k, int n) {
		int result = 0;
		for (int i = 0; i <= n; i++) {
			int num = i;
			while (num > 0) {
				if (num % 10 == k) {
					result++;
				}
				num /= 10;
			}
		}

		return result;
	}
}
