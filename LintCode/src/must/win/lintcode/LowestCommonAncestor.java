package must.win.lintcode;

// Lowest Common Ancestor
/*
 * from ZouYou, most clean way
 * 
 * Cracking the Coding Interview
 */
public class LowestCommonAncestor {
	class Result {
		public TreeNode node;
		public boolean isAncestor;

		Result(TreeNode node, boolean isAncestor) {
			this.node = node;
			this.isAncestor = isAncestor;
		}
	}

	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of the binary search tree.
	 * @param A
	 *            and B: two nodes in a Binary.
	 * @return: Return the least common ancestor(LCA) of the two nodes.
	 */

	// from ZouYou, most clean way
	public TreeNode lowestCommonAncestor(TreeNode root, TreeNode A, TreeNode B) {
		if (root == null || root == A || root == B) {
			return root;
		}
		TreeNode left = lowestCommonAncestor(root.left, A, B);
		TreeNode right = lowestCommonAncestor(root.right, A, B);

		if (left != null && right != null) {
			return root;
		}
		return left == null ? right : left;

	}

	// Cracking the Coding Interview
	public TreeNode lowestCommonAncestor1(TreeNode root, TreeNode A, TreeNode B) {
		Result result = lowestCommonAncestorHelper(root, A, B);
		if (result.isAncestor) {
			return result.node;
		}
		return null;
	}

	public Result lowestCommonAncestorHelper(TreeNode root, TreeNode A,
			TreeNode B) {
		if (root == null) {
			return new Result(null, false);
		}

		// move this verification forward instead of put it after left & right
		// dfs, for
		// performance
		if (root == A && root == B) {
			return new Result(root, true);
		}

		Result leftRes = lowestCommonAncestorHelper(root.left, A, B);
		if (leftRes.isAncestor) {
			return leftRes;
		}
		Result rightRes = lowestCommonAncestorHelper(root.right, A, B);
		if (rightRes.isAncestor) {
			return rightRes;
		}

		/*
		 * change to code snippet below which is cleaner if(leftRes.node == null
		 * && rightRes.node == null) { if(root == A || root == B) { return new
		 * Result(root, false); } else { return new Result(null, false); } }
		 * else if(leftRes.node != null && rightRes.node != null){ return new
		 * Result(root, true); } else { if(root == A || root == B) { return new
		 * Result(root, true); } else { return new Result(root, false); } }
		 */

		if (leftRes.node != null && rightRes.node != null) {
			return new Result(root, true);
		} else if (root == A || root == B) {
			return new Result(root,
					(leftRes.node != null || rightRes.node != null));
		} else {
			return new Result(leftRes.node != null ? leftRes.node
					: rightRes.node, false);
		}

	}
}
