package must.win.lintcode;

// Rotate Image (LeetCode)
/*
 *  do this in-place
 */
public class RotateImage {
	/**
	 * @param matrix
	 *            : A list of lists of integers
	 * @return: Void
	 */
	public void rotate(int[][] matrix) {
		if (matrix == null || matrix.length == 0) {
			return;
		}

		int rs = 0, re = matrix.length - 1;
		while (rs < re) {
			for (int i = rs; i < re; i++) {
				int tail = matrix.length - i - 1;
				int temp = matrix[rs][i];
				matrix[rs][i] = matrix[tail][rs];
				matrix[tail][rs] = matrix[re][tail];
				matrix[re][tail] = matrix[i][re];
				matrix[i][re] = temp;
			}
			rs++;
			re--;
		}
	}
}
