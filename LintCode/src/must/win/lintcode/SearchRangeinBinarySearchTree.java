package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// Search Range in Binary Search Tree
/*
 * 
 */
public class SearchRangeinBinarySearchTree {
	class TreeNode {
		TreeNode left;
		TreeNode right;
		int val;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// inOrder recursive helper function
	private void inOrderTraversal(TreeNode root, List<Integer> result, int k1,
			int k2) {
		if (root == null) {
			return;
		}
		if (root.val >= k1) {
			inOrderTraversal(root.left, result, k1, k2);
		}
		if (root.val >= k1 && root.val <= k2) {
			result.add(root.val);
		}
		if (root.val <= k2) {
			inOrderTraversal(root.right, result, k1, k2);
		}
	}

	/**
	 * @param root
	 *            : The root of the binary search tree.
	 * @param k1
	 *            and k2: range k1 to k2.
	 * @return: Return all keys that k1<=key<=k2 in ascending order.
	 */

	// inOrder recursive
	public ArrayList<Integer> searchRange(TreeNode root, int k1, int k2) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		inOrderTraversal(root, result, k1, k2);
		return result;
	}
}
