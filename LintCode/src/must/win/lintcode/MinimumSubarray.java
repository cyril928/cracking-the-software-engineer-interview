package must.win.lintcode;

import java.util.ArrayList;

// Minimum SubArray
/*
 * almost same as Maximum SubArray
 */
public class MinimumSubarray {
	/**
	 * @param nums
	 *            : a list of integers
	 * @return: A integer indicate the sum of minimum subarray
	 */
	public int minSubArray(ArrayList<Integer> nums) {
		if (nums == null || nums.size() == 0) {
			return Integer.MAX_VALUE;
		}

		int sum = 0;
		int min = Integer.MAX_VALUE;

		for (int i = 0; i < nums.size(); i++) {
			sum += nums.get(i);
			min = Math.min(sum, min);
			if (sum > 0) {
				sum = 0;
			}
		}

		return min;
	}
}
