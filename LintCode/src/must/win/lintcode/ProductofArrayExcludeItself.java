package must.win.lintcode;

import java.util.ArrayList;

// Product of Array Exclude Itself
/*
 *  https://github.com/jnozsc/lintcode/blob/master/Product_of_Array_Exclude_Itself.cpp
 */
public class ProductofArrayExcludeItself {
	/**
	 * @param A
	 *            : Given an integers array A
	 * @return: A Long array B and B[i]= A[0] * ... * A[i-1] * A[i+1] * ... *
	 *          A[n-1]
	 */
	public ArrayList<Long> productExcludeItself(ArrayList<Integer> A) {
		ArrayList<Long> result = new ArrayList<Long>();
		if (A == null || A.size() <= 1) {
			return result;
		}
		int size = A.size();
		long[] leftProduct = new long[size];
		long[] rightProduct = new long[size];

		leftProduct[0] = 1;
		for (int i = 1; i < size; i++) {
			leftProduct[i] = leftProduct[i - 1] * A.get(i - 1);
		}

		rightProduct[size - 1] = 1;
		for (int i = size - 2; i >= 0; i--) {
			rightProduct[i] = rightProduct[i + 1] * A.get(i + 1);
		}

		for (int i = 0; i < size; i++) {
			result.add(leftProduct[i] * rightProduct[i]);
		}

		return result;
	}
}
