package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// Minimum Adjustment Cost
/*
 * http://www.cnblogs.com/yuzhangcmu/p/4153927.html
 * recursive, recursive + cache, DP(O(N*A*T))
 * 
 * DP: c(i, v) = min(c(i - 1, v') + |v - A[i]|, |v - v'| <= target)
 * 
 */
public class MinimumAdjustmentCost {
	/**
	 * @param A
	 *            : An integer array.
	 * @param target
	 *            : An integer.
	 */

	// DP
	/**
	 * @param A
	 *            : An integer array.
	 * @param target
	 *            : An integer.
	 */
	public int MinAdjustmentCost(ArrayList<Integer> A, int target) {
		if (A == null || A.size() < 2 || target < 0) {
			return 0;
		}

		int size = A.size();
		int[][] costs = new int[2][100];
		int preRow = 0;
		int curRow = 1;

		// initialize one element cost
		for (int i = 0; i < 100; i++) {
			costs[0][i] = Math.abs((i + 1) - A.get(0));
		}

		for (int i = 1; i < size; i++) {
			for (int j = 0; j < 100; j++) {
				int min = Integer.MAX_VALUE;
				int diff = Math.abs((j + 1) - A.get(i));
				for (int k = j - target; k <= j + target; k++) {
					if (k >= 0 && k < 100) {
						min = Math.min(costs[preRow][k], min);
					}
				}
				costs[curRow][j] = min + diff;
			}
			preRow = curRow;
			curRow = curRow ^ 1;
		}

		// find the minimum cost from various values of end element
		int minCost = Integer.MAX_VALUE;
		for (int val : costs[preRow]) {
			minCost = Math.min(val, minCost);
		}
		return minCost;
	}

	// recursive + cache
	public int MinAdjustmentCost1(ArrayList<Integer> A, int target) {
		if (A == null || A.size() < 2 || target < 0) {
			return 0;
		}

		int[][] costs = new int[A.size()][100];
		for (int i = 0; i < A.size(); i++) {
			for (int j = 0; j < 100; j++) {
				costs[i][j] = Integer.MAX_VALUE;
			}
		}

		return recurHelper(A, target, 0, 0, costs);
	}

	// recursive + cache helper
	private int recurHelper(List<Integer> A, int target, int index, int preVal,
			int[][] costs) {
		int size = A.size();

		// out of list's boundary
		if (index >= size) {
			return 0;
		}

		int min = Integer.MAX_VALUE;
		for (int j = 1; j <= 100; j++) {
			if (index != 0 && Math.abs(j - preVal) > target) {
				continue;
			}
			if (costs[index][j - 1] == Integer.MAX_VALUE) {
				costs[index][j - 1] = Math.abs(A.get(index) - j)
						+ recurHelper(A, target, index + 1, j, costs);
			}
			min = Math.min(costs[index][j - 1], min);
		}
		return min;
	}
}
