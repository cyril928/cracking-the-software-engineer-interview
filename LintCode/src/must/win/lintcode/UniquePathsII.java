package must.win.lintcode;

// Unique Paths II (LeetCode)
/*
 * 
 */
public class UniquePathsII {
	/**
	 * @param obstacleGrid
	 *            : A list of lists of integers
	 * @return: An integer
	 */
	public int uniquePathsWithObstacles(int[][] obstacleGrid) {
		if (obstacleGrid == null || obstacleGrid.length == 0) {
			return 0;
		}
		int rows = obstacleGrid.length;
		int cols = obstacleGrid[0].length;

		int[] paths = new int[cols];

		paths[0] = 1;
		for (int i = 0; i < rows; i++) {
			int total = 0;
			if (obstacleGrid[i][0] == 1) {
				paths[0] = 0;
			}
			total += paths[0];
			for (int j = 1; j < cols; j++) {
				if (obstacleGrid[i][j] == 0) {
					paths[j] += paths[j - 1];
				} else {
					paths[j] = 0;
				}
				total += paths[j];
			}
			if (total == 0) {
				return 0;
			}
		}
		return paths[cols - 1];
	}
}
