package must.win.lintcode;

import java.util.Stack;

// Min Stack (LeetCode)
/*
 * push, pop and min operation all in O(1) cost.
 */
public class MinStack {
	private Stack<Integer> stack1;
	private Stack<Integer> stack2;

	public MinStack() {
		stack1 = new Stack<Integer>();
		stack2 = new Stack<Integer>();
	}

	public int min() {
		return stack2.peek();
	}

	public int pop() {
		int val = stack1.pop();
		if (val == stack2.peek()) {
			stack2.pop();
		}
		return val;
	}

	public void push(int number) {
		stack1.push(number);
		if (stack2.empty() || number <= stack2.peek()) {
			stack2.push(number);
		}
	}
}
