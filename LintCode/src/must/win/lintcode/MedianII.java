package must.win.lintcode;

import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;

// Median II
/*
 * http://codeanytime.blogspot.tw/2014/12/median-ii.html
 * O(nlogn) time, use max heap & min heap
 * implementation
 * 
 * http://stackoverflow.com/questions/10078637/reverse-natural-order-using-collections-reverseorder
 * Reverse natural order of PriorityQueue in Java
 */
public class MedianII {
	/**
	 * @param nums
	 *            : A list of integers.
	 * @return: the median of numbers
	 */
	public int[] medianII(int[] nums) {
		if (nums == null || nums.length == 0) {
			return null;
		}

		int[] medians = new int[nums.length];

		Queue<Integer> maxHeap = new PriorityQueue<Integer>(nums.length,
				Collections.reverseOrder());
		Queue<Integer> minHeap = new PriorityQueue<Integer>(nums.length);

		for (int i = 0; i < nums.length; i++) {
			minHeap.add(nums[i]);
			if ((i & 1) == 0) { // add odd element
				int minVal = minHeap.poll();
				maxHeap.add(minVal);
			} else { // add even element
				if (maxHeap.peek() > minHeap.peek()) {
					int maxVal = maxHeap.poll();
					int minVal = minHeap.poll();
					maxHeap.add(minVal);
					minHeap.add(maxVal);
				}
			}
			medians[i] = maxHeap.peek();
		}
		return medians;
	}
}
