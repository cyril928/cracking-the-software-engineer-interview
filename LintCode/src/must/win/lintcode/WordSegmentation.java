package must.win.lintcode;

import java.util.Set;

// Word Segmentation
/*
 * http://www.cnblogs.com/lishiblog/p/4183748.html
 * implementation & explanation
 * 
 * It is a DP problem. However, we need to use charAt() instead of substring() to optimize speed. 
 * Also, we can first check whether each char in s has appeared in dict, 
 * if not, then directly return false. (This is used to pass the last test case in LintCode).
 */
public class WordSegmentation {
	/**
	 * @param s
	 *            : A string s
	 * @param dict
	 *            : A dictionary of words dict
	 */
	public boolean wordSegmentation(String s, Set<String> dict) {
		if (s == null || dict == null) {
			return false;
		}

		int len = s.length();
		int[] charSet = new int[256];
		for (String word : dict) {
			for (int i = 0; i < word.length(); i++) {
				charSet[word.charAt(i)]++;
			}
		}

		for (int i = 0; i < len; i++) {
			if (charSet[s.charAt(i)] == 0) {
				return false;
			}
		}

		boolean[] dp = new boolean[len + 1];
		dp[0] = true;

		for (int i = 1; i <= len; i++) {
			StringBuilder sb = new StringBuilder();
			for (int j = i - 1; j >= 0; j--) {
				sb.insert(0, s.charAt(j)); // last word in string
				if (dp[j] && dict.contains(sb.toString())) {
					dp[i] = true;
					break;
				}
			}
		}
		return dp[len];
	}
}
