package must.win.lintcode;

// Climbing Stairs (LeetCode)
/*
 * 
 */
public class ClimbingStairs {
	/**
	 * @param n
	 *            : an integer
	 * @return: an integer
	 */

	// DP
	public int climbStairs(int n) {
		int[] res = { 0, 1 };
		while (n-- > 0) {
			int newRes = res[0] + res[1];
			res[0] = res[1];
			res[1] = newRes;
		}
		return res[1];
	}

	// recursive
	/**
	 * @param n
	 *            : an integer
	 * @return: an integer
	 */
	public int climbStairs1(int n) {
		if (n <= 2) {
			return n;
		}
		return climbStairs1(n - 2) + climbStairs1(n - 1);
	}
}
