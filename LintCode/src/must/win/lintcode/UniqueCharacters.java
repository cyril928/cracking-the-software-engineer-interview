package must.win.lintcode;

// Unique Characters
/*
 * What if you can not use additional data structures? (need to confirm)
 */
public class UniqueCharacters {
	/**
	 * @param str
	 *            : a string
	 * @return: a boolean
	 */
	public boolean isUnique(String str) {
		if (str == null) {
			return false;
		}

		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < str.length(); i++) {
			max = Math.max(max, str.charAt(i));
			min = Math.min(min, str.charAt(i));
		}

		int[] num = new int[max - min + 1];
		for (int i = 0; i < str.length(); i++) {
			if (num[str.charAt(i) - min] > 0) {
				return false;
			} else {
				num[str.charAt(i) - min]++;
			}
		}
		return true;
	}
}
