package must.win.lintcode;

// Convert Integer A to Integer B
/*
 * Cracking the Coding Interview
 * use n & (n - 1) to clear least significant bit
 * logical shift bit by bit
 * 
 * http://stackoverflow.com/questions/2811319/difference-between-and
 * java arithmetic shift >>, and logical shift >>> 
 */
public class ConvertIntegerAtoIntegerB {
	/**
	 * @param a
	 *            , b: Two integer return: An integer
	 */
	// use n & (n - 1) to clear least significant bit
	public static int bitSwapRequired(int a, int b) {
		int count = 0;
		for (int c = a ^ b; c != 0; c &= (c - 1)) {
			count++;
		}
		return count;
	}

	// logical shift bit by bit
	public static int bitSwapRequired1(int a, int b) {
		int count = 0;
		for (int c = a ^ b; c != 0; c >>>= 1) {
			count += (c & 1);
		}
		return count;
	}
}
