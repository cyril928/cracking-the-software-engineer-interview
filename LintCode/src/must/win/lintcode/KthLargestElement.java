package must.win.lintcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

// Kth Largest Element
/*
 * https://github.com/jnozsc/lintcode/blob/master/Kth_Largest_Element.cpp
 * implementation
 * O(N) time, O(1) space
 * O(N2) worst case time
 * Quick Select (recursive, iterative)
 * 
 * using max heap, Time: KlogN
 * 
 * using min heap, Time: NlogK
 */
public class KthLargestElement {
	// Quick Select recursive helper, Time : O(N) time
	private int getPivotIndex(List<Integer> numbers, int start, int end) {
		Random r = new Random();
		int randomVal = r.nextInt(end - start + 1);
		swap(numbers, start, start + randomVal);
		int pivot = numbers.get(start);

		int i = start + 1, j = end;
		while (i <= j) {
			if (numbers.get(i) > pivot) {
				i++;
			} else {
				swap(numbers, i, j);
				j--;
			}
		}
		swap(numbers, j, start);
		return j;
	}

	// Quick Select iterative, Time : O(N) time
	public int kthLargestElement(int k, ArrayList<Integer> numbers) {
		int s = 0, e = numbers.size() - 1;
		while (true) {
			int p = partition(k, numbers, s, e);
			if (p == k - 1) {
				return numbers.get(p);
			} else if (p > k - 1) {
				e = p - 1;
			} else {
				s = p + 1;
			}
		}
	}

	// Quick Select recursive helper, Time : O(N) time
	private int kthLargestElement(int k, List<Integer> numbers, int start,
			int end) {

		int pos = getPivotIndex(numbers, start, end);
		if (k == pos + 1) {
			return numbers.get(pos);
		} else if (k > pos + 1) {
			return kthLargestElement(k, numbers, pos + 1, end);
		} else {
			return kthLargestElement(k, numbers, start, pos - 1);
		}
	}

	// param k : description of k
	// param numbers : array of numbers
	// return: description of return

	// Quick Select recursive, Time : O(N) time
	public int kthLargestElement1(int k, ArrayList<Integer> numbers) {
		return kthLargestElement(k, numbers, 0, numbers.size() - 1);
	}

	// using max heap, Time: KlogN
	public int kthLargestElement2(int k, ArrayList<Integer> numbers) {
		int size = numbers.size();
		Queue<Integer> heap = new PriorityQueue<Integer>(size,
				Collections.reverseOrder());
		for (int n : numbers) {
			heap.add(n);
		}

		int val = 0;
		while (k-- > 0) {
			val = heap.poll();
		}
		return val;
	}

	// using min heap, Time: NlogK
	public int kthLargestElement3(int k, ArrayList<Integer> numbers) {
		Queue<Integer> heap = new PriorityQueue<Integer>(k);
		int size = numbers.size();
		int i = 0;
		while (i < k) {
			heap.add(numbers.get(i++));
		}

		k = size - k;
		while (k-- > 0) {
			heap.poll();
			heap.add(numbers.get(i++));
		}
		return heap.poll();
	}

	// Quick Select iterative helper, Time : O(N) time
	private int partition(int k, ArrayList<Integer> numbers, int s, int e) {
		int p = ((e - s) >> 1) + s;
		int pivot = numbers.get(p);
		swap(numbers, e, p);
		p = e--;
		while (s <= e) {
			if (numbers.get(s) > pivot) {
				s++;
			} else {
				swap(numbers, s, e--);
			}
		}
		swap(numbers, p, s);
		return s;
	}

	private void swap(List<Integer> numbers, int a, int b) {
		int temp = numbers.get(a);
		numbers.set(a, numbers.get(b));
		numbers.set(b, temp);
	}
}
