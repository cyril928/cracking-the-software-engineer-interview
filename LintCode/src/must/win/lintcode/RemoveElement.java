package must.win.lintcode;

// Remove Element (LeetCode)
/*
 * 
 */
public class RemoveElement {

	// two pointer from start
	public int removeElement(int[] A, int elem) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int len = A.length;
		int size = 0;
		for (int i = 0; i < len; i++) {
			if (A[i] != elem) {
				swap(A, size++, i);
			}
		}
		return size;
	}

	// one pointer from start, one from end
	public int removeElement1(int[] A, int elem) {
		if (A == null || A.length == 0) {
			return 0;
		}
		int len = A.length;
		int i = 0, j = len - 1;
		while (i <= j) {
			if (A[j] != elem) {
				swap(A, i++, j);
			} else {
				j--;
			}
		}
		return i;
	}

	private void swap(int[] A, int a, int b) {
		int temp = A[a];
		A[a] = A[b];
		A[b] = temp;
	}
}
