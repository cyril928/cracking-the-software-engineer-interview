package must.win.lintcode;

// Two Lists Sum (LeetCode : Add Two Numbers)
/*
 * 
 */
public class TwoListsSum {
	class ListNode {
		int val;
		ListNode next;

		ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param l1
	 *            : the first list
	 * @param l2
	 *            : the second list
	 * @return: the sum list of l1 and l2
	 */
	public ListNode addLists(ListNode l1, ListNode l2) {
		if (l1 == null) {
			return l2;
		}
		if (l2 == null) {
			return l1;
		}

		ListNode dummy = new ListNode(0);
		ListNode cur = dummy;

		int carry = 0;
		while (l1 != null && l2 != null) {
			int sum = l1.val + l2.val + carry;
			int val = sum % 10;
			carry = sum / 10;
			cur.next = new ListNode(val);
			cur = cur.next;
			l1 = l1.next;
			l2 = l2.next;
		}

		while (l1 != null) {
			if (carry == 0) {
				cur.next = l1;
				break;
			}
			int sum = l1.val + carry;
			int val = sum % 10;
			carry = sum / 10;
			cur.next = new ListNode(val);
			cur = cur.next;
			l1 = l1.next;
		}

		while (l2 != null) {
			if (carry == 0) {
				cur.next = l2;
				break;
			}
			int sum = l2.val + carry;
			int val = sum % 10;
			carry = sum / 10;
			cur.next = new ListNode(val);
			cur = cur.next;
			l2 = l2.next;
		}

		if (carry != 0) {
			cur.next = new ListNode(carry);
		}
		return dummy.next;
	}
}
