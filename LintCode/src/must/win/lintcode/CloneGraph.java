package must.win.lintcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

// Clone Graph (LeetCode)
/*
 * DFS & BFS
 */
public class CloneGraph {
	class UndirectedGraphNode {
		int label;
		List<UndirectedGraphNode> neighbors;

		UndirectedGraphNode(int x) {
			label = x;
			neighbors = new ArrayList<UndirectedGraphNode>();
		}
	};

	// DFS helper function
	private UndirectedGraphNode cloneDFS(UndirectedGraphNode node,
			Map<UndirectedGraphNode, UndirectedGraphNode> visited) {

		UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
		visited.put(node, newNode);
		newNode.neighbors = new ArrayList<UndirectedGraphNode>();

		for (UndirectedGraphNode neighbor : node.neighbors) {
			if (visited.containsKey(neighbor)) {
				newNode.neighbors.add(visited.get(neighbor));
			} else {
				newNode.neighbors.add(cloneDFS(neighbor, visited));
			}
		}

		return newNode;
	}

	/**
	 * @param node
	 *            : A undirected graph node
	 * @return: A undirected graph node
	 */
	// DFS
	public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
		if (node == null) {
			return null;
		}
		return cloneDFS(node,
				new HashMap<UndirectedGraphNode, UndirectedGraphNode>());
	}

	// BFS
	public UndirectedGraphNode cloneGraph1(UndirectedGraphNode node) {
		if (node == null) {
			return null;
		}

		Map<UndirectedGraphNode, UndirectedGraphNode> visited = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();

		UndirectedGraphNode newNode = new UndirectedGraphNode(node.label);
		visited.put(node, newNode);

		Queue<UndirectedGraphNode> queue = new ArrayDeque<UndirectedGraphNode>();
		queue.add(node);

		while (!queue.isEmpty()) {
			UndirectedGraphNode curNode = queue.remove();
			UndirectedGraphNode curNewNode = visited.get(curNode);
			for (UndirectedGraphNode neighbor : curNode.neighbors) {
				if (visited.containsKey(neighbor)) {
					curNewNode.neighbors.add(visited.get(neighbor));
				} else {
					UndirectedGraphNode newNeighbor = new UndirectedGraphNode(
							neighbor.label);
					visited.put(neighbor, newNeighbor);
					curNewNode.neighbors.add(newNeighbor);
					queue.add(neighbor);
				}
			}
		}

		return newNode;
	}
}
