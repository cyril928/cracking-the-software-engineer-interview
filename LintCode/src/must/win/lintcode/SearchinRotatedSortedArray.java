package must.win.lintcode;

// Search in Rotated Sorted Array (LeetCode)
/*
 * 
 */
public class SearchinRotatedSortedArray {
	/**
	 * @param A
	 *            : an integer rotated sorted array
	 * @param target
	 *            : an integer to be searched return : an integer
	 */
	public int search(int[] A, int target) {
		if (A == null) {
			return -1;
		}
		int start = 0, end = A.length - 1;

		while (start <= end) {
			int mid = (end - start) / 2 + start;
			if (A[mid] == target) {
				return mid;
			} else if (A[mid] < target) {
				if (A[mid] < A[end] && A[end] < target) {
					end = mid - 1;
				} else {
					start = mid + 1;
				}
			} else {
				if (A[mid] > A[end] && A[end] >= target) {
					start = mid + 1;
				} else {
					end = mid - 1;
				}
			}
		}

		return -1;
	}
}
