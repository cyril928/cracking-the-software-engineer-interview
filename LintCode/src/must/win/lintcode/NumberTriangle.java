package must.win.lintcode;

import java.util.ArrayList;

// Number Triangle (LeetCode - Triangle )
/*
 * Bonus point if you are able to do this using only O(n) extra space, 
 * where n is the total number of rows in the triangle.
 */
public class NumberTriangle {
	/**
	 * @param triangle
	 *            : a list of lists of integers.
	 * @return: An integer, minimum path sum.
	 */
	public int minimumTotal(ArrayList<ArrayList<Integer>> triangle) {
		if (triangle == null || triangle.size() == 0) {
			return 0;
		}

		int size = triangle.size();
		int[] dp = new int[size];
		for (int i = 0; i < size; i++) {
			dp[i] = triangle.get(size - 1).get(i);
		}

		for (int i = size - 2; i >= 0; i--) {
			ArrayList<Integer> list = triangle.get(i);
			for (int j = 0; j <= i; j++) {
				dp[j] = list.get(j) + Math.min(dp[j], dp[j + 1]);
			}
		}

		return dp[0];
	}
}
