package must.win.lintcode;

import java.util.ArrayList;
import java.util.Arrays;

// 3 Sum (LeetCode)
public class ThreeSum {
	/**
	 * @param numbers
	 *            : Give an array numbers of n integer
	 * @return : Find all unique triplets in the array which gives the sum of
	 *         zero.
	 */
	public ArrayList<ArrayList<Integer>> threeSum(int[] numbers) {
		ArrayList<ArrayList<Integer>> results = new ArrayList<ArrayList<Integer>>();
		if (numbers == null || numbers.length < 3) {
			return results;
		}

		Arrays.sort(numbers);

		int len = numbers.length;
		for (int i = 0; i < len - 2 && numbers[i] <= 0; i++) {
			// to skip duplicate numbers;
			if (i > 0 && numbers[i] == numbers[i - 1]) {
				continue;
			}
			int start = i + 1, end = len - 1;
			while (start < end) {
				int sum = numbers[i] + numbers[start] + numbers[end];
				if (sum > 0) {
					end--;
				} else if (sum < 0) {
					start++;
				} else {
					ArrayList<Integer> result = new ArrayList<Integer>();
					result.add(numbers[i]);
					result.add(numbers[start]);
					result.add(numbers[end]);
					results.add(result);
					do {
						end--;
					} while (end > start && numbers[end] == numbers[end + 1]);
					do {
						start++;
					} while (end > start
							&& numbers[start] == numbers[start - 1]);
				}
			}
		}

		return results;
	}
}
