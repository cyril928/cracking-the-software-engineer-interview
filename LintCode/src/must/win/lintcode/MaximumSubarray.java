package must.win.lintcode;

import java.util.ArrayList;

// Maximum SubArray (LeetCode)
/*
 * time complexity O(n)
 */
public class MaximumSubarray {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return: A integer indicate the sum of max subarray
	 */
	public int maxSubArray(ArrayList<Integer> nums) {
		if (nums == null || nums.size() == 0) {
			return Integer.MIN_VALUE;
		}

		int sum = 0;
		int max = Integer.MIN_VALUE;

		for (int num : nums) {
			sum += num;
			max = Math.max(sum, max);
			if (sum < 0) {
				sum = 0;
			}
		}

		return max;
	}
}
