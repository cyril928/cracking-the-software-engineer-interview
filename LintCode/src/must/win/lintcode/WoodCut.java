package must.win.lintcode;

// Wood Cut
/*
 * My way
 * O(n log Len), where Len is the longest length of the wood.
 */
public class WoodCut {
	private int getCutNum(int[] L, int length) {
		int cut = 0;
		for (int l : L) {
			cut += (l / length);
		}
		return cut;
	}

	/**
	 * @param L
	 *            : Given n pieces of wood with length L[i]
	 * @param k
	 *            : An integer return: The maximum length of the small pieces.
	 */

	// O(n log Len)
	public int woodCut(int[] L, int k) {
		if (L == null || L.length == 0 || k <= 0) {
			return 0;
		}

		int maxPieceLength = 0;
		for (int l : L) {
			maxPieceLength = Math.max(l, maxPieceLength);
		}
		int start = 1, end = maxPieceLength;

		int targetLen = 0;
		while (start <= end) {
			targetLen = ((end - start) >> 1) + start;
			int cut = getCutNum(L, targetLen);
			if (cut >= k) {
				start = targetLen + 1;
			} else {
				end = targetLen - 1;
			}
		}

		return end;
	}
}
