package must.win.lintcode;

// Nth to Last Node in List
/*
 * 
 */
public class NthtoLastNodeinList {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param head
	 *            : The first node of linked list.
	 * @param n
	 *            : An integer.
	 * @return: Nth to last node of a singly linked list.
	 */
	ListNode nthToLast(ListNode head, int n) {
		if (head == null) {
			return null;
		}
		ListNode fast = head;
		ListNode slow = head;
		while (n-- > 0) {
			fast = fast.next;
		}

		while (fast != null & slow != null) {
			fast = fast.next;
			slow = slow.next;
		}

		return slow;
	}
}
