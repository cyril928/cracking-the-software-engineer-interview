package must.win.lintcode;

import java.util.ArrayList;

// Majority Number (LeetCode : Majority Element)
/*
 * O(n) time and O(1) space
 */
public class MajorityNumber {
	/**
	 * @param nums
	 *            : a list of integers
	 * @return: find a majority number
	 */
	public int majorityNumber(ArrayList<Integer> nums) {
		if (nums.size() == 1) {
			return nums.get(0);
		}

		int count = 0;
		int candidate = 0;

		for (int num : nums) {
			if (count == 0) {
				candidate = num;
				count = 1;
			} else {
				count += (candidate == num) ? 1 : -1;
			}
		}

		return candidate;
	}
}
