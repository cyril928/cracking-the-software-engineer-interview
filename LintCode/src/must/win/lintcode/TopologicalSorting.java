package must.win.lintcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

// Topological Sorting (only directed acyclic graph(DAG) can come out topological sort)
/*
 * Any better coding style, or efficient way to find start nodes?
 * 
 * DFS (Stack) & BFS (Queue) with HashMap to help us add node which has 
 * incoming count == 0 currently
 * 
 * DFS use the depth to know which position in the order list should I put the node 
 * 
 * http://homepage.cs.uiowa.edu/~hzhang/c19/ch11d.pdf
 * DFS & BFS with HashMap explanation
 * 
 * http://www.quora.com/Can-topological-sorting-be-done-using-BFS
 * psuedocode snippet
 * 
 * https://www.cs.usfca.edu/~galles/visualization/TopoSortDFS.html
 * DFS, graph simulation
 */
public class TopologicalSorting {

	class DirectedGraphNode {
		int val;
		ArrayList<DirectedGraphNode> neighbors;

		DirectedGraphNode(int x) {
			this.val = x;
			neighbors = new ArrayList<DirectedGraphNode>();
		}
	}

	/**
	 * for Stack DFS, Queue BFS approach construct map of DirectedGraphNode and
	 * its incoming link count
	 */
	private Map<DirectedGraphNode, Integer> constructMap(
			ArrayList<DirectedGraphNode> graph) {
		Map<DirectedGraphNode, Integer> map = new HashMap<DirectedGraphNode, Integer>();

		for (DirectedGraphNode node : graph) {
			if (!map.containsKey(node)) {
				map.put(node, 0);
			}
			for (DirectedGraphNode neighbor : node.neighbors) {
				if (!map.containsKey(neighbor)) {
					map.put(neighbor, 0);
				}
				map.put(neighbor, map.get(neighbor) + 1);
			}
		}
		return map;
	}

	/**
	 * helper function of using the depth to know which position in the order
	 * list should I put the node. find out all nodes without incoming link
	 */
	private List<DirectedGraphNode> getStartNodeList(
			ArrayList<DirectedGraphNode> graph) {

		List<DirectedGraphNode> result = new ArrayList<DirectedGraphNode>();
		Set<DirectedGraphNode> set = new HashSet<DirectedGraphNode>();
		for (DirectedGraphNode node : graph) {
			for (DirectedGraphNode neighbor : node.neighbors) {
				set.add(neighbor);
			}
		}

		for (DirectedGraphNode node : graph) {
			if (!set.contains(node)) {
				result.add(node);
			}
		}
		return result;
	}

	// use queue to do BFS traversal
	public void queueBFS(List<DirectedGraphNode> list,
			Map<DirectedGraphNode, Integer> map) {
		Queue<DirectedGraphNode> queue = new ArrayDeque<DirectedGraphNode>();
		Iterator<Map.Entry<DirectedGraphNode, Integer>> iter = map.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Map.Entry<DirectedGraphNode, Integer> e = iter.next();
			if (e.getValue() == 0) {
				queue.add(e.getKey());
			}
		}

		while (!queue.isEmpty()) {
			DirectedGraphNode curNode = queue.remove();
			list.add(curNode);
			for (DirectedGraphNode neighbor : curNode.neighbors) {
				map.put(neighbor, map.get(neighbor) - 1);
				if (map.get(neighbor) == 0) {
					queue.add(neighbor);
				}
			}
		}
	}

	// use stack to do DFS traversal
	public void stackDFS(List<DirectedGraphNode> list,
			Map<DirectedGraphNode, Integer> map) {
		Stack<DirectedGraphNode> stack = new Stack<DirectedGraphNode>();
		// push all the nodes without any incoming links into stack
		Iterator<Map.Entry<DirectedGraphNode, Integer>> iter = map.entrySet()
				.iterator();
		while (iter.hasNext()) {
			Map.Entry<DirectedGraphNode, Integer> e = iter.next();
			if (e.getValue() == 0) {
				stack.push(e.getKey());
			}
		}

		while (!stack.isEmpty()) {
			DirectedGraphNode curNode = stack.pop();
			list.add(curNode);
			for (DirectedGraphNode neighbor : curNode.neighbors) {
				map.put(neighbor, map.get(neighbor) - 1);
				if (map.get(neighbor) == 0) {
					stack.push(neighbor);
				}
			}
		}
	}

	/**
	 * @param graph
	 *            : A list of Directed graph node
	 * @return: Any topological order for the given graph.
	 * 
	 *          for stack DFS or queue BFS
	 */
	public ArrayList<DirectedGraphNode> topSort(
			ArrayList<DirectedGraphNode> graph) {
		if (graph == null || graph.size() < 2) {
			return graph;
		}

		Map<DirectedGraphNode, Integer> map = constructMap(graph);
		ArrayList<DirectedGraphNode> orderList = new ArrayList<DirectedGraphNode>();
		stackDFS(orderList, map);
		// queueBFS(orderList, map);
		return orderList;
	}

	/**
	 * 
	 * DFS use the depth to know which position in the order list should I put
	 * the node
	 * 
	 * @param graph
	 *            : A list of Directed graph node
	 * @return: Any topological order for the given graph.
	 */
	public ArrayList<DirectedGraphNode> topSort1(
			ArrayList<DirectedGraphNode> graph) {
		if (graph == null || graph.size() < 2) {
			return graph;
		}

		List<DirectedGraphNode> startNodeList = getStartNodeList(graph);

		ArrayList<DirectedGraphNode> orderList = new ArrayList<DirectedGraphNode>();

		Set<DirectedGraphNode> visitedSet = new HashSet<DirectedGraphNode>();
		// start from each start node
		for (DirectedGraphNode startNode : startNodeList) {
			traversalDFS(startNode, visitedSet, orderList, 0);
		}
		return orderList;
	}

	/**
	 * DFS use the depth to know which position in the order list should I put
	 * the node
	 */
	public void traversalDFS(DirectedGraphNode node,
			Set<DirectedGraphNode> visitedSet, List<DirectedGraphNode> list,
			int depth) {
		// how to find position in list for putting the node, key step!
		list.add(depth, node);
		visitedSet.add(node);
		for (DirectedGraphNode neighbor : node.neighbors) {
			if (!visitedSet.contains(neighbor)) {
				traversalDFS(neighbor, visitedSet, list, depth + 1);
			}
		}
	}
}
