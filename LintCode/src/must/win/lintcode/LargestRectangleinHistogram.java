package must.win.lintcode;

import java.util.Stack;

// Largest Rectangle in Histogram (LeetCode)
/*
 * http://www.ninechapter.com/solutions/largest-rectangle-in-histogram/
 * most clean 1 stack solution
 */
public class LargestRectangleinHistogram {
	/**
	 * @param height
	 *            : A list of integer
	 * @return: The area of largest rectangle in the histogram
	 */
	public int largestRectangleArea(int[] height) {
		if (height == null || height.length == 0) {
			return 0;
		}

		int len = height.length;

		Stack<Integer> stack = new Stack<Integer>();
		int maxArea = 0;

		for (int i = 0; i <= len; i++) {
			int heightVal = (i == len) ? -1 : height[i];
			while (!stack.empty() && heightVal < height[stack.peek()]) {
				int h = height[stack.pop()];
				int w = stack.empty() ? i : i - stack.peek() - 1;
				maxArea = Math.max(h * w, maxArea);
			}
			stack.push(i);
		}

		return maxArea;
	}
}
