package must.win.lintcode;

import java.util.ArrayList;
import java.util.List;

// Partition Array
/*
 * partition the array in-place and in O(n)
 */
public class PartitionArray {
	/**
	 * @param nums
	 *            : The integer array you should partition
	 * @param k
	 *            : As description return: The index after partition
	 */
	public int partitionArray(ArrayList<Integer> nums, int k) {
		if (nums == null || nums.size() == 0) {
			return 0;
		}

		int lp = 0, rp = nums.size() - 1;
		while (lp <= rp) {
			if (nums.get(lp) < k) {
				lp++;
			} else {
				swap(nums, lp, rp--);
			}
		}

		return lp;
	}

	private void swap(List<Integer> nums, int a, int b) {
		int num = nums.get(a);
		nums.set(a, nums.get(b));
		nums.set(b, num);
	}
}
