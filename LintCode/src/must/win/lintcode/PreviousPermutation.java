package must.win.lintcode;

import java.util.ArrayList;
import java.util.Collections;

// Previous Permutation
/*
 * 
 */
public class PreviousPermutation {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return: A list of integers that's previous permutation
	 */
	public ArrayList<Integer> previousPermuation(ArrayList<Integer> nums) {
		if (nums == null || nums.size() == 0) {
			return nums;
		}

		// find the end index of descending order tail
		int i = nums.size() - 1;
		while (i > 0 && nums.get(i - 1) <= nums.get(i)) {
			i--;
		}

		// reverse tail from descending order to ascending order
		reverse(nums, i, nums.size() - 1);

		// swap the target two element
		if (i > 0) {
			int next = i--;
			while (next < nums.size() && nums.get(next) >= nums.get(i)) {
				next++;
			}
			Collections.swap(nums, i, next);
		}
		return nums;
	}

	private void reverse(ArrayList<Integer> nums, int start, int end) {
		while (start < end) {
			Collections.swap(nums, start++, end--);
		}
	}
}
