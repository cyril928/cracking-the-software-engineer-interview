package must.win.lintcode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

// Binary Tree Level Order Traversal (LeetCode)
/*
 * 
 */
public class BinaryTreeLevelOrderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: Level order a list of lists of integer
	 */
	public ArrayList<ArrayList<Integer>> levelOrder(TreeNode root) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();

		if (root == null) {
			return result;
		}

		Queue<TreeNode> queue = new ArrayDeque<TreeNode>();
		queue.add(root);

		while (!queue.isEmpty()) {
			ArrayList<Integer> level = new ArrayList<Integer>();
			int size = queue.size();
			while (size-- > 0) {
				TreeNode cur = queue.remove();
				level.add(cur.val);
				if (cur.left != null) {
					queue.add(cur.left);
				}
				if (cur.right != null) {
					queue.add(cur.right);
				}
			}
			result.add(level);
		}
		return result;
	}
}
