package must.win.lintcode;

// Two Strings Are Anagrams
/*
 * 
 */
public class TwoStringsAreAnagrams {
	/**
	 * @param s
	 *            : The first string
	 * @param b
	 *            : The second string
	 * @return true or false
	 */

	private final int LETTERS_LEN = 256;

	public boolean anagram(String s, String t) {
		if (s == null || t == null) {
			return false;
		}
		if (s.length() != t.length()) {
			return false;
		}

		int[] nums = new int[LETTERS_LEN];
		int len = s.length();
		for (int i = 0; i < len; i++) {
			nums[s.charAt(i)]++;
		}
		for (int i = 0; i < len; i++) {
			if (nums[t.charAt(i)] == 0) {
				return false;
			}
			nums[t.charAt(i)]--;
		}

		return true;
	}
}
