package must.win.lintcode;

import java.util.HashMap;
import java.util.Map;

// Minimum Window Substring (LeetCode)
/*
 * 
 */
public class MinimumWindowSubstring {
	/**
	 * @param source
	 *            : A string
	 * @param target
	 *            : A string
	 * @return: A string denote the minimum window Return "" if there is no such
	 *          a string
	 */
	public String minWindow(String source, String target) {
		if (source == null || source.length() == 0) {
			return source;
		}
		if (target == null || target.length() == 0) {
			return "";
		}

		int sLen = source.length();
		int tLen = target.length();
		Map<Character, Integer> needFind = new HashMap<Character, Integer>();
		Map<Character, Integer> hasFind = new HashMap<Character, Integer>();

		for (int i = 0; i < tLen; i++) {
			char c = target.charAt(i);
			hasFind.put(c, 0);
			if (needFind.containsKey(c)) {
				needFind.put(c, needFind.get(c) + 1);
			} else {
				needFind.put(c, 1);
			}
		}

		String minWindow = null;
		int found = 0, leftBound = 0;
		for (int i = 0; i < sLen; i++) {
			char c = source.charAt(i);
			if (!needFind.containsKey(c)) {
				continue;
			}
			hasFind.put(c, hasFind.get(c) + 1);
			if (hasFind.get(c) <= needFind.get(c)) {
				found++;
			}

			if (found == tLen) {
				while (leftBound <= i) {
					char ch = source.charAt(leftBound);
					if (!needFind.containsKey(ch)) {
						leftBound++;
						continue;
					}
					if (hasFind.get(ch) > needFind.get(ch)) {
						hasFind.put(ch, hasFind.get(ch) - 1);
						leftBound++;
						continue;
					}
					break;
				}
				if (minWindow == null
						|| minWindow.length() > (i - leftBound) + 1) {
					minWindow = source.substring(leftBound, i + 1);
				}
			}
		}

		return minWindow == null ? "" : minWindow;
	}
}
