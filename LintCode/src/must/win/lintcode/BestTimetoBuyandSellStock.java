package must.win.lintcode;

// Best Time to Buy and Sell Stock (LeetCode)
/*
 * 
 */
public class BestTimetoBuyandSellStock {
	/**
	 * @param prices
	 *            : Given an integer array
	 * @return: Maximum profit
	 */
	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}
		int minPrice = Integer.MAX_VALUE;
		int maxProfit = 0;
		for (int price : prices) {
			maxProfit = Math.max(price - minPrice, maxProfit);
			minPrice = Math.min(price, minPrice);
		}
		return maxProfit;
	}
}
