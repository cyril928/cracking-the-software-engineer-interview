package must.win.lintcode;

// Rehashing
/*
 * could I do it without new memory space for each node? (to-do)
 */
public class Rehashing {
	class ListNode {
		int val;
		ListNode next;

		ListNode(int val) {
			this.val = val;
		}
	}

	private void insert(ListNode[] table, int i, ListNode node) {
		if (table[i] == null) {
			table[i] = node;
		} else {
			ListNode cur = table[i];
			while (cur.next != null) {
				cur = cur.next;
			}
			cur.next = node;
		}
	}

	/**
	 * @param hashTable
	 *            : A list of The first node of linked list
	 * @return: A list of The first node of linked list which have twice size
	 */
	public ListNode[] rehashing(ListNode[] hashTable) {
		int size = hashTable.length * 2;
		ListNode[] newTable = new ListNode[size];
		for (ListNode node : hashTable) {
			while (node != null) {
				int i = ((node.val % size) + size) % size;
				ListNode next = node.next;
				node.next = null;
				insert(newTable, i, node);
				node = next;
			}
		}
		return newTable;
	}
}
