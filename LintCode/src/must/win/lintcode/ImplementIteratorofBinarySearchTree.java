package must.win.lintcode;

import java.util.Stack;

// Implement Iterator of Binary Search Tree
/*
 * Elements are visited in ascending order (i.e. an inorder traversal)
 * next() and hasNext() queries run in O(1) time in average.
 * 
 * Extra memory usage O(h), h is the height of the tree.
 * 
 * Super Star: Extra memory usage O(1)  -> to-do
 */
public class ImplementIteratorofBinarySearchTree {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	// Extra memory usage O(h), h is the height of the tree.
	private Stack<TreeNode> stack;

	public ImplementIteratorofBinarySearchTree(TreeNode root) {
		stack = new Stack<TreeNode>();
		while (root != null) {
			stack.push(root);
			root = root.left;
		}
	}

	// @return: True if there has next node, or false
	public boolean hasNext() {
		return !stack.isEmpty();
	}

	// @return: return next node
	public TreeNode next() {
		if (stack.empty()) {
			return null;
		} else {
			TreeNode cur = stack.pop();
			TreeNode node = cur.right;
			while (node != null) {
				stack.push(node);
				node = node.left;
			}
			return cur;
		}
	}
}
