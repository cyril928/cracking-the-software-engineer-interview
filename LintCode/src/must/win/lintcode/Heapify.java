package must.win.lintcode;

// Heapify
/*
 * O(n) time complexity
 * http://www.cs.utsa.edu/~wagner/CS3343/r6/r6ans.html
 * Max-Heapify code
 * 
 * http://www.cs.rochester.edu/~gildea/csc282/slides/C06-heap.pdf
 * explanation, time complexity analysis
 * 
 * 
 * What is heap?
 * Heap is a data structure, which usually have three methods: push, pop and top. 
 * where "push" add a new element the heap, "pop" delete the minimum/maximum element in the heap, 
 * "top" return the minimum/maximum element.
 * 
 * What is heapify?
 * Convert an unordered integer array into a heap array. If it is min-heap, 
 * for each element A[i], we will get A[i * 2 + 1] >= A[i] and A[i * 2 + 2] >= A[i].
 * 
 */
public class Heapify {
	/**
	 * @param A
	 *            : Given an integer array
	 * @return: void
	 */
	public void heapify(int[] A) {
		if (A == null || A.length == 0) {
			return;
		}

		int len = A.length;

		/**
		 * construct heap subtrees and then whole tree. Why does the loop start
		 * from n/2? It's because the nodes beyond n/2 are leaves and thus are
		 * without children. (Min-Heapify(A, n, i) does nothing if i > n/2)
		 */
		for (int i = len / 2; i >= 0; i--) {
			minHeapify(A, len, i);
		}
	}

	private int leftIndex(int i) {
		return 2 * i + 1;
	}

	/**
	 * iterative. min heapify for one node going down to a leave (for subtree)
	 */
	private void minHeapify(int[] A, int len, int i) {
		while (true) {
			int l = leftIndex(i), r = rightIndex(i);
			int smallest = i;
			if (l < len && A[l] < A[smallest]) {
				smallest = l;
			}
			if (r < len && A[r] < A[smallest]) {
				smallest = r;
			}
			if (i == smallest) {
				break;
			}
			swap(A, i, smallest);
			i = smallest;
		}
	}

	/**
	 * recursive. min heapify for one node going down to a leave (for subtree)
	 */
	public void minHeapify1(int[] A, int len, int i) {
		int l = leftIndex(i), r = rightIndex(i);
		int smallest = i;
		if (l < len && A[l] < A[smallest]) {
			smallest = l;
		}
		if (r < len && A[r] < A[smallest]) {
			smallest = r;
		}
		if (smallest != i) {
			swap(A, smallest, i);
			minHeapify(A, len, smallest);
		}
	}

	private int rightIndex(int i) {
		return 2 * i + 2;
	}

	private void swap(int[] A, int a, int b) {
		int temp = A[a];
		A[a] = A[b];
		A[b] = temp;
	}
}
