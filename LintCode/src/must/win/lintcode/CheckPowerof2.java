package must.win.lintcode;

// O(1) Check Power of 2
/*
 * O(1) time
 */
public class CheckPowerof2 {
	/*
	 * @param n: An integer
	 * 
	 * @return: True or false
	 */
	public boolean checkPowerOf2(int n) {
		return ((n & (n - 1)) == 0) && (n > 0);
	}
}
