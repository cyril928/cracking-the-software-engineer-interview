package must.win.lintcode;

import java.util.ArrayList;
import java.util.Collections;

// Subsets (LeetCode)
/*
 * 
 */
public class Subsets {
	/**
	 * @param S
	 *            : A set of numbers.
	 * @return: A list of lists. All valid subsets.
	 */
	public ArrayList<ArrayList<Integer>> subsets(ArrayList<Integer> S) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (S == null || S.size() == 0) {
			return result;
		}
		Collections.sort(S);
		subsetsHelper(result, new ArrayList<Integer>(), S, 0);
		return result;
	}

	private void subsetsHelper(ArrayList<ArrayList<Integer>> result,
			ArrayList<Integer> path, ArrayList<Integer> S, int pos) {
		result.add(new ArrayList<Integer>(path));
		for (int i = pos; i < S.size(); i++) {
			path.add(S.get(i));
			subsetsHelper(result, path, S, i + 1);
			path.remove(path.size() - 1);
		}
	}
}
