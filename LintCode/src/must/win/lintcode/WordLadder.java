package must.win.lintcode;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Set;

// Word Ladder (LeetCode)
/*
 * 
 */
public class WordLadder {
	/**
	 * @param start
	 *            , a string
	 * @param end
	 *            , a string
	 * @param dict
	 *            , a set of string
	 * @return an integer
	 */
	public int ladderLength(String start, String end, Set<String> dict) {
		if (start == null || end == null || dict == null || dict.size() == 0) {
			return 0;
		}
		int len = start.length();
		if (len == 0 || len != end.length() || start.equals(end)) {
			return 0;
		}

		Queue<String> queue = new ArrayDeque<String>();
		queue.add(start);
		int distance = 0;
		while (!queue.isEmpty()) {
			int size = queue.size();
			distance++;
			while (size-- > 0) {
				String curString = queue.remove();
				for (int i = 0; i < len; i++) {
					char[] curCharArray = curString.toCharArray();
					for (char c = 'a'; c <= 'z'; c++) {
						if (c == curCharArray[i]) {
							continue;
						}
						curCharArray[i] = c;
						String nextString = new String(curCharArray);
						if (nextString.equals(end)) {
							return distance + 1;
						} else if (dict.contains(nextString)) {
							dict.remove(nextString);
							queue.add(nextString);
						}
					}
				}
			}
		}

		return distance;
	}
}
