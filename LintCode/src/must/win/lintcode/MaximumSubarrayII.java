package must.win.lintcode;

import java.util.ArrayList;

// Maximum Subarray II
/*
 *  http://okckd.github.io/blog/2014/06/28/Maximum-subarray-II/
 *  time complexity O(N)
 */
public class MaximumSubarrayII {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return: An integer denotes the sum of max two non-overlapping subarrays
	 */
	public int maxTwoSubArrays(ArrayList<Integer> nums) {
		if (nums == null || nums.size() == 0) {
			return 0;
		}

		int size = nums.size();
		int max = Integer.MIN_VALUE;
		int[] maxTo = new int[size];
		int[] maxFrom = new int[size];
		int sum = 0;
		for (int i = 0; i < size; i++) {
			sum += nums.get(i);
			max = Math.max(sum, max);
			maxTo[i] = max;
			if (sum < 0) {
				sum = 0;
			}
		}

		max = Integer.MIN_VALUE;
		sum = 0;
		for (int i = size - 1; i >= 0; i--) {
			sum += nums.get(i);
			max = Math.max(sum, max);
			maxFrom[i] = max;
			if (sum < 0) {
				sum = 0;
			}
		}

		int maxSum = Integer.MIN_VALUE;
		for (int i = 1; i < size; i++) {
			maxSum = Math.max(maxTo[i - 1] + maxFrom[i], maxSum);
		}
		return maxSum;
	}
}
