package must.win.lintcode;

import java.util.Stack;

// Implement Queue by Stacks
/*
 * One stack
 * 
 */
class ImplementQueuebyOneStack {
	private Stack<Integer> stack1;

	public ImplementQueuebyOneStack() {
		// do initialization if necessary
		stack1 = new Stack<Integer>();
	}

	public int pop() {
		// write your code here
		int e = stack1.pop();
		if (stack1.isEmpty()) {
			return e;
		} else {
			int popE = pop();
			stack1.push(e);
			return popE;
		}
	}

	public void push(int element) {
		// write your code here
		stack1.push(element);
	}

	public int top() {
		// write your code here
		int e = stack1.pop();
		if (stack1.isEmpty()) {
			stack1.push(e);
			return e;
		} else {
			int topE = top();
			stack1.push(e);
			return topE;
		}
	}
}

// Implement Queue by Stacks
/*
 * Cracking the Coding Interview 3.5
 */
public class ImplementQueuebyStacks {

	private Stack<Integer> stack1;
	private Stack<Integer> stack2;

	public ImplementQueuebyStacks() {
		stack1 = new Stack<Integer>();
		stack2 = new Stack<Integer>();
	}

	public int pop() {
		shiftStacks();
		return stack2.pop();
	}

	public void push(int element) {
		stack1.push(element);
	}

	private void shiftStacks() {
		if (stack2.empty()) {
			while (!stack1.empty()) {
				stack2.push(stack1.pop());
			}
		}
	}

	public int top() {
		shiftStacks();
		return stack2.peek();
	}
}
