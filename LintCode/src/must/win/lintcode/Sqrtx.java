package must.win.lintcode;

// Sqrt(x) (LeetCode)
/*
 * O(log(x))
 */
public class Sqrtx {
	/**
	 * @param x
	 *            : An integer
	 * @return: The sqrt of x
	 */
	public int sqrt(int x) {
		if (x < 2) {
			return x;
		}
		int low = 1;
		int high = x / 2;
		while (low <= high) {
			int mid = (high - low) / 2 + low;
			int div = x / mid;
			if (div > mid) {
				low = mid + 1;
			} else if (div < mid) {
				high = mid - 1;
			} else {
				return mid;
			}
		}
		return high;
	}
}
