package must.win.lintcode;

// Reorder List (LeetCode)
/*
 * 
 */
public class ReorderList {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	private ListNode findMiddle(ListNode head) {
		ListNode fast = head, slow = head;
		while (fast.next != null && fast.next.next != null) {
			fast = fast.next.next;
			slow = slow.next;
		}
		return slow;
	}

	private void mergeList(ListNode firstHead, ListNode secondHead) {
		while (secondHead != null) {
			ListNode firstNext = firstHead.next;
			firstHead.next = secondHead;
			ListNode secondNext = secondHead.next;
			secondHead.next = firstNext;
			firstHead = firstNext;
			secondHead = secondNext;
		}
	}

	/**
	 * @param head
	 *            : The head of linked list.
	 * @return: void
	 */
	public void reorderList(ListNode head) {
		if (head == null || head.next == null) {
			return;
		}

		ListNode tail = findMiddle(head);
		ListNode secondHead = tail.next;
		tail.next = null;

		secondHead = reverseList(secondHead);
		mergeList(head, secondHead);
	}

	private ListNode reverseList(ListNode head) {
		ListNode prev = null;
		ListNode cur = head;

		while (cur != null) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}
		return prev;
	}
}
