package must.win.lintcode;

import java.util.ArrayList;
import java.util.Stack;

// Binary Tree Preorder Traversal (LeetCode)
/*
 * 
 */
public class BinaryTreePreorderTraversal {
	class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		public TreeNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param root
	 *            : The root of binary tree.
	 * @return: Preorder in ArrayList which contains node values.
	 */
	// add node to result when push to stack
	public ArrayList<Integer> preorderTraversal(TreeNode root) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode cur = root;
		while (cur != null) {
			stack.push(cur);
			result.add(cur.val);
			cur = cur.left;
		}

		while (!stack.isEmpty()) {
			cur = stack.pop();
			cur = cur.right;
			while (cur != null) {
				stack.push(cur);
				result.add(cur.val);
				cur = cur.left;
			}
		}

		return result;
	}

	// add node to result when pop from stack
	public ArrayList<Integer> preorderTraversal1(TreeNode root) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (root == null) {
			return result;
		}

		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);

		while (!stack.isEmpty()) {
			TreeNode cur = stack.pop();
			result.add(cur.val);
			if (cur.right != null) {
				stack.push(cur.right);
			}
			if (cur.left != null) {
				stack.push(cur.left);
			}
		}

		return result;
	}
}
