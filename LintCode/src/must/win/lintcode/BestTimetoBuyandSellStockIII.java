package must.win.lintcode;

// Best Time to Buy and Sell Stock III (LeetCode)
/*
 * 
 */
public class BestTimetoBuyandSellStockIII {
	/**
	 * @param prices
	 *            : Given an integer array
	 * @return: Maximum profit
	 */
	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}

		// DP from left to right
		int[] maxTo = new int[prices.length];
		int minPrice = prices[0];

		for (int i = 1; i < prices.length; i++) {
			maxTo[i] = Math.max(prices[i] - minPrice, maxTo[i - 1]);
			minPrice = Math.min(prices[i], minPrice);
		}

		// DP from right to left
		int[] maxFrom = new int[prices.length];
		int maxPrice = prices[prices.length - 1];

		for (int i = prices.length - 2; i >= 0; i--) {
			maxFrom[i] = Math.max(maxPrice - prices[i], maxFrom[i + 1]);
			maxPrice = Math.max(prices[i], maxPrice);
		}

		int maxProfit = 0;
		for (int i = 0; i < prices.length; i++) {
			maxProfit = Math.max(maxTo[i] + maxFrom[i], maxProfit);
		}
		return maxProfit;
	}
}
