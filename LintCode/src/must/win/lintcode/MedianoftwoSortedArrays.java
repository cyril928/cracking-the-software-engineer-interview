package must.win.lintcode;

// Median of two Sorted Arrays (LeetCode)
/*
 * O(log (m+n))
 * Time Complexity O(logn) ? need-to-confirm
 */
public class MedianoftwoSortedArrays {
	private double findKthNumber(int k, int[] A, int aStart, int[] B, int bStart) {
		if (aStart >= A.length) {
			return B[bStart + k - 1];
		}
		if (bStart >= B.length) {
			return A[aStart + k - 1];
		}

		if (k == 1) {
			return Math.min(A[aStart], B[bStart]);
		}

		int aKey = (aStart + (k >> 1) <= A.length) ? A[aStart + (k >> 1) - 1]
				: Integer.MAX_VALUE;
		int bKey = (bStart + (k >> 1) <= B.length) ? B[bStart + (k >> 1) - 1]
				: Integer.MAX_VALUE;

		if (aKey <= bKey) {
			return findKthNumber(k - (k >> 1), A, aStart + (k >> 1), B, bStart);
		} else {
			return findKthNumber(k - (k >> 1), A, aStart, B, bStart + (k >> 1));
		}

	}

	// reduce to find kth number of two sorted array,
	/**
	 * @param A
	 *            : An integer array.
	 * @param B
	 *            : An integer array.
	 * @return: a double whose format is *.5 or *.0
	 */
	public double findMedianSortedArrays(int[] A, int[] B) {
		if (A == null || B == null) {
			return Double.NaN;
		}

		int aLen = A.length;
		int bLen = B.length;

		int len = aLen + bLen;
		if (((len) & 1) == 1) {
			return findKthNumber((len >> 1) + 1, A, 0, B, 0);
		} else {
			return (findKthNumber(len >> 1, A, 0, B, 0) + findKthNumber(
					(len >> 1) + 1, A, 0, B, 0)) / 2;
		}

	}
}
