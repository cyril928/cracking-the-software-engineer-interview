package must.win.lintcode;

// Edit Distance (LeetCode)
/*
 * 
 */
public class EditDistance {
	/**
	 * @param word1
	 *            & word2: Two string.
	 * @return: The minimum number of steps.
	 */
	public int minDistance(String word1, String word2) {
		if (word1 == null || word2 == null) {
			return Integer.MAX_VALUE;
		}

		String sMin, sMax;
		if (word1.length() >= word2.length()) {
			sMax = word1;
			sMin = word2;
		} else {
			sMax = word2;
			sMin = word1;
		}

		int minLen = sMin.length();
		int maxLen = sMax.length();
		int[][] steps = new int[2][minLen + 1];

		// compare sMin's every char with empty sMax
		for (int i = 1; i <= minLen; i++) {
			steps[0][i] = i;
		}

		int preRow = 0;
		int curRow = 1;
		for (int i = 0; i < maxLen; i++) {
			steps[curRow][0] = i + 1;
			for (int j = 0; j < minLen; j++) {
				if (sMin.charAt(j) == sMax.charAt(i)) {
					steps[curRow][j + 1] = steps[preRow][j];
				} else {
					steps[curRow][j + 1] = Math.min(
							Math.min(steps[preRow][j], steps[curRow][j]),
							steps[preRow][j + 1]) + 1;
				}
			}
			preRow = curRow;
			curRow ^= 1;
		}

		return steps[preRow][minLen];
	}
}
