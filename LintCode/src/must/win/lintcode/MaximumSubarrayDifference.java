package must.win.lintcode;

import java.util.ArrayList;

// Maximum SubArray Difference
/*
 * https://github.com/jnozsc/lintcode/blob/master/Maximum_Subarray_Difference.cpp
 * O(n) time and O(n) space
 */
public class MaximumSubarrayDifference {
	/**
	 * @param nums
	 *            : A list of integers
	 * @return: An integer indicate the value of maximum difference between two
	 *          Subarrays
	 */
	// O(n) time and O(n) space
	public int maxDiffSubArrays(ArrayList<Integer> nums) {
		if (nums == null || nums.size() == 0) {
			return Integer.MIN_VALUE;
		}

		int size = nums.size();
		// max sum to this index
		int[] maxTo = new int[size];
		// min sum to this index
		int[] minTo = new int[size];
		int tmpMax = maxTo[0] = nums.get(0);
		int tmpMin = minTo[0] = nums.get(0);

		// construct maxTo, minTo
		for (int i = 1; i < size; i++) {
			if (tmpMax < 0) {
				tmpMax = 0;
			}
			tmpMax += nums.get(i);
			maxTo[i] = Math.max(tmpMax, maxTo[i - 1]);

			if (tmpMin > 0) {
				tmpMin = 0;
			}
			tmpMin += nums.get(i);
			minTo[i] = Math.min(tmpMin, minTo[i - 1]);
		}

		// max sum from this index
		int[] maxFrom = new int[size];
		// min sum from this index
		int[] minFrom = new int[size];
		tmpMax = maxFrom[size - 1] = nums.get(size - 1);
		tmpMin = minFrom[size - 1] = nums.get(size - 1);

		// construct maxFrom, minFrom
		for (int i = size - 2; i >= 0; i--) {
			if (tmpMax < 0) {
				tmpMax = 0;
			}
			tmpMax += nums.get(i);
			maxFrom[i] = Math.max(tmpMax, maxFrom[i + 1]);

			if (tmpMin > 0) {
				tmpMin = 0;
			}
			tmpMin += nums.get(i);
			minFrom[i] = Math.min(tmpMin, minFrom[i + 1]);
		}

		// try every condition;
		int result = Integer.MIN_VALUE;
		for (int i = 0; i < size - 1; i++) {
			result = Math.max(
					result,
					Math.max(maxTo[i] - minFrom[i + 1], maxFrom[i + 1]
							- minTo[i]));
		}
		return result;
	}
}
