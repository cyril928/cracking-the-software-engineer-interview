package must.win.lintcode;

// Single Number II (LeetCode)
/*
 * One-pass, constant exstra space
 */
public class SingleNumberII {
	/**
	 * @param A
	 *            : An integer array
	 * @return : An integer
	 */
	public int singleNumberII(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}
		int ones = 0, twos = 0, threes = 0;
		for (int val : A) {
			twos |= ones & val;
			ones ^= val;
			threes = ones & twos;
			ones &= ~threes;
			twos &= ~threes;
		}
		return ones;
	}
}
