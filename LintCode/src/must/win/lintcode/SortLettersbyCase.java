package must.win.lintcode;

// Sort Letters by Case
/*
 * Do it in one-pass and in-place
 */
public class SortLettersbyCase {
	private boolean isLowerCase(char c) {
		return c >= 'a' && c <= 'z';
	}

	private boolean isUpperCase(char c) {
		return c >= 'A' && c <= 'Z';
	}

	/**
	 * @param chars
	 *            : The letter array you should sort by Case
	 * @return: void
	 */
	public void sortLetters(char[] chars) {
		if (chars == null || chars.length == 0) {
			return;
		}
		int lp = 0, rp = chars.length - 1;

		while (lp < rp) {
			if (isUpperCase(chars[lp])) {
				swap(chars, lp, rp--);
			} else if (isLowerCase(chars[lp])) {
				lp++;
			}
		}
	}

	private void swap(char[] chars, int i, int j) {
		char temp = chars[i];
		chars[i] = chars[j];
		chars[j] = temp;
	}
}
