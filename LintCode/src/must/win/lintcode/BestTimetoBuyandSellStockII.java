package must.win.lintcode;

// Best Time to Buy and Sell Stock II (LeetCode)
/*
 * 
 */
public class BestTimetoBuyandSellStockII {
	/**
	 * @param prices
	 *            : Given an integer array
	 * @return: Maximum profit
	 */
	public int maxProfit(int[] prices) {
		if (prices == null || prices.length < 2) {
			return 0;
		}
		int maxProfit = 0;
		for (int i = 1; i < prices.length; i++) {
			if (prices[i] > prices[i - 1]) {
				maxProfit += prices[i] - prices[i - 1];
			}
		}
		return maxProfit;
	}
}
