package must.win.lintcode;

// Interleaving Positive and Negative Numbers
/*
 * Do it in-place and without extra memory.
 * 
 * http://codeanytime.blogspot.tw/2014/12/interleaving-positive-and-negative.html
 * implementation & explanation
 */
public class InterleavingPositiveandNegativeNumbers {
	/**
	 * @param A
	 *            : An integer array.
	 * @return an integer array
	 */
	public int[] rerange(int[] A) {
		if (A == null || A.length <= 1) {
			return A;
		}

		int len = A.length;
		boolean expectPositive = false;
		int positiveCount = 0;
		for (int val : A) {
			positiveCount += (val > 0) ? 1 : 0;
		}

		if ((positiveCount << 1) > len) {
			expectPositive = true;
		}

		int index = 0;
		int i = 0, j = 0;
		// i: following first negative number index
		// j:

		while (i < len && j < len) {
			// A[i] is the next negative
			while (i < len && A[i] > 0) {
				i++;
			}
			// A[j] is the next postive
			while (j < len && A[j] < 0) {
				j++;
			}

			if (expectPositive && A[index] < 0) {
				swap(A, index, j);
			}
			if (!expectPositive && A[index] > 0) {
				swap(A, index, i);
			}

			if (index == i) {
				i++;
			}
			if (index == j) {
				j++;
			}

			expectPositive = !expectPositive;
			index++;
		}
		return A;
	}

	private void swap(int[] A, int i, int j) {
		int temp = A[i];
		A[i] = A[j];
		A[j] = temp;
	}
}
