package must.win.lintcode;

// Jump Game II (LeetCode)
/*
 * 
 */
public class JumpGameII {
	/**
	 * @param A
	 *            : A list of lists of integers
	 * @return: An integer
	 */
	public int jump(int[] A) {
		if (A == null || A.length == 0) {
			return Integer.MAX_VALUE;
		}

		int step = 0;
		int next = 0;
		int max = 0;
		int len = A.length;

		for (int i = 0; i < len - 1; i++) {
			max = Math.max(A[i] + i, max);
			if (i == next) {
				if (max == i) {
					return -1;
				}
				next = max;
				step++;
			}
		}
		return step;
	}
}
