package must.win.lintcode;

// Longest Common Subsequence
/*
 * http://okckd.github.io/blog/2014/06/24/Longest-Common-Subsequence/
 */
public class LongestCommonSubsequence {
	/**
	 * @param A
	 *            , B: Two strings.
	 * @return: The length of longest common subsequence of A and B.
	 */
	public int longestCommonSubsequence(String A, String B) {
		if (A == null || B == null || A.isEmpty() || B.isEmpty()) {
			return 0;
		}

		String sMin, sMax;
		if (A.length() <= B.length()) {
			sMin = A;
			sMax = B;
		} else {
			sMin = B;
			sMax = A;
		}
		int maxLen = sMax.length();
		int minLen = sMin.length();

		int[][] result = new int[2][minLen + 1];

		int curRow = 1;
		int preRow = 0;

		for (int i = 0; i < maxLen; i++) {
			for (int j = 0; j < minLen; j++) {
				if (sMin.charAt(j) == sMax.charAt(i)) {
					result[curRow][j + 1] = result[preRow][j] + 1;
				} else {
					result[curRow][j + 1] = Math.max(result[curRow][j],
							result[preRow][j + 1]);
				}
			}
			preRow = curRow;
			curRow ^= 1;
		}

		return result[preRow][minLen];
	}
}
