package must.win.lintcode;

import java.util.Arrays;

// Unique Paths (LeetCode)
/*
 * 
 */
public class UniquePaths {
	/**
	 * @param n
	 *            , m: positive integer (1 <= n ,m <= 100)
	 * @return an integer
	 */
	public int uniquePaths(int m, int n) {
		if (m == 0 || n == 0) {
			return 0;
		}

		int min = Math.min(m, n);
		int max = min ^ m ^ n;

		int[] paths = new int[min];

		Arrays.fill(paths, 1);

		for (int i = 1; i < max; i++) {
			for (int j = 1; j < min; j++) {
				paths[j] += paths[j - 1];
			}
		}
		return paths[min - 1];
	}
}
