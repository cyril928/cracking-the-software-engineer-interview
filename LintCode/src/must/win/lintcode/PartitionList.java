package must.win.lintcode;


// Partition List (LeetCode)
/*
 * 
 */
public class PartitionList {
	class ListNode {
		int val;
		ListNode next;

		public ListNode(int val) {
			this.val = val;
		}
	}

	/**
	 * @param head
	 *            : The first node of linked list.
	 * @param x
	 *            : an integer
	 * @return: a ListNode
	 */
	public ListNode partition(ListNode head, int x) {
		if (head == null || head.next == null) {
			return head;
		}

		ListNode dummyLeft = new ListNode(0);
		ListNode dummyRight = new ListNode(0);

		ListNode curLeft = dummyLeft;
		ListNode curRight = dummyRight;

		while (head != null) {
			if (head.val < x) {
				curLeft.next = head;
				curLeft = head;
			} else {
				curRight.next = head;
				curRight = head;
			}
			head = head.next;
		}

		curLeft.next = dummyRight.next;
		curRight.next = null;
		return dummyLeft.next;
	}
}
