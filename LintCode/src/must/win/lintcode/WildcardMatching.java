package must.win.lintcode;

// Wildcard Matching (LeetCode)
/*
 * 
 */
public class WildcardMatching {
	public boolean isMatch(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		// without this optimization, it will fail for large data set, eliminate
		// the case like s:qwer p:qwer***s
		int plenNoStar = 0;
		for (char c : p.toCharArray()) {
			if (c != '*') {
				plenNoStar++;
			}
		}
		if (plenNoStar > s.length()) {
			return false;
		}

		s = " " + s;
		p = " " + p;

		int sLen = s.length();
		int pLen = p.length();
		boolean allStar = true;
		boolean[] match = new boolean[sLen];
		match[0] = true;

		int preFirstTruePos = 0;

		for (int i = 1; i < pLen; i++) {
			if (p.charAt(i) != '*') {
				allStar = false;
			}
			int newFirstTruePos = Integer.MAX_VALUE;
			for (int j = sLen - 1; j >= 0; j--) {
				if (j == 0) {
					match[j] = allStar;
				} else if (p.charAt(i) != '*') {
					match[j] = (match[j - 1] && (p.charAt(i) == s.charAt(j) || p
							.charAt(i) == '?'));
				} else {
					match[j] = (j >= preFirstTruePos);
				}
				if (match[j]) {
					newFirstTruePos = Math.min(j, newFirstTruePos);
				}
			}
			preFirstTruePos = newFirstTruePos;
		}
		return match[sLen - 1];
	}
}
