package must.win.lintcode;


// Reverse Linked List II (LeetCode)
/*
 * Reverse it in-place and in one-pass
 */
public class ReverseLinkedListII {
	class ListNode {
		int val;
		ListNode next;

		ListNode(int x) {
			val = x;
			next = null;
		}
	}

	/**
	 * @param ListNode
	 *            head is the head of the linked list
	 * @oaram m and n
	 * @return: The head of the reversed ListNode
	 */
	public ListNode reverseBetween(ListNode head, int m, int n) {
		if (head == null || m >= n) {
			return head;
		}

		ListNode dummy = new ListNode(0);
		dummy.next = head;
		ListNode cur = dummy, prev = dummy;

		n = n - m + 1;

		while (m-- > 0) {
			prev = cur;
			cur = cur.next;
		}

		// connect m-1 and m node
		prev.next = reverseList(cur, n);
		return dummy.next;
	}

	private ListNode reverseList(ListNode head, int n) {
		ListNode prev = null;
		ListNode cur = head;

		while (n-- > 0) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}

		// connect n and n+1 node
		head.next = cur;
		return prev;
	}
}
