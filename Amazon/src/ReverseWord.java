
public class ReverseWord {
	public static void reverseWord(char[] str) {
		int record = -1;
		for(int i = 0;i <= str.length; i++) {
			if(i == (str.length) || str[i] == ' ') {
 				if(record != -1) {
 					swapStr(str, record, i-1);
 					record = -1;
 				}
 			}
			else if(str[i] != ' ' && record == -1) {
 				record = i;
 			}
		}
	}
	
	public static void swapStr(char[] str, int s, int e) {
		while(s < e) {
			char temp = str[s];
			str[s++] = str[e];
			str[e--] = temp;
		}
	}
	public static void main(String[] args) {
		String test = "  i like this program very    much  ";
		char[] a = test.toCharArray();
		reverseWord(a);
		System.out.println(a);
	}
}
