import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class Test {
	public static void mapPass(HashMap<Integer, Integer> map) {
		map.put(4, 16);
		map.put(5, 111);
		for(Map.Entry entry : map.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}
	public static void test(Node n) {
		n.val = 9;
	}
	
	public static void main(String[] args) throws IOException {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(1, 3);
		map.put(2, 5);
		map.put(9, 17);
		for(Map.Entry entry : map.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		map.remove(1);
		for(Map.Entry entry : map.entrySet()) {
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
		mapPass(map);
		
		Node m = new Node(8);
		test(m);
		System.out.println(m.val);
	}
}

class Node {
	int val;
	Node next;
	public Node(int v) {
		this.val = v;
	}
}
