import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/*
 * Author : Tung-Keng(Cyril) Lee
 * 
 * This is a in-memory file system simulation, and support cd, mkdir, rmdir, pwd, ls command.
 * However, for mkdir and rmdir, as question's requirement, it only supports mkdir <name> 
 * but not mkdir <path> currently.
 * 
 * 
 * 
 */
public class FileSystem {

	public static void main(String[] args) {
		try {
			FileSystem fs = new FileSystem();
			BufferedReader br = new BufferedReader(new InputStreamReader(
					System.in));
			String input;
			while ((input = br.readLine()) != null) {
				input = input.trim();
				if (input.startsWith("cd")) {
					String[] paths = input.split(" +");
					fs.cd(paths[1]);
				} else if (input.startsWith("mkdir")) {
					String[] paths = input.split(" +");
					fs.mkdir(paths[1]);
				} else if (input.startsWith("rmdir")) {
					String[] paths = input.split(" +");
					fs.rmdir(paths[1]);
				} else if (input.equals("pwd")) {
					fs.pwd();
				} else if (input.equals("ls")) {
					fs.ls();
				} else {
					System.out.println("invalid command");
				}
			}

		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	// to track the current working directory
	Stack<Directory> stack;

	// to store the path of current working directory
	List<String> fullPath;

	// initialize file system simulation
	public FileSystem() {
		stack = new Stack<Directory>();
		fullPath = new ArrayList<String>();
		stack.push(new Directory("/"));
	}

	private boolean cd(String input) {
		String[] paths = input.split("/+");
		for (String path : paths) {
			if (!path.equals(".")) {
				if (path.equals("..") && stack.size() > 1) {
					stack.pop();
					fullPath.remove(fullPath.size() - 1);
				} else {
					Directory curDict = stack.peek();
					if (curDict.childrenMap.containsKey(path)) {
						stack.push(curDict.childrenMap.get(path));
						fullPath.add(path);
					}
				}
			}
		}
		return true;
	}

	private void ls() {
		if (!stack.isEmpty()) {
			Directory curDict = stack.peek();
			StringBuilder sb = new StringBuilder();
			for (String childName : curDict.childrenMap.keySet()) {
				sb.append(childName).append(" ");
			}
			System.out.println(sb.substring(0, sb.length() - 1));
		}
	}

	private void mkdir(String input) {
		String[] paths = input.split("/+");
		for (int i = 0; i < paths.length - 1; i++) {
			if (!cd(input)) {
				return;
			}
		}
		Directory curDict = stack.peek();
		String newDictName = paths[paths.length - 1];
		if (curDict.childrenMap.containsKey(newDictName)) {
			System.out.println("mkdir: " + newDictName + ": Directory exists");
		} else {
			curDict.childrenMap.put(newDictName, new Directory(newDictName));
		}
	}

	private void pwd() {
		if (fullPath.isEmpty()) {
			System.out.println("/");
		} else {
			StringBuilder sb = new StringBuilder();
			for (String path : fullPath) {
				sb.append("/").append(path);
			}
			System.out.println(sb.toString());
		}
	}

	private void rmdir(String input) {
		String[] paths = input.split("/+");
		for (int i = 0; i < paths.length - 1; i++) {
			if (!cd(input)) {
				return;
			}
		}
		Directory curDict = stack.peek();
		String targetDictName = paths[paths.length - 1];
		if (curDict.childrenMap.containsKey(targetDictName)) {
			curDict.childrenMap.remove(targetDictName);
		} else {
			System.out.println("rmdir: " + targetDictName
					+ ": Directory doesn't exist");
		}
	}

}
