import java.util.HashMap;
import java.util.Map;

public class Directory {
	String name;
	Map<String, Directory> childrenMap;

	/*
	 * directory node, using map to maintain children list is for searching
	 * target child directory in O(1) time
	 */
	public Directory(String name) {
		this.name = name;
		this.childrenMap = new HashMap<String, Directory>();
	}
}
