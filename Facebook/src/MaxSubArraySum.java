
public class MaxSubArraySum {

	public static int maxSubArraySum(int[] array, int len) {
		if(len < 2)	return array[0];
		int max_so_far_a = array[0];
		int max_ending_a = array[0];
		int max_so_far_b = array[1];
		int max_ending_b = array[1];
		for(int i=0;i<len;i+=2) {
			max_ending_a = Math.max(array[i], array[i] + max_ending_a);
			max_so_far_a = Math.max(max_ending_a, max_so_far_a);
		}
		for(int i=1;i<len;i+=2) {
			max_ending_b = Math.max(array[i], array[i] + max_ending_b);
			max_so_far_b = Math.max(max_ending_b, max_so_far_b);
		}
		return Math.max(max_so_far_a, max_so_far_b);
	}
	
	public static void main(String[] args) {
	   int a[] =  {-2, -3, 4, -1, -2, 1, 5, -3, 7, 6, 5, -1, -3};
	   int max_sum = maxSubArraySum(a, a.length);
	   System.out.println(max_sum);
	}
}
