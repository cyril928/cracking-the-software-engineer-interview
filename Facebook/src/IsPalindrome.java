
public class IsPalindrome {
	public static void main(String[] args) {
		System.out.println(isPalindrome("abcddcba", "abcddcba".length()));
		System.out.println(isPalindrome("abcdecba", "abcdecba".length()));
		System.out.println(isPalindrome("cdgdc", "cdgdc".length()));
		System.out.println(isPalindrome("cdgec", "cdgec".length()));
	}
	public static boolean isPalindrome(String s, int len) {
		if(len == 0 || len == 1)	return true;
		if(s.charAt(0) == s.charAt(len-1)) {
			return isPalindrome(s.substring(1), len-2);
		}
		else {
			return false;
		}
	}
}
