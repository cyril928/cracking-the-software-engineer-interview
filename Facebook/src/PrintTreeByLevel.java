import java.util.*;

public class PrintTreeByLevel {
	public static void main(String[] args) {
		int[] array = {1, 3, 4, 5, 7, 8, 13, 21, 24, 26, 31, 33, 36};
		TreeNode root = sortedArrayToTree(array, 0, array.length - 1);
		printTreeByLevel(root);
	}
	public static TreeNode sortedArrayToTree(int[] array, int start, int end) {
		if(array.length < 1) return null;
		if(end < start)	return null;
		int mid = end + (start - end) / 2;
		TreeNode parent = new TreeNode(array[mid]);
		parent.left = sortedArrayToTree(array, start, mid - 1);
		parent.right = sortedArrayToTree(array, mid + 1, end);
		return parent;
	}
	public static void printTreeByLevel(TreeNode root) {
		if(root == null)	return;
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		Queue<Integer> levels = new LinkedList<Integer>();
		queue.add(root);
		levels.add(0);
		int lastlevel = 0;
		
		while(queue.size() > 0) {
			TreeNode parent = queue.poll();
			int level = levels.poll();
			if(level != lastlevel) {
				System.out.println();
				lastlevel = level;
			}
			System.out.print(parent.val + " ");
			if(parent.left != null) {
				queue.add(parent.left);
				levels.add(level + 1);
			}
			
			if(parent.right != null) {
				queue.add(parent.right);
				levels.add(level + 1);
			}
			
		}
	}
}

class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	public TreeNode(int val) {
		this.val = val;
		left = null;
		right = null;
	}
}