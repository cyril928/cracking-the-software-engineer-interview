import java.io.IOException;

public class StringMatch {
	public static void main(String[] args) throws IOException {
		System.out.println(match("Facebook", "F*cebo.k", "Facebook".length(),
				"F*cebo.k".length(), ' '));
		System.out.println(match("Facebook", "F.cebo.k", "Facebook".length(),
				"F.cebo.k".length(), ' '));
		System.out.println(match("Facebooo", "Facebo*", "Facebooo".length(),
				"Facebo*".length(), ' '));
		System.out.println(match("Facebooker", "F.cebo*",
				"Facebooker".length(), "F.cebo*".length(), ' '));
		System.out.println(match("Facebooker", "*F.cebo*",
				"Facebooker".length(), "F.cebo*".length(), ' '));
		System.out.println(match("Facebooker", "*acebo*",
				"Facebooker".length(), "F.cebo*".length(), ' '));

		System.out.println(match("*Faceboo", "Faceboo",
				"Facebooker".length(), "F.cebo*".length(), ' '));
	}

	private static boolean match(String input1, String input2, int i, int j, char prev) {
		if (i != 0 && j == 0)
			return false;
		if (i == 0 && j != 0)
			return false;
		if (i == 0 && j == 0)
			return true;
		else {
			if (input1.charAt(0) == input2.charAt(0) || input2.charAt(0) == '.')
				return match(input1.substring(1), input2.substring(1), i - 1,
						j - 1, input2.charAt(0));
			else if (input2.charAt(0) == '*') {
				if(input1.charAt(0) == prev) {
					return match(input1.substring(1), input2, i - 1, j, prev) ||  match(input1.substring(1), input2.substring(1), i - 1, j - 1, '*');
				}
				else {
					return match(input1, input2.substring(1), i, j - 1, '*');
				}
			}
			return false;
		}
	}

}
