import java.io.IOException;
import java.util.*;


public class UniSubSet {
	public static ArrayList<String> subset(String s) {
		ArrayList<String> res = new ArrayList<String>();
		res.add("");
		for(int i=0;i<s.length();i++) {
			char c = s.charAt(i);
			ArrayList<String> nres = new ArrayList<String>(res);
			for(String str : nres) {
				if(!res.contains(str + c)) {
					res.add(str + c);
				}
			}
		}
		return res;
	}
	
	public static void main(String[] args) throws IOException {
		ArrayList<String> res = subset("abcb");
		for(String s : res) {
			System.out.println(s);
		}
	}
}
