import java.io.IOException;
import java.util.*;

public class AddToSum {
	
	public static void addToSum(int n) {
		if(n < 2) return;
		ArrayList<Integer> res = new ArrayList<Integer>();
		genSumStr(res, n);
	}
	
	public static void genSumStr(ArrayList<Integer> res, int n) {
		if(n == 0) {
			for(Integer i : res) {
				System.out.print(i + ",");
			}
			System.out.println();
		}

		for(int i=1;i<=n;i++) {
			if(res.size() > 0 && i < res.get(res.size()-1)) continue;
			ArrayList<Integer> nres = new ArrayList<Integer>(res);
			nres.add(i);
			genSumStr(nres, n-i);
		}
	}
	
	public static void main(String[] args) throws IOException {
		addToSum(4);
	}
}
