import java.io.IOException;


public class PalindromeSub {
	public static int find(String inputText) {
		if (inputText == null) {
			System.out.println("Input cannot be null!");
			return 0;
		}
		// ODD Occuring Palindromes
		int len = inputText.length();
		int palindromes = len;
		for (int i = 0; i < len; i++) {
			for (int j = i - 1, k = i + 1; j >= 0 && k < len; j--, k++) {
				if (inputText.charAt(j) == inputText.charAt(k)) {
					palindromes++;
					System.out.println(inputText.substring(j, k + 1));
				} else {
					break;
				}
			}
		}
		// EVEN Occuring Palindromes
		for (int i = 0; i < len; i++) {
			for (int j = i, k = i + 1; j >= 0 && k < len; j--, k++) {
				if (inputText.charAt(j) == inputText.charAt(k)) {
					palindromes++;
					System.out.println(inputText.subSequence(j, k + 1));
				} else {
					break;
				}
			}
		}
		return palindromes;
	}
	
	public static void main(String[] args) throws IOException {
		System.out.println(find("ccabba"));
	}
}
