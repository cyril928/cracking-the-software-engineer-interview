
public class SquareRoot {
	public static double squareRoot(int n) {
		double e = 0.0000001;
		double x = n;
		double y = 1;
		while(x - y > e) {
			x = (x + y) / 2;
			y = n /x;
		}
		return x;
	}
	
	public static int perfectSquareRoot(int n) {
		int i,c;
		for(i=1, c=0; n>0; i+=2, c++) {
			n-=i;
		}
		return c;
	}
	
	
	public static void main(String[] args) {
		System.out.println(squareRoot(37));
		System.out.println(perfectSquareRoot(49));
	}
}
