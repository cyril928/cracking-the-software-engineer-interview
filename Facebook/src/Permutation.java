import java.io.IOException;
import java.util.*;


public class Permutation {
	public static void permu(String s) {
		if(s.length() == 0)	return;
		genPerm(s,"");
	}
	public static void genPerm(String s, String res) {
		if(s.length() == 0) {
			System.out.println(res);
		}
		HashMap<Character, Boolean> table = new HashMap<Character, Boolean>();
		for(int i=0;i<s.length();i++) {
			if(!table.containsKey(s.charAt(i))) {
				table.put(s.charAt(i), true);
				genPerm(s.substring(0, i) + s.substring(i + 1), res + s.charAt(i));
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		permu("abdd");
		String s = "fghacbdfgh";
		char[] str = s.toCharArray();
		Arrays.sort(str);
		System.out.println(new String(str));
	}
}
