import java.io.IOException;


public class Parenthesis {

	public static void paren(int n) {
		genParen("", n, n);
	}
	
	public static void genParen(String res, int left, int right) {
		if(left == 0) {
			System.out.print(res);
			for(int i = 0; i<right;i++) {
			System.out.print(")");
			}
			System.out.println();
			return;
		}
		//if(left > right) return;
		if(left < right) {
			genParen(res + ")", left, right - 1);
		}
		genParen(res + "(", left - 1, right);
	}
	public static void main(String[] args) throws IOException {
		paren(4);
	}
}
