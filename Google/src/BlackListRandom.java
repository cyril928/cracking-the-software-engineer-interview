import java.util.Random;

public class BlackListRandom {

	public static void main(String[] args) {
		BlackListRandom b = new BlackListRandom();
		int[] A = { 3, 6, 9 };
		for (int i = 0; i < 20; i++) {
			System.out.println(b.blackListRandom(A, 11));
		}
	}

	public int blackListRandom(int[] A, int n) {
		n = n - A.length;
		Random r = new Random();
		int val = r.nextInt(n);
		// System.out.println(val);
		// System.out.println("==========================");
		int left = 0, right = A.length - 1;
		while (left <= right) {
			int mid = ((right - left) >> 1) + left;
			if (val == A[mid] - mid) {
				return val + mid + 1;
			} else if (val > A[mid] - mid) {
				left = mid + 1;
			} else {
				right = mid - 1;
			}
		}

		return val + left;
	}
}
