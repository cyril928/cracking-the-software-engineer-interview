/*
 * 第一个是给个数组，打乱了，比如
 * 索引  0 1 2 3 4
 * 值   3 2 1 4 0
 * 数组的值是下次跳的索引位置，这样的话数组有环，比如 0 -> 3 -> 4 -> 0  1 -> 2 -> 1， 求最长环的长度.
 */
public class LongestLoopinArray {

	public static void main(String[] args) {

		LongestLoopinArray sl = new LongestLoopinArray();
		int[] A1 = { -1, 1, 3, 4, 1 };
		System.out.println(sl.longestLoopinArray(A1));
		int[] A2 = { 1, 2, 3, 4, 5, 3 };
		System.out.println(sl.longestLoopinArray(A2));
	}

	public int longestLoopinArray(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}

		int[] lens = new int[A.length];
		int[] S = new int[A.length];

		int maxSize = 0;
		for (int i = 0; i < A.length; i++) {
			if (lens[i] == 0) {
				int j = i;
				lens[j] = 1;
				S[j] = i;
				while (A[j] < A.length && A[j] >= 0 && lens[A[j]] == 0) {
					lens[A[j]] = lens[j] + 1;
					j = A[j];
					S[j] = i;
				}
				if (A[j] < A.length && A[j] >= 0 && S[A[j]] == S[j]) {
					maxSize = Math.max(lens[j] + 1 - lens[A[j]], maxSize);
				}
			}
		}

		return maxSize;
	}
}
