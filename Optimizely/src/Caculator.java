import java.util.ArrayList;
import java.util.List;

/*
 Calculator: Write a function/method that takes a mathematical expression represented as a string and returns the value of the expression.

 The expression is written in infix notation (number operator number) with a single space between operators and operands.

 You only need to support four operations: addition, subtraction, division and multiplication (+ - * /)

 No parenthesis.

 Throw an exception or raise an error if you encounter invalid input.

 Valid input:
 "15 + 2 - 3" -> 14
 "145" -> 145
 "1 + 233 / 233" -> 2


 Invalid input:
 "1 1"
 "+ 1"
 "1 + 2 +"
 "+ 1 1"
 "1+2"
 */

public class Caculator {
	/*
	 * To execute Java, please define "static void main" on a class named
	 * Solution.
	 * 
	 * If you need more classes, simply define them inline.
	 */
	// 2 + 55 * 3 - 1

	public static void main(String[] args) {
		ArrayList<String> strings = new ArrayList<String>();
		strings.add("Hello, World!");
		strings.add("Welcome to CoderPad.");
		strings.add("This pad is running Java 8.");

		for (String string : strings) {
			System.out.println(string);
		}

		Caculator s = new Caculator();
		System.out.println(s.calculate("15 + 2 - 3"));
		System.out.println(s.calculate("145"));
		System.out.println(s.calculate("1 + 233 / 233"));
		// System.out.println(s.calculate("1 1"));
		// System.out.println(s.calculate("+ 1"));
		// System.out.println(s.calculate("1 + 2 +"));
		// System.out.println(s.calculate("+ 1 1"));
		// System.out.println(s.calculate("1+2"));
	}

	private int calculate(int a, int b, String oper) {
		switch (oper) {
		case "+":
			return a + b;
		case "-":
			return a - b;
		case "*":
			return a * b;
		case "/":
			return a / b;
		default:
			throw new IllegalArgumentException("");
		}
	}

	public int calculate(String s) {
		String[] strs = s.split(" ");

		int len = strs.length;
		if ((len & 1) == 0) {
			throw new IllegalArgumentException("");
		}
		for (int i = 0; i < len; i++) {
			if ((i & 1) == 0 && !isNumber(strs[i])) {
				throw new IllegalArgumentException("");
			} else if ((i & 1) == 1 && !isOperator(strs[i])) {
				throw new IllegalArgumentException("");
			}
		}

		List<Integer> operands = new ArrayList<Integer>();
		List<String> operators = new ArrayList<String>();
		int j = 1;
		int preVal = Integer.parseInt(strs[0]);

		while (j < len) {
			if (strs[j].equals("*") || strs[j].equals("/")) {
				int a = Integer.parseInt(strs[j - 1]);
				int b = Integer.parseInt(strs[j + 1]);
				preVal = calculate(a, b, strs[j]);
			} else {
				operands.add(preVal);
				operators.add(strs[j]);
				preVal = Integer.parseInt(strs[j + 1]);
			}
			j += 2;
		}
		operands.add(preVal);

		int res = operands.get(0);
		for (int i = 0; i < operators.size(); i++) {
			res = calculate(res, operands.get(i + 1), operators.get(i));
		}
		return res;
	}

	private boolean isNumber(String s) {
		if (s.charAt(0) == '0' && s.length() > 1) {
			return false;
		}

		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) > '9' || s.charAt(i) < '0') {
				return false;
			}
		}
		return true;
	}

	private boolean isOperator(String s) {
		return s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/");
	}

}
