public class TwoDimFind {
  
  public static class Coordinate {
    int row,col;
    public Coordinate (int x, int y){
      this.row = x;
      this.col = y;
    }

  }
  public static Coordinate findElement(int[][] matrix, int elem, Coordinate start, Coordinate end) {
	System.out.println("start :" + start.row + " " + start.col);
	System.out.println("end :" + end.row + " " + end.col);
	if(end.row < start.row || end.col < start.col) {
      return null;
    } 

    int row_mid = start.row + (end.row - start.row) / 2;
    int col_mid = start.col + (end.col - start.col) / 2;

    if(elem == matrix[row_mid][col_mid]) {
      return new Coordinate(row_mid, col_mid);
    }
    else if(elem < matrix[row_mid][col_mid]) {
      Coordinate a, b, c;
      a = b = c = null;
      if(row_mid > start.row || col_mid > start.col)
    	  a = findElement(matrix, elem, start, new Coordinate(row_mid, col_mid));
      if(col_mid < end.col && start.row < row_mid) {
        b = findElement(matrix, elem, 
          new Coordinate(start.row, col_mid + 1), new Coordinate(row_mid - 1, end.col));
      }
      if(col_mid > start.col && row_mid < end.row) {
        c = findElement(matrix, elem, 
          new Coordinate(row_mid + 1, start.col), new Coordinate(end.row, col_mid - 1));
      }
      if(a != null) return a;
      else if(b != null) return b;
      else  return c;
    }
    else if(elem > matrix[row_mid][col_mid]) {
      if(row_mid < end.row && col_mid < end.col) {
        if(elem == matrix[row_mid + 1][col_mid + 1]) {
          return new Coordinate(row_mid + 1, col_mid + 1);
        }
        else if(elem < matrix[row_mid + 1][col_mid + 1]) {
          Coordinate a,b;
          a = findElement(matrix, elem, 
            new Coordinate(start.row, col_mid + 1), new Coordinate(row_mid, end.col));
          b = findElement(matrix, elem, 
            new Coordinate(row_mid + 1, start.col), new Coordinate(end.row, col_mid));
          if(a != null)   return a;
          else return b;
        }
        else {
        	return findElement(matrix, elem, new Coordinate(row_mid, col_mid), new Coordinate(end.row, end.col));
        }
      }
    }
	return null;
  }

  public static void main(String[] args) {
    int[][] matrix = {{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15}};
    for(int i =0;i<matrix.length;i++){
    	for(int j=0;j<matrix[0].length;j++) {
    		System.out.println(matrix[i][j]);
    	}
    }
    Coordinate co = findElement(matrix, 19, 
      new Coordinate(0, 0), new Coordinate(matrix.length - 1, matrix[0].length -1));
    System.out.println(co);
    System.out.println(co.row);
    System.out.println(co.col); 
  }

}