import java.util.HashSet;
import java.util.Set;

public class PermutationStr {

	public static void main(String[] args) {
		permutation("ABDD");
		String s = "abc\n";
		System.out.println(s + "cdf");
	}

	public static void permutation(String s) {
		if (s.length() == 0) {
			return;
		}
		permutation(s, "");
	}

	public static void permutation(String s, String res) {
		if (s.length() == 0) {
			System.out.println(res);
			return;
		}
		Set<Character> set = new HashSet<Character>();
		for (int i = 0; i < s.length(); i++) {
			if (!set.contains(s.charAt(i))) {
				set.add(s.charAt(i));
				permutation(s.substring(0, i) + s.substring(i + 1),
						res + s.charAt(i));
			}
		}
	}
}
