import java.util.Map;
import java.util.PriorityQueue;

public class Huffman {

	public class TreeNode {
		char c;
		int sum;
		TreeNode left;
		TreeNode right;

		public TreeNode(char c, int sum) {
			this.c = c;
			this.sum = sum;
		}
	}

	private PriorityQueue<TreeNode> freList = null;
	private Map<Character, String> encodeMap;

	public Huffman(PriorityQueue<TreeNode> list) {
		this.freList = list;
		buildTree(freList);
	}

	private void buildMap(TreeNode root, StringBuilder path,
			Map<Character, String> encodeMap) {
		if (root.left == null && root.right == null) {
			encodeMap.put(root.c, path.toString());
		}
		if (root.left != null) {
			buildMap(root.left, path.append("1"), encodeMap);
			path.deleteCharAt(path.length() - 1);
		}
		if (root.right != null) {
			buildMap(root.right, path.append("0"), encodeMap);
			path.deleteCharAt(path.length() - 1);
		}
	}

	private void buildTree(PriorityQueue<TreeNode> freList) {

		// Collections.sort(freList, comparator);

		TreeNode root = null;
		TreeNode rightNode = null;
		TreeNode leftNode = null;
		while (freList.size() > 1) {
			rightNode = freList.poll();
			leftNode = freList.poll();
			root = new TreeNode(' ', rightNode.sum + leftNode.sum);
			root.right = rightNode;
			root.left = leftNode;
			freList.add(root);
		}
		root = freList.poll();
		buildMap(root, new StringBuilder(), encodeMap);
	}

}
