public class Test {

	public static void main(String[] args) {
		ListNode head = new ListNode(1);
		ListNode cur = head;
		for (int i = 2; i <= 5; i++) {
			cur.next = new ListNode(i);
			cur = cur.next;
		}

		cur = head;
		while (cur != null) {
			System.out.println(cur.val);
			cur = cur.next;
		}

		ListNode newHead = reverseList(head);

		cur = newHead;
		while (cur != null) {
			System.out.println(cur.val);
			cur = cur.next;
		}
	}

	private static ListNode reverseList(ListNode head) {
		ListNode prev = null;
		ListNode cur = head;

		while (cur != null) {
			ListNode next = cur.next;
			cur.next = prev;
			prev = cur;
			cur = next;
		}
		return prev;
	}

}
