import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Snake {

	List<Position> snake;
	Direction direction;
	int rows, cols;
	Position apple;
	int score;
	Lock lock;

	public Snake(List<Position> snake, Direction direction, int rows, int cols) {
		this.snake = snake;
		this.direction = direction;
		this.rows = rows;
		this.cols = cols;
		randomApple();
		lock = new ReentrantLock();
	}

	private boolean hitBody(Position newNode) {
		for (Position p : snake) {
			if (p.x == newNode.x || p.y == newNode.y) {
				return true;
			}
		}
		return false;
	}

	public synchronized void input(Direction newDirection) {
		lock.lock();
		if ((newDirection == Direction.UP && direction == Direction.DOWN)
				|| (newDirection == Direction.DOWN && direction == Direction.UP)
				|| (newDirection == Direction.LEFT && direction == Direction.RIGHT)
				|| (newDirection == Direction.RIGHT && direction == Direction.LEFT)) {
			return;
		}

		direction = newDirection;
		lock.unlock();
	}

	private boolean overBound(Position pos) {
		return !(pos.x < 0 || pos.x >= rows || pos.y < 0 || pos.y >= cols);
	}

	private void randomApple() {
		do {
			Random r = new Random();
			int apple_x = r.nextInt() % rows;
			int apple_y = r.nextInt() % cols;
			apple = new Position(apple_x, apple_y);
		} while (hitBody(apple));
	}

	public synchronized boolean tick() {
		Position pos = snake.get(0);
		Position newHead = new Position(pos.x, pos.y);

		lock.lock();
		if (direction == Direction.UP) {
			newHead.x--;
		} else if (direction == Direction.DOWN) {
			newHead.x++;
		} else if (direction == Direction.LEFT) {
			newHead.y--;
		} else {
			newHead.y++;
		}
		lock.unlock();

		if (overBound(newHead)) {
			return false;
		}
		if (hitBody(newHead)) {
			return false;
		}

		snake.add(0, newHead);

		if (newHead.x == apple.x && newHead.y == apple.y) {
			score++;
			randomApple();
		} else {
			snake.remove(snake.size() - 1);
		}

		return true;

	}
}
