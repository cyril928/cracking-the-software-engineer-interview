public class RotateBinarySearch {
	public static int search(int a[], int left, int right, int x) {
		if (right < left)
			return -1;
		int mid = left + (right - left) / 2;
		if (x == a[mid])
			return mid;
		if (x > a[mid]) {
			if (a[right] < a[mid]) {
				return search(a, mid + 1, right, x);
			} else if (a[right] > a[mid]) {
				if (a[right] >= x)
					return search(a, mid + 1, right, x);
				else
					return search(a, left, mid - 1, x);
			} else {
				int result = search(a, mid + 1, right, x);
				if (result == -1)
					return search(a, left, mid - 1, x);
				else
					return result;
			}
		}
		if (x < a[mid]) {
			if (a[left] > a[mid]) {
				return search(a, left, mid - 1, x);
			} else if (a[left] < a[mid]) {
				if (a[left] <= x)
					return search(a, left, mid - 1, x);
				else
					return search(a, mid + 1, right, x);
			} else {
				int result = search(a, mid + 1, right, x);
				if (result == -1)
					return search(a, left, mid - 1, x);
				else
					return result;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		int[] test1 = {2,2,2,2,2,3,4,2,2,2};
		int[] test2 = {15,16,45,78,4,5,7,9};
		int[] test3 = {1,2,3,4,5,6,7,8};
		int index = search(test1, 0, test1.length-1, 4);
		System.out.println(index);
		index = search(test2, 0, test2.length-1, 16);
		System.out.println(index);
		index = search(test3, 0, test3.length-1, 7);
		System.out.println(index);
	}
}
