import java.util.*;

public class GetSubsets {
 
    public static ArrayList<ArrayList<Integer>> getSubsets(ArrayList<Integer> set, int index) {
        
        ArrayList<ArrayList<Integer>> allsubsets;
        if(index == set.size()) {
            allsubsets = new ArrayList<ArrayList<Integer>>();
            allsubsets.add(new ArrayList<Integer>());                
        }
        else {
            allsubsets = getSubsets(set, index + 1);
            int item = set.get(index);
            ArrayList<ArrayList<Integer>> moresubsets = new ArrayList<ArrayList<Integer>>();
            for(ArrayList<Integer> oldset : allsubsets) {
                ArrayList<Integer> newset = new ArrayList<Integer>(oldset);
                //newset.addAll(oldset);    
                newset.add(item);
                moresubsets.add(newset);
            }
            allsubsets.addAll(moresubsets);
        }
        return allsubsets;
    } 
    
    public static ArrayList<ArrayList<Integer>> getSubsets2(ArrayList<Integer> set) {
        ArrayList<ArrayList<Integer>> allsubsets = new ArrayList<ArrayList<Integer>>();
        int k = (1 << set.size());
        for(int i = 0; i < k; i++) {
            ArrayList<Integer> newset = new ArrayList<Integer>();
            for(int index = 0; index < set.size(); index ++) {
                if(((i >> index) & 1) != 0) {
                    newset.add(set.get(index));
                }
            }
            allsubsets.add(newset);
        }
        return allsubsets;
    }
 
 
    public static void printf(ArrayList<ArrayList<Integer>> allsubsets) {
        for(ArrayList<Integer> set : allsubsets) {
            System.out.print("{ ");
            for(Integer val : set) {
                System.out.print(val + ", ");
            }
            System.out.println("}");
        }    
    }
 
    public static void main(String[] args) {
        ArrayList<Integer> set = new ArrayList<Integer>(Arrays.asList(1,2,3,4));
        if(set.size() >= 0) {
            printf(getSubsets2(set));
        }
    }
}