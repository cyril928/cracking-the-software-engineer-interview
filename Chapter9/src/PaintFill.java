
public class PaintFill {
	public enum Color {
	    Black, White, Yellow, Green, Red;
	}

	boolean paintFill(Color[][] screen, int x, int y, Color ocolor, Color ncolor) {
	    if(x < 0 || y < 0 || x >= screen[0].length || y >= screen.length) {
	        return false;
	    } 

	    if(screen[y][x] == ocolor) {
	        screen[y][x] = ncolor;
	        paintFill(screen, x - 1, y, ocolor, ncolor);
	        paintFill(screen, x + 1, y, ocolor, ncolor);
	        paintFill(screen, x, y - 1, ocolor, ncolor);
	        paintFill(screen, x, y + 1, ocolor, ncolor);
	    }
	    return true;
	}

	boolean paintFill(Color[][] screen, int x, int y, Color ncolor) {
	    if(screen[y][x] == ncolor)    return false;
	    else {
	        return paintFill(screen, x, y, screen[y][x], ncolor);
	    }
	}
}
