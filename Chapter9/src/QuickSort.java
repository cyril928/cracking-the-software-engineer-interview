public class QuickSort {
	static void quickSort(int arr[], int left, int right) {
		if (left < right) {
			int index = partition(arr, left, right);
			quickSort(arr, left, index);
			quickSort(arr, index + 1, right);
		}
	}

	static int partition(int arr[], int left, int right) {
		int pivot = arr[left + (right - left) / 2];
		while (left < right) {
			while (arr[left] < pivot)
				left++;
			while (arr[right] > pivot)
				right--;
			if (left < right) {
				swap(arr, left, right);
				left++;
				right--;
			}
		}
		return left;
	}

	static void swap(int arr[], int left, int right) {
		int temp = arr[left];
		arr[left] = arr[right];
		arr[right] = temp;
	}

	public static void main(String[] args) {
		int[] test = { 22, 66, 33, 88, 99, 27, 85, 19, 35, 48 };

		quickSort(test, 0, test.length - 1);
		for (int i = 0; i < test.length; i++) {
			System.out.print(test[i] + " ");
		}
	}
}
