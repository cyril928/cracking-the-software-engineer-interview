import java.util.*;

public class GenerateParents {

	public static ArrayList<String> generateParents(int count) {
	    char[] str = new char[count * 2];
	    ArrayList<String> list = new ArrayList<String>();
	    addParens(list, count, count, str, 0);
	    return list;        
	}

	public static void addParens(ArrayList<String> list, int leftRem, int rightRem, char[] str, int count) {
	    
	    if(leftRem < 0 || rightRem < leftRem) {
	        return;
	    }
	    else if(count == str.length)    {
	        String s = String.copyValueOf(str);
	        list.add(s);
	        return;
	    }
	    //if(leftRem > 0) {
	        str[count] = '(';
	        addParens(list, leftRem - 1, rightRem, str, count + 1);
	    //}
	    //if(rightRem > leftRem) {
	        str[count] = ')';
	        addParens(list, leftRem, rightRem - 1, str, count + 1);
	    //}
	    return;
	}

	
	public static void printf(ArrayList<String> list) {
		for (String s : list) {
			System.out.println(s);
		}
	}

	public static void main(String args[]) {
		printf(generateParents(3));
		System.out.println("==================");
	}

}
