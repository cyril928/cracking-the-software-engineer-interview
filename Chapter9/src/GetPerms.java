import java.util.*;

public class GetPerms {
    public static ArrayList<String> getPerms(String str) {
        if(str == null) {
            return null;
        }   
        
        ArrayList<String> perms =  new ArrayList<String>();
        for(int i=0; i<str.length(); i++) {
            char c = str.charAt(i);
            String new_str;
            ArrayList<String> tmp = new ArrayList<String>();
            for(String s : perms) {
                for(int j=0;j<=s.length();j++) {
                    new_str = insertCharAt(s, c, j);
                    tmp.add(new_str);
                }
            }
            perms.addAll(tmp);
            new_str = new StringBuilder().append(c).toString();
            perms.add(new_str);
        }
        return perms;
    }    
    
    public static String insertCharAt(String word, char c, int index) {
        String start = word.substring(0, index);
        String end = word.substring(index);
        return start + c + end;                    
    }
    
    public static void printf(ArrayList<String> perms) {
        for(String str : perms) {
            System.out.println(str);
        }
    }
    
    public static void main(String[] args) {
        String str = new String("abc");
        printf(getPerms(str));
    }
}