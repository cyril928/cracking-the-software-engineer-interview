public class MergeSort {
	public static void mergesort(int[] array) {
	    int[] helper = new int[array.length];
	    mergeSort(array, helper, 0, array.length - 1);
	}

	public static void mergeSort(int[] array, int[] helper, int low, int high) {
	    if(high <= low)    return;
	    int mid = low + (high - low) / 2;
	    mergeSort(array, helper, low, mid);
	    mergeSort(array, helper, mid + 1, high);
	    merge(array, helper, low, mid, high);
	}


	public static void merge(int[] array, int[] helper, int low, int mid, int high) {
	    for(int i = low; i <= high; i++) {
	        helper[i] = array[i];
	    }
	    
	    int leftPointer = low;
	    int rightPointer = mid + 1;
	    int currentPointer = low;
	    
	    while(leftPointer <= mid && rightPointer <= high) {
	        if(helper[leftPointer] > helper[rightPointer]) {
	            array[currentPointer++] = helper[rightPointer++];
	        }
	        else {
	            array[currentPointer++] = helper[leftPointer++];
	        }
	    }
	    
	    while(leftPointer <= mid) {
	        array[currentPointer++] = helper[leftPointer++];
	    }
	}
	
	
	public static void main(String[] args) {
		int[] test = {22, 66, 33, 88, 99, 27, 85, 19, 35, 48};
		mergesort(test);
		for(int i = 0; i < test.length; i++) {
			System.out.print(test[i] + " ");
		}
	}
}
