public class MakeChange {
	public static int makeChange(int n) {
		int[] ways = new int[n + 1];
		ways[0] = 1;
		for (int i = 1; i <= n; i++) {
			if (i >= 1) {
				ways[i] += ways[i - 1];
			}
			if (i >= 5) {
				ways[i] += ways[i - 5];
			}
			if (i >= 10) {
				ways[i] += ways[i - 10];
			}
			if (i >= 25) {
				ways[i] += ways[i - 25];
			}
		}
		return ways[n];
	}

	public static int makeChange1(int n, int now_coin) {
		int ways = 0;
		if (n == 0)
			return 1;
		if (n >= 1)
			ways += makeChange1(n - 1, 1);
		if (n >= 5 && now_coin > 1)
			ways += makeChange1(n - 5, 5);
		if (n >= 10 && now_coin > 5)
			ways += makeChange1(n - 10, 10);
		if (n >= 25 && now_coin > 10)
			ways += makeChange1(n - 25, 25);
		return ways;
	}

	public static int makeChange2(int n, int denom) {
		int next_denom = 0;
		
		
		switch (denom) {
		case 25:
			next_denom = 10;
			break;
		case 10:
			next_denom = 5;
			break;
		case 5:
			next_denom = 1;
			break;
		case 1:
			return 1;
		}

		int ways = 0;
		for (int i = 0; i * denom <= n; i++) {
			ways += makeChange2(n - i * denom, next_denom);
		}
		return ways;
	}

	public static void main(String[] args) {
		System.out.println(makeChange(50));
		System.out.println(makeChange1(50, 25));
		System.out.println(makeChange2(50, 25));
	}

}
